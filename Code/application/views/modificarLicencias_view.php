<?php $this->load->view('head_view');

?>
<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">  
	
<style type="text/css">

		.table-striped>tbody>tr{
			background-color: #e2e0e0 !important;
		}
  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		.nombreProfesional {
			font-size: 1.5em;
    		color: rgba(0,0,0,.5);
		}

		.licencias{
			color: #000000 !important;
			
		}

		.botonregistrar{
			padding: 10px 80px;
    		margin-left: 1em;
    		margin-top: 1.5em;
		}

		.fila{
			display: flex;
    		flex-direction: row;
    		align-items: center;
		}
		.botoncancelar{
			display: flex;
    		flex-direction: row-reverse;
    		margin-right: 10px; 
    		
		}
		td.day.disabled{
		    color: #f70808 !important;
		    }
		
</style>


<body class="mybodydatepicker" > 
	
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/agenda/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>

	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Modificar Días de Licencias</h3>
								<p>Ingresar Datos </p>
							</div>
						</div>
						
						<div class="myform-top-right">
							<i class="far fa-address-book"></i>
						</div>
					
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
							<?php echo form_open('agenda/modificarLicencias/'.$ids); ?>
							

							<div class="row">
								<div class="col-sm-12">
									<div  class="form-group" align="left">
													<label >Profesional:</label>
													<label class="nombreProfesional"><?php echo $datosprof['personasnombrecompuesto'];?></label>
													<!--<input id="profesionales" type="text" class="form-control lectura" name="inputProfesional"  value="" readonly>-->

									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<div  class="form-group" align="left">
										<label> Usted está por modificar los días de Licencias comprendidos entre el día
											<?php echo $fechainicio; ?> y el día <?php echo $fechafin; ?>
														  
										</label>
									</div>
								</div>
							</div>

							<div class = "row fila" >
								
								<div class="col-sm-2">
								</div>

								<div class="col-sm-4">
									<div  class="form-group" align="left">
										<label  id="fecha">*Fecha Inicio Licencia</label>
										

										<!------------------------------------------- -->

											<div class='input-group input-append date' id='datetimepicker1'>
												<input type='text' class="form-control" id="fechaI" name="fechaI"/>
												<span class="input-group-addon add-on">
													<span id="calendario" class="glyphicon glyphicon-calendar"></span>
												</span>
                							</div>

										<!------------------------------------------- -->

									</div>	
								</div>
								<div class="col-sm-4">
									<div  class="form-group" align="left">
										<label  id="fecha">*Fecha Fin Licencia</label>
										

										

											<div class='input-group input-append date' id='datetimepicker2'>
												<input type='text' class="form-control" id="fechaF" name="fechaF"/>
												<span class="input-group-addon add-on">
													<span id="calendario" class="glyphicon glyphicon-calendar"></span>
											 	</span>
                							</div>
	
									</div>
								</div>
								
								<div class="col-sm-2">
								</div>

								
							</div>
							<div class = "row ">
								<div class="col-sm-3">	
								</div>
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary submitBtn" style="padding:15px 50px" value="Registrar" name="Registrar">Registrar</button>
								</div>

								<div class="col-sm-3" >
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/agenda/registrarLicencias'">Cancelar</button>
								</div>
								<div class="col-sm-3">	
								</div>
							</div>	


							<div class="row fila" style="margin-left: 1px;" >
								<div class="error" style="color: #f76060;"> 
									<?php 
										echo $mensajeerror.'<br>';
									?>

								</div>
																
							</div>	
						<br><br>
						
						
						<?php echo form_close(); ?>	
						<!------------------------------------------>
						
						<!------------------------------------------------>
						
					</div>
				</div>
    		</div>
	</div>
	<?php $this->load->view("footer");?>

	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
	


<script type="text/javascript">
		$("#datetimepicker1").datepicker({
			language: 'es',
			autoclose: true,
			//endDate: new Date(),//aqui ver
			format:'dd/mm/yyyy',
			firstDay:1,
			beforeShowDay: function(date){
				var dn = date.getDay();
				var d = date.getDate();
				var m = date.getMonth();
				var a = date.getFullYear();
				<?php
				$con = ''; 
				foreach ($dias as $d) {
					$con = $con.'dn != '.$d->diasid." && ";
				};
						
				$con = substr($con,0,-3);
				if( !empty($licencias)){
					$dLicencias = "var diasLicencias = [";
					$mLicencias = "var mesesLicencias = [";
					$aLicencias = "var anioLicencias = [";
					foreach ($licencias as $l) {
						$dLicencias = $dLicencias.$l->dia.",";
						$mLicencias = $mLicencias.$l->mes.",";
						$aLicencias = $aLicencias.$l->anio.",";
					}
					$dLicencias = substr($dLicencias,0,-1);
					$dLicencias = $dLicencias."];\n\t";
					$mLicencias = substr($mLicencias,0,-1);
					$mLicencias = $mLicencias."];\n\t";
					$aLicencias = substr($aLicencias,0,-1);
					$aLicencias = $aLicencias."];\n\t";
					echo $dLicencias;
					echo $mLicencias;
					echo $aLicencias;

				}
				
				?>
				if (<?php echo $con;?>) {
					return false;
				} else {
					<?php
						if(!empty($licencias)){ 
							echo "\t\tvar bandera=0;\n\t\t\tvar i = 0;\n\t\t ";
							echo "while( (bandera == 0) && (i < diasLicencias.length)){\n\t\t\t\t";
							echo "if ( (diasLicencias[i] == d) && (mesesLicencias[i] == m) && (anioLicencias[i] == a) ){\n\t\t\t\t\t";
							echo "bandera =1;\n\t\t\t\t}\n\t\t\ti = i + 1;\n\t\t}\n\t";
							echo "if(bandera == 0){return true;}else{return false;}";
						}else{
							echo "return true;";
						}	
					?>
					//return true;
				}
			}

		}).datepicker("setDate","<?php echo $fechaI;?>");
	
</script>
<script type="text/javascript">

	$("#datetimepicker2").datepicker({
		language: 'es',
		autoclose: true,
		//endDate: new Date(),//aqui ver
		format:'dd/mm/yyyy',
		firstDay:1,
		beforeShowDay: function(date){
			var dn = date.getDay();
			var d = date.getDate();
			var m = date.getMonth();
			var a = date.getFullYear();
			<?php
			$con = ''; 
			foreach ($dias as $d) {
				$con = $con.'dn != '.$d->diasid." && ";
			};
					
			$con = substr($con,0,-3);
			if( !empty($licencias)){
				$dLicencias = "var diasLicencias = [";
				$mLicencias = "var mesesLicencias = [";
				$aLicencias = "var anioLicencias = [";
				foreach ($licencias as $l) {
					$dLicencias = $dLicencias.$l->dia.",";
					$mLicencias = $mLicencias.$l->mes.",";
					$aLicencias = $aLicencias.$l->anio.",";
				}
				$dLicencias = substr($dLicencias,0,-1);
				$dLicencias = $dLicencias."];\n\t";
				$mLicencias = substr($mLicencias,0,-1);
				$mLicencias = $mLicencias."];\n\t";
				$aLicencias = substr($aLicencias,0,-1);
				$aLicencias = $aLicencias."];\n\t";
				echo $dLicencias;
				echo $mLicencias;
				echo $aLicencias;

			}
			
			?>
			if (<?php echo $con;?>) {
				return false;
			} else {
				<?php
					if(!empty($licencias)){ 
						echo "\t\tvar bandera=0;\n\t\t\tvar i = 0;\n\t\t ";
						echo "while( (bandera == 0) && (i < diasLicencias.length)){\n\t\t\t\t";
						echo "if ( (diasLicencias[i] == d) && (mesesLicencias[i] == m) && (anioLicencias[i] == a) ){\n\t\t\t\t\t";
						echo "bandera =1;\n\t\t\t\t}\n\t\t\ti = i + 1;\n\t\t}\n\t";
						echo "if(bandera == 0){return true;}else{return false;}";
					}else{
						echo "return true;";
					}	
				?>
				//return true;
			}
		}

	}).datepicker("setDate","<?php echo $fechaF;?>");
		
</script>
	

</body>

</html>