<?php $this->load->view('head_view');?>
<style type="text/css">
	.pass{
		color: #2e9af7;
		font-size: larger;

	}
	a:focus{
		color:#b1b1b1 !important;
	}

	a:hover {
		color:#fff;
	}

	.recu{
		color:#ccf35b;
	}
</style>
<body>
	<div class="my-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1> <strong> SALUTIA </strong> </h1>
					<div class="mydescription">
						<!--<p>Servicios de Salud y Perfeccionamiento Odontol�gico </p>-->
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3 myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<h3 class="recu">Recuperación de contraseña</h3>
							<p>Digite su Usuario ó Correo Electronico </p>
						</div>
						
						<div class="myform-top-right">
							<i class="fas fa-lock"></i>
						</div>
					</div>
					
					<div class="myform-bottom">
						<form role="form" name="form_iniciar" action="<?= base_url().'index.php/login/recuperarPass'?>" method="POST">

							<div class="form-group">
								<!--<label for="contrase?">Contraseña</label>-->
								<input type="text" name="pass" onKeyUp="this.value=this.value.toUpperCase();" placeholder="Correo Electronico..." value="<?= @set_value('user')?>" class="form-control" id="form-username"/>
							</div>
							<div class="error" style="color: #f76060">
							<?php if(isset($mensajerecu)): ?>
								<?= $mensajerecu; ?>
							<?php endif; ?>
							</div>
							

							<input type="submit" class="mybtn" value="Buscar" name="Buscar" />
							<!--<button type="submit" class="mybtn" name="submit">Entrar</button>-->
						</form>
						<div class="row">
							
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/bootstrap/js/bootstrap.min.js"></script>	
</body>

</html>
