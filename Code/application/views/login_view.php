<style type="text/css">
	.pass{
		color: #2e9af7;
		font-size: larger;

	}
	a:hover {
		color:#fff;
	}
</style>
<body>
	<div class="my-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1> <strong id="salutia-login"> SALUTIA </strong> </h1>
					<div class="mydescription">
						<!--<p>Servicios de Salud y Perfeccionamiento Odontol�gico </p>-->
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3 myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<h3 id="form-login">Ingrese al Sitio</h3>
							<p>Digite su Usuario y Contraseña </p>
						</div>
						
						<div class="myform-top-right">
							<i class="fa fa-user"></i>
						</div>
					</div>
					
					<div class="myform-bottom">
						<form role="form" name="form_iniciar" action="<?= base_url().'index.php/login/very_sesion'?>" method="POST">
							<div class="form-group">
								<!--<label for="Usuario">Usuario </label>-->
								<input type="text" name="user" onKeyUp="this.value=this.value.toUpperCase();" placeholder="Usuario..." value="<?= @set_value('user')?>" class="form-control" id="form-username"/>
								
							</div>
							<div class="form-group">
								<!--<label for="contrase?">Contraseña</label>-->
								<input type="password" name="pass" placeholder="Contraseña..." value="<?= @set_value('pass');?>" class="form-control" id="form-password"/>
							</div>
							<?php if(isset($mensaje)): ?>
								<div id="msj">
									
								
								<?=$mensaje; ?>
								</div>
							<?php endif; ?>

							

							<input type="submit" class="mybtn" value="Entrar" name="submit" />
							<!--<button type="submit" class="mybtn" name="submit">Entrar</button>-->
						</form>
						<div class="row">
							<a href="<?=  base_url().'index.php/login/recuperarPass'?>" class="pass">Se Olvido la contraseña? Presionar aquí </a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/bootstrap/js/bootstrap.min.js"></script>	
</body>

</html>
