<!DOCTYPE html>
<html lang="es">
<head>
	
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-2.0.0.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-1.10.2.js"></script>
	<script src="<?php echo base_url();?>push.js-master/bin/push.min.js"></script>
	<link href="<?php echo base_url();?>css/jquery-ui.css" rel="Stylesheet"/></link>
	<?php
	$title = '';
	$typeLeter = '';
	$style = '';
	if((current_url() == site_url()."/login") || (current_url() == site_url()."/login/very_sesion") ||  (current_url() == site_url()."/login/recuperarPass") || (current_url() == site_url()."/login/recuperarPassMen") ){
		$title = $title."Login";
		$typeLeter = $typeLeter.'<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga:100,300,400,500">';
		$style = $style.'<link rel="stylesheet" type="text/css" href="'.base_url().'/css/index_style.css">';
	}
	if(current_url()== site_url()."/pacientes" || current_url()== site_url()."/pacientes/registrarPaciente" || strpos(current_url(),'/pacientes/RegistrarTelefono') !== False || strpos(current_url(),'/RegistrarObraS') !== False || strpos(current_url(),'/modificarPaciente') !== False){
		$title = $title."Pacientes";
		$typeLeter = $typeLeter.'<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">';
		
	}
	if(current_url()== site_url()."/turnos" || current_url()== site_url()."/turnos/registrarTurnos"){
		$title = $title."Turnos";
		$typeLeter = $typeLeter.'<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">';
		
	}
	if(current_url()== site_url()."/agenda" || current_url()== site_url()."/agenda/turnoNormales" || current_url()== site_url()."/agenda/DiasyHorasLab" || strpos(current_url(),'/modificarHorasLab') !== False ||current_url()== site_url()."/agenda/registrarLicencias" || strpos(current_url(),'/modificarLicencias') !== False || current_url()== site_url()."/agenda/Turnos" ||current_url()== site_url()."/agenda/registrarVacaciones" || strpos(current_url(),'/modificarVacaciones') !== False){
		$title = $title."Agenda";
		$typeLeter = $typeLeter.'<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">';
		
	}
	if(current_url()== site_url()."/historiaclinica" || current_url() == site_url()."/historiaclinica/registrarHC" || strpos(current_url(),'/detalleHC') !== False || current_url()== site_url()."/historiaclinica/CargaTablaDHC" || strpos(current_url(),'/modificarHC') !== False){
		$title = $title."Historia Clínica";
		$typeLeter = $typeLeter.'<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">';
		
	}
	if(current_url()== site_url()."/profesionales" || current_url()== site_url()."/profesionales/registrarProfesional" || strpos(current_url(),'/modificarProfesional') !== False){
		$title = $title."Profesionales";
		$typeLeter = $typeLeter.'<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">';
		
	}
	if(current_url()== site_url()."/configuracion" || current_url()== site_url()."/configuracion/registrarTipoTelefono"  || current_url() == site_url()."/configuracion/intervalo" || current_url()== site_url()."/configuracion/registrarIntervalo" || current_url() == site_url()."/configuracion/sexo" || current_url() == site_url()."/configuracion/registrarSexo" || strpos(current_url(),'modificarTipoTelefonos') !== False || strpos(current_url(),'modificarIntervalo') !== False || current_url() == site_url()."/configuracion/obrasocial" || current_url() == site_url()."/configuracion/registrarObraSocial" || strpos(current_url(),'modificarObraSocial') !== False || strpos(current_url(),'modificarSexo') !== False || current_url()== site_url()."/configuracion/obrasocial" ||  current_url() == site_url()."/configuracion/feriados" || current_url() == site_url()."/configuracion/registrarFeriados" || strpos(current_url(),'modificarFeriado') !== False || current_url() == site_url()."/configuracion/especialidades" || current_url() == site_url()."/configuracion/registrarEspecialidad" || strpos(current_url(),'modificarEspecialidad') !== False){
		$title = $title."Configuracion";
		$typeLeter = $typeLeter.'<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">';
		
	}
	if(current_url()== site_url()."/usuarios" || current_url()== site_url()."/usuarios/registrarUsuario" || current_url()== site_url()."/usuarios/asignarPerfil" || current_url()== site_url()."/secretario" || current_url()== site_url()."/secretario/registrarSecretario"||current_url() == site_url()."/usuarios/CambiarPass" || current_url() == site_url()."/secretario/secretarioList" || strpos(current_url(),'/modificarSecretario') !== False || strpos(current_url(),'secretario/RegistrarTelefono') !== False){
		$title = $title."Usuarios";
		$typeLeter = $typeLeter.'<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">';	
	}

	if(current_url() == site_url()."/reportes" || current_url() == site_url()."/reportes/reporteusuarios" || current_url() == site_url()."/reportes/turnosDiarios" || current_url() == site_url()."/reportes/reporteEstadistico" || current_url() == site_url()."/reportes/reportesHC" || current_url() == site_url()."/reportes/reportesPacientesxOS" || current_url() == site_url()."/reportes/reportesPacientesAusentes" ||  current_url() == site_url()."/reportes/reportesTurnosCancelados"){
		$title = $title."Reportes";
		$typeLeter = $typeLeter.'<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">';	
	}
	
	echo "<title>".$title."</title>";
	echo $typeLeter;
	echo $style;	
	?>

	<!-- HEAD LOGIN-->

	<link href="<?php echo base_url(); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/font-awesome/css/font-awesome.min.css"> <!--iconos -->
	<link rel="icon" href="<?php echo base_url(); ?>css/imagenes/favicon.ico" type="image/gif">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/bootstrap/css/custom.css">

	
	
	<!--END HEAD LOGIN-->

	<!-- HEAD PACIENTES TURNOS AGENDA PROFESIONALES USUARIOS-->

	<!--<link href="<?php ##echo base_url(); ?>DataTables/media/css/jquery.dataTables.min.css" rel="stylesheet">-->
	<link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!--Para menú desplegable automático data-hover="dropdown" en <a>-->
	<link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome-free-5.2.0/css/all.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>DataTables/media/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/custom.css">

	<!-- para Datepicker-->

	<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
  	<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">  
	
	<!-- END PACIENTES TURNOS AGENDA PROFESIONAlES USUARIOS-->
	<?php 
		if (current_url() == site_url()."/reportes/turnosDiarios"){
		?>
			<script type="text/javascript">
		$(document).ready(function(){
			$("#profesionales").change(function(){
			$.ajax({
			url:"<?php echo base_url(); ?>index.php/reportes/CargaEspecialidades",
			type: "POST",
			data:"profesionalesid="+$("#profesionales").val(),
			success: function(opciones){
				$("#especialidades").html(opciones);
			}
			})
		});
		});
		</script>
		<?php } ?>
</head>
