<?php $this->load->view('head_view'); ?>
<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"> 
<style>

  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		input.mybtn{
            height: 45px;
            background: #1AAE88;
            border:0;
            font-size: 20px;
            color:#fff;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius:4px;
			text-align: center;
		}

		input.mybtn:hover, input.mybtn:active{
                   opacity:0.6;
                   color:#fff;
		}

		input.mybtn:focus{
       			opacity:0.6;
       			color:#fff;
       			background: #de615e;
		}

		.linea{
			border-top: 4px solid #a8a0a0;
		}

		
		.caja {
			width: 20px;
			height: 20px;
		}
		.lectura{ background-color: #b0dede !important }

		td.day.disabled{
		    color: #f70808 !important;
		}
		
		.btn-siguiente{
			margin-top: 5px;
			background-color: #7ca6ad;
    		border-color: #244d54;
			padding: 0.30em;
			color: #454545;
		}
</style>
<body class="mybodydatepicker" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/reportes/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container"><br>
	<!-- Menu -->
	<?php $this->load->view('menu_view'); ?>
<!-- ------------------------------------------------------------------------------------ ---->
<!-- IR a Registrar Nuevo Paciente -->
		


<!-- -------------------------------------------------------------------------------------- -->
        <div class="mycontainersmall" >
 			<h3 class="myh3">Generar Reportes de Pacientes Ausentes</h3>
        </div>
    	
        <div class="row">
            <div class="col-sm-12">
                
                	<div class="col-sm-3" align="left">
                        
                            <label>Profesional:</label>
                            <select id="profesionales" name="profesionales" class="form-control">
                                <option value="0">Seleccione un Profesional</option>
                                <?php
                                foreach ($profesionales as $prof) {
                                    echo'<option  value="'.$prof->profesionalesid.'">'.$prof->profesional.'</option>';
                                }
                                ?> 
                                
                            </select>
                         
					</div>
                    <div class="col-sm-3" align="left">
                    <label>Fecha Inicio:</label>
                    <div class='input-group input-append date' id='datetimepicker1'>
						<input type='text' class="form-control" id="fechaInicio" name="fechaInicio"/>
						<span class="input-group-addon add-on">
							<span id="calendario" class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
                    </div>
					<div class="col-sm-3" align="left">
                    <label>Fecha Fin:</label>
                    <div class='input-group input-append date' id='datetimepicker2'>
						<input type='text' class="form-control" id="fechaFin" name="fechaFin"/>
						<span class="input-group-addon add-on">
							<span id="calendario" class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
                    </div>

					<div class="col-sm-3" align="left">
                    <label> </label>	
						<button id="" type="button" data-toggle="tooltip" title="Generar Reporte" class="btn btn-primary" style="margin-top: 0.3em;width: 100%;" onclick="pdf()" >Generar Reporte</button>
					</div>
                    
            </div>
        </div>
        <br>
        <div class="row">
			<div id="error" class="error" style="color: #f76060">					 
			</div>
		</div>
       
        
    </div>
	
	<?php $this->load->view("footer");?>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
								
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

<script type="text/javascript">
$("#datetimepicker1").datepicker({
		language: 'es',
		autoclose: true,
		format:'dd/mm/yyyy',
		firstDay:1
	}).datepicker("setDate","<?php echo $fecha;?>");
</script>

<script type="text/javascript">
$("#datetimepicker2").datepicker({
		language: 'es',
		autoclose: true,
		format:'dd/mm/yyyy',
		firstDay:1
	}).datepicker("setDate","<?php echo $fecha;?>");
</script>

<script type="text/javascript">

	function pdf() {

		$('#error').empty();
		var idProf = $('#profesionales').val();
        var fecha_inicio = $('#fechaInicio').val().split('/');
		fecha_inicio = new Date(fecha_inicio[2],fecha_inicio[1] - 1,fecha_inicio[0]);
		var fecha_fin = $('#fechaFin').val().split('/');
		fecha_fin = new Date(fecha_fin[2],fecha_fin[1] - 1,fecha_fin[0]);

		if (idProf == 0 ){
			$('#error').append('Debe seleccionar profesional  para generar el reporte.');
		}else{
			if ( fecha_inicio.getTime() >= fecha_fin.getTime()){
				$('#error').append('La fecha de inicio debe ser menor a la fecha de fin ');
			}else{

				$('#error').empty();

				$.ajax({
							url:"<?php echo base_url(); ?>index.php/reportes/getPacientesAusentes",
							type:'POST',
							dataType:'json',
							data:{ profesionalid:$('#profesionales').val(),
								fechaI: $('#fechaInicio').val(),
								fechaF: $('#fechaFin').val()
						}
					}).done(function(respuesta){
						//console.log(respuesta[0]);
						// onlyPrint: Boolean
						// DE: http://stackoverflow.com/questions/2255291/print-the-contents-of-a-div
						var title = "Reporte de Turnos Diarios";
						var mywindow = window.open('', 'my div', 'height=400px,width=600px');
						var fecha = new Date();
						var tipo;
						var tabla = '';
						var hora = fecha.getHours();
						var minutos = fecha.getMinutes();
						var cantidad = respuesta[0];
						var pacientes = respuesta[1];
						var profesional = respuesta[2];
						if(profesional.sexoid ==1){
							Dr = "Dra. ";
						}else{
							Dr = "Dr. ";
						}
						
						if (minutos<10){
							minutos = '0' + String(minutos);
						} else{
							minutos = String(minutos);
						}

						hora = String(hora);

						if (pacientes != null){
							for (var i = 0; i<pacientes.length; i++){
								tabla = tabla + '<tr role="row" class="seven" style:"text-align:center;">';
								tabla = tabla + '<td><center>' + pacientes[i].t_profesional + '</center></td>';
								tabla = tabla + '<td><center>' + pacientes[i].t_especialidad + '</center></td>';
								tabla = tabla + '<td><center>' + pacientes[i].t_paciente + '</center></td>';
								tabla = tabla + '<td><center>' + pacientes[i].t_fecha + '</center></td>';
								tabla = tabla + '<td><center>' + pacientes[i].t_hinicio + '</center></td>';
								tabla = tabla + '<td><center>' + pacientes[i].t_hfin + '</center></td>';
								tabla = tabla + '<td><center>' + pacientes[i].t_estadoturno + '</center></td></tr>';
							}
						}else{
							tabla = tabla + '<tr role="row" class="seven" style:"text-align:center;">';
							tabla = tabla + '<td><center> No hay datos</center></td>';
							tabla = tabla + '<td><center> No hay datos</center></td>';
							tabla = tabla + '<td><center> No hay datos</center></td>';
							tabla = tabla + '<td><center> No hay datos</center></td>';
							tabla = tabla + '<td><center> No hay datos</center></td>';
							tabla = tabla + '<td><center> No hay datos</center></td>';
							tabla = tabla + '<td><center> No hay datos</center></td></tr>';
						}
						fecha = fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
						
						
										
						mywindow.document.write('<head> <title> ' + title +'</title> <meta charset="UTF-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta name="viewport" content="width=device-width, initial-scale=1"> <script type="text/javascript" src="http://localhost/Seminario/Code/jquery/ajax-jquery-2.0.0.js">');
						mywindow.document.write('<'+'/script> <script type="text/javascript" src="http://localhost/Seminario/Code/jquery/ajax-jquery-1.10.2.js"> '+'<'+'/script> <script src="http://localhost/Seminario/Code/push.js-master/bin/push.min.js">'+'<'+'/script> <link href="http://localhost/Seminario/Code/css/jquery-ui.css" rel="Stylesheet"/></link> <link href="http://localhost/Seminario/Code//bootstrap/css/bootstrap.min.css" rel="stylesheet"> <link rel="stylesheet" href="http://localhost/Seminario/Code//font-awesome/css/font-awesome.min.css"> <!--iconos --> <link rel="icon" href="http://localhost/Seminario/Code/css/imagenes/favicon.ico" type="image/gif"> <link rel="stylesheet" href="http://localhost/Seminario/Code//bootstrap/css/custom.css"> <link href="http://localhost/Seminario/Code/bootstrap/css/bootstrap.min.css" rel="stylesheet"> <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/font-awesome/css/font-awesome.min.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/fontawesome-free-5.2.0/css/all.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/DataTables/media/css/dataTables.bootstrap.min.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/bootstrap/css/custom.css"></head>');

						mywindow.document.write('<body class="mybodytables" style="background-color:white;" > <div class="container"><div class="mylogout"><img src="http://localhost/Seminario/Code/css/imagenes/salutiaLogo.png" width =100% height=80 align=middle></div></div><div class="container"><br><div class="mycontainersmall" >');
						mywindow.document.write('<h3 class="myh3">' + title + '</h3><div style="display:flex;"><label>Fecha de Reporte:</label> <p>'+fecha+' '+hora+':'+minutos+' hs.</p> </div>');
						mywindow.document.write('<div style="display:flex;"><label>Profesional:</label><p>'+ Dr + profesional.personasnombrecompuesto +'.</p></div>');
						mywindow.document.write('<div style="display:flex;"><label>Fecha Inicio:</label><p>'+ $('#fechaInicio').val() +'</p>    <label style:"margin-left:1px;">Fecha Fin:</label>' + $('#fechaFin').val() + '</div>');
						mywindow.document.write('<div style="display:flex;"><p>La cantidad de pacientes que hubo en el intervalo de fechas dadas es:</p> <label>' + cantidad[0].cantidad +'<label></div>');
						mywindow.document.write('</div><div class="row"><div class="col-sm-12"><div class="form-group"><div class="table-responsive">');
						mywindow.document.write('<table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2" width="100%" style="font-size:small"><thead class="mytable">');
						mywindow.document.write('<tr><th><center>Profesional</center></th><th><center>Especialidad</center></th><th><center>Paciente</center></th><th><center>Fecha</center></th><th><center>Hora Incio</center></th><th><center>Hora Fin</center></th><th><center>Estado</center></th></tr></thead><tbody class="mytbody">'+tabla+ '</tbody></table></div></div></div></div></div>');

						mywindow.document.write('<script src="http://localhost/Seminario/Code/jquery/jquery-1.11.1.js">'+'<'+'/script><script src="http://localhost/Seminario/Code/jquery/jquery-3.2.1.min.js">'+'<'+'/script><script src="http://localhost/Seminario/Code/DataTables/media/js/jquery.js">'+'<'+'/script><!-- Datatable --><script src="http://localhost/Seminario/Code/DataTables/media/js/jquery.dataTables.min.js">'+'<'+'/script><script src="http://localhost/Seminario/Code/DataTables/media/js/dataTables.bootstrap.min.js">');

						mywindow.document.write('<script type="text/javascript">window.onload = function(){Push.Permission.request();}'+'<'+'/script><script type="text/javascript">var table;$(document).ready(function() { $('+"'#table'"+').DataTable({ "responsive": true,"processing": true,  "serverSide": true, "paging":false,"searching":false, "language": idioma_espanol }); }); var idioma_espanol= {  "sProcessing":     "Procesando...", "sLengthMenu":     "Mostrar _MENU_ registros", "sZeroRecords":    "No se encontraron resultados", "sEmptyTable":     "Ningún dato disponible en esta tabla", "sInfo":           "", "sInfoEmpty":      "", "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)", "sInfoPostFix":    "", "sSearch":         "Buscar:", "sUrl":            "","sInfoThousands":  ",","sLoadingRecords": "Cargando...","oPaginate": {"sFirst":    "Primero","sLast":     "Último","sNext":     "Siguiente","sPrevious": "Anterior"},"oAria": {"sSortAscending":  ": Activar para ordenar la columna de manera ascendente","sSortDescending": ": Activar para ordenar la columna de manera descendente" } }'+'<'+'/script></body></html>');

						setTimeout(function () {
							mywindow.print();
							mywindow.close();
						}, 5000);

					});
			}
		} 

		
			};

</script>



</body>

</html>