
<!----------------------------------------------------------------------------------------------- -->

<!DOCTYPE html>
<html lang="es">
<head>
	
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Turnos</title>
	<script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jqueryui-1.10.2.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>css/jqueryiu-1.10.3.css"/>
	<script type="text/javascript" >
		$(document).ready(function(){
			$( "#pacienteDNI" ).autocomplete({
			source: "<?php echo base_url(); ?>index.php/turnos/BuscarPacientes",
			minLength: 1
			});
			
			$("#pacienteDNI").focusout(function(){
				$.ajax({
					url:"<?php echo base_url(); ?>index.php/turnos/CargaPaciente",
					type:'POST',
					dataType:'json',
					data:{ pacienteDNI:$('#pacienteDNI').val()}
				}).done(function(respuesta){
					if (respuesta.respuesta == null || respuesta.respuesta.length > 1){
						$("#pacienteID").val('');
						$("#pacienteNombreCompuesto").val('');
						console.log(respuesta.respuesta.length);
					}else{
						console.log(respuesta.respuesta.length);
						$("#pacienteID").val(respuesta.respuesta["0"].pacientesid);
						$("#pacienteNombreCompuesto").val(respuesta.respuesta["0"].nombredocumento);
					}
					
				});
				});


		});
	</script>


<script type="text/javascript">
  $(document).ready(function(){
    $("#profesionales").change(function(){
    $.ajax({
      url:"<?php echo base_url(); ?>index.php/turnos/CargaEspecialidades",
      type: "POST",
      data:"profesionalesid="+$("#profesionales").val(),
      success: function(opciones){
        $("#especialidadesid").html(opciones);
      }
    })
  });
});
</script>

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">




	<!-- HEAD PACIENTES TURNOS AGENDA PROFESIONALES USUARIOS-->

	
	<link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="icon" href="<?php echo base_url(); ?>css/imagenes/favicon.ico" type="image/gif">
	
	<!--Para menú desplegable automático data-hover="dropdown" en <a>-->
	<link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome-free-5.2.0/css/all.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>DataTables/media/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/custom.css">

	<!-- para Datepicker-->

	<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
  	<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">  
		
	<!-- END PACIENTES TURNOS AGENDA PROFESIONAlES USUARIOS-->
</head>

<style>

  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		input.mybtn{
            height: 45px;
            background: #1AAE88;
            border:0;
            font-size: 20px;
            color:#fff;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius:4px;
			text-align: center;
		}

		input.mybtn:hover, input.mybtn:active{
                   opacity:0.6;
                   color:#fff;
		}

		input.mybtn:focus{
       			opacity:0.6;
       			color:#fff;
       			background: #de615e;
		}

		.linea{
			border-top: 4px solid #a8a0a0;
		}

		
		.caja {
			width: 20px;
			height: 20px;
		}
		.lectura{ background-color: #b0dede !important }

		td.day.disabled{
		    color: #f70808 !important;
		}
		
		.btn-siguiente{
			margin-top: 5px;
			background-color: #7ca6ad;
    		border-color: #244d54;
			padding: 0.30em;
			color: #454545;
		}
</style>
<!----------------------------------------------------------------------------------------------- -->


<body class="mybodydatepicker" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/pacientes/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Registrar Nuevo Turno</h3>
								<p>Ingresar Datos </p>
							</div>
						</div>
						
						<div class="myform-top-right">
							
							<i class="fa fa-tooth"></i>
							
						</div>
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
						<div class="error" style="color:#e03636"><?php echo $mensaje;?></div>
						<?php echo form_open('turnos/CargaTabla')?>
						
							<div class="row">
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											
											<label >*DNI del Paciente</label>
											<input id="pacienteDNI" type="text" class="form-control " name="inputDNI" placeholder="Ingrese DNI" value="<?php echo $dni;?>" 
											<?php
												if (isset($_POST['Buscar'])){
													echo "style='background-color:darkgrey;' readonly";
												}
											?>>
											<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Buscar'])){
													echo $mensajeDNI;
												} 
											?>
													
											</div>
										</div>
								</div>
								<div class="col-sm-2">
										<div  class="form-group" align="left">
											<label >*Ident.</label>
											<input id="pacienteID" type="text" class="form-control lectura" name="inputId" value="<?php echo $pacienteid;?>" required="true" readonly>
										</div>
								</div>
								<div class="col-sm-7">
										<div  class="form-group" align="left">
											<label >*Apellido y Nombre</label>
											<input id="pacienteNombreCompuesto" type="text" class="form-control lectura" name="inputNombreCompuesto" value="<?php echo $pacientenombre;?>" required="true" readonly >
										</div>
								</div>
								
							</div>
							<div class="row" >
								<div class="col-sm-4">
									<div  class="form-group" align="left">
												<label >*Profesional</label>
												<select class="form-control" 
												<?php
												if (isset($_POST['Buscar'])){
													echo ' style="background-color:darkgrey;height:40px;" disabled';
												}else{
													echo ' style="height:40px;"';
												}
												?> name="inputProfesional" id="profesionales" value="<?php echo $profesionalid;?>"
												
												>
												<option value ='0' >Seleccione un profesional</option>
												<?php
												if (isset($_POST['Buscar']) || isset($_POST['registrar'])){
													foreach ($profesionales as $prof) {
														if($prof->profesionalesid == $profesionalid ){
															echo'<option  value="'.$prof->profesionalesid.'" selected >'.$prof->profesional.'</option>';
														}else{
															echo'<option  value="'.$prof->profesionalesid.'">'.$prof->profesional.'</option>';
														}
														
													}
												}else{
													foreach ($profesionales as $prof) {
														echo'<option  value="'.$prof->profesionalesid.'">'.$prof->profesional.'</option>';
													}
												}	
												
												?>
												</select>
									</div>
								</div>


								<div class="col-sm-4">
									<div  class="form-group" align="left">
												<label  id="especialidades">*Especialidades</label>
												<select class="form-control" 
												<?php
												if (isset($_POST['Buscar'])){
													echo ' style="background-color:darkgrey;height:40px;" disabled ';
												}else{
													echo ' style="height:40px;"';
												}
												?>
												 name="inputEspecialidades" id="especialidadesid" value="<?php echo $especialidadid;?>">
												
												<?php
												if (isset($_POST['Buscar']) || isset($_POST['registrar'])){
													foreach ($especialidades as $esp) {
														if($esp->especialidadesid == $especialidadid ){
															echo'<option  value="'.$esp->especialidadesid.'" selected >'.$esp->especialidadesnombre.'</option>';
														}
														
													}
												}else{
													echo "<option value ='0' >Seleccione primero el profesional</option>";
												}	
												
												?>
												</select>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group" align="left">
										<label >   </label>
										<button id="btnSiguiente" type="button" class="mybtn btn-siguiente" 
										<?php
											if(isset($_POST['Buscar'])){
												echo 'style="background-color:darkgrey;border-color:darkgrey;" disabled';
											} 
										?>
										> SIGUIENTE >></button>
									</div>
								</div>
							</div>
							<div class="row">
							     <div id="error" class="error" style="color: #f76060">
								 
								 </div>
							</div>
							<div class = "row" id="datosTurnos" <?php echo $hidden; ?>>

								<div class="col-sm-4">
									<div  class="form-group" align="left">
												<label  id="turnos">*Tipo Turnos</label>
												<select class="form-control" style="height:40px;" name="inputTipoTurno" id="tipoturnosid" value="<?php echo $tipoturnosid;?>">
												<option value ='0' >Seleccione tipo de turno</option>"
												<?php
												if (isset($_POST['Buscar']) || isset($_POST['registrar'])){
													foreach ($turnos as $tur) {
														if($tur->tipoturnosid == $tipoturnosid ){
															echo'<option  value="'.$tur->tipoturnosid.'" selected >'.$tur->tipoturnosnombre.'</option>';
														}else{
															echo'<option  value="'.$tur->tipoturnosid.'">'.$tur->tipoturnosnombre.'</option>';
														}
														
													}
												}else{
													foreach ($turnos as $tur) {
														echo'<option  value="'.$tur->tipoturnosid.'">'.$tur->tipoturnosnombre.'</option>';
													}
												}	
												
												?>
												?>
												</select>
									</div>
								</div>



								<div class="col-sm-4">
									<div  class="form-group" align="left">
										<label  id="fecha">*Fecha</label>
										

										<!------------------------------------------- -->

											<div class='input-group input-append date' id='datetimepicker1'>
												<input type='text' class="form-control" id="fechaN" name="fechaN"/>
												<span class="input-group-addon add-on">
													<span id="calendario" class="glyphicon glyphicon-calendar"></span>
												</span>
                							</div>

										<!------------------------------------------- -->

									</div>	
								</div>
								<div class="col-sm-4">
									<div class="form-group" align="left">
										<label >   </label>
										<button id="btnAtras" type="button" class="mybtn btn-siguiente" > << ATRAS</button>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
								<input id="profesionalesxespecialidadesid" type="hidden" class="form-control lectura" name="inputProfecionalxEspecialidad" value="<?php echo $especialidadesxprofesionalesid;?>" required="true" readonly >		
								</div>
							</div>
							<div class="row" id="Buscar" <?php echo $hidden ?>>	
								<div class="col-sm-12">
									<div  class="form-group" align="left">
										<label  id="boton"></label>
										<input type="submit" class="mybtn"  value="Buscar" name="Buscar"/>
										
									</div>				
								</div>	

							</div>

							<div class="row" id="datosTabla" <?php echo $hiddenTabla; ?>>
								<div class="col-sm-12">
									<div  class="form-group" >
											<table id="table" class="table table-striped table-bordered table-sm" cellspacing="2" width="100%" style="font-size:small">
													<thead class="mytable">
														<tr>
															<th><center>Hora Turno</center></th>
															<th><center>Paciente</center></th> 
															<th><center>Selección</center></th>                   
														</tr>
														<tbody class="mytbody"  style="color:black;background:white;">
														<?php
														$i=1;
														if (isset($_POST['Buscar']) || isset($_POST['registrar'])){
															if (!empty($intervalo)) {
																foreach ($agenda as $a){
																    if (($a->tb_turnohora.":00" >= $intervalo[0]->agendastmini) && ($a->tb_turnohora.":00" <= $intervalo[0]->agendastmfin)) {
																	    	echo '<tr role="row" class="seven" style:"text-align:center;">
																				<td >'.$a->tb_turnohora."</td>
																				<td >".$a->tb_paciente.'</td>';
																			if (empty(trim($a->tb_paciente))==TRUE){
																				echo '<td ><input type="checkbox" class="form-check-input caja" name="'.$i.'" value="'.$a->tb_turnohora.'"></td>
																				</tr>';
																			}else{
																				echo '<td ><input type="checkbox" class="form-check-input caja" disabled></td>
																				</tr>';
																			}
																			$i=$i+1;
																    } else {
																    	if (($a->tb_turnohora.":00" >= $intervalo[0]->agendasttini) && ($a->tb_turnohora.":00" <= $intervalo[0]->agendasttfin)) {
																    		echo '<tr role="row" class="seven" style:"text-align:center;">
																				<td >'.$a->tb_turnohora."</td>
																				<td >".$a->tb_paciente.'</td>';
																			if (empty(trim($a->tb_paciente))==TRUE){
																				echo '<td ><input type="checkbox" class="form-check-input caja" name="'.$i.'" value="'.$a->tb_turnohora.'"></td>
																				</tr>';
																			}else{
																				echo '<td ><input type="checkbox" class="form-check-input caja" disabled></td>
																				</tr>';
																			}
																			$i=$i+1;
																    	} 
																    	
																    }
																    
																	
																}
																
															} else {
																foreach ($agenda as $a){
																
																	echo '<tr role="row" class="seven" style:"text-align:center;">
																			<td >'.$a->tb_turnohora."</td>
																			<td >".$a->tb_paciente.'</td>';
																	if (empty(trim($a->tb_paciente))==TRUE){
																		echo '<td ><input type="checkbox" class="form-check-input caja" name="'.$i.'" value="'.$a->tb_turnohora.'"></td>
																		</tr>';
																	}else{
																		echo '<td ><input type="checkbox" class="form-check-input caja" disabled></td>
																		</tr>';
																	}
																	$i=$i+1;
																}
															}
															
															
														}
														
														
					
                    
															
														?>
																
														</tbody>

													</thead>
											</table>
			
									</div>
								</div>
							</div>

							<div class="row" id="botones" <?php echo $hiddenTabla;?>>		
								<div class="col-sm-3">
									<input id="licencias" type="hidden" class="form-control lectura" name="diaslicencias" 
									<?php
										if (isset($_POST['Buscar'])){
											echo 'value="'.$licencias.'" ';
										}else{
											echo 'value="" ';
										}
									
									?> readonly >
								</div>
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary submitBtn" style="padding:15px 50px"value="Registrar" name="registrar">Registrar</button>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/turnos'">Cancelar</button>
								</div>
								<div class="col-sm-3">

									<input id="dias" type="hidden" class="form-control lectura" name="diasagenda" 
									<?php
										if (isset($_POST['Buscar'])){
											echo 'value="'.$laborales.'" ';
										}else{
											echo 'value="" ';
										}
									?>
									readonly >
								</div>				

							</div>	
							<div class="row">
							    <div class="col-sm-3">
								<input id="profid" type="hidden" class="form-control lectura" name="profid" 
								<?php
									if (isset($_POST['Buscar'])){
										echo ' value="'.$profesionalid.'" ';
									}else{
										echo 'value=""';
									}
								?>  readonly >
								</div>
								<div class="col-sm-3">
								<input id="espid" type="hidden" class="form-control lectura" name="espid" 
								<?php
									if (isset($_POST['Buscar'])){
										echo ' value="'.$especialidadid.'" ';
									}else{
										echo 'value=""';
									}
								?>   readonly >
								</div>
								<div class ="col-sm-3">
								<input id="vacaciones" type="hidden" class="form-control lectura" name="diasvacaciones" 
									<?php
										if (isset($_POST['Buscar'])){
											echo 'value="'.$vacaciones.'" ';
										}else{
											echo 'value="" ';
										}
									?>
									readonly >
								<div>
								<div class ="col-sm-3">
								<input id="feriados" type="hidden" class="form-control lectura" name="diasferiados" 
									<?php
										if (isset($_POST['Buscar'])){
											echo 'value="'.$feriados.'" ';
										}else{
											echo 'value="" ';
										}
									?>
									readonly >
								<div>
							</div>

								
						<?php echo form_close()?>				
						
						
					</div>
				</div>
			</div>
    	
		
        
        

    </div>
	<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-dropdown-hover/js/bootstrap-dropdownhover.min.js"></script>
	
    <script src="<?php echo base_url(); ?>DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>DataTables/media/js/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>


	<script type="text/javascript">
	setTimeout(function () {
		
		
		$(document).on('click','#btnSiguiente',function(){
			$('#error').empty();
			var profesional = $('#profesionales').val();
			var especialidad = $('#especialidadesid').val();
			if ((profesional == 0) && (especialidad == 0)){
				$('#error').append('Debe seleccionar profesional y especialidad para realizar las siguientes operaciones');
			}else{
				$('#profid').val(profesional);
				$('#espid').val(especialidad);
				$('#error').empty();
				$('#Buscar').css('display','block');
				$('#datosTurnos').css('display','block');
				$('#pacienteDNI').css('background-color','darkgrey');
				$('#profesionales').css('background-color','darkgrey');
				$('#especialidadesid').css('background-color','darkgrey');
				$('#btnSiguiente').css('background-color','darkgrey');
				$('#btnSiguiente').css('border-color','darkgrey');
				$('#pacienteDNI').prop('readonly', true);
				$('#profesionales').prop('disabled',true);
				$('#especialidadesid').prop('disabled',true);
				$('#btnSiguiente').prop('disabled', true);
				$.ajax({
						url:"<?php echo base_url(); ?>index.php/turnos/CargaDias",
						type:'POST',
						dataType:'json',
						data:{ profesionalid:$('#profesionales').val(),
							especialidadid:$('#especialidadesid').val()
							}
					}).done(function(respuesta){
						var laborales = respuesta[0];
						var licencias = respuesta[1];
						var vacaciones = respuesta[2];
						var feriados = respuesta[3];

						console.log("calendario :",respuesta);

						op = '';
						op1 = '';
						op2 = '';
						op3 = '';
						for(i = 0; i<laborales.length;i++){
							op = op + laborales[i].diasid+",";
						}
						console.log(licencias);
						if( licencias != null ){
							
							for( var j = 0; j < licencias.length; j++){
								op1 = op1 + licencias[j].dia + "/" + licencias[j].mes + "/" + licencias[j].anio +",";
							}
							op1 = op1.slice(0,-1);
							console.log("op1: ",op1);
						}
						if (vacaciones != null){
							for( var j = 0; j < vacaciones.length; j++){
								op2 = op2 + vacaciones[j].dia + "/" + vacaciones[j].mes + "/" + vacaciones[j].anio +",";
							}
							op2 = op2.slice(0,-1);
							console.log("op2: ",op2);
						}
						if (feriados != null){
							for( var j = 0; j < feriados.length; j++){
								op3 = op3 + feriados[j].dia + "/" + feriados[j].mes + "/" + feriados[j].anio +",";
							}
							op3 = op3.slice(0,-1);
							console.log("op3: ",op3);
						}
						
						
						op = op.slice(0,-1);
						
						console.log("op: ",op);
						
						$("#dias").val(op);
						$("#licencias").val(op1);
						$("#vacaciones").val(op2);
						$('#feriados').val(op3);
					});
			}
			
    	});
		
	}, 1000)
		
	</script>

	<script type="text/javascript">
		$(document).on('click','#btnAtras',function(){
			$('#error').empty();
			$('#pacienteDNI').prop('readonly', false);
			$('#profesionales').prop('disabled',false);
			$('#especialidadesid').prop('disabled',false);
			$('#btnSiguiente').prop('disabled',false);
			$('#pacienteDNI').css('background-color', '#fff');
			$('#profesionales').css('background-color', '#fff');
			$('#especialidadesid').css('background-color', '#fff');
			$('#Buscar').css('display','none');
			$('#datosTurnos').css('display','none');
			$('#datosTabla').css('display', 'none');
			$('#btnSiguiente').removeAttr("style");
			$('#datosTabla').css('display','none');
			$('#botones').css('display','none');
		});
	</script>

	<script type="text/javascript">


$("#datetimepicker1").datepicker({
		language: 'es',
		autoclose: true,
		format:'dd/mm/yyyy',
		firstDay:1,
		beforeShowDay: function(date) {
			var dn = date.getDay();
			var d = date.getDate();
			var m = date.getMonth();
			var a = date.getFullYear();
			
			var laborales = $("#dias").val();
			var licencias = $("#licencias").val();
			var vacaciones = $("#vacaciones").val();
			var feriados = $("#feriados").val();
			/*console.log(laborales);
			console.log(licencias);*/
			console.log(feriados);
			laborales = laborales.split(',');
			licencias = licencias.split(',');
			vacaciones = vacaciones.split(',');
			feriados = feriados.split(',');
			var diasLicencias = [];
			var mesesLicencias = [];
			var anioLicencias = [];
			var aux;
			var aux2;
			var aux3;
			for( var k=0; k < licencias.length; k ++){
				//console.log("lic: ",  licencias[k]);
				aux = licencias[k].split('/');
				diasLicencias.push(aux[0]);
				mesesLicencias.push(aux[1]);
				anioLicencias.push(aux[2]);
			}
			for(var j = 0; j < vacaciones.length; j++){
				aux2 = vacaciones[j].split('/');
				diasLicencias.push(aux2[0]);
				mesesLicencias.push(aux2[1]);
				anioLicencias.push(aux2[2]);
			}
			for(var i = 0; i < feriados.length; i++){
				aux3 = feriados[i].split('/');
				diasLicencias.push(aux3[0]);
				mesesLicencias.push(aux3[1]);
				anioLicencias.push(aux3[2]);
			}

			dn=dn.toString();
			d=d.toString();
			m=m.toString();
			a=a.toString();

			

			if ( laborales.includes(dn) ){

				var bandera = 0;
				var i = 0;
				while((bandera == 0) && (i < diasLicencias.length)){
					if((diasLicencias[i] == d && mesesLicencias[i] == m && anioLicencias[i] == a)){
						bandera = 1;
					}
					i = i + 1;
				}

				if (bandera == 0){
					return true;
				}else{
					return false
				}
				
			}else{

				return false;

			}
			
			
		

		
		}
	}).datepicker("setDate","<?php echo $fecha;?>");




</script>

<script type = "text/javascript">	
$(document).ready(function () {
	$('#table').DataTable({
		"paginatype":"simple_numbers",
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
		}
	});
	
	$('.dataTables_length').addClass('bs-select');
});
	
		
	</script>


</body>

</html>