<?php $this->load->view('head_view');
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>/bootstrap-select/dist/css/bootstrap-select.css" type="text/css">
<script type="text/javascript" src="<?php echo base_url(); ?>/bootstrap-select/dist/js/bootstrap-select.js"></script>

<style type="text/css">
	.altura{
		height: 40px !important;
	}
	.filter-option{
		height: 26px !important;
	}
	.lectura{ background-color: #b0dede !important }

	.color{ color:#f76060; }
</style>
<script type="text/javascript">
	$(document).ready(function(){

		$("#usuarioDNI").focusout(function(){

				$.ajax({
						url:"<?php echo base_url(); ?>index.php/usuarios/CargaPersonas",
					type:'POST',
					dataType:'json',
					data:{ usuarioDNI:$('#usuarioDNI').val(),
						   usuarioperfil:$('#usuarioPerfil').val()}
				}).done(function(respuesta){
					if (respuesta.respuesta == null || respuesta.respuesta.length > 1){
						$("#usuarioNombreCompuesto").val('');
						if( $('#usuarioPerfil').val() == 3){
							$("#secretario").show()
						}else{
							if ($('#usuarioPerfil').val() == 2){
								$("#profesional").show()
							}
						}
						
					}else{
						$("#usuarioNombreCompuesto").val(respuesta.respuesta["0"].nombredocumento);
						$("#secretario").hide();
						$("#profesional").hide();
					}
					
				});
		});
	});


</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#usuarioPerfil').change(function(){
			usuarioDNI:$('#usuarioDNI').val('')
			$('#usuarioNombreCompuesto').val('');
			$("#secretario").hide();
			$("#profesional").hide();
		});	
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		if ($('#usuarioNombreCompuesto').val() != ' ' ){
			$("#secretario").hide();
			$("#profesional").hide();
			
		}
		});
</script>
<body class="mybodytables" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/usuarios/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Registrar Nuevo Usuario</h3>
								<p>Ingresar Datos </p>
								

							</div>
						</div>
						
						<div class="myform-top-right">
							
								<i class="fa fa-user"></i>
							
						</div>
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
							<div class="row">
								<div class="col-sm-12">
									<div  class="form-group" align="left">
										<p>Si el Secretario no esta registrado en el sistema presione aquí   <button type="button" class="btn btn-primary" onclick="location.href='<?php echo base_url(); ?>index.php/secretario'"><i class="fa fa-plus"></i></button></p>
													
									</div> 
								</div>
							</div>
						<?php echo form_open('usuarios/registrarUsuario'); ?>
	
							<div class="row">
								<div class="col-sm-4">
										<div  class="form-group" align="left">
											<label for="usuarioPerfil" >*Perfil</label>
											
											<select class="form-control" style="height:40px;" name="usuarioPerfil" id="usuarioPerfil">
											<?php
													if (isset($_POST['Registrar'])){
														foreach ($perfiles as $per){
															if($per->perfilesid == $idPerfil){
																echo'<option  value="'.$per->perfilesid.'" selected>'.$per->perfilesnombre.'</option>';
															}else{
																echo'<option  value="'.$per->perfilesid.'">'.$per->perfilesnombre.'</option>';
															}
														}

													}else{
														foreach ($perfiles as $per) {
														echo'<option  value="'.$per->perfilesid.'">'.$per->perfilesnombre.'</option>';
														}
													}
													
																							
													
												?>
											</select>
											
										</div>
								</div>
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											<label for="inputDNI">*DNI</label>
											
											<input id="usuarioDNI" type="text" class="form-control " name="inputDNI" placeholder="Ingrese DNI" value="<?php echo $DNI;?>" >
											<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajeDNI;
												} 
											?>
													
											</div>
											
										</div>
								</div>
								<div class="col-sm-5">
										<div  class="form-group" align="left">
											<label >*Apellido y Nombre</label>
											<input id="usuarioNombreCompuesto" type="text" class="form-control lectura" name="inputNombreCompuesto" required="true" value="<?php echo $NombreCompuesto;?>" readonly >
										</div>
								</div>


							</div>	
							<div class="row">
								<a hidden="true" class=" color"id="secretario" href="<?php echo base_url(); ?>index.php/secretario">El Secretario no se encuentra en el sistema presiona aquí para registraralo</a>
							</div>
							<div class="row">
								<a hidden="true"class=" color"id="profesional" href="<?php echo base_url(); ?>index.php/profesionales/registrarProfesional">El Profesional no se encuentra en el sistema presiona aquí para registraralo</a>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div  class="form-group" align="left">
											<label >*Usuario</label>
											<input id="usuarionombre" type="text" class="form-control" placeholder="Ingrese Usuario" name="inputNombreUsuario" value="<?php echo $usuarioNombre;?>" onKeyUp="this.value=this.value.toUpperCase();">
											<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajeUsuario;
												} 
											?>
													
											</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div  class="form-group" align="left">
											<label >*Contraseña</label>
											<input id="usuariopass" type="password" class="form-control" name="inputPasswordUsuario"  placeholder="Ingrese contraseña" >
											<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajePass;
												} 
											?>
												
											</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div  class="form-group" align="left">
											<label >* Repetir Contraseña</label>
											<input id="usuariopass2" type="password" class="form-control" name="inputPasswordUsuario2" placeholder="Repetir contraseña" >
											<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajePassrepet;
												} 
											?>
												
											</div>

									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="error"style="color: #f76060">
									<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajePassIguales;
												} 
											?>
								</div>
							</div>
							<div class="row">		
								<div class="col-sm-3">

								</div>
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary submitBtn" style="padding:15px 50px"value="Registrar" name="Registrar">Registrar</button>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/usuarios'">Cancelar</button>
								</div>
								<div class="col-sm-3">

								</div>				

							</div>			
								
						<?php echo form_close(); ?>							
						
						
					</div>
				</div>
			</div>
    	
		
        
        

    </div>
	
	<?php $this->load->view("footer");?>




</body>

</html>