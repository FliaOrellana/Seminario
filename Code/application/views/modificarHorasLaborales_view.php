<?php $this->load->view('head_view');
?>



	
<style>

		html, body {
		    width: 100%;
		    margin: 0;
		    padding: 0;
		    box-sizing: border-box;
		}
		#table{
		    width: 100% !important;
		    margin: 0 auto !important;
		}
		.caja {
			width: 20px;
			height: 20px;
		}
  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		input.mybtn{
            height: 45px;
            background: #1AAE88;
            border:0;
            font-size: 20px;
            color:#fff;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius:4px;
			text-align: center;
		}

		input.mybtn:hover, input.mybtn:active{
                   opacity:0.6;
                   color:#fff;
		}
		th {
			align-content: center;
		}
		select {
			align-content: center;
		}
		input.mybtn:focus{
       opacity:0.6;
       color:#fff;
       background: #de615e;
		}

		.linea{
			border-top: 4px solid #a8a0a0;
		}

		
		.caja {
			width: 20px;
			height: 20px;
		}
		.lectura{ background-color: #b0dede !important }

</style>
<script type="text/javascript">
  		$(document).ready(function(){
    		$("#franjahorariaid").change(function(){
    			if ($(this).val() == 1){
    				$("#tarde").hide();
    				$("#mañana").show();
    			}
    			if ($(this).val() == 2){
    				$("#tarde").show();
    				$("#mañana").hide();
    			}
    			if ($(this).val() == 0){
    				$("#tarde").show();
    				$("#mañana").show();
    			}
  			});
		});
</script>
<script type="text/javascript">
  		$(document).ready(function(){
    		$("#inputEspecialidades").change(function(){
    			$("#search").hiden();
  			});
		});
</script>

<body class="mybodydatepicker" > 
	
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/agenda/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>

	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>
		<?php 
		/*foreach ($dias as $d) {
			
				print_r($d);
				echo "<br>";																							
		}*/	

		 ?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Modificar Horas Laborales</h3>
								<p>Ingresar Datos </p>
								

							</div>
						</div>
						
						<div class="myform-top-right">
							<i class="far fa-address-book"></i>
						</div>
					
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
							<?php echo form_open('agenda/modificarHorasLab/'.$id);
							 ?>
							

							
						<div id="search" <?php echo $ocultar;?>>
										<div class="row">
											
											<div class="col-sm-6">
												<div class="form-group" align="left">
													<label  id="franjahoraria">*Franja Horaria</label>
													<select class="form-control" style="height:40px;" name="inputFranjaHoraria" id="franjahorariaid" value="">
														<option value="0">Turno Mañana y Tarde</option>
														<option value="1">Turno Mañana</option>
														<option value="2">Turno Tarde</option>
													</select>
															<!--<div class="error" style="color: #f76060">
																<?php/* 
																	if(isset($_POST['Registrar'])){
																		echo $MensajeEspecialidad;
																	} */
																?>
																
															</div>-->
												</div>	
											</div>
											<div class="col-sm-6">
													
											</div>
											
										</div>
										
										<div class="row">
											<div id="mañana">
												<div class="col-sm-3">
													<div class="form-group" align="left">
														<label id="mañanainicio">Turno Mañana Inicio</label>
														<input type="time" class="form-control" name="inputMañanaInicio" id="mañanainicioid" value="08:00" min="08:00" max="13:00" >
													</div>	
												</div>
												<div class="col-sm-3">
													<div class="form-group" align="left">
														<label id="mañanafin">Turno Mañana Fin</label>
														<input type="time" class="form-control" name="inputMañanaFin" id="mañanafinid" value="08:00" min="08:00" max="13:00" >
													</div>	
												</div>	
											</div>
											<div id="tarde">
												<div class="col-sm-3">
													<div class="form-group" align="left">
														<label id="tardeinicio">Turno Tarde Inicio</label>
														<input type="time" class="form-control" name="inputTardeInicio" id="tardeinicioid" value="13:00" min="13:00" max="21:00" >
													</div>	
												</div>
												<div class="col-sm-3">
													<div class="form-group" align="left">
														<label id="tardefin">Turno Tarde Fin</label>
														<input type="time" class="form-control" name="inputTardeFin" id="tardefinid" value="13:00" min="13:00" max="21:00" >
													</div>	
												</div>
											</div>
										</div>
										<div class="row">
												<div class="error" style="color: #f76060">
													<?php 
														if(isset($_POST['Registrar'])){
															echo $MensajeTurnoMañana;
														} 
													?>				
												</div>
										</div>
										
										<div class="row">
												<div class="error" style="color: #f76060">
													<?php 
														if(isset($_POST['Registrar'])){
															echo $MensajeTurnoTarde;
														} 
													?>				
												</div>
										</div>
										

										<div class="row">
											<div class="col-sm-3">
											</div>
											
											<div class="col-sm-3">
													<div  class="form-group" align="left">
														<label > </label><br>
														<button type="submit" class="btn btn-primary submitBtn" style="padding:12px 60px"value="Registrar" name="Registrar">Registrar</button>
													</div>
											</div>
											<div class="col-sm-3">
												<label > </label><br>
												<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:12px 60px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/agenda/DiasyHorasLab'">Cancelar</button>
											</div>
											<div class="col-sm-3">
											</div>
										</div>
										
										
									
									
											<div class="error" style="color: #f76060">
											<?php 
																if(isset($_POST['Registrar'])){
																	echo $mensajeinsert;
																} 
											?>
											</div>
									
										
						</div>								
						<?php echo form_close(); ?>	
						
					</div>
				</div>
			</div>
    	
		
        
        

    </div>
	
	<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-dropdown-hover/js/bootstrap-dropdownhover.min.js"></script>
	
    <script src="<?php echo base_url(); ?>DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>DataTables/media/js/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
	


</body>

</html>