<?php $this->load->view('head_view'); ?>
<body class="mybodytables" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/reportes/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container"><br>
	<!-- Menu -->
	<?php $this->load->view('menu_view'); ?>
<!-- ------------------------------------------------------------------------------------ ---->
<!-- IR a Registrar Nuevo Paciente -->
		


<!-- -------------------------------------------------------------------------------------- -->
        <div class="mycontainersmall" >
 			<h3 class="myh3">Generar Reportes Estadistico</h3>
        </div>
    	
        <div class="row">
            <div class="col-sm-12">
                
                	<div class="col-sm-3" align="left">
                        
                            <label>Profesional:</label>
                            <select id="profesionales" name="profesionales" class="form-control">
                                <option value="0">Seleccione un Profesional</option>
                                <?php
                                foreach ($profesionales as $prof) {
                                    echo'<option  value="'.$prof->profesionalesid.'">'.$prof->profesional.'</option>';
                                }
                                ?> 
                                
                            </select>
                         
					</div>
                    <div class="col-sm-3" align="left">
                        <label>Mes:</label>
                        <select id="meses" name="meses" class="form-control">
                        <option value ='0' >Seleccione un mes</option>
                        <?php 
                            foreach ($meses as $m => $nombre) {
                                echo '<option value="'.$m.'" >'.$nombre.'</option>';
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-sm-3" align="left">
                        <label>Año:</label>
                        <select id="year" name="year" class="form-control">
                        <option value ='0' >Seleccione un año</option>
                        <?php
                            for ($i=$year; $i >= 1945; $i--) { 
                                echo '<option value="'.$i.'" >'.$i.'</option>';
                            }
                         ?>
                        </select>
                    </div>

					<div class="col-sm-3" align="left">
                    <label> </label>	
						<button id="" type="button" data-toggle="tooltip" title="Generar Reporte" class="btn btn-primary" style="margin-top: 0.3em;width: 100%;" onclick="pdf()" >Generar Reporte</button>
					</div>
                    
            </div>
        </div>
        <br>
        <div class="row">
			<div id="error" class="error" style="color: #f76060">					 
			</div>
		</div>
       
        
    </div>
	
	<?php $this->load->view("footer");?>
 
 
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>


<script type="text/javascript">

	function diasEnUnMes(mes, año) {
		return new Date(año, mes, 0).getDate();
	}
	function pdf() {
		var listMeses = {
			"1" : "Enero",
			"2" : "Febrero",
			"3" : "Marzo",
			"4" : "Abril",
			"5" : "Mayo",
			"6" : "Junio",
			"7" : "Julio",
			"8" : "Agosto",
			"9" : "Septiembre",
			"10" : "Octubre",
			"11" : "Noviembre",
			"12" : "Diciembre"
		}
        var y = new Date().getFullYear();
        var m = new Date().getMonth();
		$('#error').empty();
		var idProf = $('#profesionales').val();
        var mes = $('#meses').val();
        var year = $('#year').val();
		var cant_dias = diasEnUnMes(parseInt(mes),parseInt(year));
		console.log(cant_dias);

		if (idProf == 0 || mes == 0 || year == 0){
			$('#error').append('Debe seleccionar profesional, el mes y el año para generar el reporte estadistico.');
		}else{
            console.log(m,mes,y,year)
            if ( m+1 <= parseInt(mes) && y <= parseInt(year)){
                $('#error').empty();
                $('#error').append('No se puede Generar un reporte estadistico en un mes en curso o que no transcurrio. Seleccione un mes que ya fue transcurrido.');
            }else{
                $('#error').empty();
                $.ajax({
						url:"<?php echo base_url(); ?>index.php/reportes/getDatosEstadisticosMes",
						type:'POST',
						dataType:'json',
						data:{ profesionalid:$('#profesionales').val(),
							   mes: $('#meses').val(),
							   year: $('#year').val(),
							   cant: cant_dias
					}
				}).done(function(respuesta){
					console.log(respuesta);

					// onlyPrint: Boolean
					// DE: http://stackoverflow.com/questions/2255291/print-the-contents-of-a-div
					var title = "Reporte Estadístico de Pacientes";
					var mywindow = window.open('', 'my div', 'height=1600px,width=1600px');
					var fecha = new Date();
					var tipo;
					var cantidad = respuesta[2][0].cantidad;
					var datosDias = respuesta[3];

					//var tabla = '';
					var hora = fecha.getHours();
					var minutos = fecha.getMinutes();
					var data = respuesta[0];
					var profesional = respuesta[1];
					if(profesional.sexoid ==1){
						Dr = "Dra. ";
					}else{
						Dr = "Dr. ";
					}

					if (minutos<10){
						minutos = '0' + String(minutos);
					} else{
						minutos = String(minutos);
					}
					dia = fecha.getDate();
					fm = fecha.getMonth()+1;
					if (dia < 10){
						dia = '0' + String(dia);
					}else{
						dia = String(dia);
					}
					if(fm < 10){
						fm = '0' + String(fm)
					}else{
						fm = String(fm);
					}
				
					fecha = dia + "/" + fm + "/" + fecha.getFullYear();
					reporte_cabezera ='<div class="row" align="left" style="display:flex;margin-left:0.1em;">' + 
					'<label>Fecha de Reporte: </label><p>' + fecha +' ' + hora +':' + minutos +' hs.</p></div>' +
					'<div class="row" align="left" style="display:flex;margin-left:0.1em;"><label>Prefesional: </label><p>'+ Dr + profesional.personasnombrecompuesto +'.</p></div>' +
					'<div align="justify"><p>Los siguientes gráficos muestran la cantidad de pacientes que fueron atendidos , que cancelaron su turno y los que no asistieron al turno programado en sus distintas especialidades. Los datos que se muestran son del mes <strong>' + listMeses[String(mes)] + '</strong> del año <strong>' + year +'</strong>.</p></div><br>' + 
					'<div style="display:flex;><label>Referencias:</label></div>' +
					'<div style="display:flex;><div class="green" style="width:1em;height:1em;margin-right:1em;"></div><div style="margin-right:1em;"><label>Atendidos</label></div>' +
					'<div class="red" style="width:1em;height:1em;margin-right:1em;"></div><div style="margin-right:1em;"><label>Cancelados</label></div>' + 
					'<div class="gris" style="width:1em;height:1em;margin-right:1em;"></div><div style="margin-right:1em;"><label>Ausentes</label></div></div><br>';
					
					canvas = '';
					hora = String(hora);
					repeticion = Math.floor(cantidad/2);
					var j = 1;
					for (var i = 0; i < repeticion; i++) {
						
							canvas = canvas + '<div class="row" style:"margin-bottom=0.1em;" align="center"><div class="col-sm-4"><label>Espacialidad: ' + data[j-1].especialidad + '</label>' + 
						'<canvas id="myChart' + j +'" class="canvas"></canvas></div><br>' +
						'<div class="col-sm-4"><label>Espacialidad: ' + data[j].especialidad + '</label>' + '<canvas id="myChart' + (j+1) +'" class="canvas"></canvas></div><br></div>';
						j = j + 2;
						
						
					} 
					
					if( cantidad % 2 != 0 ){
						canvas = canvas + '<br><br><div class="row" align="center"><label>Espacialidad: ' + data[j-1].especialidad + '</label><div class="col-sm-4">' + 
							'<canvas id="myChart' + j +'" class="canvas"></canvas></div></div>';
					}				
					
					head = '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge">' +
							'<meta name="viewport" content="width=device-width, initial-scale=1.0"><title>' + title + '</title>' +
							'<' + '/script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-2.0.0.js">' + '<' + '/script>' +
							'<script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-1.10.2.js">'+'<'+'/script>' +
    						'<link href="<?php echo base_url();?>css/jquery-ui.css" rel="Stylesheet"/></link>' +
							'<link href="<?php echo base_url(); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">' +
							'<link href="<?php echo base_url(); ?>Print/print.css" rel="stylesheet">' +
							'<link rel="icon" href="<?php echo base_url(); ?>css/imagenes/favicon.ico" type="image/gif">' +
							'<link rel="stylesheet" href="<?php echo base_url(); ?>/bootstrap/css/custom.css">' +
							'<link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">' +
							'<link rel="stylesheet" href="<?php echo base_url(); ?>Chart/css/Chart.css"></head>';
					body = '<body class="mybodytables" style="background-color:white;" > ' + 
							'<div class="container"><div class="mylogout">' + 
							'<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>' +
							'</div></div><div class="container"><br><div class="mycontainersmall" ><h3 class="myh3">' + title +'</h3>' +
							'</div>' + reporte_cabezera + ' ' +canvas +'</div>' +
							'<br><div class = "container"><div align="justify"><p>El Siguiente grafico mostrará la cantidad de pacientes que ud atendio por dia.</p></div><br><br>' +
							'<div class="row" align="center"><label>Cantidad de Pacientes Atendidos por dia.</label>' +
							'<div class="col-sm-12"><canvas id="chartdias" ></canvas></div></div></div>' +
							'<script src="<?php echo base_url();?>jquery/jquery-1.11.1.js">'+'<'+'/script>' +
    						'<script src="<?php echo base_url();?>jquery/jquery-3.2.1.min.js">'+'<'+'/script>' +
							'<script  src="<?php echo base_url(); ?>Chart/js/Chart.js">' + '<' + '/script>' + 
    						'<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js">' + '<' + '/script>';

					mywindow.document.write(head + body );
					
					charts = '';
					for (var i= 0; i < cantidad; i++) {
						var m = Math.max.apply(null,[ parseInt(data[i].atendidos),parseInt(data[i].cancelados), parseInt(data[i].ausente)]);
						
						mywindow.document.write('<script>');
						mywindow.document.write('var chartData = {\n\t\t\tlabels:["Atendidos","Cancelados","Ausentes"],\n');
						mywindow.document.write('\t\t\tdatasets: [{\n\t\t\t\tlabel:'+ "'Pacientes',\n");
						mywindow.document.write('\t\t\t\tdata:['+ data[i].atendidos + ',' + data[i].cancelados + ',' + data[i].ausente + '],\n');
						mywindow.document.write('\t\t\t\tbackgroundColor:' + "['rgb(164, 241, 133)','rgb(236, 150, 150)','rgb(133, 131, 131)']\n" + '\t\t\t}]\n   \t\t\t};\n');
						mywindow.document.write('var opt = {\n\t\t\tscales:{\n\t\t\t\tyAxes:[{\n\t\t\t\t\t ticks:{beginAtZero:true,max:' + (m+1) + '}\n\t\t\t\t}]\n\t\t\t},\n\t\t\t');
						mywindow.document.write('legend:{\n\t\t\t\t\tdisplay:false\n\t\t\t\t}\n\t\t\t\t,events:false,\n');
						mywindow.document.write('\t\t\ttooltips:{enabled:false},\n\t\t\thover:{animationDuration: 0},\n\t\t\t');
						
						mywindow.document.write('animation:{\n\t\t\t\tduration: 1,\n\t\t\t\t');
						mywindow.document.write('onComplete: function () {\n\t\t\t\t\t');
						mywindow.document.write('var chartInstance = this.chart;\n\t\t\t\t\tctx = chartInstance.ctx;\n\t\t\t\t\t');
						mywindow.document.write('ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);\n\t\t\t\t\t');
						mywindow.document.write('ctx.textAlign = ' + "'center'" + ';\n\t\t\t\t\tctx.textBaseline = ' + "'bottom'" + ';\n\t\t\t\t\t');
                
						mywindow.document.write('this.data.datasets.forEach(function (dataset, i) {\n\t\t\t\t\t\t');
						mywindow.document.write('var meta = chartInstance.controller.getDatasetMeta(i);\n\t\t\t\t\t\tmeta.data.forEach(function (bar, index) {\n\t\t\t\t\t\t\t');
						mywindow.document.write('var data = dataset.data[index];\n\t\t\t\t\t\t\tctx.fillText(data, bar._model.x, bar._model.y - 5);\n\t\t\t\t\t\t});\n\t\t\t\t\t});\n\t\t\t\t}\n\t\t\t\t}\n\t\t\t\t};\n');
						mywindow.document.write('var ctx= document.getElementById("myChart'+ (i+1)+ '").getContext("2d");\n');
						mywindow.document.write('myLineChart = new Chart(ctx, {\n\t\ttype:' + "'bar'," + '\n\t\tdata: chartData,\n\t\toptions: opt\n\t});<' + '/script>');
					
						
					} ;
					
					var datadias = '[';
					var listdias = '[';
					var listcolors = '[';
					for (var index = 0; index < datosDias.length; index++) {
						datadias = datadias + String(datosDias[index].cantidad) + ',';
						listdias = listdias + "'dia " +String(datosDias[index].dia) + "',";
						listcolors = listcolors + "'rgb(164, 241, 133)',"; 
						
					}
					datadias = datadias.slice(0,-1);
					datadias = datadias + ']';
					listdias = listdias.slice(0,-1);
					listdias = listdias + ']';
					listcolors = listcolors.slice(0 ,-1);
					listcolors = listcolors + ']';
					mywindow.document.write('<script>var m = Math.max.apply(null,'+ datadias + ');');
					mywindow.document.write('var chartData = {\n\t\t\tlabels:' + listdias +',\n');
					mywindow.document.write('\t\t\tdatasets: [{\n\t\t\t\tlabel:'+ "'Pacientes',\n");
					mywindow.document.write('\t\t\t\tdata:' + datadias +',\n');
					mywindow.document.write('\t\t\t\tbackgroundColor:' + listcolors +  '\n\t\t\t}]\n   \t\t\t};\n');
					mywindow.document.write('var opt = {\n\t\t\tscales:{\n\t\t\t\tyAxes:[{\n\t\t\t\t\t ticks:{beginAtZero:true,max:(m+1)}\n\t\t\t\t}],\nxAxes:[{ticks:{beginAtZero:true,max:(m+1)}}]\t\t\t},\n\t\t\t');
					mywindow.document.write('legend:{\n\t\t\t\t\tdisplay:false\n\t\t\t\t}\n\t\t\t\t,events:false,\n');
					mywindow.document.write('\t\t\ttooltips:{enabled:false},\n\t\t\thover:{animationDuration: 0},\n\t\t\t');
					
					mywindow.document.write('animation:{\n\t\t\t\tduration: 1,\n\t\t\t\t');
					mywindow.document.write('onComplete: function () {\n\t\t\t\t\t');
					mywindow.document.write('var chartInstance = this.chart;\n\t\t\t\t\tctx = chartInstance.ctx;\n\t\t\t\t\t');
					mywindow.document.write('ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);\n\t\t\t\t\t');
					mywindow.document.write('ctx.textAlign = ' + "'center'" + ';\n\t\t\t\t\tctx.textBaseline = ' + "'bottom'" + ';\n\t\t\t\t\t');
			
					mywindow.document.write('this.data.datasets.forEach(function (dataset, i) {\n\t\t\t\t\t\t');
					mywindow.document.write('var meta = chartInstance.controller.getDatasetMeta(i);\n\t\t\t\t\t\tmeta.data.forEach(function (bar, index) {\n\t\t\t\t\t\t\t');
					mywindow.document.write('var data = dataset.data[index];\n\t\t\t\t\t\t\tctx.fillText(data, bar._model.x, bar._model.y - 5);\n\t\t\t\t\t\t});\n\t\t\t\t\t});\n\t\t\t\t}\n\t\t\t\t}\n\t\t\t\t};\n');
					mywindow.document.write('var ctx= document.getElementById("chartdias").getContext("2d");\n');
					mywindow.document.write('myLineChart = new Chart(ctx, {\n\t\ttype:' + "'horizontalBar'," + '\n\t\tdata: chartData,\n\t\toptions: opt\n\t});<' + '/script>');

					mywindow.document.write('</body></html>');

			      	setTimeout(function () {
						mywindow.print();
						mywindow.close();
					}, 2000);

				});
            }
			
		} 

		
			};

</script>



</body>

</html>