<!DOCTYPE html>
<html lang="es">
<head>
	
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Turnos</title>
	<script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jqueryui-1.10.2.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>css/jqueryiu-1.10.3.css"/>
	

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">




	<!-- HEAD PACIENTES TURNOS AGENDA PROFESIONALES USUARIOS-->

	
	<link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="icon" href="<?php echo base_url(); ?>css/imagenes/favicon.ico" type="image/gif">
	
	<!--Para menú desplegable automático data-hover="dropdown" en <a>-->
	<link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome-free-5.2.0/css/all.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>DataTables/media/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/custom.css">

	<!-- para Datepicker-->

	<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
  	<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">  
		
	<!-- END PACIENTES TURNOS AGENDA PROFESIONAlES USUARIOS-->
</head>

<style>

  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		input.mybtn{
            height: 45px;
            background: #1AAE88;
            border:0;
            font-size: 20px;
            color:#fff;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius:4px;
			text-align: center;
		}

		input.mybtn:hover, input.mybtn:active{
                   opacity:0.6;
                   color:#fff;
		}

		input.mybtn:focus{
       opacity:0.6;
       color:#fff;
       background: #de615e;
		}

		.linea{
			border-top: 4px solid #a8a0a0;
		}

		
		.caja {
			width: 20px;
			height: 20px;
		}
		.lectura{ background-color: #b0dede !important }

		td.day.disabled{
		    color: #f70808 !important;
		    }

</style>
<!----------------------------------------------------------------------------------------------- -->


<body class="mybodydatepicker" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/turnos/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Reprogramar Turno</h3>
								<p>Ingresar Datos </p>
								

							</div>
						</div>
						
						<div class="myform-top-right">
							
							<i class="fa fa-tooth"></i>
							
						</div>
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
						<div class="error" style="color:#e03636"><?php echo $mensaje;?></div>
						<?php echo form_open('turnos/CargaTablaRepTurno/'.$id);?>
						
							<div class="row">
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											
											<label >*DNI del Paciente</label>
											<input id="pacienteDNI" type="text" class="form-control lectura " name="inputDNI" placeholder="Ingrese DNI" value="<?php echo $datospaciente[0]->personasnrodocumento;?>" readonly >

											<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Buscar'])){
													echo $mensajeDNI;
												} 
											?>
													
											</div>
										</div>
								</div>
								<div class="col-sm-2">
										<div  class="form-group" align="left">
											<label >*Ident.</label>
											<input id="pacienteID" type="text" class="form-control lectura" name="inputId" value="<?php echo $datospaciente[0]->pacientesid;?>" required="true" readonly>
										</div>
								</div>
								<div class="col-sm-7">
										<div  class="form-group" align="left">
											<label >*Apellido y Nombre</label>
											<input id="pacienteNombreCompuesto" type="text" class="form-control lectura" name="inputNombreCompuesto" value="<?php echo $datospaciente[0]->personasnombrecompuesto;?>" required="true" readonly >
										</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div  class="form-group" align="left">
												<label >*Profesional</label>
												<input id="profesionales" type="text" class="form-control lectura" name="inputProfesional" value="<?php echo $datosprofesional[0]->personasnombrecompuesto;?>" required="true" readonly >
												<!--<select class="form-control" style="height:40px;" name="inputProfesional" id="profesionales" value="<?php echo $profesionalid;?>">
												<option value ='0' >Seleccione un profesional</option>
												<?php
												if (isset($_POST['Buscar']) || isset($_POST['registrar'])){
													foreach ($profesionales as $prof) {
														if($prof->profesionalesid == $profesionalid ){
															echo'<option  value="'.$prof->profesionalesid.'" selected >'.$prof->profesional.'</option>';
														}else{
															echo'<option  value="'.$prof->profesionalesid.'">'.$prof->profesional.'</option>';
														}
														
													}
												}else{
													foreach ($profesionales as $prof) {
														echo'<option  value="'.$prof->profesionalesid.'">'.$prof->profesional.'</option>';
													}
												}	
												
												?>
												</select> -->
									</div>
								</div>


								<div class="col-sm-4">
									<div  class="form-group" align="left">
												<label  id="especialidades">*Especialidades</label>
												<input id="especialidadesid" type="text" class="form-control lectura" name="inputEspecialidades" value="<?php echo $datosprofesional[0]->especialidadesnombre;?>" required="true" readonly >
												<!--<select class="form-control" style="height:40px;" name="inputEspecialidades" id="especialidadesid" value="<?php echo $especialidadid;?>">
												
												<?php
												if (isset($_POST['Buscar']) || isset($_POST['registrar'])){
													foreach ($especialidades as $esp) {
														if($esp->especialidadesid == $especialidadid ){
															echo'<option  value="'.$esp->especialidadesid.'" selected >'.$esp->especialidadesnombre.'</option>';
														}
														
													}
												}else{
													echo "<option value ='0' >Seleccione primero el profesional</option>";
												}	
												
												?>
												</select>-->
									</div>
								</div>
								<div class="col-sm-4">
									<div  class="form-group" align="left">
												<label  id="turnos">*Tipo Turnos</label>
												<select class="form-control" style="height:40px;" name="inputTipoTurno" id="tipoturnosid" value="<?php echo $tipoturnosid;?>">
												<option value ='0' >Seleccione tipo de turno</option>"
												<?php
												if (isset($_POST['Buscar']) || isset($_POST['registrar'])){
													foreach ($turnos as $tur) {
														if($tur->tipoturnosid == $tipoturnosid ){
															echo'<option  value="'.$tur->tipoturnosid.'" selected >'.$tur->tipoturnosnombre.'</option>';
														}else{
															echo'<option  value="'.$tur->tipoturnosid.'">'.$tur->tipoturnosnombre.'</option>';
														}
														
													}
												}else{
													foreach ($turnos as $tur) {
														echo'<option  value="'.$tur->tipoturnosid.'">'.$tur->tipoturnosnombre.'</option>';
													}
												}	
												
												?>
												?>
												</select>
									</div>
								</div>

							</div>
							<div class = "row">
								<div class="col-sm-4">
									<div  class="form-group" align="left">
										<label  id="fecha">*Fecha</label>
										

										<!------------------------------------------- -->

											<div class='input-group input-append date' id='datetimepicker1'>
												<input type='text' class="form-control" id="fechaN" name="fechaN"/>
												<span class="input-group-addon add-on">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
                							</div>

										<!------------------------------------------- -->

									</div>	
								</div>
								<div class="col-sm-2">
								<input id="profesionalesid" type="hidden" class="form-control lectura" name="inputProfecionalID" value="<?php echo $profesionalid;?>">		
								</div>
									
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label  id="boton"></label>
										<input type="submit" class="mybtn"  value="Buscar" name="Buscar"/>
										
									</div>				
								</div>	

							</div>
							<div class="row">
								<input id="especialidadid" type="hidden" class="form-control lectura" name="inputEspecialidadID" value="<?php echo $especialidadid;?>"  >
							</div>

							<div class="row">
								<div class="col-sm-12">
									<div  class="form-group" >
											<table id="table" class="table table-striped table-bordered table-sm" cellspacing="2" width="100%" style="font-size:small">
													<thead class="mytable">
														<tr>
															<th><center>Hora Turno</center></th>
															<th><center>Paciente</center></th> 
															<th><center>Selección</center></th>                   
														</tr>
														<tbody class="mytbody"  style="color:black;background:white;">
														<?php
														$i=1;
														if (isset($_POST['Buscar']) || isset($_POST['registrar'])){
															if (!empty($intervalo)) {
																foreach ($agenda as $a){
																    if (($a->tb_turnohora.":00" >= $intervalo[0]->agendastmini) && ($a->tb_turnohora.":00" <= $intervalo[0]->agendastmfin)) {
																	    	echo '<tr role="row" class="seven" style:"text-align:center;">
																				<td >'.$a->tb_turnohora."</td>
																				<td >".$a->tb_paciente.'</td>';
																			if (empty(trim($a->tb_paciente))==TRUE){
																				echo '<td ><input type="checkbox" class="form-check-input caja" name="'.$i.'" value="'.$a->tb_turnohora.'"></td>
																				</tr>';
																			}else{
																				echo '<td ><input type="checkbox" class="form-check-input caja" disabled></td>
																				</tr>';
																			}
																			$i=$i+1;
																    } else {
																    	if (($a->tb_turnohora.":00" >= $intervalo[0]->agendasttini) && ($a->tb_turnohora.":00" <= $intervalo[0]->agendasttfin)) {
																    		echo '<tr role="row" class="seven" style:"text-align:center;">
																				<td >'.$a->tb_turnohora."</td>
																				<td >".$a->tb_paciente.'</td>';
																			if (empty(trim($a->tb_paciente))==TRUE){
																				echo '<td ><input type="checkbox" class="form-check-input caja" name="'.$i.'" value="'.$a->tb_turnohora.'"></td>
																				</tr>';
																			}else{
																				echo '<td ><input type="checkbox" class="form-check-input caja" disabled></td>
																				</tr>';
																			}
																			$i=$i+1;
																    	} 
																    	
																    }
																    
																	
																}
																
															} else {
																foreach ($agenda as $a){
																
																	echo '<tr role="row" class="seven" style:"text-align:center;">
																			<td >'.$a->tb_turnohora."</td>
																			<td >".$a->tb_paciente.'</td>';
																	if (empty(trim($a->tb_paciente))==TRUE){
																		echo '<td ><input type="checkbox" class="form-check-input caja" name="'.$i.'" value="'.$a->tb_turnohora.'"></td>
																		</tr>';
																	}else{
																		echo '<td ><input type="checkbox" class="form-check-input caja" disabled></td>
																		</tr>';
																	}
																	$i=$i+1;
																}
															}
															
															
														}
														
														
					
                    
															
														?>
																
														</tbody>

													</thead>
											</table>
			
									</div>
								</div>
							</div>

							<div class="row">		
								<div class="col-sm-3">
								</div>
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary submitBtn" style="padding:15px 50px"value="Registrar" name="registrar">Reprogramar</button>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/turnos'">Cancelar</button>
								</div>
								<div class="col-sm-3">
								
								</div>				

							</div>	
								
						<?php echo form_close()?>				
						
						
					</div>
				</div>
			</div>

    </div>
	<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-dropdown-hover/js/bootstrap-dropdownhover.min.js"></script>
	
    <script src="<?php echo base_url(); ?>DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>DataTables/media/js/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
	<script type="text/javascript">
		$(function () {

		$("#datetimepicker1").datepicker({
				language: 'es',
				autoclose: true,
				format:'dd/mm/yyyy',
				firstDay:1,
				beforeShowDay: function(date) {
						
						var dn = date.getDay();
						var d = date.getDate();
						var m = date.getMonth();
						var a = date.getFullYear();
						<?php
						$con = ''; 
						foreach ($dias as $d) {
							$con = $con.'dn != '.$d->diasid." && ";
						};
								
						$con = substr($con,0,-3);
						$dLicencias = "var diasLicencias = [";
						$mLicencias = "var mesesLicencias = [";
						$aLicencias = "var anioLicencias = [";
						$b = 0;
						if(!empty($licencias)){
							$b = 1;
							foreach ($licencias as $l) {
								$dLicencias = $dLicencias.$l->dia.",";
								$mLicencias = $mLicencias.$l->mes.",";
								$aLicencias = $aLicencias.$l->anio.",";
							}
						}
						if(!empty($vacaciones)){
							$b = 1;
							foreach ($vacaciones as $v) {
								$dLicencias = $dLicencias.$v->dia.",";
								$mLicencias = $mLicencias.$v->mes.",";
								$aLicencias = $aLicencias.$v->anio.",";
							}
						}
						if(!empty($feriados)){
							$b = 1;
							foreach ($feriados as $f) {
								$dLicencias = $dLicencias.$f->dia.",";
								$mLicencias = $mLicencias.$f->mes.",";
								$aLicencias = $aLicencias.$f->anio.",";
							}
						}
						if ($b == 0){
							$dLicencias = $dLicencias."];\n\t";
							$mLicencias = $mLicencias."];\n\t";
							$aLicencias = $aLicencias."];\n\t";
						}else{
							$dLicencias = substr($dLicencias,0,-1);
							$dLicencias = $dLicencias."];\n\t";
							$mLicencias = substr($mLicencias,0,-1);
							$mLicencias = $mLicencias."];\n\t";
							$aLicencias = substr($aLicencias,0,-1);
							$aLicencias = $aLicencias."];\n\t";
							echo $dLicencias;
							echo $mLicencias;
							echo $aLicencias;
						}

						/*if( !empty($licencias) && !empty($vacaciones)){
							
							
							foreach ($vacaciones as $v) {
								$dLicencias = $dLicencias.$v->dia.",";
								$mLicencias = $mLicencias.$v->mes.",";
								$aLicencias = $aLicencias.$v->anio.",";
							}
							$dLicencias = substr($dLicencias,0,-1);
							$dLicencias = $dLicencias."];\n\t";
							$mLicencias = substr($mLicencias,0,-1);
							$mLicencias = $mLicencias."];\n\t";
							$aLicencias = substr($aLicencias,0,-1);
							$aLicencias = $aLicencias."];\n\t";
							echo $dLicencias;
							echo $mLicencias;
							echo $aLicencias;
		
						}else{
							if( !empty($licencias)){
								$dLicencias = "var diasLicencias = [";
								$mLicencias = "var mesesLicencias = [";
								$aLicencias = "var anioLicencias = [";
								foreach ($licencias as $l) {
									$dLicencias = $dLicencias.$l->dia.",";
									$mLicencias = $mLicencias.$l->mes.",";
									$aLicencias = $aLicencias.$l->anio.",";
								}
								$dLicencias = substr($dLicencias,0,-1);
								$dLicencias = $dLicencias."];\n\t";
								$mLicencias = substr($mLicencias,0,-1);
								$mLicencias = $mLicencias."];\n\t";
								$aLicencias = substr($aLicencias,0,-1);
								$aLicencias = $aLicencias."];\n\t";
								echo $dLicencias;
								echo $mLicencias;
								echo $aLicencias;
							}else {
								if(!empty($vacaciones)){
									$dLicencias = "var diasLicencias = [";
									$mLicencias = "var mesesLicencias = [";
									$aLicencias = "var anioLicencias = [";
									foreach ($vacaciones as $v) {
										$dLicencias = $dLicencias.$v->dia.",";
										$mLicencias = $mLicencias.$v->mes.",";
										$aLicencias = $aLicencias.$v->anio.",";
									}
									$dLicencias = substr($dLicencias,0,-1);
									$dLicencias = $dLicencias."];\n\t";
									$mLicencias = substr($mLicencias,0,-1);
									$mLicencias = $mLicencias."];\n\t";
									$aLicencias = substr($aLicencias,0,-1);
									$aLicencias = $aLicencias."];\n\t";
									echo $dLicencias;
									echo $mLicencias;
									echo $aLicencias;
		
								}
							}
						}*/
					
						
						?>
						if (<?php echo $con;?>) {
							return false;
						} else {
							<?php
								if(!empty($licencias) || !empty($vacaciones) || !empty($feriados)){ 
									echo "\t\tvar bandera=0;\n\t\t\tvar i = 0;\n\t\t ";
									echo "while( (bandera == 0) && (i < diasLicencias.length)){\n\t\t\t\t";
									echo "if ( (diasLicencias[i] == d) && (mesesLicencias[i] == m) && (anioLicencias[i] == a) ){\n\t\t\t\t\t";
									echo "bandera =1;\n\t\t\t\t}\n\t\t\ti = i + 1;\n\t\t}\n\t";
									echo "if(bandera == 0){return true;}else{return false;}";
								}else{
									echo "return true;";
								}	
							?>
							//return true;
						}
					}
			}).datepicker("setDate","<?php echo $fecha;?>");
		});
	</script>
	<script type = "text/javascript">	
		$(document).ready(function () {
			$('#table').DataTable({
				"paginatype":"simple_numbers",
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
				}
			});
			
			$('.dataTables_length').addClass('bs-select');
		});
	</script>


</body>

</html>