<body class="mybodytables" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/pacientes/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container"><br>
		<?php $this->load->view('menu_view');?>
		<div align="right">	
					<button id="addTurnos" type="button" data-toggle="tooltip" title="Registrar Nuevo Turno" class="btn btn-primary" onclick="location.href='<?php echo base_url(); ?>index.php/turnos/registrarTurnos'"><i class="fa fa-plus"></i></button>
		</div>
		
        <div class="mycontainersmall" >
 			<h3 class="myh3">Datos de Turnos</h3>
        </div>
    	
		
        <?php 
            if(!empty($comando)){
                echo $comando;
            }
        ?>
        <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2" width="100%" style="font-size:small">
            <thead class="mytable">
                <tr>
                    
                    <th><center>Profesional</center></th>
                    <th><center>Especialidad<center></th>
                    <th><center>Fecha<center></th>
                    <th><center>Hora Inicio<center></th>
                    <th><center>Hora Fin<center></th>
                    <th><center>Tipo de Turno<center></th>
                    <th><center>Paciente<center></th>
                    <th><center>Estado<center></th>
                    <th><center>Acciones<center></th>
                    
                </tr>

            </thead>
            <tbody class="mytbody">
                
            </tbody>
        </table>
        </div>
        
    </div>
	
	<?php $this->load->view("footer");?>
    
 
 
<script type="text/javascript">

	var table;
 	$(document).ready(function() {
 
    //datatables
    table = $('#table').DataTable({ 

    	"responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: "<?php echo site_url('turnos/ajax_list')?>",
            type: "POST"
            
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [

        	{ className: "dt-right", "targets": [0,1,2,3,4,5,6] },
        	{ 
 
            	"targets": [0], //first column / numbering column
            	"orderable": false, //set not orderable
        	},

        ],

        "language": idioma_espanol,

        "createdRow":function(row,data,dataIndex)
        {
          if(data[7] == "CANCELADO" || data[7] == "CANCELACION CONFIRMADA")
          {
            $('td', row).css('background-color', '#ec9696');
          }

           if(data[7] == "ESPERA"){
            $('td', row).css('background-color', '#e8ff3acf');
           }
           
          if(data[7] == "ATENDIENDO"){
            $('td', row).css('background-color', '#48a2b7');
            Push.create('EL PACIENTE ESTA SIENDO ATENDIDO', {
                            body : 'EL paciente '+data[6]+' está siendo atendido. Hora de Turno '+ data[3],
                            icon: "<?php echo base_url(); ?>css/imagenes/atendiendo.png",
                            timeout : 8000,
                            vibrate: [100,100,100,100] ,
                            sound: "<?php echo base_url(); ?>/sound/testmp3"      
                    });
          }

          if(data[7] == "ATENDIDO" ){
            $('td', row).css('background-color', '#5ff523');
            Push.create('PACIENTE ATENDIDO', {
                            body : 'EL paciente '+data[6]+' HA SIDO ATENDIDO. Hora de Turno '+ data[3],
                            icon: "<?php echo base_url(); ?>css/imagenes/atendido.png",
                            timeout : 8000,
                            vibrate: [100,100,100,100] ,
                            sound: "<?php echo base_url(); ?>/sound/testmp3"      
                    });
          }

          if(data[7] == "LISTO" ){
             $('td', row).css('background-color', '#5ff523');
          }
          if(data[5] == "SOBRETURNO" && data[7] == 'PENDIENTE'){
              $('td', row).css('background-color', '#9ba8bf');
          }

        }
 
    });
 
});

 	var idioma_espanol= {
		    "sProcessing":     "Procesando...",
		    "sLengthMenu":     "Mostrar _MENU_ registros",
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "Buscar:",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    } 
		}
</script>	

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

<script type="text/javascript">
    var int=self.setInterval("refresh()",1000000);
    function refresh()
    {
        location.reload(true);
    }
</script>

</body>

</html>
 
    
