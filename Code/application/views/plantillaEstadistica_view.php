<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-2.0.0.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>push.js-master/bin/push.min.js"></script>
    <link href="<?php echo base_url();?>css/jquery-ui.css" rel="Stylesheet"/></link>


    <link href="<?php echo base_url(); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/font-awesome/css/font-awesome.min.css"> <!--iconos -->
    <link rel="icon" href="<?php echo base_url(); ?>css/imagenes/favicon.ico" type="image/gif">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/bootstrap/css/custom.css">

    <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!--Para menú desplegable automático data-hover="dropdown" en <a>-->
    <link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome-free-5.2.0/css/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>DataTables/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/custom.css">

    <!-- para Datepicker-->

    <link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
    <link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">  
    
    <!-- END PACIENTES TURNOS AGENDA PROFESIONAlES USUARIOS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>Chart/css/Chart.css">
    
</head>

<body class="mybodytables" style="background-color:white;" > 
	<div class="container">
		<div class="mylogout">
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>

	<div class="container"><br>
        <div class="mycontainersmall" >
 			<h3 class="myh3">Reporte Policonsultorio</h3>
        </div>
    	
        <div class="row">
            <div class="col-sm-4">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
            <div class="col-sm-4">
                <canvas id="myChart2" width="400" height="400"></canvas>
            </div>
            <div class="col-sm-4">
                <canvas id="Chart1" width="400" height="400"></canvas>
            </div>
        </div>
    </div>
	
	<script src="<?php echo base_url();?>jquery/jquery-1.11.1.js"></script>
    <script src="<?php echo base_url();?>jquery/jquery-3.2.1.min.js"></script>


    <!--Para menu desplegable-->
    <script  src="<?php echo base_url(); ?>Chart/js/Chart.js"></script>
    <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
   



 
 <script>
        
        var ctx= document.getElementById("myChart").getContext("2d");
        var myChart= new Chart(ctx,{
            type:"bar",
            data:{
                labels:['col1','col2','col3'],
                datasets:[{
                        label:'Num datos',
                        data:[10,9,15],
                        backgroundColor:[
                            'rgb(66, 134, 244,0.5)',
                            'rgb(74, 135, 72,0.5)',
                            'rgb(229, 89, 50,0.5)'
                        ]
                },
                {label:'datos2',
                        data:[5,15,8],
                        backgroundColor:[
                            'rgb(35, 250, 244,0.5)',
                            'rgb(100, 13, 122,0.5)',
                            'rgb(10, 189, 150,0.5)'
                        ]
                    }]
            },
            options:{
                scales:{
                    yAxes:[{
                            ticks:{
                                beginAtZero:true
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'probability'
                            }
                    }]
                },
                
                events: false,
    tooltips: {
        enabled: false
    },
    hover: {
        animationDuration: 0
    },
    animation: {
        duration: 1,
        onComplete: function () {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';

            this.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function (bar, index) {
                    var data = dataset.data[index];                            
                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                });
            });
        }
    }
            }
        });
    </script>
     <script>
     
        var ctx= document.getElementById("myChart2").getContext("2d");
        var myChart= new Chart(ctx,{
            type:"pie",
            data:{
                labels:['col1','col2','col3'],
                datasets:[{
                        label:'Num datos',
                        data:[10,9,15],
                        backgroundColor:[
                            'rgb(66, 134, 244,0.5)',
                            'rgb(74, 135, 72,0.5)',
                            'rgb(229, 89, 50,0.5)'
                        ]
                }]
            },
            options: {
            legend: {
                display: false
            },
            events: false,
            animation: {
                duration: 1200,
                easing: "easeOutQuart",
                onComplete: function () {
                    var ctx = this.chart.ctx;
                    ctx.font='14px LatoRegular, Helvetica,sans-serif';
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';
                    this.data.datasets.forEach(function (dataset) {
                        for (var i = 0; i < dataset.data.length; i++) {
                            var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                 mR = m.innerRadius +
      (m.outerRadius - m.innerRadius) / 2,
                                sA = m.startAngle,
                                eA = m.endAngle,
                                mA = sA + (eA - sA)/2;
                            var x = mR * Math.cos(mA);
                            var y = mR * Math.sin(mA);
                            ctx.fillStyle = '#fff';

                            var p = String(Math.round(dataset.data[i]/34*100)) + "%";
                            if(dataset.data[i] > 0) {
                                ctx.fillText(dataset.data[i], m.x + x, m.y + y-10);
                                ctx.fillText(p, m.x + x, m.y + y + 5);
                            }
                        }
                    });
                }
            }
        }
        });
    </script>

    <script>
        var chartData = {
    labels: ["January", "February", "March", "April", "May", "June"],
        datasets: [
            {
                label: "Test",

                data: [60, 80, 81, 56, 55, 40],
                backgroundColor:[
                            'rgb(66, 134, 244,0.5)',
                            'rgb(74, 135, 72,0.5)',
                            'rgb(229, 89, 50,0.5)',
                            'rgb(66, 134, 244,0.5)',
                            'rgb(74, 135, 72,0.5)',
                            'rgb(229, 89, 50,0.5)'
                        ]
            },
            {
                label: "Test",

                data: [15, 20, 55, 10, 104, 120],
                backgroundColor:[
                            'rgb(66, 134, 244,0.5)',
                            'rgb(74, 135, 72,0.5)',
                            'rgb(229, 89, 50,0.5)',
                            'rgb(66, 134, 244,0.5)',
                            'rgb(74, 135, 72,0.5)',
                            'rgb(229, 89, 50,0.5)'
                        ]
            }
        ]
    };

var opt = {
    scales:{
                    yAxes:[{
                            ticks:{
                                beginAtZero:true
                            }
                    }]
                },
    events: false,
    tooltips: {
        enabled: false
    },
    hover: {
        animationDuration: 0
    },
    animation: {
        duration: 1,
        onComplete: function () {
            var chartInstance = this.chart;
                ctx = chartInstance.ctx;
            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';

            this.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function (bar, index) {
                    var data = dataset.data[index];                            
                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                });
            });
        }
    }
};
 var ctx = document.getElementById("Chart1").getContext("2d");
 
     myLineChart = new Chart(ctx, {
        type: 'bar',
        data: chartData,
        options: opt
     });
    </script>
</body>

</html>
 
    
