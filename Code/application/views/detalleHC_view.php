<?php $this->load->view('head_view');
?>
<script type="text/javascript">
  		$(document).ready(function(){
    		$("#profesionales").change(function(){
    			$.ajax({
      				url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaEspecialidades",
      				type: "POST",
      				data:"profesionalesid="+$("#profesionales").val(),
      				success: function(opciones){
        				$("#especialidadesid").html(opciones);
      				}
    			})
  			});
		});
</script>

<script type="text/javascript" >
		$(document).ready(function(){
			
			$("#pacienteEdad").val(CalcularEdad());

		});
</script>
<script type="text/javascript">
		function CalcularEdad(){
			var fechaNac = document.getElementById("pacientefechanac").value;
			var valores = fechaNac.split("-");
			var dia = valores[2];
			var mes = valores[1];
			var anio = valores[0];
			var fecha_actual = new Date();
			var dia_actual = fecha_actual.getDate();
			var mes_actual = fecha_actual.getMonth()+1;
			var anio_actual = fecha_actual.getYear();

			var edad = (anio_actual+1900)-anio; 

			if ( mes_actual < mes ){
	            edad--;
	        }
        	if ((mes == mes_actual) && (dia_actual < dia)){
            	edad--;
        	}
        	if (edad > 1900){
            	edad -= 1900;
       	 	}

       	 	if (edad < 18){
       	 		//document.getElementById("oculto").style.display = 'block';
       	 	}
       	 	
       	 	return edad;
		}
		
	</script>
	<script type="text/javascript">
	$(document).ready(function(){
			
		console.log('edad:',$('#pacienteEdad').val());
		if (($('#pacienteEdad').val() < 18) && ($('#pacienteEdad').val() != '')){
			$("#oculto").show();
		}else{
			console.log("estoy despues del else");
			$("#oculto").hide();
		}
		});
	</script>
	
<style>

		html, body {
		    width: 100%;
		    margin: 0;
		    padding: 0;
		    box-sizing: border-box;
		}
		#table{
		    width: 100% !important;
		    margin: 0 auto !important;
		}
  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		input.mybtn{
            height: 45px;
            background: #1AAE88;
            border:0;
            font-size: 20px;
            color:#fff;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius:4px;
			text-align: center;
		}

		input.mybtn:hover, input.mybtn:active{
                   opacity:0.6;
                   color:#fff;
		}

		input.mybtn:focus{
       opacity:0.6;
       color:#fff;
       background: #de615e;
		}

		.linea{
			border-top: 4px solid #a8a0a0;
		}

		
		.caja {
			width: 20px;
			height: 20px;
		}
		.lectura{ background-color: #b0dede !important }

</style>


<body class="mybodydatepicker" > 
	
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/historiaclinica/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>

	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>


			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Registrar un Detalle de Historia Clínica</h3>
								<p>Ingresar Datos </p>
							</div>
						</div>
						
						<div class="myform-top-right">
							<i class="fas fa-notes-medical"></i>
						</div>
					
					</div>
					
					<div class="myform-bottom">
						<hr>
						<div align="left">
							<h4 align="left">Datos de Historia Clínica del Paciente</h4>
						</div>
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
							<?php echo form_open('historiaclinica/CargaTablaDHC'); ?>

							<div class="row">
								<div class="col-sm-3">
								</div>
								<div class="col-sm-3">
								</div>
								<div class="col-sm-3">
								</div>
								<div class="col-sm-3">
									<div  class="form-group" align="left">
													<label >Fecha Alta</label>
													<input id="hcfechaalta" type="date" class="form-control lectura" name="hcFechaAlta"  value="<?php echo $hc[0]->historiasclinicasfechaalta;?>" readonly disabled>
										</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
										<div  class="form-group" align="left">
													<label >Id HC</label>
													<input id="hcId" type="text" class="form-control lectura" name="idHC"  value="<?php echo $hc[0]->historiasclinicasid;?>" readonly>
										</div>
								</div>
								<div class="col-sm-4">
										<div  class="form-group" align="left">
													<label >Apellido y Nombre</label>
													<input id="pacientenombre" type="text" class="form-control lectura" name="pacientenombre"  value="<?php echo $hc[0]->personasnombrecompuesto;?>" disabled >
										</div>
								</div>
								<div class="col-sm-3">
										<div  class="form-group" align="left">
													<label >DNI</label>
													<input id="pacienteDNI" type="text" class="form-control lectura" name="pacienteDNI"  value="<?php echo $hc[0]->personasnrodocumento;?>" readonly>
										</div>
								</div>
								<div class="col-sm-3">
										<div  class="form-group" align="left">
													<label >Sexo</label>
													<input id="pacienteSexo" type="text" class="form-control lectura" name="pacienteSexo"  value="<?php echo $hc[0]->sexonombre;?> ">
										</div>
								</div>
							</div>
							<div class="row">
								
								<div class="col-sm-3">
										<div  class="form-group" align="left">
													<label >Fecha de Nacimiento</label>
													<input id="pacientefechanac" type="date" class="form-control lectura" name="pacienteFechaNac"  value="<?php echo $hc[0]->personasfechanacimiento;?>" >
										</div>
								</div>
								<div class="col-sm-2">
										<div  class="form-group" align="left">
													<label >Edad</label>
													<input id="pacienteEdad" type="text" class="form-control lectura" name="inputEdad"  value="<?php echo $pacienteedad; ?>" readonly>
													
										</div>
								</div>
								<div class="col-sm-7">
										<div  class="form-group" align="left">
													<label >Teléfonos</label>
													<input id="pacienteTelefonos" type="text" class="form-control lectura" name="inputTelefono"  value="<?php echo $tel ?>" readonly>
										</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div  class="form-group" align="left">
										<label >Procedencia</label>
										<input id="pacienteProcedencia" type="text" class="form-control lectura" name="inputProcedencia"  value="<?php echo $hc[0]->historiasclinicasprocedenciapaciente; ?>" readonly disabled >
									</div>
								</div>
								
							</div>	
							<div class="row">
								<div class="col-sm-5">
									<div  class="form-group" align="left">
										<label >Obra Social</label>
										<input id="pacienteObraSocial" type="text" class="form-control lectura" name="inputObraSocial" value="<?php echo $os ?>"  readonly >
										
									</div>
								</div>
								<div class="col-sm-2">
									<div  class="form-group" align="left">
										<label >Número</label>
										<input id="pacienteNroOS" type="text" class="form-control lectura" name="inputNroOS" value="<?php echo $hc[0]->historiasclinicasnumeroospaciente; ?>" readonly >
									</div>
								</div>
								<div class="col-sm-5">
									<div  class="form-group" align="left">
										<label >Titular</label>
										<input id="pacienteTitularOS" type="text" class="form-control lectura" name="inputTitularOS" value="<?php echo $hc[0]->historiasclinicastitularospaciente; ?>" readonly >
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >Ocupación</label>
										<input id="pacienteOcupacion" type="text" class="form-control lectura" name="inputOcupacion" value="<?php echo $hc[0]->historiasclinicasocupacionpaciente; ?>" readonly >
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >Derivado</label>
										<input id="pacienteDerivado" type="text" class="form-control lectura" name="inputDerivado" value="<?php echo $hc[0]->historiasclinicasderivadopaciente; ?>" readonly >
									</div>
								</div>
							</div>
						<div id="oculto" hidden="true" >
							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >Nombre de la Madre</label>
										<input id="pacienteMadre" type="text" class="form-control lectura " name="inputMadre" value="<?php echo $hc[0]->historiasclinicasmadrepaciente; ?>" readonly>
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >Ocupación de la Madre</label>
										<input id="pacienteOcupacionM" type="text" class="form-control lectura" name="inputOcuapcionM" value="<?php echo $hc[0]->historiasclinicasocupacionmadrepaciente; ?>" readonly>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >Nombre del Padre</label>
										<input id="pacientePadre" type="text" class="form-control lectura " name="inputPadre" value="<?php echo $hc[0]->historiasclinicaspadrepaciente; ?>" readonly>
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >Ocupación del Padre</label>
										<input id="pacienteOcupacionP" type="text" class="form-control lectura " name="inputOcupacionP" value="<?php echo $hc[0]->historiasclinicasocupacionpadrepaciente; ?>" readonly>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div align="left">
							<h4 align="left">Datos del Profesional</h4>
						</div>
							<div class="row">
								<div class="col-sm-3">
									<div  class="form-group" align="left">
												<label >*Profesional</label>
												<select class="form-control" style="height:40px;" name="inputProfesional" id="profesionales" value="<?php echo $profesionalid;?>">
												<option value ='0' >Seleccione un profesional</option>
												<?php
												if (isset($_POST['registrar'])){
													foreach ($profesionales as $prof) {
														if($prof->profesionalesid == $profesionalid ){
															echo'<option  value="'.$prof->profesionalesid.'" selected >'.$prof->profesional.'</option>';
														}else{
															echo'<option  value="'.$prof->profesionalesid.'">'.$prof->profesional.'</option>';
														}
														
													}
												}else{
													foreach ($profesionales as $prof) {
														echo'<option  value="'.$prof->profesionalesid.'">'.$prof->profesional.'</option>';
													}
												}	
												
												?>
												</select>
												<div class="error" style="color: #f76060">
													<?php 
														if(isset($_POST['Registrar'])){
															echo $MensajeProfesional;
														} 
													?>
													
												</div>

									</div>
								</div>
								<div class="col-sm-3">
									<div  class="form-group" align="left">
												<label  id="especialidades">*Especialidades</label>
												<select class="form-control" style="height:40px;" name="inputEspecialidades" id="especialidadesid" value="<?php echo $especialidadid;?>">
												
												<?php
												if (isset($_POST['Buscar']) || isset($_POST['registrar'])){
													foreach ($especialidades as $esp) {
														if($esp->especialidadesid == $especialidadid ){
															echo'<option  value="'.$esp->especialidadesid.'" selected >'.$esp->especialidadesnombre.'</option>';
														}
														
													}
												}else{
													echo "<option value ='0' >Seleccione primero el profesional</option>";
												}	
												
												?>
												</select>
												<div class="error" style="color: #f76060">
													<?php 
														if(isset($_POST['Registrar'])){
															echo $MensajeEspecialidad;
														} 
													?>
													
												</div>

									</div>
								</div>
								<div class="col-sm-3">
									<div  class="form-group" align="left">
										<label  id="fecha">*Fecha</label>
										

										<!------------------------------------------- -->

											<div class='input-group input-append date' id='datetimepicker1'>
												<input type='text' class="form-control" id="fechaN" name="fechaN"/>
												<span class="input-group-addon add-on">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
                							</div>

										<!------------------------------------------- -->
												<div class="error" style="color: #f76060">
													<?php 
														if(isset($_POST['Registrar'])){
															echo $MensajeFecha;
														} 
													?>
													
												</div>


									</div>	
								</div>
								<div class="col-sm-3">
									<div  class="form-group" align="left">
										<label  id="fecha">*Hora</label>
										

										

											
												<input type='time' style="height:40px;"class="form-control" id="hora" name="DHChora" min="00:00" value="00:00"/>
												<div class="error" style="color: #f76060">
													<?php 
														if(isset($_POST['Registrar'])){
															echo $MensajeHora;
														} 
													?>
													
												</div>


										

									</div>	
								</div>
							</div>
							
								<div class="row">
									<div class="col-sm-12">
										<div  class="form-group" align="left">
													<label >*Diagnostico</label>
													<textarea id="DHCdiagnostico"  class="form-control " name="DHCdiagnostico" >
													</textarea> 
													<div class="error" style="color: #f76060">
													<?php 
														
														if(isset($_POST['Registrar'])){
															echo $MensajeDiagnostico;
														} 
													?>
													
												</div>

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-9">
										<div  class="form-group" align="left">
													<label >Observaciones</label>
													<textarea id="DHCobservaciones" class="form-control" name="DHCobservaciones">
													</textarea>  
										</div>
									</div>
									<div class="col-sm-3">
										<div  class="form-group" align="left">
											<label > </label><br>
											<button type="submit" class="btn btn-primary submitBtn" style="padding:12px 60px"value="Registrar" name="Registrar">Registrar</button>
										</div>
									</div>
								</div>	
								
						
						<div class="row">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2"  style="font-size:small">
							            <thead class="mytable">
							                <tr>
							                    <th><center>Id<center></th>
							                    <th><center>Profesionales</center></th>
							                    <th><center>Especialidades<center></th>
							                    <th><center>Fecha<center></th>
							                    <th><center>Hora<center></th>
							                    <th><center>Diagnóstico<center></th>
							                    <th><center>Observaciones<center></th>
							                    
							                </tr>

							            </thead>
							            <tbody class="mytbody" style="color:black;background:white;">
							            	
							            </tbody>
							        </table>
						        </div>
							</div>
						</div>
						
							<div class="row">		
								<div class="col-sm-3">
									<input id="profesionalesxespecialidadesid" type="hidden" class="form-control lectura" name="inputProfecionalxEspecialidad" value="<?php echo $especialidadesxprofesionalesid;?>" required="true" readonly >	
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/historiaclinica'">Finalizar</button>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/historiaclinica'">Cancelar</button>
								</div>
								<div class="col-sm-3">
									<input id="idpacinete" type="hidden" class="form-control lectura" name="idpaciente" value="<?php echo $id; ?>" required="true" readonly >	
								</div>				

							</div>							
						<?php echo form_close(); ?>	
						
					</div>
				</div>
			</div>
    	
		
        
        

    </div>
	
	<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-dropdown-hover/js/bootstrap-dropdownhover.min.js"></script>
	
    <script src="<?php echo base_url(); ?>DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>DataTables/media/js/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
	<script type="text/javascript">
		$(function () {

		$("#datetimepicker1").datepicker({
				language: 'es',
				autoclose: true,
				format:'dd/mm/yyyy',
				firstDay:1
			}).datepicker("setDate","<?php echo $fecha;?>");
		});
	</script>
<script type="text/javascript">

	var table;
 	$(document).ready(function() {
 
    //datatables
    table = $('#table').DataTable({ 

    	"responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: "<?php echo site_url('historiaclinica/ajax_list_detalle')?>",
            type: "POST",
            data:{HCid:$("#hcId").val()}
            
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [

        	{ className: "dt-right", "targets": [0,1,2,3,4,5,6] },
        	{ 
 
            	"targets": [0], //first column / numbering column
            	"orderable": false, //set not orderable
        	},
        ],

        "language": idioma_espanol

 
    });
 
});

 	var idioma_espanol= {
		    "sProcessing":     "Procesando...",
		    "sLengthMenu":     "Mostrar _MENU_ registros",
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "Buscar:",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    } 
		}
</script>	

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

</body>

</html>