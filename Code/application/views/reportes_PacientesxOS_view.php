<body class="mybodytables" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/reportes/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container"><br>
	<!-- Menu -->
	<?php $this->load->view('menu_view'); ?>

<!-- -------------------------------------------------------------------------------------- -->
        <div class="mycontainersmall" >
 			<h3 class="myh3">Generar Reportes de Cantidad de Pacientes por Obra Social</h3>
        </div>
    	
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                	<div class="col-sm-6">
	                    <select id="reporte" name="select" class="form-control">
	  						<option value="0">Seleccione una Opción</option> 
	  						<option value="1">Reporte de Cantidad de Pacientes por OS</option>
						</select>
					</div>

					<div class="col-sm-4">	
						<button id="" type="button" data-toggle="tooltip" title="Generar Reporte" class="btn btn-primary" onclick="pdf()" >Generar Reporte</button>
					</div>
                </div>     
            </div>
        </div>
        <div class="row">
			<div id="error" class="error" style="color: #f76060">					 
			</div>
		</div>
       
        
    </div>
	
	<?php $this->load->view("footer");?>
 
 
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>


<script type="text/javascript">

	function pdf() {

		$('#error').empty();
		var opcion = $('#reporte').val();

		if (opcion == 0){
			$('#error').append('Debe seleccionar al menos una operación a realizar.');
		}else{
			$('#error').empty();

			$.ajax({
						url:"<?php echo base_url(); ?>index.php/reportes/get_CantPacxOS",
						type:'POST',
						dataType:'json',
						data:{ estadoid:$('#reporte').val()
					}
				}).done(function(respuesta){
					
					// onlyPrint: Boolean
					// DE: http://stackoverflow.com/questions/2255291/print-the-contents-of-a-div
					var title = "Reporte de Cantidad de Pacientes por Obra Social";
					var mywindow = window.open('', 'my div', 'height=400px,width=600px');
					var fecha = new Date();
					var tipo;
					var tabla = '';
					var hora = fecha.getHours();
					var minutos = fecha.getMinutes();

					if (minutos<10){
						minutos = '0' + String(minutos);
					} else{
						minutos = String(minutos);
					}

					hora = String(hora);

					
					fecha = fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
					var estado = $('#reporte').val();

			
					
					mywindow.document.write('<head> <title> ' + title + '</title> <meta charset="UTF-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta name="viewport" content="width=device-width, initial-scale=1"> <script type="text/javascript" src="http://localhost/Seminario/Code/jquery/ajax-jquery-2.0.0.js">');
					mywindow.document.write('<'+'/script> <script type="text/javascript" src="http://localhost/Seminario/Code/jquery/ajax-jquery-1.10.2.js"> '+'<'+'/script> <script src="http://localhost/Seminario/Code/push.js-master/bin/push.min.js">'+'<'+'/script> <link href="http://localhost/Seminario/Code/css/jquery-ui.css" rel="Stylesheet"/></link> <link href="http://localhost/Seminario/Code//bootstrap/css/bootstrap.min.css" rel="stylesheet"> <link rel="stylesheet" href="http://localhost/Seminario/Code//font-awesome/css/font-awesome.min.css"> <!--iconos --> <link rel="icon" href="http://localhost/Seminario/Code/css/imagenes/favicon.ico" type="image/gif"> <link rel="stylesheet" href="http://localhost/Seminario/Code//bootstrap/css/custom.css"> <link href="http://localhost/Seminario/Code/bootstrap/css/bootstrap.min.css" rel="stylesheet"> <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/font-awesome/css/font-awesome.min.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/fontawesome-free-5.2.0/css/all.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/DataTables/media/css/dataTables.bootstrap.min.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/bootstrap/css/custom.css"></head>');

					mywindow.document.write('<body class="mybodytables" style="background-color:white;" > <div class="container"><div class="mylogout"><img src="http://localhost/Seminario/Code/css/imagenes/salutiaLogo.png" width =100% height=80 align=middle></div></div><div class="container"><br><div class="mycontainersmall" ><h3 class="myh3">'+title+'</h3><div style="display:flex;"><label>Fecha de Reporte:</label> <p>'+fecha+' '+hora+':'+minutos+' hs.</p> </div></div><div class="row"><div class="col-sm-12"><div class="form-group"><div class="table-responsive"><table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2" width="100%" style="font-size:small"><thead class="mytable"><tr><th><center>Id<center></th><th><center>Nombre de Obra Social</center></th><th><center>Cantidad de Pacientes<center></th></tr></thead><tbody class="mytbody">');


					for (var i = 0; i<respuesta.length ; i++){

    					mywindow.document.write('<tr style="border: #ffffff 3px solid !important;"><td><center>'+respuesta[i].obrasocialid+'</center></td><td><center>'+respuesta[i].obrasocialnombre+'</center></td><td><center>'+respuesta[i].cantpac+'</center></td></tr>');
    				}


					mywindow.document.write('</tbody></table></div></div></div></div></div>');

					mywindow.document.write('<script type="text/javascript">window.onload = function(){Push.Permission.request();}<'+'/script><script src="http://localhost/Seminario/Code/jquery/jquery-1.11.1.js"><'+'/script><script src="http://localhost/Seminario/Code/jquery/jquery-3.2.1.min.js"><'+'/script></body></html>');


			      	setTimeout(function () {
						mywindow.print();
						mywindow.close();
					}, 2000);

				});
		} 

		
			};

</script>



</body>

</html>
 
    
