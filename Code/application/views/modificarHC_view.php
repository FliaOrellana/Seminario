<?php $this->load->view('head_view');
?>
<style type="text/css">
	.mybodydatepicker{
		font-family:'Convergence', sans-serif;
 		font-size:20px;
 		font-weight: 300;
 		color: #000000;
 		line-height: 30px;
 		text-align: center;
 		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

	.lectura{ background-color: #b0dede !important }
</style>
<script type="text/javascript" >
		$(document).ready(function(){
			
			$("#pacienteEdad").val(CalcularEdad());

		});
	</script>
<script type="text/javascript">
		function CalcularEdad(){
			var fechaNac = document.getElementById("pacienteFechaNac").value;
			var valores = fechaNac.split("-");
			var dia = valores[2];
			var mes = valores[1];
			var anio = valores[0];
			var fecha_actual = new Date();
			var dia_actual = fecha_actual.getDate();
			var mes_actual = fecha_actual.getMonth()+1;
			var anio_actual = fecha_actual.getYear();

			var edad = (anio_actual+1900)-anio; 

			if ( mes_actual < mes ){
	            edad--;
	        }
        	if ((mes == mes_actual) && (dia_actual < dia)){
            	edad--;
        	}
        	if (edad > 1900){
            	edad -= 1900;
       	 	}

       	 	if (edad < 18){
       	 		//document.getElementById("oculto").style.display = 'block';
       	 	}
       	 	
       	 	return edad;
		}
		
	</script>
	<script type="text/javascript">
	$(document).ready(function(){
			
		console.log($('#pacienteEdad').val());
		if (($('#pacienteEdad').val() < 18) && ($('#pacienteEdad').val() != '')){
			$("#oculto").show();
		}else{
			console.log("estoy despues del else");
			$("#oculto").hide();
		}
		});
	</script>
<body class="mybodydatepicker" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/historiaclinica/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Modificar Datos de Historia Clínica</h3>
								<p>Ingresar Datos </p>
								

							</div>
						</div>
						
						<div class="myform-top-right">
							<i class="fas fa-notes-medical"></i>
						</div>
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
						
						<?php echo form_open('historiaclinica/modificarHC/'.$id);?>
							<div class = "row">

								<div class="col-sm-4">
								</div>

								<div class="col-sm-4">
								</div>

								<div class="col-sm-4">
									<div  class="form-group" align="left">
										<label >Fecha Alta</label>
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'hcfechaalta',
																		'name' => 'hcFechaAlta',
																		'placeholder' => '',
																		'value' => $fecha,
																		'class' => 'form-control lectura',
																		'type' => 'date',
																		'readonly' => 'true'));
											 } else {
											 	echo form_input(array('id' => 'hcfechaalta',
																		'name' => 'hcFechaAlta',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicasfechaalta,
																		'class' => 'form-control lectura',
																		'type' => 'date',
																		'readonly' => 'true'));
											 }
											 
											 //print_r($datos->result()[0]->historiasclinicasfechaalta);
											 //var_dump($idPa); 
											 //print_r($datos[0]->personasnrodocumento);
								
										 ?>
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											
											<label >*DNI</label>
											
											<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteDNI',
																		'name' => 'inputDNI',
																		'placeholder' => '',
																		'value' => $pacientedni,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'));
											 } else {
											 	echo form_input(array('id' => 'pacienteDNI',
																		'name' => 'inputDNI',
																		'placeholder' => '',
																		'value' => $datos[0]->personasnrodocumento,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'));
											 }
								
										 ?>
										</div>
								</div>
								<div class="col-sm-2">
										<div  class="form-group" align="left">
											<label >*Ident.</label>
											<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteID',
																		'name' => 'inputID',
																		'placeholder' => '',
																		'value' => $pacienteid,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'));
											 } else {
											 	echo form_input(array('id' => 'pacienteID',
																		'name' => 'inputID',
																		'placeholder' => '',
																		'value' => $datos[0]->pacientesid,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'));
											 }
								
										 ?>
										</div>
								</div>
								<div class="col-sm-7">
										<div  class="form-group" align="left">
											<label >*Apellido y Nombre</label>
											
											<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteNombreCompuesto',
																		'name' => 'inputNombreCompuesto',
																		'placeholder' => '',
																		'value' => $pacientenombre,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'));
											 } else {
											 	echo form_input(array('id' => 'pacienteNombreCompuesto',
																		'name' => 'inputNombreCompuesto',
																		'placeholder' => '',
																		'value' => $datos[0]->personasnombrecompuesto,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'));
											 }
								
										 ?>
										</div>
								</div>
								
							</div>
							
							<div class="row">

								<div class="col-sm-3">
									<div  class="form-group" align="left">
										<label >*Fecha de Nacimiento</label>
										
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteFechaNac',
																		'name' => 'inputFechaNac',
																		'placeholder' => '',
																		'value' => $fechanac,
																		'class' => 'form-control lectura',
																		'type' => 'date',
																		'readonly' => 'true'));
											 } else {
											 	echo form_input(array('id' => 'pacienteFechaNac',
																		'name' => 'inputFechaNac',
																		'placeholder' => '',
																		'value' => $datos[0]->personasfechanacimiento,
																		'class' => 'form-control lectura',
																		'type' => 'date',
																		'readonly' => 'true'));
											 }
								
										 ?>
									</div>
								</div>

								<div class="col-sm-2">
									<div  class="form-group" align="left">
										<label >*Edad</label>
									
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteEdad',
																		'name' => 'inputEdad',
																		'placeholder' => '',
																		'value' => $pacienteedad,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'));
											 } else {
											 	echo form_input(array('id' => 'pacienteEdad',
																		'name' => 'inputEdad',
																		'placeholder' => '',
																		'value' => $pacienteedad,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'));
											 }
								
										 ?>
									</div>
								</div>

								<div class="col-sm-7">
									<div  class="form-group" align="left">
										<label >*Telefono</label>
										
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteTelefonos',
																		'name' => 'inputTelefono',
																		'placeholder' => '',
																		'value' => $tel,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteTelefonos',
																		'name' => 'inputTelefono',
																		'placeholder' => '',
																		'value' => $tel,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'
																		));
											 }
										
										 ?>

									</div>
								</div>

							</div>

							<div class="row">
								<div class="col-sm-12">
									<div  class="form-group" align="left">
										<label >* Procedencia</label>
										
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteProcedencia',
																		'name' => 'inputProcedencia',
																		'placeholder' => '',
																		'value' => $pacienteprocedencia,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteProcedencia',
																		'name' => 'inputProcedencia',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicasprocedenciapaciente,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 }
								
										 ?>
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajeProcedencia;
												} 
											?>
										</div>
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-sm-5">
									<div  class="form-group" align="left">
										<label >*Obra Social</label>
										
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteObraSocial',
																		'name' => 'inputObraSocial',
																		'placeholder' => '',
																		'value' => $os,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteObraSocial',
																		'name' => 'inputObraSocial',
																		'placeholder' => '',
																		'value' => $os,
																		'class' => 'form-control lectura',
																		'type' => 'text',
																		'readonly' => 'true'
																		));
											 }
										
										 ?>
										
									</div>
								</div>
								<div class="col-sm-2">
									<div  class="form-group" align="left">
										<label >* Número</label>
										
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteNroOS',
																		'name' => 'inputNroOS',
																		'placeholder' => '',
																		'value' => $pacientenroos,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteNroOS',
																		'name' => 'inputNroOS',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicasnumeroospaciente,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 }
								
										 ?>
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajeNroOs;
												} 
											?>
										</div>
									</div>
								</div>
								<div class="col-sm-5">
									<div  class="form-group" align="left">
										<label >* Titular</label>
							
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteTitularOS',
																		'name' => 'inputTitularOS',
																		'placeholder' => '',
																		'value' => $pacientetitularos,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteTitularOS',
																		'name' => 'inputTitularOS',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicastitularospaciente,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 }
								
										 ?>
									</div>
									<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajeTitularOs;
												} 
											?>
									</div>
								</div>
								
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Ocupación</label>
									
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteOcupacion',
																		'name' => 'inputOcupacion',
																		'placeholder' => '',
																		'value' => $pacienteocupacion,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteOcupacion',
																		'name' => 'inputOcupacion',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicasocupacionpaciente,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 }
								
										 ?>
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajeOcupacion;
												} 
											?>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Derivado</label>
										
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteDerivado',
																		'name' => 'inputDerivado',
																		'placeholder' => '',
																		'value' => $pacientederivado,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteDerivado',
																		'name' => 'inputDerivado',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicasderivadopaciente,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 }
								
										 ?>
									</div>
								</div>
							</div>
						
						<div id="oculto" hidden="true">
							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Nombre de la Madre</label>
						
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteMadre',
																		'name' => 'inputMadre',
																		'placeholder' => '',
																		'value' => $pacientemadre,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteMadre',
																		'name' => 'inputMadre',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicasmadrepaciente,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 }
								
										 ?>
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajeMadre;
												} 
											?>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Ocupación de la Madre</label>
										
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteOcupacionM',
																		'name' => 'inputOcupacionM',
																		'placeholder' => '',
																		'value' => $pacienteocupacionm,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteOcupacionM',
																		'name' => 'inputOcupacionM',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicasocupacionmadrepaciente,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 }
								
										 ?>
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajeOcupacionM;
												} 
											?>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Nombre del Padre</label>
									
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacientePadre',
																		'name' => 'inputPadre',
																		'placeholder' => '',
																		'value' => $pacientepadre,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 } else {
											 	echo form_input(array('id' => 'pacientePadre',
																		'name' => 'inputPadre',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicaspadrepaciente,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 }
								
										 ?>
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajePadre;
												} 
											?>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Ocupación del Padre</label>
										
										<?php
											if (isset($_POST['Registrar'])) {
											 	echo form_input(array('id' => 'pacienteOcupacionP',
																		'name' => 'inputOcupacionP',
																		'placeholder' => '',
																		'value' => $pacienteocupacionp,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 } else {
											 	echo form_input(array('id' => 'pacienteOcupacionP',
																		'name' => 'inputOcupacionP',
																		'placeholder' => '',
																		'value' => $datos[0]->historiasclinicasocupacionpadrepaciente,
																		'class' => 'form-control',
																		'type' => 'text',
																		));
											 }
								
										 ?>
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Registrar'])){
													echo $mensajeOcupacionP;
												} 
											?>
										</div>
									</div>
								</div>
							</div>
						</div>	
								<!--<div class="col-sm-2">
								<input id="profesionalesxespecialidadesid" type="hidden" class="form-control lectura" name="inputProfecionalxEspecialidad" value="<?php echo $especialidadesxprofesionalesid;?>" required="true" readonly >		
								</div>	
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label  id="boton"></label>
										<input type="submit" class="mybtn"  value="Buscar" name="Buscar"/>
										
									</div>				
								</div>	-->

						

							
							<div class="row">		
								<div class="col-sm-3">
								</div>		
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary submitBtn" style="padding:15px 50px"value="Registrar" name="Registrar">Registrar</button>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/historiaclinica'">Cancelar</button>
								</div>
								<div class="col-sm-3">
								</div>				

							</div>	
								
						<?php echo form_close()?>				
						
						
					</div>
				</div>
			</div>
    	
		
        
        

    </div>
	<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-dropdown-hover/js/bootstrap-dropdownhover.min.js"></script>
	
    <script src="<?php echo base_url(); ?>DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>DataTables/media/js/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
	<script type="text/javascript">
		$(function () {

		$("#datetimepicker1").datepicker({
				language: 'es',
				autoclose: true,
				format:'dd/mm/yyyy',
				firstDay:1
			}).datepicker("setDate","<?php echo $fecha;?>");
		});
	</script>
	<script type = "text/javascript">	
		$(document).ready(function () {
			$('#table').DataTable({
				"paginatype":"simple_numbers",
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
				}
			});
			
			$('.dataTables_length').addClass('bs-select');
		});
	</script>


</body>

</html>