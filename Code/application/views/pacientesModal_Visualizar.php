<div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #65e26f;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Datos Pacientes</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-sm-6">
							<div  class="form-group" align="left">
								<label for="inputApellido">Apellido y Nombre</label>
								<input id="pacienteApellido" type="text" class="form-control" name="inputApellido" disabled="true">
							</div>

						</div>
						<div class="col-sm-6">
							<div  class="form-group" align="left">
								<label for="inputDNI">DNI</label>
								<input id="pacienteDNI" type="text" class="form-control" name="inputDNI" disabled="true">
							</div>
							
						</div>
						
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div  class="form-group" align="left">
								<label for="inputFechaNacimiento">Fecha Nacimiento</label>
								<input id="pacienteNacimiento" type="text" class="form-control" name="inputFechaNacimiento" disabled="true">
							</div>
							
						</div>
						<div class="col-sm-4">
							<div  class="form-group" align="left">
								<label for="inputEdad">Edad</label>
								<input id="pacienteEdad" type="text" class="form-control" name="inputEdad" disabled="true">
							</div>
						</div>
						<div class="col-sm-4">
							<div  class="form-group" align="left">
								<label for="inputSexo">Sexo</label>
								<input id="pacienteSexo" type="text" class="form-control" name="inputSexo" disabled="true">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div  class="form-group" align="left">
								<label for="inputDomicilio">Domicilio</label>
								<input id="pacienteDomicilio" type="text" class="form-control" name="inputDomicilio" disabled="true">
							</div>
						</div>

						<div class="col-sm-6">
							<div  class="form-group" align="left">
								<label for="inputEmail">Email</label>
								<input id="pacienteEmail" type="text" class="form-control" name="inputEmail" disabled="true">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div  class="form-group" align="left">
								<label for="inputTelefonos">Telefonos</label>
								<input id="pacienteTelefonos" type="text" class="form-control" name="inputTelefonos" disabled="true">
							</div>
						</div>

						<div class="col-sm-6">
							<div  class="form-group" align="left">
								<label for="inputOS">Obras Sociales</label>
								<input id="pacienteOS" type="text" class="form-control" name="inputOS" disabled="true">
							</div>
						</div>
						
					</div>
				</form>
			</div>
			
							
		</div>
	</div>
</div>