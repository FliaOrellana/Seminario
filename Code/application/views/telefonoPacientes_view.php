
						        <div class="myform-top">
									<div class="myform-top-left">
										<div class="col-sm-6">
											<p>Registrar Teléfonos</p>
											<h5>* campos obligatorios</h5>
										</div>
									</div>
									
									<div class="myform-top-right">
										<i class="fas fa-mobile-alt"></i>
									</div>
								</div>
						
								<div class="myform-bottom">
									<div align="left">
										<p style="font-size:13px" align="left"></p>
									</div>
									<?php echo form_open('pacientes/RegistrarTelefono/'.$id); ?>
										<!----------------------------------------------------->
										<div class="row">
											<div class="col-sm-5">
													<div  class="form-group" align="left">
														<label for="pacienteTipoTelefono" >* Tipo Telefono</label>
														
														<select class="form-control" style="height:40px;" name="pacienteTipoTelefono" id="pacienteTipoTelefono">
														<?php
															if (isset($_POST['Registrar'])) {
																foreach ($tipotelefonos as $tipo){
																	if ($tipo->tipotelefonosid == $tipotelefonoid) {
																		echo'<option  value="'.$tipo->tipotelefonosid.'" selected >'.$tipo->tipotelefonosnombre.'</option>';
																	} else {
																		echo'<option  value="'.$tipo->tipotelefonosid.'">'.$tipo->tipotelefonosnombre.'</option>';
																	}
																	
																}
																
															} else {
																foreach ($tipotelefonos as $tipo) {
																	echo'<option  value="'.$tipo->tipotelefonosid.'">'.$tipo->tipotelefonosnombre.'</option>';
																}
															}
															
																
															?>
														</select>
														
													</div>
											</div>
											<div class="col-sm-5">
													<div  class="form-group" align="left">
														<label for="inputTelefono">* Telefono</label>
														
														<?php echo form_input(array('id' => 'pacienteTelefono',
																					'name' => 'pacienteTelefono',
																					'placeholder' => 'Ingrese Telefono',
																					'value' => $telefono,
																					'class' => 'form-control')); ?>
														<?php echo form_error('pacienteTelefono');?><br />
														<!--<input id="pacienteTelefono" type="text" class="form-control" name="inputTelefono" placeholder="Ingrese Telefono" required="true">-->
													</div>
											</div>
											<div class="col-sm-2">
												<div  class="form-group" align="left">
													<label></label>
													<button type="submit" class="btn btn-primary submitBtn" style="padding:10px 50px"value="Registrar" name="Registrar">Registrar</button>	
												</div>
											</div>
										</div>

										

										<div class="row">
											<div class="col-sm-12">
												<div class="table-responsive">
													<table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2"  style="font-size:small;">
											            <thead class="mytable">
											                <tr>
											                    <th><center>Tipo<center></th>
											                    <th><center>Número</center></th>
											                    <th><center>Acciones<center></th>   
											                </tr>

											            </thead>
											            <tbody class="mytbody" style="color:black;background:white;">
											            	
											            </tbody>
											        </table>
										        </div>
											</div>
										</div>
										
										<!----------------------------------------------------->
										<div class="row">		
											<div class="col-sm-3">
											</div>
											<div class="col-sm-3">
												<button type="button" class="btn btn-primary submitBtn" style="background-color:#2e6da4;border-color:#337ab7;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/pacientes/RegistrarObraS/<?php echo $id ?>'">Siguiente</button>
											</div>
											<div class="col-sm-3">
												<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/pacientes'">Cancelar</button>
											</div>
											<div class="col-sm-3">
											</div>				

										</div>			
										<?php echo form_close(); ?>
																
									
									
								</div>

					    <!----------------------------------fin tab telefono------------------->