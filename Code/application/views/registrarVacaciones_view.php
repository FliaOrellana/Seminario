<?php $this->load->view('head_view');

?>
<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">  
	
<style type="text/css">

		.table-striped>tbody>tr{
			background-color: #e2e0e0 !important;
		}
  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		.nombreProfesional {
			font-size: 1.5em;
    		color: rgba(0,0,0,.5);
		}

		.licencias{
			color: #000000 !important;
			
		}

		.botonregistrar{
			padding: 10px 80px;
    		margin-left: 1em;
    		margin-top: 1.5em;
		}

		.fila{
			display: flex;
    		flex-direction: row;
    		align-items: center;
		}
		.botoncancelar{
			display: flex;
    		flex-direction: row-reverse;
    		margin-right: 10px; 
    		
		}
		td.day.disabled{
		    color: #f70808 !important;
		    }
		
</style>


<body class="mybodydatepicker" > 
	
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/agenda/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>

	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Registrar Días de Vacaciones</h3>
								<p>Ingresar Datos </p>
							</div>
						</div>
						
						<div class="myform-top-right">
							<i class="far fa-address-book"></i>
						</div>
					
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
							<?php echo form_open('agenda/registrarVacaciones'); ?>
							

							<div class="row">
								<div class="col-sm-5">
									<div  class="form-group" align="left">
													<label >Profesional:</label>
													<label class="nombreProfesional"><?php echo $datosprof['personasnombrecompuesto'];?></label>
													<!--<input id="profesionales" type="text" class="form-control lectura" name="inputProfesional"  value="" readonly>-->

									</div>
								</div>
							</div>

							<div class = "row fila" >
								<div class="col-sm-4">
									<div  class="form-group" align="left">
										<label  id="fecha">*Fecha Inicio Vacaciones</label>
										

										<!------------------------------------------- -->

											<div class='input-group input-append date' id='datetimepicker1'>
												<input type='text' class="form-control" id="fechaI" name="fechaI"/>
												<span class="input-group-addon add-on">
													<span id="calendario" class="glyphicon glyphicon-calendar"></span>
												</span>
                							</div>

										<!------------------------------------------- -->

									</div>	
								</div>
								<div class="col-sm-4">
									<div  class="form-group" align="left">
										<label  id="fecha">*Fecha Fin Vacaciones</label>
										

										

											<div class='input-group input-append date' id='datetimepicker2'>
												<input type='text' class="form-control" id="fechaF" name="fechaF"/>
												<span class="input-group-addon add-on">
													<span id="calendario" class="glyphicon glyphicon-calendar"></span>
											 	</span>
                							</div>
	
									</div>
								</div>

								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary submitBtn botonregistrar" value="Registrar" name="Registrar">Registrar</button>
								</div>
							</div>	


							<div class="row fila" style="margin-left: 1px;" >
								<div class="error" style="color: #f76060;"> 
									<?php 
										echo $mensajeerror.'<br>';
									?>

								</div>
																
							</div>	
						<br><br>
						
						
						<?php echo form_close(); ?>	
						<!------------------------------------------>
						<div class="row licencias">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="table-responsive">
										<table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2" width="100%" style="font-size:small">
											<thead class="mytable">
												<tr>
													<th><center>Id<center></th>
													<th><center>Fecha Inicio</center></th>
													<th><center>Fecha Fin<center></th>
													<th><center>Espacialidad<center></th>
													<th><center>Acciones<center></th>
												</tr>

											</thead>
											<tbody class="mytbody">
											
											</tbody>
										</table>
									</div>
								</div>
								<div class="row botoncancelar" >
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/agenda'">Cancelar</button>
								</div>	
							</div>
							
						</div>
						<!------------------------------------------------>
						
					</div>
				</div>
    		</div>
	</div>
	<?php $this->load->view("footer");?>

	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
	
	<script type="text/javascript">

var table;
 $(document).ready(function() {

//datatables
table = $('#table').DataTable({ 

	"responsive": true,
	"processing": true, //Feature control the processing indicator.
	"serverSide": true, //Feature control DataTables' server-side processing mode.
	"order": [], //Initial no order.

	// Load data for the table's content from an Ajax source
	"ajax": {
		url: "<?php echo site_url('agenda/Vacaciones')?>",
		type: "POST"
		
	},

	//Set column definition initialisation properties.
	"columnDefs": [

		{ className: "dt-right", "targets": [0,1,2,3] },
		{ 

			"targets": [0], //first column / numbering column
			"orderable": false, //set not orderable
		},
	],

	"language": idioma_espanol


});

});

 var idioma_espanol= {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "",
		"sInfoEmpty":      "",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		} 
	}
</script>
<script type="text/javascript">
		$("#datetimepicker1").datepicker({
			language: 'es',
			autoclose: true,
			//endDate: new Date(),//aqui ver
			format:'dd/mm/yyyy',
			firstDay:1,
			beforeShowDay: function(date){
				var dn = date.getDay();
				var d = date.getDate();
				var m = date.getMonth();
				var a = date.getFullYear();
				<?php
				$con = ''; 
				foreach ($dias as $d) {
					$con = $con.'dn != '.$d->diasid." && ";
				};
						
				$con = substr($con,0,-3);
				$dLicencias = "var diasLicencias = [";
				$mLicencias = "var mesesLicencias = [";
				$aLicencias = "var anioLicencias = [";
				$b = 0;
				if(!empty($licencias)){
					$b = 1;
					foreach ($licencias as $l) {
						$dLicencias = $dLicencias.$l->dia.",";
						$mLicencias = $mLicencias.$l->mes.",";
						$aLicencias = $aLicencias.$l->anio.",";
					}
				}
				if(!empty($vacaciones)){
					$b = 1;
					foreach ($vacaciones as $v) {
						$dLicencias = $dLicencias.$v->dia.",";
						$mLicencias = $mLicencias.$v->mes.",";
						$aLicencias = $aLicencias.$v->anio.",";
					}
				}
				if(!empty($feriados)){
					$b = 1;
					foreach ($feriados as $f) {
						$dLicencias = $dLicencias.$f->dia.",";
						$mLicencias = $mLicencias.$f->mes.",";
						$aLicencias = $aLicencias.$f->anio.",";
					}
				}
				if ($b == 0){
					$dLicencias = $dLicencias."];\n\t";
					$mLicencias = $mLicencias."];\n\t";
					$aLicencias = $aLicencias."];\n\t";
				}else{
					$dLicencias = substr($dLicencias,0,-1);
					$dLicencias = $dLicencias."];\n\t";
					$mLicencias = substr($mLicencias,0,-1);
					$mLicencias = $mLicencias."];\n\t";
					$aLicencias = substr($aLicencias,0,-1);
					$aLicencias = $aLicencias."];\n\t";
					echo $dLicencias;
					echo $mLicencias;
					echo $aLicencias;
				}

				/*if( !empty($licencias) && !empty($vacaciones)){
					$dLicencias = "var diasLicencias = [";
					$mLicencias = "var mesesLicencias = [";
					$aLicencias = "var anioLicencias = [";
					foreach ($licencias as $l) {
						$dLicencias = $dLicencias.$l->dia.",";
						$mLicencias = $mLicencias.$l->mes.",";
						$aLicencias = $aLicencias.$l->anio.",";
					}
					foreach ($vacaciones as $v) {
						$dLicencias = $dLicencias.$v->dia.",";
						$mLicencias = $mLicencias.$v->mes.",";
						$aLicencias = $aLicencias.$v->anio.",";
					}
					$dLicencias = substr($dLicencias,0,-1);
					$dLicencias = $dLicencias."];\n\t";
					$mLicencias = substr($mLicencias,0,-1);
					$mLicencias = $mLicencias."];\n\t";
					$aLicencias = substr($aLicencias,0,-1);
					$aLicencias = $aLicencias."];\n\t";
					echo $dLicencias;
					echo $mLicencias;
					echo $aLicencias;

				}else{
					if( !empty($licencias)){
						$dLicencias = "var diasLicencias = [";
						$mLicencias = "var mesesLicencias = [";
						$aLicencias = "var anioLicencias = [";
						foreach ($licencias as $l) {
							$dLicencias = $dLicencias.$l->dia.",";
							$mLicencias = $mLicencias.$l->mes.",";
							$aLicencias = $aLicencias.$l->anio.",";
						}
						$dLicencias = substr($dLicencias,0,-1);
						$dLicencias = $dLicencias."];\n\t";
						$mLicencias = substr($mLicencias,0,-1);
						$mLicencias = $mLicencias."];\n\t";
						$aLicencias = substr($aLicencias,0,-1);
						$aLicencias = $aLicencias."];\n\t";
						echo $dLicencias;
						echo $mLicencias;
						echo $aLicencias;
					}else {
						if(!empty($vacaciones)){
							$dLicencias = "var diasLicencias = [";
							$mLicencias = "var mesesLicencias = [";
							$aLicencias = "var anioLicencias = [";
							foreach ($vacaciones as $v) {
								$dLicencias = $dLicencias.$v->dia.",";
								$mLicencias = $mLicencias.$v->mes.",";
								$aLicencias = $aLicencias.$v->anio.",";
							}
							$dLicencias = substr($dLicencias,0,-1);
							$dLicencias = $dLicencias."];\n\t";
							$mLicencias = substr($mLicencias,0,-1);
							$mLicencias = $mLicencias."];\n\t";
							$aLicencias = substr($aLicencias,0,-1);
							$aLicencias = $aLicencias."];\n\t";
							echo $dLicencias;
							echo $mLicencias;
							echo $aLicencias;

						}
					}
				}*/
				
				
				?>
				if (<?php echo $con;?>) {
					return false;
				} else {
					<?php
						if(!empty($licencias) || !empty($vacaciones)){ 
							echo "\t\tvar bandera=0;\n\t\t\tvar i = 0;\n\t\t ";
							echo "while( (bandera == 0) && (i < diasLicencias.length)){\n\t\t\t\t";
							echo "if ( (diasLicencias[i] == d) && (mesesLicencias[i] == m) && (anioLicencias[i] == a) ){\n\t\t\t\t\t";
							echo "bandera =1;\n\t\t\t\t}\n\t\t\ti = i + 1;\n\t\t}\n\t";
							echo "if(bandera == 0){return true;}else{return false;}";
						}else{
							echo "return true;";
						}	
					?>
					//return true;
				}
			}

		}).datepicker("setDate","<?php echo $fechaI;?>");
	
</script>
<script type="text/javascript">

	$("#datetimepicker2").datepicker({
		language: 'es',
		autoclose: true,
		//endDate: new Date(),//aqui ver
		format:'dd/mm/yyyy',
		firstDay:1,
		beforeShowDay: function(date){
			var dn = date.getDay();
			var d = date.getDate();
			var m = date.getMonth();
			var a = date.getFullYear();
			<?php
			$con = ''; 
			foreach ($dias as $d) {
				$con = $con.'dn != '.$d->diasid." && ";
			};
					
			$con = substr($con,0,-3);
			$dLicencias = "var diasLicencias = [";
			$mLicencias = "var mesesLicencias = [";
			$aLicencias = "var anioLicencias = [";
			$b = 0;
			if(!empty($licencias)){
				$b = 1;
				foreach ($licencias as $l) {
					$dLicencias = $dLicencias.$l->dia.",";
					$mLicencias = $mLicencias.$l->mes.",";
					$aLicencias = $aLicencias.$l->anio.",";
				}
			}
			if(!empty($vacaciones)){
				$b = 1;
				foreach ($vacaciones as $v) {
					$dLicencias = $dLicencias.$v->dia.",";
					$mLicencias = $mLicencias.$v->mes.",";
					$aLicencias = $aLicencias.$v->anio.",";
				}
			}
			if(!empty($feriados)){
				$b = 1;
				foreach ($feriados as $f) {
					$dLicencias = $dLicencias.$f->dia.",";
					$mLicencias = $mLicencias.$f->mes.",";
					$aLicencias = $aLicencias.$f->anio.",";
				}
			}
			if ($b == 0){
				$dLicencias = $dLicencias."];\n\t";
				$mLicencias = $mLicencias."];\n\t";
				$aLicencias = $aLicencias."];\n\t";
			}else{
				$dLicencias = substr($dLicencias,0,-1);
				$dLicencias = $dLicencias."];\n\t";
				$mLicencias = substr($mLicencias,0,-1);
				$mLicencias = $mLicencias."];\n\t";
				$aLicencias = substr($aLicencias,0,-1);
				$aLicencias = $aLicencias."];\n\t";
				echo $dLicencias;
				echo $mLicencias;
				echo $aLicencias;
			}

			/*if( !empty($licencias) && !empty($vacaciones)){
				$dLicencias = "var diasLicencias = [";
				$mLicencias = "var mesesLicencias = [";
				$aLicencias = "var anioLicencias = [";
				foreach ($licencias as $l) {
					$dLicencias = $dLicencias.$l->dia.",";
					$mLicencias = $mLicencias.$l->mes.",";
					$aLicencias = $aLicencias.$l->anio.",";
				}
				foreach ($vacaciones as $v) {
					$dLicencias = $dLicencias.$v->dia.",";
					$mLicencias = $mLicencias.$v->mes.",";
					$aLicencias = $aLicencias.$v->anio.",";
				}
				$dLicencias = substr($dLicencias,0,-1);
				$dLicencias = $dLicencias."];\n\t";
				$mLicencias = substr($mLicencias,0,-1);
				$mLicencias = $mLicencias."];\n\t";
				$aLicencias = substr($aLicencias,0,-1);
				$aLicencias = $aLicencias."];\n\t";
				echo $dLicencias;
				echo $mLicencias;
				echo $aLicencias;

			}else{
				if( !empty($licencias)){
					$dLicencias = "var diasLicencias = [";
					$mLicencias = "var mesesLicencias = [";
					$aLicencias = "var anioLicencias = [";
					foreach ($licencias as $l) {
						$dLicencias = $dLicencias.$l->dia.",";
						$mLicencias = $mLicencias.$l->mes.",";
						$aLicencias = $aLicencias.$l->anio.",";
					}
					$dLicencias = substr($dLicencias,0,-1);
					$dLicencias = $dLicencias."];\n\t";
					$mLicencias = substr($mLicencias,0,-1);
					$mLicencias = $mLicencias."];\n\t";
					$aLicencias = substr($aLicencias,0,-1);
					$aLicencias = $aLicencias."];\n\t";
					echo $dLicencias;
					echo $mLicencias;
					echo $aLicencias;
				}else {
					if(!empty($vacaciones)){
						$dLicencias = "var diasLicencias = [";
						$mLicencias = "var mesesLicencias = [";
						$aLicencias = "var anioLicencias = [";
						foreach ($vacaciones as $v) {
							$dLicencias = $dLicencias.$v->dia.",";
							$mLicencias = $mLicencias.$v->mes.",";
							$aLicencias = $aLicencias.$v->anio.",";
						}
						$dLicencias = substr($dLicencias,0,-1);
						$dLicencias = $dLicencias."];\n\t";
						$mLicencias = substr($mLicencias,0,-1);
						$mLicencias = $mLicencias."];\n\t";
						$aLicencias = substr($aLicencias,0,-1);
						$aLicencias = $aLicencias."];\n\t";
						echo $dLicencias;
						echo $mLicencias;
						echo $aLicencias;

					}
				}
			}*/
			
			?>
			if (<?php echo $con;?>) {
				return false;
			} else {
				<?php
					if(!empty($licencias) || !empty($vacaciones)){ 
						echo "\t\tvar bandera=0;\n\t\t\tvar i = 0;\n\t\t ";
						echo "while( (bandera == 0) && (i < diasLicencias.length)){\n\t\t\t\t";
						echo "if ( (diasLicencias[i] == d) && (mesesLicencias[i] == m) && (anioLicencias[i] == a) ){\n\t\t\t\t\t";
						echo "bandera =1;\n\t\t\t\t}\n\t\t\ti = i + 1;\n\t\t}\n\t";
						echo "if(bandera == 0){return true;}else{return false;}";
					}else{
						echo "return true;";
					}	
				?>
				//return true;
			}
		}

	}).datepicker("setDate","<?php echo $fechaF;?>");
		
</script>
	

</body>

</html>