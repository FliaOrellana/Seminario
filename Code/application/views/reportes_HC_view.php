<script type="text/javascript">
        $(document).ready(function(){

            $("#buscarEspecialidad").click(function(){
            	$('#error').empty();
                $.ajax({
                    url:"<?php echo base_url(); ?>index.php/reportes/CargaEspecialidadesHC",
                    type: "POST",
                    data:"pacienteDNI="+$("#pacienteDNI").val(),
                    success: function(opciones){
                    	if ($("#pacienteDNI").val() == '' || $("#pacienteDNI").val() == null){
                    		$('#error').append('Debe ingresar número de documento del paciente.');
                    	}else{
                    		if (opciones == 'null'){
                    		$('#error').append('No registra atención el DNI ingresado. Por favor ingrese un DNI correcto');
                    		}else{

	                    		$('#error').empty();
	                    		$('#especialidades').css('display','block');
	                        	$('#especialidadesid').html(opciones);
                    		}
                        }
                    	
                    	
                    }
                })
            });
        });
</script>
<script >
		$(document).ready(function(){

            $("#pacienteDNI").click(function(){
            	console.log("estoy en el change");
            	$('#error').empty();
              	$('#especialidades').css('display','none');  
            });
        });
</script>
<body class="mybodytables" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/reportes/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container"><br>
	<!-- Menu -->
	<?php $this->load->view('menu_view'); ?>

<!-- -------------------------------------------------------------------------------------- -->
        <div class="mycontainersmall" >
 			<h3 class="myh3">Generar Reportes de Historia Clínica de Paciente por Especialidad</h3><br>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                	<div class="row">
	                	<div class="col-sm-2">
	                		<div class="form-group">
	                		</div>
	                	</div>
	                	<div class="col-sm-4">	
	                		<div class="form-group">
								<input id="pacienteDNI" type="text" class="form-control " name="inputDNI" placeholder="Ingrese DNI" value="" 
								<?php
									/*if (isset($_POST['Buscar'])){
										echo "style='background-color:darkgrey;' readonly";
									}*/
								?>>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">	
								<button id="buscarEspecialidad" type="button" data-toggle="tooltip" title="Buscar Especialidades" class="btn btn-primary" onclick="" >Buscar</button>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
							</div>
	                	</div>
                	</div>


                	<div class="row" id="especialidades" style="display: none">
                		<div class="col-sm-2">
							<div class="form-group">
							</div>
	                	</div>
	                	<div class="col-sm-4">
	                		<div class="form-group">
			                    <select class="form-control" style="height:40px;" name="inputEspecialidades" id="especialidadesid" value="<?php echo $especialidadid;?>">
										<?php
												if (isset($_POST['Buscar'])){
													foreach ($especialidades as $esp) {
														if($esp->especialidadesid == $especialidadid ){
															echo'<option  value="'.$esp->especialidadesid.'" selected >'.$esp->especialidadesnombre.'</option>';
														}
														
													}
												}else{
													echo "<option value ='0' >Seleccione Especialidad</option>";
												}	
												
												?>
												</select>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">	
								<button id="" type="button" data-toggle="tooltip" title="Generar Reporte" class="btn btn-primary" onclick="pdf()" >Generar Reporte</button>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
							</div>
	                	</div>
					</div>
                </div>     
            </div>
        </div>
        <div class="row">
			<div id="error" class="error" style="color: #f76060">					 
			</div>
		</div>
       
        
    </div>
	
	<?php $this->load->view("footer");?>
 
 
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>


<script type="text/javascript">
	function Calculaedad(edad) {
		arr_edad = edad.split('/');
		dia_nac = parseInt(arr_edad[0])
		mes_nac = parseInt(arr_edad[1]);
		year_nac = parseInt(arr_edad[2]);
		fecha = new Date();
		dia = fecha.getDate();
		mes = fecha.getMonth() + 1;
		year = fecha.getFullYear();
		edad = year - year_nac;
		if( mes < mes_nac){
			edad = edad -1;
		}else{
			if ( (mes == mes_nac) && (dia <= dia_nac)){
				edad = edad -1;
			}
		}
	return edad;		
	}

	function pdf() {

		$('#error').empty();
		var opcion = $('#especialidadesid').val();

		if (opcion == 0){
			$('#error').append('Debe seleccionar al menos una operación a realizar.');
		}else{
			$('#error').empty();

			$.ajax({
						url:"<?php echo base_url(); ?>index.php/reportes/getHC",
						type:'POST',
						dataType:'json',
						data:{ 

							EspId:$('#especialidadesid').val(),
							DNI:$('#pacienteDNI').val()
					}
				}).done(function(respuesta){
					console.log(respuesta);
					console.log(Calculaedad(respuesta[0][0].per_fechanacimiento));
					edad = Calculaedad(respuesta[0][0].per_fechanacimiento);
					// onlyPrint: Boolean
					// DE: http://stackoverflow.com/questions/2255291/print-the-contents-of-a-div
					var title = "Reporte de Historia Clínica";
					var mywindow = window.open('', 'my div', 'height=400px,width=600px');
					var fecha = new Date();
					var tabla = '';
					var hora = fecha.getHours();
					var minutos = fecha.getMinutes();

					if (minutos<10){
						minutos = '0' + String(minutos);
					} else{
						minutos = String(minutos);
					}

					hora = String(hora);

					
					fecha = fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();

					
    				mywindow.document.write('<head> <title> ' + title + '</title> <meta charset="UTF-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta name="viewport" content="width=device-width, initial-scale=1"> <script type="text/javascript" src="http://localhost/Seminario/Code/jquery/ajax-jquery-2.0.0.js">');

    				mywindow.document.write('<'+'/script> <script type="text/javascript" src="http://localhost/Seminario/Code/jquery/ajax-jquery-1.10.2.js"> '+'<'+'/script> <script src="http://localhost/Seminario/Code/push.js-master/bin/push.min.js">'+'<'+'/script> <link href="http://localhost/Seminario/Code/css/jquery-ui.css" rel="Stylesheet"/></link> <link href="http://localhost/Seminario/Code//bootstrap/css/bootstrap.min.css" rel="stylesheet"> <link rel="stylesheet" href="http://localhost/Seminario/Code//font-awesome/css/font-awesome.min.css"> <!--iconos --> <link rel="icon" href="http://localhost/Seminario/Code/css/imagenes/favicon.ico" type="image/gif"> <link rel="stylesheet" href="http://localhost/Seminario/Code//bootstrap/css/custom.css"> <link href="http://localhost/Seminario/Code/bootstrap/css/bootstrap.min.css" rel="stylesheet"> <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/font-awesome/css/font-awesome.min.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/fontawesome-free-5.2.0/css/all.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/DataTables/media/css/dataTables.bootstrap.min.css"><link rel="stylesheet" href="http://localhost/Seminario/Code/bootstrap/css/custom.css"></head>');

    				mywindow.document.write('<script type="text/javascript">');
    				mywindow.document.write('$(document).ready(function(){$("#pacienteEdad").val(CalcularEdad());});<'+'/script>');

    				

    				mywindow.document.write('<body class="mybodytables" style="background-color:white;" > <div class="container"><div class="mylogout"><img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle></div></div><div class="container"><br><div class="mycontainersmall" ><h3 class="myh3">Reporte Historia Clínica de un Paciente por Especialidad</h3><div style="display:flex;"><label>Fecha de Reporte:</label> <p>'+fecha+' '+hora+':'+minutos+' hs.</p></div>');

    				//fecha alta de historia clinica
    				mywindow.document.write('<div class="row"><div class="col-sm-3"></div><div class="col-sm-3"></div><div class="col-sm-3"></div><div class="col-sm-3"><div  class="form-group" align="left"><label>Fecha Alta</label><input id="hcfechaalta" class="form-control lectura" name="hcFechaAlta"  value="'+respuesta[0][0].hc_fechaalta+'" readonly disabled></div></div></div>');

    				//id hc -- nombrepaciente --
    				mywindow.document.write('<div class="row"><div class="col-sm-2"><div  class="form-group" align="left"><label >Id HC</label><input id="hcId" type="text" class="form-control lectura" name="idHC"  value="'+respuesta[0][0].hc_id+'" readonly disabled></div></div><div class="col-sm-4"><div  class="form-group" align="left"><label >Apellido y Nombre</label><input id="pacientenombre" type="text" class="form-control lectura" name="pacientenombre"  value="'+respuesta[0][0].per_nombre+'" disabled ></div></div><div class="col-sm-3"><div  class="form-group" align="left"><label >DNI</label><input id="pacienteDNI" type="text" class="form-control lectura" name="pacienteDNI"  value="'+ respuesta[0][0].per_documento+'" readonly disabled></div></div><div class="col-sm-3"><div  class="form-group" align="left"><label >Sexo</label><input id="pacienteSexo" type="text" class="form-control lectura" name="pacienteSexo"  value="'+respuesta[0][0].s_nombre +'" readonly disabled></div></div></div>');

    				

    				mywindow.document.write('<div class="row"><div class="col-sm-3"><div  class="form-group" align="left"><label >Fecha de Nacimiento</label><input id="pacientefechanac"  class="form-control lectura" name="pacienteFechaNac"  value="'+respuesta[0][0].per_fechanacimiento+'" readonly disabled></div></div><div class="col-sm-2"><div  class="form-group" align="left"><label >Edad</label><input id="pacienteEdad" type="text" class="form-control lectura" name="inputEdad"  value="'+edad+'" readonly disabled></div></div><div class="col-sm-7"><div  class="form-group" align="left"><label >Teléfonos</label><input id="pacienteTelefonos" type="text" class="form-control lectura" name="inputTelefono"  value="'+respuesta[1][0].telefonos+'" readonly disabled></div></div></div>');

    				mywindow.document.write('<div class="row"><div class="col-sm-12"><div  class="form-group" align="left"><label >Procedencia</label><input id="pacienteProcedencia" type="text" class="form-control lectura" name="inputProcedencia"  value="'+respuesta[0][0].hc_procedencia +'" readonly disabled ></div></div></div>');

    				

    				mywindow.document.write('<div class="row"><div class="col-sm-5"><div  class="form-group" align="left"><label >Obra Social</label><input id="pacienteObraSocial" type="text" class="form-control lectura" name="inputObraSocial" value="'+respuesta[2][0].obrasocialnombre+'"  readonly disabled></div></div><div class="col-sm-2"><div  class="form-group" align="left"><label >Número</label><input id="pacienteNroOS" type="text" class="form-control lectura" name="inputNroOS" value="'+respuesta[0][0].hc_numeroos +'" readonly disabled></div></div><div class="col-sm-5"><div  class="form-group" align="left"><label >Titular</label><input id="pacienteTitularOS" type="text" class="form-control lectura" name="inputTitularOS" value="'+respuesta[0][0].hc_titularos +'" readonly disabled></div></div></div>');

    				mywindow.document.write('<div class="row"><div class="col-sm-6"><div  class="form-group" align="left"><label >Ocupación</label><input id="pacienteOcupacion" type="text" class="form-control lectura" name="inputOcupacion" value="'+respuesta[0][0].hc_ocupacion +'" readonly disabled></div></div><div class="col-sm-6"><div  class="form-group" align="left"><label >Derivado</label><input id="pacienteDerivado" type="text" class="form-control lectura" name="inputDerivado" value="'+respuesta[0][0].hc_derivado +'" readonly disabled></div></div></div>');

    				if ( edad < 18 ){

						mywindow.document.write('<div id="oculto"><div class="row"><div class="col-sm-6"><div  class="form-group" align="left"><label >Nombre de la Madre</label><input id="pacienteMadre" type="text" class="form-control lectura " name="inputMadre" value="'+respuesta[0][0].hc_madrepaciente +'" readonly disabled></div></div><div class="col-sm-6"><div  class="form-group" align="left"><label >Ocupación de la Madre</label><input id="pacienteOcupacionM" type="text" class="form-control lectura" name="inputOcuapcionM" value="'+respuesta[0][0].hc_ocupacionmadre +'" readonly disabled></div></div></div>');

	    				mywindow.document.write('<div class="row"><div class="col-sm-6"><div  class="form-group" align="left"><label >Nombre del Padre</label><input id="pacientePadre" type="text" class="form-control lectura " name="inputPadre" value="'+respuesta[0][0].hc_padre +'" readonly disabled></div></div><div class="col-sm-6"><div  class="form-group" align="left"><label >Ocupación del Padre</label><input id="pacienteOcupacionP" type="text" class="form-control lectura " name="inputOcupacionP" value="'+respuesta[0][0].hc_ocupacionpadre +'" readonly disabled></div></div></div>');
    				}

    				mywindow.document.write('<table class="table"><thead><tr style="border: #ffffff 3px solid !important;"><td><b>Profesional</b></td><td><b>Especialidad</b></td><td><b>Fecha</b></td><td><b>Hora</b></td><td><b>Diagnóstico</b></td><td><b>Observaciones</b></td></tr></thead><tbody>');

    				for (var i = 0; respuesta[0].length ; i++){
    					mywindow.document.write('<tr style="border: #ffffff 3px solid !important;"><td>'+respuesta[0][i].per_nombreprofesional+'</td><td>'+respuesta[0][i].dhc_especialidadnombre+'</td><td>'+respuesta[0][i].dhc_fecha+'</td><td>'+respuesta[0][i].dhc_hora+'</td><td>'+respuesta[0][i].dhc_diagnostico+'</td><td>'+respuesta[0][i].dhc_observaciones+'</td></tr>');
    				}

    				mywindow.document.write('</tbody></table>');

    				mywindow.document.write('</div>');// del row principal

    				

    				

    				mywindow.document.write('</body></html>');
					

			  //     	setTimeout(function () {
					// 	mywindow.print();
					// 	mywindow.close();
					// }, 5000);

				});
		} 

		
			};

</script>



</body>

</html>