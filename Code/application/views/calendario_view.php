<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href="<?php echo base_url(); ?>fullcalendar-3.4.0/fullcalendar.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>fullcalendar-3.4.0/fullcalendar.print.css" rel="stylesheet" media="print" />
	<script src="<?php echo base_url(); ?>fullcalendar-3.4.0/lib/moment.min.js" rel="stylesheet"></script> 
	<script src="<?php echo base_url(); ?>fullcalendar-3.4.0/lib/jquery.min.js" rel="stylesheet"></script> 
	<script src="<?php echo base_url(); ?>fullcalendar-3.4.0/fullcalendar.min.js" rel="stylesheet"></script> 

	<script>
		$(document).ready(function(){
			$('#calendar').fullcalendar({
				header:{
					left:'prev,next,today',
					center: 'title',
					rigth:'month, basicWeek,basicDay'
				},

				defaultDate: today(),
				navLinks: true,
				editable: true,
				eventLimit: true,
				events:[
					{
						title:'All Day Event',
						start: '2017-07-01',
					},

					{
						title:'Long Event',
						start: '2017-07-07',
						end: '2017-07-10'	
					},

					{
						id: 999,
						title:'Repeating Event',
						start: '2017-07-09T16:00:00',	
					},

					{
						id: 999,
						title:'Repeating Event',
						start: '2017-07-09T16:00:00',	
					},

					{
						title:'Conference',
						start: '2017-07-11',
						end: '2017-07-13'	
					},

					{
						title:'Meeting',
						start: '2017-07-12T10:30:00',
						end: '2017-07-12T12:30:00'	
					},

					{
						tittle:'Lunch',
						star: '2017-07-12T12:00:00'
					},

					{
						tittle:'Meeting',
						star: '2017-07-12T14:30:00'	
					},

					{
						tittle:'Happy Hour',
						star: '2017-07-12T17:30:00'	
					},

					{
						tittle:'Dinner',
						star: '2017-07-12T20:00:00'	
					},

					{
						tittle:'Birthday Party',
						star: '2017-07-13T07:00:00'	
					},

					{
						tittle:'Click for google',
						url: 'http://google.com',
						star: '2017-07-28'	
					}

				]

			});
		}

			);
	</script>
	<title> Calendario</title>

	<style>
		body{
			margin: 40px 10px;
			padding: 0px;
			font-family: "Lucida Grande", Helvetica,Arial,Verdana,sans-serif;
			font-size: 14px;
		}

		#calendar {
			max-width: 900px;
			margin: 0 auto;
		}		
	</style>
</head>
<body>

</body>
</html>