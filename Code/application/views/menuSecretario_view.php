<?php
$active_pacientes = "";
$active_turnos = "";
$active_agendas = "";
$active_profesionales = "";
$active_usuarios = "";
$active_historiaclinica = "";
$active_configuracion = "";
$active_reportes = "";


if(current_url() == site_url()."/pacientes" || current_url() == site_url()."/pacientes/registrarPaciente" || strpos(current_url(),'/RegistrarTelefono') !== False || strpos(current_url(),'/RegistrarObraS') !== False || strpos(current_url(),'/modificarPaciente') !== False ){
		$active_pacientes = $active_pacientes."active";		
	}
	if(current_url() == site_url()."/turnos" || current_url() == site_url()."/turnos/registrarTurnos"){
		$active_turnos = $active_turnos."active";
	}
	if(current_url() == site_url()."/agenda" || current_url() == site_url()."/agenda/turnoNormales" || strpos(current_url(),'/modificarHorasLab') !== False){
		$active_agendas = $active_agendas."active";
	}
	if(current_url() == site_url()."/historiaclinica" || current_url() == site_url()."/historiaclinica/registrarHC" || strpos(current_url(),'/detalleHC') !== False || current_url() == site_url()."/historiaclinica/CargaTablaDHC"  || strpos(current_url(),'/modificarHC') !== False){
		$active_historiaclinica = $active_historiaclinica."active";
	}
	if(current_url() == site_url()."/configuracion" || current_url() == site_url()."/configuracion/registrarTipoTelefono" || current_url() == site_url()."/configuracion/intervalo" || current_url() == site_url()."/configuracion/registrarIntervalo" || current_url() == site_url()."/configuracion/sexo" || current_url() == site_url()."/configuracion/registrarSexo" || strpos(current_url(),'/modificarTipoTelefonos') !== False || strpos(current_url(),'/modificarIntervalo') !== False || current_url() == site_url()."/configuracion/obrasocial" || current_url() == site_url()."/configuracion/registrarObraSocial" || strpos(current_url(),'modificarObraSocial') !== False){
		$active_configuracion =$active_configuracion."active";
	}
	if(current_url() == site_url()."/profesionales" || current_url() == site_url()."/profesionales/registrarProfesional" || strpos(current_url(),'/modificarProfesional') !== False ){
		$active_profesionales =$active_profesionales."active";
	}
	
	if(current_url() == site_url()."/usuarios" || current_url() == site_url()."/usuarios/registrarUsuario" ||current_url() == site_url()."/usuarios/asignarPerfil" ||current_url() == site_url()."/usuarios/CambiarPass"){
        $active_usuarios=$active_usuarios."active";
    }
	if(current_url() == site_url()."/secretario" || current_url() == site_url()."/secretario/registrarSecretario"){
        $active_usuarios=$active_usuarios."active";
    }
    		
    if(current_url() == site_url()."/reportes" || current_url() == site_url()."/reportes/turnosDiarios" || current_url() == site_url()."/reportes/reporteEstadistico" || current_url() == site_url()."/reportes/reportesHC" || current_url() == site_url()."/reportes/reportesPacientesxOS" ||  current_url() == site_url()."/reportes/reportesPacientesAusentes" ||  current_url() == site_url()."/reportes/reportesTurnosCancelados"){
		$active_reportes = $active_reportes."active";
	}   
?>
<header>
			<nav class="navbar   navbar-inverse navbar-static-top" role="navigation"> <!-- navbar-fixed-top para cuando haga scroll el menu continue en pantalla-->
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
							<span sr-only> Menu</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="#" class="navbar-brand">Salutia</a>
					</div>
					<div class="collapse navbar-collapse" id="navbar-1">
						<ul class="nav navbar-nav">
							<li class="dropdown <?php echo $active_pacientes?>">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
									Pacientes<span class="caret"></span>
								</a>
								<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownmenu1">
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/pacientes">Mostrar Pacientes</a></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/pacientes/registrarPaciente">Registrar Paciente</a></li>
								</ul>
							</li>
							<li class="dropdown <?php echo $active_turnos?>">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Turnos<span class="caret"></span>
								</a>
								<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownmenu1">
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/turnos">Mostrar Turnos</a></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/turnos/registrarTurnos">Registrar Turno</a></li>
								</ul>
							</li>
							
							<li class="dropdown <?php echo $active_historiaclinica?>">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
									Historia Clínica <span class="caret"></span>
								</a>
								<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownmenu1">
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/historiaclinica">Mostrar HC</a></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/historiaclinica/registrarHC">Registrar HC</a></li>
								</ul>
							</li>

							<li class="dropdown  <?php echo $active_reportes?>">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
									Reportes <span class="caret"></span>
								</a>
								<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownmenu1">
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/reportes">Generar Reportes de Pacientes</a></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/reportes/reportesPacientesAusentes">Generar Reporte de Ausencias de Pacietnes</a></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/reportes/turnosDiarios">Generar Reportes de Turnos Diarios</a></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/reportes/reporteEstadistico">Generar Reporte Estadistico</a></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/reportes/reportesHC">Generar Reporte de Historias Clínicas</a></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/reportes/reportesPacientesxOS">Generar Reporte de Pacientes por Obras Sociales</a></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>index.php/reportes/reportesTurnosCancelados">Generar Reporte de Turnos Cancelados</a></li>

									
								</ul>
							</li>
						</ul>

						<ul class="nav navbar-nav navbar-right">
							

							

							
						</ul>


					</div>
					
				</div>
			</nav>
</header>