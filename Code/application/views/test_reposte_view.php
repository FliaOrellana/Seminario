<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte Estadistico de Pacientes</title>
    <script type="text/javascript" src="http://localhost/Seminario/Code/jquery/ajax-jquery-1.10.2.js"></script>
    <link href="http://localhost/Seminario/Code/css/jquery-ui.css" rel="Stylesheet">
    <link href="http://localhost/Seminario/Code//bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="http://localhost/Seminario/Code/css/imagenes/favicon.ico" type="image/gif">
    <link rel="stylesheet" href="http://localhost/Seminario/Code//bootstrap/css/custom.css">
    <link href="http://localhost/Seminario/Code/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://localhost/Seminario/Code/Chart/css/Chart.css">
</head>
<body class="mybodytables" style="background-color:white;">
    <div class="container">
        <div class="mylogout">
            <img src="http://localhost/Seminario/Code/css/imagenes/salutiaLogo.png" width="100%" height="80" align="middle">
        </div>
    </div>
    <div class="container">
        <br>
        <div class="mycontainersmall">
            <h3 class="myh3">Reporte Estadistico de Pacientes</h3>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <canvas id="myChart1" width="400" height="400"></canvas>
            </div>
            <div class="col-sm-6">
                <canvas id="myChart2" width="400" height="400"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <canvas id="myChart3" width="400" height="400"></canvas>
            </div>
        </div>
    </div>
    <script src="http://localhost/Seminario/Code/jquery/jquery-1.11.1.js"></script>
    <script src="http://localhost/Seminario/Code/jquery/jquery-3.2.1.min.js"></script>
    <script src="http://localhost/Seminario/Code/Chart/js/Chart.js"></script>
    <script src="http://localhost/Seminario/Code/bootstrap/js/bootstrap.min.js"></script>
    <script>
    var chartData = {
			labels:["Atendidos","Cancelados","Ausentes"],
			datasets: [{
				label:'Pacientes',
				data:[0,0,0],
				backgroundColor:['rgb(95, 245, 35)','rgb(236, 150, 150)','rgb(133, 131, 131)']
			}]
   			};
    var opt = {
                scales:{
                    yAxes:[{
                        ticks:{beginAtZero:true}
                    }]
                },
                events:false,
                tooltips:{enabled:false},
                hover:{animationDuration: 0},
                animation:{
                    duration: 1,
                    onComplete: function () {
                        var chartInstance = this.chart;
                        ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                    }
                    };
    var ctx= document.getElementById("myChart1").getContext("2d");
    myLineChart = new Chart(ctx, {
            type:'bar',
            data: chartData,
            options: opt
	    });
</script>
<script>
var chartData = {
			labels:["Atendidos","Cancelados","Ausentes"],
			datasets: [{
				label:'Pacientes',
				data:[2,2,1],
				backgroundColor:['rgb(95, 245, 35)','rgb(236, 150, 150)','rgb(133, 131, 131)']
			}]
   			};
    var opt = {
                scales:{
                    yAxes:[{
                        ticks:{beginAtZero:true,
                        max: 3}
                    }]
                },
                legend: {
                
                    display:false

                    /*generateLabels: function(chart) {
                    return [{
                        text: 'AQUI DOS LINEAS'
                    }]
                    }
                },
                onClick: function(e, legendItem) {
                    var ci = this.chart;
                    ci.data.datasets.forEach(function(e, i) {
                    var meta = ci.getDatasetMeta(i);
                    if (meta.hidden == false) {
                        meta.hidden = true;
                    } else {
                        meta.hidden = false;
                    }
                    })
                    ci.update();
                    }*/
                },
                events:false,
                tooltips:{
                    yAlign: 'bottom',
                    enabled:false},
                hover:{animationDuration: 0},
                animation:{
                    duration: 1,
                    onComplete: function () {
                        var chartInstance = this.chart;
                        ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                    }
                    };
    var ctx= document.getElementById("myChart2").getContext("2d");
    myLineChart = new Chart(ctx, {
            type:'bar',
            data: chartData,
            options: opt
	    });
</script>
<script>
var chartData = {
			labels:["Atendidos","Cancelados","Ausentes"],
			datasets: [{
				label:'Pacientes',
				data:[0,0,0],
				backgroundColor:['rgb(95, 245, 35)','rgb(236, 150, 150)','rgb(133, 131, 131)']
			}]
   			};
var opt = {
			scales:{
				yAxes:[{
					 ticks:{beginAtZero:true}
				}]
			},
			events:false,
			tooltips:{enabled:false},
			hover:{animationDuration: 0},
			animation:{
				duration: 1,
				onComplete: function () {
					var chartInstance = this.chart;
					ctx = chartInstance.ctx;
					ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
					ctx.textAlign = 'center';
					ctx.textBaseline = 'bottom';
					this.data.datasets.forEach(function (dataset, i) {
						var meta = chartInstance.controller.getDatasetMeta(i);
						meta.data.forEach(function (bar, index) {
							var data = dataset.data[index];
							ctx.fillText(data, bar._model.x, bar._model.y - 5);
						});
					});
				}
				}
				};
var ctx= document.getElementById("myChart3").getContext("2d");
myLineChart = new Chart(ctx, {
		type:'bar',
		data: chartData,
		options: opt
	});</script>
    </body>
</html>