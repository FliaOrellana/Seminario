<?php $this->load->view('head_view');
?>



	
<style>

		html, body {
		    width: 100%;
		    margin: 0;
		    padding: 0;
		    box-sizing: border-box;
		}
		#table{
		    width: 100% !important;
		    margin: 0 auto !important;
		}
		.caja {
			width: 20px;
			height: 20px;
		}
  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		input.mybtn{
            height: 45px;
            background: #1AAE88;
            border:0;
            font-size: 20px;
            color:#fff;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius:4px;
			text-align: center;
		}

		input.mybtn:hover, input.mybtn:active{
                   opacity:0.6;
                   color:#fff;
		}
		th {
			align-content: center;
		}
		select {
			align-content: center;
		}
		input.mybtn:focus{
       opacity:0.6;
       color:#fff;
       background: #de615e;
		}

		.linea{
			border-top: 4px solid #a8a0a0;
		}

		
		.caja {
			width: 20px;
			height: 20px;
		}
		.lectura{ background-color: #b0dede !important }

</style>
<script type="text/javascript">
  		$(document).ready(function(){
    		$("#franjahorariaid").change(function(){
    			if ($(this).val() == 1){
    				$("#tarde").hide();
    				$("#mañana").show();
    			}
    			if ($(this).val() == 2){
    				$("#tarde").show();
    				$("#mañana").hide();
    			}
    			if ($(this).val() == 0){
    				$("#tarde").show();
    				$("#mañana").show();
    			}
  			});
		});
</script>
<script type="text/javascript">
  		$(document).ready(function(){
    		$("#inputEspecialidades").change(function(){
    			$("#search").hiden();
  			});
		});
</script>

<body class="mybodydatepicker" > 
	
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/agenda/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	
	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>
		<?php 
		/*foreach ($dias as $d) {
			
				print_r($d);
				echo "<br>";																							
		}*/	

		 ?>
			
			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Registrar Días y Horas Laborales</h3>
								<p>Ingresar Datos </p>
								

							</div>
						</div>
						
						<div class="myform-top-right">
							<i class="far fa-address-book"></i>
						</div>
					
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
							<?php echo form_open('agenda/DiasyHorasLab'); ?>
							

							<div class="row">
								<div class="col-sm-5">
									<div  class="form-group" align="left">
													<label >Profesional</label>
													<input id="profesionales" type="text" class="form-control lectura" name="inputProfesional"  value="<?php echo $datosprof['personasnombrecompuesto'];?>" readonly>

									</div>
								</div>
								<div class="col-sm-5">
									<div  class="form-group" align="left">
												<label  id="especialidades">*Especialidades</label>
												<select class="form-control" style="height:40px;" name="inputEspecialidades" id="especialidadesid" value="">
												
												<?php
													foreach ($especialidades as $esp) {
														
															echo'<option  value="'.$esp->especialidadesid.'" selected >'.$esp->especialidadesnombre.'</option>';
													}
												?>
												</select>
												<div class="error" style="color: #f76060">
													<?php 
														/*if(isset($_POST['Registrar'])){
															echo $MensajeEspecialidad;
														} */
													?>
													
												</div>

												

									</div>
								</div>
								<div class="col-sm-2">
										<div  class="form-group" align="left">
											<label  id="boton"></label>
											<input id="Buscar" type="submit" class="mybtn" value="Buscar" name="Buscar"/>
										</div>
								</div>
								
								
							</div>
						<div id="search" <?php echo $ocultar;?>>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group" align="left">
													<label  id="dias">*Días</label>
													<select class="form-control" style="height:40px;" name="inputDias" id="diasid" value="">
														<?php
														
																foreach ($dias as $d) {
																	
																		echo'<option  value="'.$d->diasid.'" selected >'.$d->diasnombre.'</option>';
																														
																}	
															
															?>
													</select>
															<!--<div class="error" style="color: #f76060">
																<?php 
																	/*if(isset($_POST['Registrar'])){
																		echo $MensajeEspecialidad;
																	} */
																?>
																
															</div>-->
												</div>	
											</div>
											<div class="col-sm-6">
												<div class="form-group" align="left">
													<label  id="franjahoraria">*Franja Horaria</label>
													<select class="form-control" style="height:40px;" name="inputFranjaHoraria" id="franjahorariaid" value="">
														<option value="0">Turno Mañana y Tarde</option>
														<option value="1">Turno Mañana</option>
														<option value="2">Turno Tarde</option>
													</select>
															<!--<div class="error" style="color: #f76060">
																<?php/* 
																	if(isset($_POST['Registrar'])){
																		echo $MensajeEspecialidad;
																	} */
																?>
																
															</div>-->
												</div>	
											</div>
											
										</div>
										
										<div class="row">
											<div id="mañana">
												<div class="col-sm-3">
													<div class="form-group" align="left">
														<label id="mañanainicio">Turno Mañana Inicio</label>
														<input type="time" class="form-control" name="inputMañanaInicio" id="mañanainicioid" value="08:00" min="08:00" max="13:00" >
													</div>	
												</div>
												<div class="col-sm-3">
													<div class="form-group" align="left">
														<label id="mañanafin">Turno Mañana Fin</label>
														<input type="time" class="form-control" name="inputMañanaFin" id="mañanafinid" value="08:00" min="08:00" max="13:00" >
													</div>	
												</div>	
											</div>
											<div id="tarde">
												<div class="col-sm-3">
													<div class="form-group" align="left">
														<label id="tardeinicio">Turno Tarde Inicio</label>
														<input type="time" class="form-control" name="inputTardeInicio" id="tardeinicioid" value="13:00" min="13:00" max="21:00" >
													</div>	
												</div>
												<div class="col-sm-3">
													<div class="form-group" align="left">
														<label id="tardefin">Turno Tarde Fin</label>
														<input type="time" class="form-control" name="inputTardeFin" id="tardefinid" value="13:00" min="13:00" max="21:00" >
													</div>	
												</div>
											</div>
										</div>
										<div class="row">
												<div class="error" style="color: #f76060">
													<?php 
														if(isset($_POST['Registrar'])){
															echo $MensajeTurnoMañana;
														} 
													?>				
												</div>
										</div>
										
										<div class="row">
												<div class="error" style="color: #f76060">
													<?php 
														if(isset($_POST['Registrar'])){
															echo $MensajeTurnoTarde;
														} 
													?>				
												</div>
										</div>
										

										<div class="row">
											<div class="col-sm-3">
											</div>
											<div class="col-sm-3">
											</div>
											<div class="col-sm-3">
													<div  class="form-group" align="left">
														<label > </label><br>
														<button type="submit" class="btn btn-primary submitBtn" style="padding:12px 60px"value="Registrar" name="Registrar">Registrar</button>
													</div>
											</div>
											<div class="col-sm-3">
											</div>
										</div>
										
									<div class="row">
										<div class="col-sm-12">
											<div class="table-responsive">
												<table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2"  style="font-size:small">
										            <thead class="mytable">
										                <tr>
										                    <th><center>Dias</center></th>
										                    <th><center>Turno Mañana Inicio</center></th>
										                    <th><center>Turno Mañana Fin</center></th>
										                    <th><center>Turno Tarde Inicio</center></th>
										                    <th><center>Turno tarde Fin</center></th>
										                    <th><center>Acciones</center></th>
										                    
										                </tr>

										            </thead>
										            <tbody class="mytbody" style="color:black;background:white;">
										            	<?php
																$i = 0;
																foreach ($agenda as $j) {
																	if ($j->estadosid == 1){
																	echo "<tr>
																		  	<td>".$j->diasnombre."</td>
																			<td>".$j->agendastmini."</td>
																			<td>".$j->agendastmfin."</td>
																			<td>".$j->agendasttini."</td>
																			<td>".$j->agendasttfin."</td>";
																			echo '<td><button type="button" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/agenda/modificarHorasLab/".$j->agendasid."'".'"><i class="fas fa-pencil-alt"></i></button>
																				<button id="mostrar'.$i.'" type="button" class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal" data-target="#miModal'.$i.'" value="'.$j->agendasid.'"><i class="fas fa-times-circle" ></i></button>
																				</td>
																		  </tr>';
																			echo '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																				<div class="modal-dialog modal-lg" role="document">
																					<div class="modal-content">
																						<div class="modal-header" >
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">&times;</span>
																							</button>
																							
																						</div>
																						<div class="modal-body">
																							<label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
																						</div>
																						<div class="row">		
																							<div class="col-sm-3">
																								
																							</div>
																							<div class="col-sm-3">
																								<button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/agenda/cancelarDiaLab/".$j->agendasid."'".'">Si</button>
																							</div>
																							<div class="col-sm-3">
																							<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
																								
																							</div>
																							
																							<div class="col-sm-3">
																								
																							</div>
																						</div>
																						<br><br>
																										
																					</div>
																				</div>
																			</div>';
																	}
																	$i = $i + 1;
																}
															
														?>
										            </tbody>
										        </table>
									        </div>
										</div>
									</div>	
									
									<br>
											<div class="error" style="color: #f76060">
											<?php 
																if(isset($_POST['Registrar'])){
																	echo $mensajeinsert;
																} 
											?>
											</div>
									
										<div class="row">		
											<div class="col-sm-3">
												
											</div>
											<div class="col-sm-3">
												<button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/agenda'">Finalizar</button>
											</div>
											<div class="col-sm-3">
												<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/agenda'">Cancelar</button>
											</div>
											<div class="col-sm-3">
												
											</div>				

										</div>
						</div>								
						<?php echo form_close(); ?>	
						
					</div>
				</div>
			</div>
    	
		
        
        

    </div>
	
	<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-dropdown-hover/js/bootstrap-dropdownhover.min.js"></script>
	
    <script src="<?php echo base_url(); ?>DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>DataTables/media/js/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
	


</body>

</html>