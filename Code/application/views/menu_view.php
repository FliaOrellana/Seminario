<?php 
        $listPer = $this->session->userdata('perfil');
        if (count($listPer)>1) {
            $perfil = 'ADMINISTRADOR';
        }else{
            $perfil = strtoupper($listPer[0]);
        }
        if ($perfil == 'ADMINISTRADOR') {
            $this->load->view('menuAdministrador_view');
        }elseif ($perfil == 'SECRETARIO') {
            $this->load->view('menuSecretario_view');
        }else{
            $this->load->view('menuProfesional_view');
        }
        
?>