<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>lquery-ui/jquery-ui.css">
<script src="<?php echo base_url(); ?>jquery-ui/external/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>jquery-ui/jquery-ui.js"></script>
<script>
$(function() {
$( "#datepicker" ).datepicker();
});
</script>

<style>
/*div principal del datepicker*/
.ui-datepicker
{
width: auto;
background: black;
}

/*Tabla con los días del mes*/
.ui-datepicker table
{
font-size: auto;
}

/*La cabecera*/
.ui-datepicker .ui-datepicker-header
{
font-size: auto;
background: #FFFFFF;
}

/*Para los días de la semana: Sa Mo ... */
.ui-datepicker th
{
color: #FFFFFF;
}

/*Para items con los días del mes por defecto */
.ui-datepicker .ui-state-default
{
background: #FFFFFF;
}

/*Para el item del día del mes seleccionado */
.ui-datepicker .ui-state-active
{
background: blue;
color: #FFFFFF;
}
</style>

</head>
<body>
<p>Fecha: <input type="text" id="datepicker"></p>
</body>
</html>