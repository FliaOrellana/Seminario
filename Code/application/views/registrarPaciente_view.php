<?php $this->load->view('head_view');

?>
<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">  
<style type="text/css">
  		.mybodydatepicker{
		font-family:'Convergence', sans-serif;
 		font-size:20px;
 		font-weight: 300;
 		color: #000000;
 		line-height: 30px;
 		text-align: center;
 		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}
</style>
<body class=" mybodydatepicker" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/pacientes/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Registrar Nuevo Paciente</h3>
								<p>Ingresar Datos </p>
								

							</div>
						</div>
						
						<div class="myform-top-right">
							
								<i class="fa fa-user"></i>
							
						</div>
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
						<?php echo form_open('pacientes/registrarPaciente'); ?>

							<div class="row">
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											
											<label for="inputApellido">*Apellido</label>
											
											<?php echo form_input(array('id' => 'pacienteApellido',
																		'name' => 'pacienteApellido',
																		'placeholder' => 'Ingrese Apellido',
																		'value' => $apellido,
																		'class' => 'form-control')); ?>
											<?php echo form_error('pacienteApellido');?><br />
											<!--<input id="pacienteApellido" type="text" class="form-control" name="inputApellido" placeholder="Ingrese Apellido" required="true">-->
										</div>
								</div>
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											<label for="inputNombre">*Nombre</label>
											
											<?php echo form_input(array('id' => 'pacienteNombre',
																		'name' => 'pacienteNombre',
																		'placeholder' => 'Ingrese Nombre',
																		'value' => $nombre,
																		'class' => 'form-control')); ?>
											<?php echo form_error('pacienteNombre');?><br />
											<!--<input id"pacienteNombre" type="text" class="form-control" name="inputNombre" placeholder="Ingrese Nombre" required="true">-->
										</div>
								</div>
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											<label for="inputDNI">*DNI</label>
											
											<?php echo form_input(array('id' => 'pacienteDNI',
																		'name' => 'pacienteDNI',
																		'placeholder' => 'Ingrese DNI',
																		'value' => $dni,
																		'class' => 'form-control')); ?>
											<?php echo form_error('pacienteDNI');?><br />
											<!--<input id="pacienteDNI" type="text" class="form-control" name="inputDNI" placeholder="Ingrese DNI" required="true">-->
										</div>
								</div>
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											<label for="pacienteSexo" >*Sexo</label>
											
											<select class="form-control"  name="pacienteSexo" style="height:40px;" id="pacienteSexo">
											<?php
												if (isset($_POST['Registrar'])) {
													foreach ($sexo as $sex){
														if ($sex->sexoid == $sexoid) {
															echo'<option  value="'.$sex->sexoid.'" selected >'.$sex->sexonombre.'</option>';
														} else {
															echo'<option  value="'.$sex->sexoid.'"  >'.$sex->sexonombre.'</option>';
														}
														
													}
												} else {
													foreach ($sexo as $sex) {
														echo'<option  value="'.$sex->sexoid.'">'.$sex->sexonombre.'</option>';
													}
												}
												
													
												?>
											</select>
											
										</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div  class="form-group" align="left">
										<label  id="fecha">*Fecha Nacimiento</label>
										

										<!------------------------------------------- -->

											<div class='input-group input-append date' id='datetimepicker1'>
												<input type='text' class="form-control" id="fechaN" name="fechaN"/>
												<span class="input-group-addon add-on">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
                							</div>

										<!------------------------------------------- -->

									</div>	
								</div>
								<div class="col-sm-6">
										<div  class="form-group" align="left">
											<label for="inputDomicilio">*Domicilio</label>
											
											<?php echo form_input(array('id' => 'pacienteDomicilio',
																		'name' => 'pacienteDomicilio',
																		'placeholder' => 'Ingrese Domicilio',
																		'value' => $domicilio,
																		'class' => 'form-control')); ?>
											<?php echo form_error('pacienteDomicilio');?><br />
											<!--<input id="pacienteDomicilio" type="text" class="form-control" name="inputDomicilio" placeholder="Ingrese Domicilio" >-->
										</div>
								</div>

								
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											<label for="inputEmail">Email</label>
											
											<?php echo form_input(array('id' => 'pacienteEmail',
																		'name' => 'pacienteEmail',
																		'placeholder' => 'Ingrese Email',
																		'class' => 'form-control')); ?>
											<?php echo form_error('pacienteEmail');?><br />
											<!--<input id="pacienteEmail" type="text" class="form-control" name="inputEmail" placeholder="Ingrese Email">-->
										</div>
								</div>		
							</div>
	

							<div class="row">		
								<div class="col-sm-3">
								</div>
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary submitBtn" style="padding:15px 50px"value="Registrar" name="Registrar">Registrar</button>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/pacientes'">Cancelar</button>
								</div>
								<div class="col-sm-3">
								</div>				

							</div>			
								
						<?php echo form_close(); ?>							
						
						
					</div>
				</div>
			</div>
    	
		
        
        

    </div>

	<?php $this->load->view("footer");?>
    
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
	<script type="text/javascript">
		$(function () {

		$("#datetimepicker1").datepicker({
				language: 'es',
				autoclose: true,
				endDate: new Date(),
				format:'dd/mm/yyyy',
				firstDay:1,

			}).datepicker("setDate","<?php echo $fecha;?>");
		});
	</script>



</body>

</html>