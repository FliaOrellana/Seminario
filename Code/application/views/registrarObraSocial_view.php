<?php $this->load->view('head_view');
?>

<body class="mybodytables" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/configuracion/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Registrar Nueva Obra Social</h3>
							</div>
						</div>
						
						<div class="myform-top-right">
								<i class="fas fa-users"></i>
						</div>	

					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
						<?php echo form_open('configuracion/registrarObraSocial'); ?>

							<div class="row">
								<!--<div class="col-sm-3">-->
										<div  class="form-group" align="left">
											
											<label >*Nueva Obra Social</label>
											
											<?php 
											if (isset($_POST['Registrar'])) {
												echo form_input(array('id' => 'obrasocial',
																		'name' => 'obrasocial',
																		'placeholder' => 'Ingrese Obra Social',
																		'value' => $obrasocial,
																		'type' => 'text',
																		'class' => 'form-control')); } 
												else{
													echo form_input(array('id' => 'obrasocial',
																		'name' => 'obrasocial',
																		'placeholder' => 'Ingrese Obra Social',
																		'value' => '',
																		'type' => 'text',
																		'class' => 'form-control'));
												}?>
											<?php echo form_error('obrasocial');?><br />
											<!--<input id="pacienteApellido" type="text" class="form-control" name="inputApellido" placeholder="Ingrese Apellido" required="true">-->
										</div>
								<!--</div>-->
				
							</div>
								

							<div class="row">		
								<div class="col-sm-3">
								</div>
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary submitBtn" style="padding:15px 50px"value="Registrar" name="Registrar">Registrar</button>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/configuracion/obrasocial'">Cancelar</button>
								</div>
								<div class="col-sm-3">
								</div>				

							</div>			
								
						<?php echo form_close(); ?>							
						
						
					</div>
				</div>
			</div>
    	
		
        
        

    </div>
	
	<?php $this->load->view("footer");?>
    




</body>

</html>