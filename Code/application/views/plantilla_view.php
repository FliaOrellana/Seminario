<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-2.0.0.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>push.js-master/bin/push.min.js"></script>
    <link href="<?php echo base_url();?>css/jquery-ui.css" rel="Stylesheet"/></link>


    <link href="<?php echo base_url(); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/font-awesome/css/font-awesome.min.css"> <!--iconos -->
    <link rel="icon" href="<?php echo base_url(); ?>css/imagenes/favicon.ico" type="image/gif">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/bootstrap/css/custom.css">

    <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!--Para menú desplegable automático data-hover="dropdown" en <a>-->
    <link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome-free-5.2.0/css/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>DataTables/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/custom.css">

    <!-- para Datepicker-->

    <link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
    <link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">  
    
    <!-- END PACIENTES TURNOS AGENDA PROFESIONAlES USUARIOS-->

</head>

<body class="mybodytables" style="background-color:white;" > 
	<div class="container">
		<div class="mylogout">
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>

	<div class="container"><br>
        <div class="mycontainersmall" >
 			<h3 class="myh3">Reporte Policonsultorio</h3>
        </div>
    	
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2" width="100%" style="font-size:small">
                            <thead class="mytable">
                                <tr>
                                    <th><center>Id<center></th>
                                    <th><center>Apellido</center></th>
                                    <th><center>Nombre<center></th>
                                    <th><center>DNI<center></th>
                                    <th><center>Domicilio<center></th>
                                    <th><center>Estado<center></th>
                                </tr>

                            </thead>
                            <tbody class="mytbody">
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<?php $this->load->view("footer");?>
 
    <script type="text/javascript">

    var table;
    $(document).ready(function() {
 
    //datatables
    table = $('#table').DataTable({ 

        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "paging":false,
        "searching":false,
 
        // Load data for the table's content from an Ajax source
        
        "ajax": {
            url: "<?php echo site_url('pacientes/ajax_list')?>",
            type: "POST"
            
        },
        //Set column definition initialisation properties.
        "columnDefs": [

            { className: "dt-right", "targets": [0,1,2,3,4,5] },
            { 
 
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],

        "language": idioma_espanol

 
    });
 
});

    var idioma_espanol= {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "",
            "sInfoEmpty":      "",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            } 
        }
</script>


    <script>
        $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>

</body>

</html>
 
    
