
<!----------------------------------------------------------------------------------------------- -->

<!DOCTYPE html>
<html lang="es">
<head>
	
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Historia Clínica</title>
	<script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jqueryui-1.10.2.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>css/jqueryiu-1.10.3.css"/>
	
	<script type="text/javascript" >
		$(document).ready(function(){
			$( "#pacienteDNI" ).autocomplete({
			source: "<?php echo base_url(); ?>index.php/historiaclinica/BuscarPacientes",
			minLength: 1
			});
			
			$("#pacienteDNI").focusout(function(){
				$.ajax({
						url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaPaciente",
					type:'POST',
					dataType:'json',
					data:{ pacienteDNI:$('#pacienteDNI').val()}
				}).done(function(respuesta){
					if (respuesta.respuesta == null || respuesta.respuesta.length > 1){
						$("#pacienteID").val('');
						$("#pacienteNombreCompuesto").val('');
						$("#pacienteFechaNac").val('');
						$("#pacienteEdad").val('');
						console.log(respuesta.respuesta.length);
					}else{

						console.log(respuesta.respuesta.length);
						$("#pacienteID").val(respuesta.respuesta["0"].pacientesid);
						$("#pacienteNombreCompuesto").val(respuesta.respuesta["0"].nombredocumento);
						$("#pacienteFechaNac").val(respuesta.respuesta["0"].personasfechanacimiento);
						$("#pacienteEdad").val(CalcularEdad());
						if ($("#pacienteEdad").val() < 18){
							$("#oculto").show();
						}else{
							$("#oculto").hide();
						}
					}
					
				});
				});


		});
	</script>

	<!-------------------CALCULAR EDAD----------->
	<script type="text/javascript">
		function CalcularEdad(){
			var fechaNac = document.getElementById("pacienteFechaNac").value;
			var valores = fechaNac.split("-");
			var dia = valores[2];
			var mes = valores[1];
			var anio = valores[0];
			var fecha_actual = new Date();
			var dia_actual = fecha_actual.getDate();
			var mes_actual = fecha_actual.getMonth()+1;
			var anio_actual = fecha_actual.getYear();

			var edad = (anio_actual+1900)-anio; 

			if ( mes_actual < mes ){
	            edad--;
	        }
        	if ((mes == mes_actual) && (dia_actual < dia)){
            	edad--;
        	}
        	if (edad > 1900){
            	edad -= 1900;
       	 	}
       	 	/*alert(edad);
       	 	var band;
       	 	alert("despues de var band");
       	 	band = document.getElementById("bandid").value;
       	 	alert(band);
       	 	if ( (band==1)){
       	 		document.getElementById("oculto").style.display = 'block';
       	 	}*/

       	 	return edad;
       	 	
		}

	</script>
	<!----------------FIN CALCULAR EDAD----------->
	<script type="text/javascript">
	$(document).ready(function(){
			
		console.log($('#pacienteEdad').val());
		if (($('#pacienteEdad').val() < 18) && ($('#pacienteEdad').val() != '')){
			$("#oculto").show();
		}else{
			console.log("estoy despues del else");
			$("#oculto").hide();
		}
		});


	</script>

	<!----------------CARGA TELEFONOS------------->
	<script type="text/javascript">
  		$(document).ready(function(){
    		$("#pacienteDNI").change(function(){
    			$.ajax({
      				url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaTelefonos",
      				type: "POST",
      				data:"personasnrodocumento="+$("#pacienteDNI").val()
      				}).done(function(respuesta){
      					$("#pacienteTelefonos").val(respuesta);
      				})
  			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajax({
      				url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaTelefonos",
      				type: "POST",
      				data:"personasnrodocumento="+$("#pacienteDNI").val()
      				}).done(function(respuesta){
      					$("#pacienteTelefonos").val(respuesta);
      				})
		});
	</script>
	<!----------------FIN CARGA TELEFONOS------------->


	<!----------------CARGA OBRA SOCIAL------------->
	<script type="text/javascript">
  		$(document).ready(function(){
    		$("#pacienteDNI").change(function(){
    			$.ajax({
      				url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaObrasSociales",
      				type: "POST",
      				data:"personasnrodocumento="+$("#pacienteDNI").val()
      				}).done(function(respuesta){
      					$("#pacienteObraSocial").val(respuesta);
      				})
  			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajax({
      				url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaObrasSociales",
      				type: "POST",
      				data:"personasnrodocumento="+$("#pacienteDNI").val()
      				}).done(function(respuesta){
      					$("#pacienteObraSocial").val(respuesta);
      				})
		});
	</script>
	<!----------------FIN CARGA OBRA SOCIAL------------->

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette|Montaga|Raleway|Francois+One|Convergence:100,300,400,500">




	<!-- HEAD PACIENTES TURNOS AGENDA PROFESIONALES USUARIOS-->

	
	<link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="icon" href="<?php echo base_url(); ?>css/imagenes/favicon.ico" type="image/gif">
	
	<!--Para menú desplegable automático data-hover="dropdown" en <a>-->
	<link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>bootstrap-dropdown-hover/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome-free-5.2.0/css/all.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>DataTables/media/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/custom.css">

	<!-- para Datepicker-->

	<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
  	<link href="<?php echo base_url(); ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">  
		
	<!-- END PACIENTES TURNOS AGENDA PROFESIONAlES USUARIOS-->
</head>

<style>

  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		input.mybtn{
            height: 45px;
            background: #1AAE88;
            border:0;
            font-size: 20px;
            color:#fff;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius:4px;
			text-align: center;
		}

		input.mybtn:hover, input.mybtn:active{
                   opacity:0.6;
                   color:#fff;
		}

		input.mybtn:focus{
       opacity:0.6;
       color:#fff;
       background: #de615e;
		}

		.linea{
			border-top: 4px solid #a8a0a0;
		}

		
		.caja {
			width: 20px;
			height: 20px;
		}
		.lectura{ background-color: #b0dede !important }

</style>
<!----------------------------------------------------------------------------------------------- -->


<body class="mybodydatepicker" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/historiaclinica/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Registrar Nueva Historia Clínica</h3>
								<p>Ingresar Datos </p>
								

							</div>
						</div>
						
						<div class="myform-top-right">
							
							<i class="fas fa-notes-medical"></i>
							
						</div>
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
						
						<?php echo form_open('historiaclinica/CargaHC')?>
							<div class = "row">

								<div class="col-sm-4">
								</div>

								<div class="col-sm-4">
								</div>

								<div class="col-sm-4">
									<div  class="form-group" align="left">
										<label  id="fecha" align="left">*Fecha</label>
										

										<!------------------------------------------- -->

											<div class='input-group input-append date' id='datetimepicker1'>
												<input type='text' class="form-control" id="fechaN" name="fechaN"/>
												<span class="input-group-addon add-on">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
                							</div>

										<!------------------------------------------- -->
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajeFecha;
												} 
											?>
													
											</div>
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											
											<label >*DNI del Paciente</label>
											<input id="pacienteDNI" type="text" class="form-control " name="inputDNI" placeholder="Ingrese DNI" value="<?php echo $dni;?>">
											<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajeDNI;
												} 
											?>
													
											</div>
										</div>
								</div>
								<div class="col-sm-2">
										<div  class="form-group" align="left">
											<label >*Ident.</label>
											<input id="pacienteID" type="text" class="form-control lectura" name="inputId" value="<?php echo $pacienteid;?>" required="true" readonly>
										</div>
								</div>
								<div class="col-sm-7">
										<div  class="form-group" align="left">
											<label >*Apellido y Nombre</label>
											<input id="pacienteNombreCompuesto" type="text" class="form-control lectura" name="inputNombreCompuesto" value="<?php echo $pacientenombre;?>" required="true" readonly >
										</div>
								</div>
								
							</div>
							
							<div class="row">

								<div class="col-sm-3">
									<div  class="form-group" align="left">
										<label >*Fecha de Nacimiento</label>
										<input id="pacienteFechaNac" type="date" class="form-control lectura" name="inputFechaNac" value="<?php echo $fechanac; ?>" required="true" readonly>
									</div>
								</div>

								<div class="col-sm-2">
									<div  class="form-group" align="left">
										<label >*Edad</label>
										<input id="pacienteEdad" type="text" class="form-control lectura" name="inputEdad" value="<?php echo $pacienteedad; ?>"  readonly >
									</div>
								</div>

								<div class="col-sm-7">
									<div  class="form-group" align="left">
										<label >*Telefono</label>
										<input id="pacienteTelefonos" type="text" class="form-control lectura" name="inputTelefono" value=""  readonly >

									</div>
								</div>

							</div>

							<div class="row">
								<div class="col-sm-12">
									<div  class="form-group" align="left">
										<label >* Procedencia</label>
										<input id="pacienteProcedencia" type="text" class="form-control" name="inputProcedencia" placeholder="Ingrese Procedencia del Paciente" value="<?php echo $pacienteprocedencia; ?>" >
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajeProcedencia;
												} 
											?>
										</div>
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-sm-5">
									<div  class="form-group" align="left">
										<label >*Obra Social</label>
										<input id="pacienteObraSocial" type="text" class="form-control lectura" name="inputObraSocial" value="" readonly >
										
												<!--<select class="form-control" style="height:40px;" name="inputObraSocial" id="pacienteObraSocial" >
													<option value ='0' >Obras Sociales del Paciente</option>
												</select>
												<div class="error" style="color: #f76060">
													<?php 
														/*if(isset($_POST['Registrar'])){
															echo $MensajeProfesional;
														} */
													?>	
												</div>-->
									</div>
								</div>
								<div class="col-sm-2">
									<div  class="form-group" align="left">
										<label >* Número</label>
										<input id="pacienteNroOS" type="text" class="form-control" name="inputNroOS" value="<?php echo $pacientenroos; ?>" placeholder="Número de OS" >
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajeNroOs;
												} 
											?>
										</div>
									</div>
								</div>
								<div class="col-sm-5">
									<div  class="form-group" align="left">
										<label >* Titular</label>
										<input id="pacienteTitularOS" type="text" class="form-control" name="inputTitularOS" value="<?php echo $pacientetitularos; ?>" placeholder="Ingrese Titular de la Obra Social"  >
									</div>
									<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajeTitularOs;
												} 
											?>
									</div>
								</div>
								
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Ocupación</label>
										<input id="pacienteOcupacion" type="text" class="form-control " name="inputOcupacion" value="<?php echo $pacienteocupacion; ?>" placeholder="Ingrese Ocupación del Paciente"  >
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajeOcupacion;
												} 
											?>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Derivado</label>
										<input id="pacienteDerivado" type="text" class="form-control " name="inputDerivado" value="<?php echo $pacientederivado; ?>" placeholder="Ingrese lugar de Derivación del Paciente"  >
									</div>
								</div>
							</div>
						
						<div id="oculto" hidden="true">
							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Nombre de la Madre</label>
										<input id="pacienteMadre" type="text" class="form-control " name="inputMadre" value="<?php echo $pacientemadre; ?>" placeholder="Ingrese Nombre de la Madre del Paciente"  >
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajeMadre;
												} 
											?>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Ocupación de la Madre</label>
										<input id="pacienteOcupacionM" type="text" class="form-control " name="inputOcupacionM" value="<?php echo $pacienteocupacionm; ?>" placeholder="Ingrese Ocupación de la Madre del Paciente"  >
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajeOcupacionM;
												} 
											?>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Nombre del Padre</label>
										<input id="pacientePadre" type="text" class="form-control " name="inputPadre" value="<?php echo $pacientepadre; ?>" placeholder="Ingrese Nombre del Padre del Paciente"  >
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajePadre;
												} 
											?>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label >* Ocupación del Padre</label>
										<input id="pacienteOcupacionP" type="text" class="form-control " name="inputOcupacionP" value="<?php echo $pacienteocupacionp; ?>" placeholder="Ingrese Ocupación del Padre del Paciente"  >
										<div class="error" style="color: #f76060">
											<?php 
												if(isset($_POST['Siguiente'])){
													echo $mensajeOcupacionP;
												} 
											?>
										</div>
									</div>
								</div>
							</div>
						</div>	
								<!--<div class="col-sm-2">
								<input id="profesionalesxespecialidadesid" type="hidden" class="form-control lectura" name="inputProfecionalxEspecialidad" value="<?php echo $especialidadesxprofesionalesid;?>" required="true" readonly >		
								</div>	
								<div class="col-sm-6">
									<div  class="form-group" align="left">
										<label  id="boton"></label>
										<input type="submit" class="mybtn"  value="Buscar" name="Buscar"/>
										
									</div>				
								</div>	-->

						

							
							<div class="row">		
								<div class="col-sm-3">
									<!--<input id="bandid" type="hidden" class="form-control lectura" name="bandId" value="<?php echo $band;?>" required="true" readonly >	
								</div>-->
								</div>
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary submitBtn" style="padding:15px 50px"value="Siguiente" name="Siguiente">Siguiente</button>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/historiaclinica'">Cancelar</button>
								</div>
								<div class="col-sm-3">
								</div>				

							</div>	
								
						<?php echo form_close()?>				
						
						
					</div>
				</div>
			</div>
    	
		
        
        

    </div>
	<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-dropdown-hover/js/bootstrap-dropdownhover.min.js"></script>
	
    <script src="<?php echo base_url(); ?>DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>DataTables/media/js/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
	<script type="text/javascript">
		$(function () {

		$("#datetimepicker1").datepicker({
				language: 'es',
				autoclose: true,
				format:'dd/mm/yyyy',
				firstDay:1
			}).datepicker("setDate","<?php echo $fecha;?>");
		});
	</script>
	<script type = "text/javascript">	
		$(document).ready(function () {
			$('#table').DataTable({
				"paginatype":"simple_numbers",
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
				}
			});
			
			$('.dataTables_length').addClass('bs-select');
		});
	</script>


</body>

</html>