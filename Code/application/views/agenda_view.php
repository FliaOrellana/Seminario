
<style>
  		.mybodydatepicker{
    		font-family:'Convergence', sans-serif;
     		font-size:20px;
     		font-weight: 300;
     		color: #000000;
     		line-height: 30px;
     		text-align: center;
     		background-color: #E6E6E6;/*#ffffff;*/
		}

		.mybodydatepicker .container .mylogout{
    		text-align: right;
    		font-size: small;
		}

		input.mybtn{
            height: 40px;
            background: #1AAE88;
            border:0;
            font-size: 20px;
            color:#fff;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius:4px;
			text-align: center;
		}

		input.mybtn:hover, input.mybtn:active{
                   opacity:0.6;
                   color:#fff;
		}

		input.mybtn:focus{
       opacity:0.6;
       color:#fff;
       background: #de615e;
		}

		.linea{
			border-top: 4px solid #a8a0a0;
		}

		 td.day.disabled{
		    color: #f70808 !important;
		    }

		#table2{
		  border-collapse: collapse;
		  border: #000000 3px solid;
		}

  	</style>

<body class="mybodydatepicker" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a id ="logout" href="<?php echo base_url(); ?>index.php/agenda/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container"><br>
		<?php $this->load->view('menu_view');?>
			<div class="row">
				<div class="col-lg-12  myform-cont">
					<div class="myform-top">
						<div class="myform-top-left">
							<div class="col-sm-6">
								<h3>Agenda</h3>
								<p>Identifique Especialidad</p>
								

							</div>
						</div>
						
						<div class="myform-top-right">
							
							<i class="fas fa-notes-medical"></i>
							
						</div>
					</div>
					
					<div class="myform-bottom">
						<div align="left">
							<p style="font-size:13px" align="left">*campos obligatorios</p>
						</div>
						<?php echo form_open('agenda/turnoNormales')?>
						
							<div class="row">
								<div class="col-sm-4">
										<div  class="form-group" align="left">
											<label  id="fecha">*Fecha</label>
											

											<!------------------------------------------- -->

												<div class='input-group input-append date' id='datetimepicker1'>
													<input type='text' class="form-control" id="fechaN" name="fechaN"/>
													<span class="input-group-addon add-on">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
												</div>

											<!------------------------------------------- -->

										</div>	
								</div>
								<div class="col-sm-5">
									<div  class="form-group" align="left">
												<label >*Especialidades</label>
												
												<select class="form-control" style="height:40px;" name="especialidadesid" id="especialidadesid">
														<?php
																foreach ($especialidades as $i) {
																	echo'<option value="'.$i->especialidadesid.'">'.$i->especialidadesnombre.'</option>';
																}
															?>
												</select>
									</div>
								</div>
								<div class="col-sm-3">
										<div  class="form-group" align="left">
											<label  id="boton"></label>
											<input id="Buscar" type="submit" class="mybtn" value="Buscar" name="Buscar"/>
										</div>
								</div>
								
							</div>
							<?php echo form_close()?>

							<div class="row">
								<div class="col-sm-6">
									<h3>Turnos Normales</h3>
								</div>

								<div class="col-sm-6">
									<h3>Sobre Turnos</h3>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div  class="form-group" >
										<div class="table-responsive">
											<table id="table" class="table table-striped table-bordered table-sm" cellspacing="2" width="100%" style="font-size:small">
													<thead class="mytable">
														<tr>
															<th><center>Hora Turno<center></th>
															<th><center>Paciente</center></th> 
															
														</tr>
														<tbody class="mytbody"  style="color:black;background:white;">
														<?php
														
														if (isset($_POST['Buscar'])){
															if (!empty($intervalo)) {
																foreach ($agenda as $i) {
																	if (($i->tb_turnohora.":00" >= $intervalo[0]->agendastmini) && ($i->tb_turnohora.":00" <= $intervalo[0]->agendastmfin)) {
																		echo "<tr>
																		<td>".$i->tb_turnohora."</td>
																		<td>".$i->tb_paciente."</td>
																	  	</tr>";
																	} else {
																		if (($i->tb_turnohora.":00" >= $intervalo[0]->agendasttini) && ($i->tb_turnohora.":00" <= $intervalo[0]->agendasttfin)) {
																			echo "<tr>
																			<td>".$i->tb_turnohora."</td>
																			<td>".$i->tb_paciente."</td>
																		  	</tr>";
																		}
																	}
																	
																
																	}
															} else {
																foreach ($agenda as $i) {
																echo "<tr>
																		<td>".$i->tb_turnohora."</td>
																		<td>".$i->tb_paciente."</td>
																	  </tr>";
																	}
															}
															
															
														}
														
														
					
                    
															
														?>
																
														</tbody>

													</thead>
											</table>
										</div>
			
									</div>
								</div>
								<div class="col-sm-6">
									<div  class="form-group" >
										<div class="table-responsive">
											<table id="table2" class="table table-striped table-bordered table-sm" cellspacing="2" width="100%" style="font-size:small">
													<thead class="mytable">
														<tr>
															<th><center>Hora Turno<center></th>
															<th><center>Paciente</center></th> 
															
														</tr>
														<tbody class="mytbody"  style="color:black;background:white;">
														<?php
														
														if (isset($_POST['Buscar'])){
															if (!empty($intervalo)) {
																foreach ($agenda2 as $i) {
																	if (($i->tb_turnohora.":00" >= $intervalo[0]->agendastmini) && ($i->tb_turnohora.":00" <= $intervalo[0]->agendastmfin)  ) {
																		echo "<tr>
																		<td>".$i->tb_turnohora."</td>
																		<td>".$i->tb_paciente."</td>
																	  	</tr>";
																	} else {
																		if (($i->tb_turnohora.":00" >= $intervalo[0]->agendasttini) && ($i->tb_turnohora.":00" <= $intervalo[0]->agendasttfin)) {
																			echo "<tr>
																			<td>".$i->tb_turnohora."</td>
																			<td>".$i->tb_paciente."</td>
																		  	</tr>";
																		}
																	}
																	
																
																	}
															} else {
																foreach ($agenda2 as $j) {
																echo "<tr>
																		<td>".$j->tb_turnohora."</td>
																		<td>".$j->tb_paciente."</td>
																	  </tr>";
																	}
															}
															
															
														}
														
														
					
                    
															
														?>
																
														</tbody>

													</thead>
											</table>
										</div>
									</div>
								</div>
							</div>

										
						
						
					</div>
				</div>
			</div>
		
	</div>
	



	<?php $this->load->view("footer");?>	
	
 	<!-- Para datepicker
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>


	<script type="text/javascript">

		

			$("#datetimepicker1").datepicker({
					language: 'es',
					autoclose: true,
					format:'dd/mm/yyyy',
					firstDay:1,
					beforeShowDay: function(date) {
						
						var dn = date.getDay();
						var d = date.getDate();
						var m = date.getMonth();
						var a = date.getFullYear();
						<?php
						$con = ''; 
						foreach ($dias as $d) {
							$con = $con.'dn != '.$d->diasid." && ";
						};
								
						$con = substr($con,0,-3);
						$dLicencias = "var diasLicencias = [";
						$mLicencias = "var mesesLicencias = [";
						$aLicencias = "var anioLicencias = [";
						$b = 0;
						if(!empty($licencias)){
							$b = 1;
							foreach ($licencias as $l) {
								$dLicencias = $dLicencias.$l->dia.",";
								$mLicencias = $mLicencias.$l->mes.",";
								$aLicencias = $aLicencias.$l->anio.",";
							}
						}
						if(!empty($vacaciones)){
							$b = 1;
							foreach ($vacaciones as $v) {
								$dLicencias = $dLicencias.$v->dia.",";
								$mLicencias = $mLicencias.$v->mes.",";
								$aLicencias = $aLicencias.$v->anio.",";
							}
						}
						if(!empty($feriados)){
							$b = 1;
							foreach ($feriados as $f) {
								$dLicencias = $dLicencias.$f->dia.",";
								$mLicencias = $mLicencias.$f->mes.",";
								$aLicencias = $aLicencias.$f->anio.",";
							}
						}
						if ($b == 0){
							$dLicencias = $dLicencias."];\n\t";
							$mLicencias = $mLicencias."];\n\t";
							$aLicencias = $aLicencias."];\n\t";
						}else{
							$dLicencias = substr($dLicencias,0,-1);
							$dLicencias = $dLicencias."];\n\t";
							$mLicencias = substr($mLicencias,0,-1);
							$mLicencias = $mLicencias."];\n\t";
							$aLicencias = substr($aLicencias,0,-1);
							$aLicencias = $aLicencias."];\n\t";
							echo $dLicencias;
							echo $mLicencias;
							echo $aLicencias;
						}

						/*if( !empty($licencias) && !empty($vacaciones)){
							
							
							foreach ($vacaciones as $v) {
								$dLicencias = $dLicencias.$v->dia.",";
								$mLicencias = $mLicencias.$v->mes.",";
								$aLicencias = $aLicencias.$v->anio.",";
							}
							$dLicencias = substr($dLicencias,0,-1);
							$dLicencias = $dLicencias."];\n\t";
							$mLicencias = substr($mLicencias,0,-1);
							$mLicencias = $mLicencias."];\n\t";
							$aLicencias = substr($aLicencias,0,-1);
							$aLicencias = $aLicencias."];\n\t";
							echo $dLicencias;
							echo $mLicencias;
							echo $aLicencias;
		
						}else{
							if( !empty($licencias)){
								$dLicencias = "var diasLicencias = [";
								$mLicencias = "var mesesLicencias = [";
								$aLicencias = "var anioLicencias = [";
								foreach ($licencias as $l) {
									$dLicencias = $dLicencias.$l->dia.",";
									$mLicencias = $mLicencias.$l->mes.",";
									$aLicencias = $aLicencias.$l->anio.",";
								}
								$dLicencias = substr($dLicencias,0,-1);
								$dLicencias = $dLicencias."];\n\t";
								$mLicencias = substr($mLicencias,0,-1);
								$mLicencias = $mLicencias."];\n\t";
								$aLicencias = substr($aLicencias,0,-1);
								$aLicencias = $aLicencias."];\n\t";
								echo $dLicencias;
								echo $mLicencias;
								echo $aLicencias;
							}else {
								if(!empty($vacaciones)){
									$dLicencias = "var diasLicencias = [";
									$mLicencias = "var mesesLicencias = [";
									$aLicencias = "var anioLicencias = [";
									foreach ($vacaciones as $v) {
										$dLicencias = $dLicencias.$v->dia.",";
										$mLicencias = $mLicencias.$v->mes.",";
										$aLicencias = $aLicencias.$v->anio.",";
									}
									$dLicencias = substr($dLicencias,0,-1);
									$dLicencias = $dLicencias."];\n\t";
									$mLicencias = substr($mLicencias,0,-1);
									$mLicencias = $mLicencias."];\n\t";
									$aLicencias = substr($aLicencias,0,-1);
									$aLicencias = $aLicencias."];\n\t";
									echo $dLicencias;
									echo $mLicencias;
									echo $aLicencias;
		
								}
							}
						}*/
					
						
						?>
						if (<?php echo $con;?>) {
							return false;
						} else {
							<?php
								if(!empty($licencias) || !empty($vacaciones) || !empty($feriados)){ 
									echo "\t\tvar bandera=0;\n\t\t\tvar i = 0;\n\t\t ";
									echo "while( (bandera == 0) && (i < diasLicencias.length)){\n\t\t\t\t";
									echo "if ( (diasLicencias[i] == d) && (mesesLicencias[i] == m) && (anioLicencias[i] == a) ){\n\t\t\t\t\t";
									echo "bandera =1;\n\t\t\t\t}\n\t\t\ti = i + 1;\n\t\t}\n\t";
									echo "if(bandera == 0){return true;}else{return false;}";
								}else{
									echo "return true;";
								}	
							?>
							//return true;
						}
					}
				}).datepicker("setDate","<?php echo $fecha;?>");



	</script>

	<script>
	$(document).ready(function () {
  		$('#table').DataTable({
			  "paginatype":"simple_numbers",
			  "language": {
				"url": "<?php echo base_url(); ?>js/Spanish.json"
			  }
		  });
		 
  		$('.dataTables_length').addClass('bs-select');
	});
	</script>
	<script>
	$(document).ready(function () {
  		$('#table2').DataTable({
			  "paginatype":"simple_numbers",
			  "language": {
				"url": "<?php echo base_url(); ?>js/Spanish.json"
			  }
		  });
		 
  		$('.dataTables_length').addClass('bs-select');
	});
	</script>


	<script type="text/javascript">
		$(document).ready(function(){
            // indicamos que se ejecuta la función a los 5 segundos de haberse
            // cargado la pagina
            setTimeout(clickbutton,150000);
 
            function clickbutton()
            {
                // simulamos el click del mouse en el boton del formulario
                $("#Buscar").click();
            }
        });
	</script>


  	
</body>

</html>
 
    

