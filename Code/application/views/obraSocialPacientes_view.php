						        <div class="myform-top">
									<div class="myform-top-left">
										<div class="col-sm-6">
											<p>Registrar Obra Social</p>
											<h5>* campos obligatorios</h5>
										</div>
									</div>
									
									<div class="myform-top-right">
										<i class="fas fa-user-friends"></i>
									</div>
								</div>
						
								<div class="myform-bottom">
									<div align="left">
										<p style="font-size:13px" align="left"></p>
									</div>
									<?php echo form_open('pacientes/RegistrarObraS/'.$id); ?>
										<!----------------------------------------------------->
										<div class="row">
											<div class="col-sm-5">
													<div  class="form-group" align="left">
														<label for="obrasSociales" >* Obra Social</label>
														
														<select class="form-control" style="height:40px;" name="obrasSociales" id="obrasSociales">
															<option value="0">Seleccione Obra Social</option>
														<?php
															if (isset($_POST['Registrar'])) {
																foreach ($obraSociales as $os){
																	if ($os->obrasocialid == $obrasocialid) {
																		echo'<option  value="'.$os->obrasocialid.'" selected >'.$os->obrasocialnombre.'</option>';
																	} else {
																		echo'<option  value="'.$os->obrasocialid.'">'.$os->obrasocialnombre.'</option>';
																	}
																	
																}
																
															} else {
																foreach ($obraSociales as $os) {
																	echo'<option  value="'.$os->obrasocialid.'">'.$os->obrasocialnombre.'</option>';
																}
															}
															
																
															?>
														</select>
														<?php echo form_error('obrasSociales');?><br />
													</div>
											</div>
											<div class="col-sm-2">
												<div  class="form-group" align="left">
													<label></label>
													<button type="submit" class="btn btn-primary submitBtn" style="padding:10px 50px"value="Registrar" name="Registrar">Registrar</button>	
												</div>
											</div>
										</div>

										

										<div class="row">
											<div class="col-sm-12">
												<div class="table-responsive">
													<table id="tableO" class="table table-striped table-bordered table-hover cell-border" cellspacing="2"  style="font-size:small;">
											            <thead class="mytable">
											                <tr>
											                    <th><center>Id<center></th>
											                    <th><center>ObraSocial</center></th>
											                    <th><center>Acciones<center></th>   
											                </tr>

											            </thead>
											            <tbody class="mytbody" style="color:black;background:white;">
											            	
											            </tbody>
											        </table>
										        </div>
											</div>
										</div>
										
										<!----------------------------------------------------->
										<div class="row">		
											<div class="col-sm-3">
											</div>
											<div class="col-sm-3">
												<button type="button" class="btn btn-primary submitBtn" style="background-color:#2e6da4;border-color:#337ab7;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/pacientes'">Finalizar</button>
											</div>
											<div class="col-sm-3">
												<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" value="Cancelar" onclick="location.href='<?php echo base_url(); ?>index.php/pacientes'">Cancelar</button>
											</div>
											<div class="col-sm-3">
											</div>				

										</div>			
											
										<?php echo form_close(); ?>						
									
									
								</div>
