<body class="mybodytables" > 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/profesionales/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container"><br>
	<?php $this->load->view('menu_view');?>
	<!-- ------------------------------------------------------------------------------------ ---->
    <!-- IR a Registrar Nuevo Profesional -->
            <div align="right"> 
                        <button type="button" data-toggle="tooltip" title="Registrar Nuevo Profesional" class="btn btn-primary" onclick="location.href='<?php echo base_url(); ?>index.php/profesionales/registrarProfesional'"><i class="fa fa-plus"></i></button>
            </div>


    <!-- -------------------------------------------------------------------------------------- -->	
        <div class="mycontainersmall" >
 			<h3 class="myh3">Datos de Profesionales</h3>
        </div>
    	
		
        
        <div class="table-responsive">
             <table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2" width="100%" style="font-size:small">
                <thead class="mytable">
                    <tr>
                        <th><center>Id<center></th>
                        <th><center>Matrícula</center></th>
                        <th><center>DNI</center></th>
                        <th><center>Doctor/a<center></th>
                        <th><center>Especialidad<center></th>
                        <th><center>Estado<center></th>    
                        <th><center>Acciones</center></th>    
                        
                    </tr>

                </thead>
                <tbody class="mytbody">
                    
                </tbody>
            </table> 
        </div>
           
    </div>

	<?php $this->load->view("footer");?>
 
 
<script type="text/javascript">

	var table;
 	$(document).ready(function() {
 
    //datatables
    table = $('#table').DataTable({ 

    	"responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: "<?php echo site_url('profesionales/ajax_list')?>",
            type: "POST"
            
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [

        	{ className: "dt-right", "targets": [0,1,2,3,4,5] },
        	{ 
 
            	"targets": [0], //first column / numbering column
            	"orderable": false, //set not orderable
        	},
        ],

        "language": idioma_espanol,

        "createdRow":function(row,data,dataIndex)
        {
          if(data[5] == "NO ACTIVO")
          {
            $('td', row).css('background-color', '#ec9696');
          } 
        }

 
    });
 
});

 	var idioma_espanol= {
		    "sProcessing":     "Procesando...",
		    "sLengthMenu":     "Mostrar _MENU_ registros",
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "",
		    "sInfoEmpty":      "",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "Buscar:",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    } 
		}
</script>	

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

</body>

</html>
 
    
