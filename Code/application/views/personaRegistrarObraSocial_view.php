<?php $this->load->view('head_view');

?>

<link rel="stylesheet" href="<?php echo base_url(); ?>/bootstrap-select/dist/css/bootstrap-select.css" type="text/css">
<script type="text/javascript" src="<?php echo base_url(); ?>/bootstrap-select/dist/js/bootstrap-select.js"></script>
<style type="text/css">
	.altura{
		height: 40px !important;
	}
	.filter-option{
		height: 26px !important;
	}

	.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #333;
	}
	a {
    color: #333;
    text-decoration: none;
	}
</style>
<body class="mybodytables"> 
	<div class="container">
		<div class="mylogout">
			<?php echo "Hola usuario: ".$this->session->userdata('usuario');?>
			<a href="<?php echo base_url(); ?>index.php/pacientes/CerrarSesion">(Salir)</a>
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>
	<div class="container">
		<!-- Menu -->
		<?php  $this->load->view('menu_view');?>

			<div class="row">
				<div class="col-lg-12  myform-cont">

					
  				
  						<!----------------------------------tab telefono--------------->
					    <?php $this->load->view('obraSocialPacientes_view');?>
					    
					
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group" align="left">
						<input id="pacienteid"  class="form-control" name="pacienteid" type="hidden" value="<?php echo $id ?>">
					</div>
					
				</div>
			</div>
	</div>

	<?php $this->load->view("footer");?>
	
	<script src="<?php echo base_url(); ?>moment/js/moment.min.js"></script>
	

	
<script type="text/javascript">

	var table;
 	$(document).ready(function() {
 
    //datatables
    table = $('#tableO').DataTable({ 

    	"responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: "<?php echo site_url('pacientes/ajax_Obra')?>",
            type: "POST",
            data:{pacienteid:$("#pacienteid").val()}
            
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [

        	{ className: "dt-right", "targets": [0,1,2] },
        	{ 
 
            	"targets": [0], //first column / numbering column
            	"orderable": false, //set not orderable
        	},
        ],

        "language": idioma_espanol

 
    });
 
});

 	var idioma_espanol= {
		    "sProcessing":     "Procesando...",
		    "sLengthMenu":     "Mostrar _MENU_ registros",
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "Buscar:",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    } 
		}
</script>

</body>

</html>