<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reporte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-2.0.0.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>jquery/ajax-jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>push.js-master/bin/push.min.js"></script>
    <link href="<?php echo base_url();?>css/jquery-ui.css" rel="Stylesheet"/></link>


    <link href="<?php echo base_url(); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/font-awesome/css/font-awesome.min.css"> <!--iconos -->
    <link rel="icon" href="<?php echo base_url(); ?>css/imagenes/favicon.ico" type="image/gif">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/bootstrap/css/custom.css">

    <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome-free-5.2.0/css/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>DataTables/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/custom.css">


</head>

<script type="text/javascript" >
    $(document).ready(function(){
        $("#pacienteEdad").val(CalcularEdad());
    });
</script>

<script type="text/javascript">
    function CalcularEdad(){
        var fechaNac = document.getElementById("pacientefechanac").value;
        var valores = fechaNac.split("-");
        var dia = valores[2];
        var mes = valores[1];
        var anio = valores[0];
        var fecha_actual = new Date();
        var dia_actual = fecha_actual.getDate();
        var mes_actual = fecha_actual.getMonth()+1;
        var anio_actual = fecha_actual.getYear();
        var edad = (anio_actual+1900)-anio; 

        if ( mes_actual < mes ){
             edad--;
        }
        if ((mes == mes_actual) && (dia_actual < dia)){
            edad--;
        }
        if (edad > 1900){
            edad -= 1900;
        }
        if (edad < 18){
                //document.getElementById("oculto").style.display = 'block';
        }
            
        return edad;
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){
        console.log('edad:',$('#pacienteEdad').val());
        if (($('#pacienteEdad').val() < 18) && ($('#pacienteEdad').val() != '')){
            $("#oculto").show();
        }else{
            console.log("estoy despues del else");
            $("#oculto").hide();
        }
    });
</script>

<!----------------CARGA TELEFONOS------------->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#pacienteDNI").change(function(){
                $.ajax({
                    url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaTelefonos",
                    type: "POST",
                    data:"personasnrodocumento="+$("#pacienteDNI").val()
                    }).done(function(respuesta){
                        $("#pacienteTelefonos").val(respuesta);
                    })
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $.ajax({
                    url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaTelefonos",
                    type: "POST",
                    data:"personasnrodocumento="+$("#pacienteDNI").val()
                    }).done(function(respuesta){
                        $("#pacienteTelefonos").val(respuesta);
                    })
        });
    </script>
    <!----------------FIN CARGA TELEFONOS------------->


    <!----------------CARGA OBRA SOCIAL------------->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#pacienteDNI").change(function(){
                $.ajax({
                    url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaObrasSociales",
                    type: "POST",
                    data:"personasnrodocumento="+$("#pacienteDNI").val()
                    }).done(function(respuesta){
                        $("#pacienteObraSocial").val(respuesta);
                    })
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $.ajax({
                    url:"<?php echo base_url(); ?>index.php/historiaclinica/CargaObrasSociales",
                    type: "POST",
                    data:"personasnrodocumento="+$("#pacienteDNI").val()
                    }).done(function(respuesta){
                        $("#pacienteObraSocial").val(respuesta);
                    })
        });
    </script>
    <!----------------FIN CARGA OBRA SOCIAL------------->

<body class="mybodytables" style="background-color:white;" > 
	<div class="container">
		<div class="mylogout">
			<img src="<?php echo base_url(); ?>css/imagenes/salutiaLogo.png" width =100% height=80 align=middle>
		</div>
	</div>

	<div class="container"><br>
        <div class="mycontainersmall" >
 			<h3 class="myh3">Reporte Historia Clínica de un Paciente por Especialidad</h3>
        </div>
        <div class="row">
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-3">
                                    <div  class="form-group" align="left">
                                                    <label >Fecha Alta</label>
                                                    <input id="hcfechaalta" type="date" class="form-control lectura" name="hcFechaAlta"  value="<?php echo $hc[0]->historiasclinicasfechaalta;?>" readonly disabled>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                        <div  class="form-group" align="left">
                                                    <label >Id HC</label>
                                                    <input id="hcId" type="text" class="form-control lectura" name="idHC"  value="<?php echo $hc[0]->historiasclinicasid;?>" readonly disabled>
                                        </div>
                                </div>
                                <div class="col-sm-4">
                                        <div  class="form-group" align="left">
                                                    <label >Apellido y Nombre</label>
                                                    <input id="pacientenombre" type="text" class="form-control lectura" name="pacientenombre"  value="<?php echo $hc[0]->personasnombrecompuesto;?>" disabled >
                                        </div>
                                </div>
                                <div class="col-sm-3">
                                        <div  class="form-group" align="left">
                                                    <label >DNI</label>
                                                    <input id="pacienteDNI" type="text" class="form-control lectura" name="pacienteDNI"  value="<?php echo $hc[0]->personasnrodocumento;?>" readonly disabled>
                                        </div>
                                </div>
                                <div class="col-sm-3">
                                        <div  class="form-group" align="left">
                                                    <label >Sexo</label>
                                                    <input id="pacienteSexo" type="text" class="form-control lectura" name="pacienteSexo"  value="<?php echo $hc[0]->sexonombre;?> " readonly disabled>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-sm-3">
                                        <div  class="form-group" align="left">
                                                    <label >Fecha de Nacimiento</label>
                                                    <input id="pacientefechanac" type="date" class="form-control lectura" name="pacienteFechaNac"  value="<?php echo $hc[0]->personasfechanacimiento;?>" readonly disabled>
                                        </div>
                                </div>
                                <div class="col-sm-2">
                                        <div  class="form-group" align="left">
                                                    <label >Edad</label>
                                                    <input id="pacienteEdad" type="text" class="form-control lectura" name="inputEdad"  value="<?php echo $pacienteedad; ?>" readonly disabled>
                                                    
                                        </div>
                                </div>
                                <div class="col-sm-7">
                                        <div  class="form-group" align="left">
                                                    <label >Teléfonos</label>
                                                    <input id="pacienteTelefonos" type="text" class="form-control lectura" name="inputTelefono"  value="<?php echo $tel ?>" readonly disabled>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div  class="form-group" align="left">
                                        <label >Procedencia</label>
                                        <input id="pacienteProcedencia" type="text" class="form-control lectura" name="inputProcedencia"  value="<?php echo $hc[0]->historiasclinicasprocedenciapaciente; ?>" readonly disabled >
                                    </div>
                                </div>
                                
                            </div>  
                            <div class="row">
                                <div class="col-sm-5">
                                    <div  class="form-group" align="left">
                                        <label >Obra Social</label>
                                        <input id="pacienteObraSocial" type="text" class="form-control lectura" name="inputObraSocial" value="<?php echo $os ?>"  readonly disabled>
                                        
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div  class="form-group" align="left">
                                        <label >Número</label>
                                        <input id="pacienteNroOS" type="text" class="form-control lectura" name="inputNroOS" value="<?php echo $hc[0]->historiasclinicasnumeroospaciente; ?>" readonly disabled>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div  class="form-group" align="left">
                                        <label >Titular</label>
                                        <input id="pacienteTitularOS" type="text" class="form-control lectura" name="inputTitularOS" value="<?php echo $hc[0]->historiasclinicastitularospaciente; ?>" readonly disabled>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div  class="form-group" align="left">
                                        <label >Ocupación</label>
                                        <input id="pacienteOcupacion" type="text" class="form-control lectura" name="inputOcupacion" value="<?php echo $hc[0]->historiasclinicasocupacionpaciente; ?>" readonly disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div  class="form-group" align="left">
                                        <label >Derivado</label>
                                        <input id="pacienteDerivado" type="text" class="form-control lectura" name="inputDerivado" value="<?php echo $hc[0]->historiasclinicasderivadopaciente; ?>" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        <div id="oculto" hidden="true" >
                            <div class="row">
                                <div class="col-sm-6">
                                    <div  class="form-group" align="left">
                                        <label >Nombre de la Madre</label>
                                        <input id="pacienteMadre" type="text" class="form-control lectura " name="inputMadre" value="<?php echo $hc[0]->historiasclinicasmadrepaciente; ?>" readonly disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div  class="form-group" align="left">
                                        <label >Ocupación de la Madre</label>
                                        <input id="pacienteOcupacionM" type="text" class="form-control lectura" name="inputOcuapcionM" value="<?php echo $hc[0]->historiasclinicasocupacionmadrepaciente; ?>" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div  class="form-group" align="left">
                                        <label >Nombre del Padre</label>
                                        <input id="pacientePadre" type="text" class="form-control lectura " name="inputPadre" value="<?php echo $hc[0]->historiasclinicaspadrepaciente; ?>" readonly disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div  class="form-group" align="left">
                                        <label >Ocupación del Padre</label>
                                        <input id="pacienteOcupacionP" type="text" class="form-control lectura " name="inputOcupacionP" value="<?php echo $hc[0]->historiasclinicasocupacionpadrepaciente; ?>" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table id="table" class="table table-striped table-bordered table-hover cell-border" cellspacing="2"  style="font-size:small">
                                            <thead class="mytable">
                                                <tr>
                                                    <th><center>Id<center></th>
                                                    <th><center>Profesionales</center></th>
                                                    <th><center>Especialidades<center></th>
                                                    <th><center>Fecha<center></th>
                                                    <th><center>Hora<center></th>
                                                    <th><center>Diagnóstico<center></th>
                                                    <th><center>Observaciones<center></th>
                                                    
                                                </tr>

                                            </thead>
                                            <tbody class="mytbody" style="color:black;background:white;">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
    </div>
	
	<?php $this->load->view("footer");?>
 
<script type="text/javascript">
    var table;
    $(document).ready(function() {
    table = $('#table').DataTable({ 

        "responsive": true,
        "processing": true, 
        "serverSide": true,
        "paging":false,
        "searching":false,
        "order": [],
        "ajax": {
            
            url: "<?php echo site_url('reportes/getDHC')?>",
            type: "POST",
            data:{HCid:$("#hcId").val()
                    //;
                  //IdEsp:$("").val()
                }
            
        },
 
        "columnDefs": [

            { className: "dt-right", "targets": [0,1,2,3,4,5,6] },
            { 
 
                "targets": [0], 
                "orderable": false,
            },
        ],

        "language": idioma_espanol
     });
 
});

    var idioma_espanol= {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            } 
        }
</script>   

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });
</script>

</body>

</html>
 
    
