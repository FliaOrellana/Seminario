<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Secretario_modelo extends CI_Model{

    var $table = 'fn_mostrar_secretarios()';
    var $select_column = array('id','nombrecompleto','dni','domicilio','email','telefono','estadosid');
    var $order_column = array(null,'nombrecompleto','dni','domicilio','email','telefono');

	public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    public function registrarSecretario($data2){
        $this->db->insert('secretarios', $data2);
    }

    private function _get_datatables_query(){
         
        $this->db->select($this->select_column);
        $this->db->from($this->table);

        if(isset($_POST["search"]["value"])) {
            $this->db->like("nombrecompleto",strtoupper($_POST["search"]["value"]));
           $this->db->or_like("dni",strtoupper($_POST["search"]["value"]));
           $this->db->or_like("domicilio",strtoupper($_POST["search"]["value"]));
           $this->db->or_like("email",strtoupper($_POST["search"]["value"]));
           $this->db->or_like("telefono",strtoupper($_POST["search"]["value"]));
           
        } 
        if (isset($_POST['order'])) {
            $this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else{
            $this->db->order_by("id","DESC");
        }

   
   }

   function get_datatables()
    {
        $this->_get_datatables_query();
        // if($_POST['length'] != -1){
        // 	$this->db->limit($_POST['length'], $_POST['start']);	
        // }
        
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function buscarSecretario($id){
        $nombres=array('p.personasid',
                       'p.personasnombre',
                       'p.personasapellido',
                       'p.personasnombrecompuesto',
                       'p.personasnrodocumento',
                       'p.personasdomicilio',
                       'p.personasemail',
                       'sex.sexoid');
        $this->db->select($nombres);
        $this->db->from('personas p');
        $this->db->join('secretarios s','s.personasid = p.personasid');
        $this->db->join('sexo sex','sex.sexoid = p.sexoid');
        $this->db->where('s.secretariosid',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
     }
     return null; 


    }

    public function modificarPersona($id,$data)
    {
        $this->db->where('personasid', $id);
        $this->db->update('personas', $data);
    }

    public function bajaSecretario($id,$data)
    {
        $this->db->where('secretariosid', $id);
        $this->db->updated('secretarios', $data);
    }

}

?>