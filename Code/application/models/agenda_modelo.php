<?php  
    defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Agenda_modelo extends CI_Model{


    //var $table = "fn_agendaturnonormales(null,null,null)";
    var $select_column=  array('tb_turnohora','tb_paciente');
    //var $orden_columna = array('tb_turnohora','tb_paciente');
    var $select_column_turnos=  array('turnosid','profesional','especialidad','fecha','horainicio','horafin','tipoturno','pacientes','estadoturnoid','estadoturno');
    var $orden_columna_turnos= array(null,'profesional','especialidad','fecha','horainicio','horafin','tipoturno','pacientes',null,'estadoturno');




    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    

    public function Especialidades($profesionalid){
        $nombres=array('e.especialidadesid','especialidadesnombre');
        $this->db->select($nombres);
        $this->db->from('especialidadesxprofesionales ep');
        $this->db->join('especialidades e','ep.especialidadesid=e.especialidadesid');
        $this->db->where('profesionalesid', $profesionalid);
        $this->db->order_by('especialidadesnombre','DESC');
        $query=$this->db->get();
        return $query->result();

    }

    /*public function obtineID($idprof,$idEsp)
    {
        $this->db->select('agid');
        $this->db->from('especialidadesxprofesionales ');
        $this->db->where('profesionalesid', $idprof);
        $this->db->where('especialidadesid', $idEsp);
        $query=$this->db->get();
        return $query->result();
    }*/
    public function dias()
    {
        $nombre = array('diasid','diasnombre');
        $this->db->select($nombre);
        $this->db->from('dias');
        $this->db->order_by('diasid','ASC');
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null; 
    }
    public function diasagendaprof($idprof)
    {
        $nombre = array('d.diasid','d.diasnombre');
        $this->db->select($nombre);
        $this->db->from('dias d');
        $this->db->join('agendas a','d.diasid=a.diasid');
        $this->db->where('profesionalesid', $idprof);
        $this->db->where('estadosid', 1);
        $this->db->order_by('diasid','ASC');
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null; 
    }
    public function diasagenda($idprof,$idesp)
    {
        $nombre = array('d.diasid','d.diasnombre');
        $this->db->select($nombre);
        $this->db->from('dias d');
        $this->db->join('agendas a','d.diasid=a.diasid');
        $this->db->where('profesionalesid', $idprof);
        $this->db->where('especialidadesid', $idesp);
        $this->db->where('estadosid', 1);
        $this->db->order_by('diasid','ASC');
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null; 
    }

    public function obtieneIntervalo($idprof,$idEsp,$iddia)
    {
        $nombre = array('a.agendastmini','a.agendastmfin','a.agendasttini','a.agendasttfin');
        $this->db->select($nombre);
        $this->db->from('agendas a');
        $this->db->where('profesionalesid', $idprof);
        $this->db->where('especialidadesid', $idEsp);
        $this->db->where('diasid', $iddia);
        $this->db->where('estadosid', 1);
        $this->db->order_by('diasid','ASC');
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null; 
    }
    public function agenda_query($table){    
         $this->db->select($this->select_column);
         
         $this->db->from($table);

    }

    public function agenda_query2($idProf,$idEsp){    
         $this->db->select('d.diasnombre,a.agendastmini,a.agendastmfin,a.agendasttini,a.agendasttfin,a.agendasid,a.estadosid');
         
         $this->db->from('especialidades e');
         $this->db->join('agendas a','e.especialidadesid=a.especialidadesid');
         $this->db->join('profesionales p','p.profesionalesid=a.profesionalesid');
         $this->db->join('dias d','a.diasid=d.diasid');
         $this->db->where('a.profesionalesid', $idProf);
         $this->db->where('a.especialidadesid', $idEsp);
          $this->db->order_by('a.diasid','ASC');

    }

    private function _get_datatables_query($tableturnos){
         
         $this->db->select($this->select_column_turnos);
         $this->db->from($tableturnos);
         
         if(isset($_POST["search"]["value"])) {
            $this->db->like("profesional",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("especialidad",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("fecha",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("horainicio",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("horafin",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("tipoturno",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("pacientes",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("estadoturno",strtoupper($_POST["search"]["value"]));
         } 
         if (isset($_POST['order'])) {
            $this->db->order_by($this->orden_columna_turnos[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
         }
         else{
            $this->db->order_by("fecha","DESC");
         }

    
    }

    function get_datatables($table)
    {
        $this->agenda_query($table);

        $query = $this->db->get();
        return $query->result();
    }

    function get_datatables2($idprof,$idEsp)
    {
        $this->agenda_query2($idprof,$idEsp);

        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered($table)
    {
        
        $this->agenda_query($table);
        $query = $this->db->get();
        return $query->num_rows();
    }


 
    public function count_all($table)
    {
    	$this->db->select("*");
        $this->db->from($table);
        return $this->db->count_all_results();
    }


    public function insertarAgenda($data)
    {
       return $this->db->insert('agendas', $data);
    }

    public function updateHorasLab($data,$id)
    {
       $this->db->where('agendasid', $id);
       $this->db->update('agendas', $data);
    }

    public function cancelaDia($idAg,$data){
        
        $this->db->where('agendasid',$idAg);
        $this->db->update('agendas',$data);
    }
    public function VerificaDias($idprof,$idEsp,$idD)
    {
        $this->db->select('agendasid');
        $this->db->from('agendas ');
        $this->db->where('profesionalesid', $idprof);
        $this->db->where('especialidadesid', $idEsp);
        $this->db->where('diasid', $idD);
        $this->db->where('estadosid', 1);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return true;
        }else{
            return false; 
        }
    }

     function get_datatables_turnos($table)
    {
        $this->_get_datatables_query($table);
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);    
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered_turnos($table)
    {
        $this->_get_datatables_query($table);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_turnos($table)
    {
        $this->db->select("*");
        $this->db->from($table);
        return $this->db->count_all_results();
    }

     public function pacienteAtendiendo($data,$id)
    {
        $this->db->where('turnosid', $id);
        $this->db->update('turnos', $data);
    }
     public function pacienteAtendido($data,$id)
    {
        $this->db->where('turnosid', $id);
        $this->db->update('turnos', $data);
    }
    public function agendalisto($data,$id)
    {
        $this->db->where('turnosid', $id);
        $this->db->update('turnos', $data);
    }

    

}

?>