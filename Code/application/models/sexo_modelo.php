<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Sexo_modelo extends CI_Model{


	var $table = 'sexo s';
	var $select_column=  array('sexoid','sexonombre');
	var $orden_columna = array('sexoid','sexonombre');
	

	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    
    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);
          
		 if (isset($_POST['order'])) {
		 	$this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		 }
		 else{
		 	$this->db->order_by("sexoid");
		 }
    }


    function get_datatables(){

        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);	
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function registrarSexo($data){
        $this->db->insert('sexo', $data);
    }

    public function obtieneDatosSexo($id)
    {
        $this->db->select($this->select_column);
        $this->db->from($this->table);
        $this->db->where('sexoid',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
        }
        return null; 
    }


    public function modificarSexo($data,$id)
    {
        $this->db->where('sexoid', $id);
        $this->db->update('sexo', $data);
    }
    
    public function buscarSexo($data){

        $this->db->select($this->select_column);
        $this->db->from('sexo');
        $this->db->where('sexonombre',$data);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
     }
     return null; 


    }


    public function bajaSexo($id)
    {
        $this->db->where('sexoid',$id);
        $this->db->delete('sexo');
    }


}

?>