<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Usuarios_modelo extends CI_Model{


	var $table = 'vista_listausuarios';
	var $select_column=  array('usuariosid','usuariosnombre','personasnombrecompuesto','perfilesnombre','estadosid');
	var $orden_columna = array(null,'usuariosnombre','personasnombrecompuesto','perfilesnombre','estadosid');
	




	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);

		 if(isset($_POST["search"]["value"])) {
            $this->db->where("estadosid",1);
		 	$this->db->like("usuariosnombre",strtoupper($_POST["search"]["value"]));
			$this->db->or_like("personasnombrecompuesto",strtoupper($_POST["search"]["value"]));
			$this->db->or_like("perfilesnombre",strtoupper($_POST["search"]["value"]));
			
		 } 
		 if (isset($_POST['order'])) {
		 	$this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		 }
		 else{
		 	$this->db->order_by("usuariosid","DESC");
		 }

    
    }



    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
        	$this->db->limit($_POST['length'], $_POST['start']);	
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function Perfiles()
    {
        $nombres = array('perfilesid','perfilesnombre');
        $this->db->select($nombres);
        $this->db->from('perfiles');
        $this->db->order_by('perfilesnombre','DESC');
        $query=$this->db->get();
        return $query->result();
    }

    public function buscarPersona($perfil, $DNI)
    {
        
        if ($perfil == 2) {
            $this->db->select("p.personasid,p.personasnombrecompuesto ||' - '|| p.personasnrodocumento as nombredocumento,pr.profesionalesid as ident");
            $this->db->from('personas p');
            $this->db->join('profesionales pr','p.personasid=pr.personasid');
        }else{
            if ($perfil == 3){
                $this->db->select("p.personasid,p.personasnombrecompuesto ||' - '|| p.personasnrodocumento as nombredocumento,s.secretariosid as ident");
                $this->db->from('personas p');
                $this->db->join('secretarios s','p.personasid=s.personasid');
            }
        }
        $this->db->where("p.personasnrodocumento",strtoupper($DNI));
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null; 

    }

    public function BuscarUsuario($nombre)
    {
        $this->db->select("usuariosid,usuariosnombre, count(usuariosnombre) as cantidad");
        $this->db->from('usuarios');
        $this->db->where("usuariosnombre",strtoupper($nombre));
        $this->db->group_by("usuariosid");
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null; 
    }

    public function BuscarUsuarioCompleto($dni)
    {
        $this->db->select("u.usuariosid,u.usuariosnombre,p.personasnombrecompuesto,p.personasemail, count(u.usuariosnombre) as cantidad");
        $this->db->from('usuarios u');
        $this->db->join('secretarios s','u.usuariosid=s.usuariosid');
        $this->db->join('personas p','s.personasid=p.personasid');
        $this->db->where("p.personasnrodocumento",strtoupper($dni)); 
        $this->db->group_by("u.usuariosid,p.personasnombrecompuesto,p.personasemail");
        $query1 = $this->db->get_compiled_select(); 
        $this->db->select("u.usuariosid,u.usuariosnombre,p.personasnombrecompuesto,p.personasemail,count(u.usuariosnombre) as cantidad");
        $this->db->from('usuarios u');
        $this->db->join('profesionales pr','u.usuariosid=pr.usuariosid');
        $this->db->join('personas p','pr.personasid=p.personasid');
        $this->db->where("p.personasnrodocumento",strtoupper($dni)); 
        $this->db->group_by("u.usuariosid, p.personasnombrecompuesto,p.personasemail");
        $query2 = $this->db->get_compiled_select();
        return $query = $this->db->query($query1." UNION ".$query2)->result();
         
    }

    public function RegistrarUsuario($data)
    {
        $this->db->insert('usuarios', $data);
    }

    public function RegistrarPerfilesUsuarios($data){
         $this->db->insert('perfilesusuarios', $data);
    }
    
    public function AsignarUsuario($data,$id,$perfil)
    {
        if ($perfil == 2){
           $this->db->where('profesionalesid', $id); 
           $this->db->update('profesionales', $data);
        }else{
            $this->db->where('secretariosid', $id); 
             $this->db->update('secretarios', $data);
        }
       
    }

    public function cambiarPass($id,$data)
    {
        $this->db->where('usuariosid', $id); 
        $this->db->update('usuarios', $data);
    }

    public function NoPerfiles($id)
    {
         $this->db->select("p.perfilesid");
         $this->db->from('perfiles p');
         $this->db->join('perfilesusuarios pr','p.perfilesid = pr.perfilesid');
         $this->db->where("usuariosid", $id ); 
         $query=$this->db->get();
         if ( $query->num_rows() > 0){
            return $query->result();
        }else{
            return null;
        }

    }

    public function cancelaUsuario($idUs,$data){
        $this->db->where('usuariosid',$idUs);
        $this->db->update('usuarios',$data);
    }

    public function ObtienePerfiles($id)
    {
            $perfiles = $this->NoPerfiles($id);
            if ($perfiles == null) {
                 return null;
            }else{
                $row=array();
                for ($i = 0; $i<count($perfiles); $i++){

                    $row[] = $perfiles[$i]->perfilesid;
                }
                $this->db->select("perfilesid, perfilesnombre");
                $this->db->from('perfiles');
                $this->db->where_not_in("perfilesid", $row);
                $query2 =  $this->db->get();
                if ($query2->num_rows() > 0) {
                    return $query2->result();
                }else{
                    return null;
                }
            }         
    }



}

?>