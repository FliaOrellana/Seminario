<?php  
    defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Turnos_modelo extends CI_Model{


    var $table = 'mostrarturnos()';
    var $select_column=  array('turnosid','profesional','especialidad','fecha','horainicio','horafin','tipoturno','pacientes','estadoturnoid','estadoturno');
    var $orden_columna = array(null,'profesional','especialidad','fecha','horainicio','horafin','tipoturno','pacientes',null,'estadoturno');
    

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);
         
         if(isset($_POST["search"]["value"])) {
            $this->db->like("profesional",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("especialidad",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("fecha",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("horainicio",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("horafin",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("tipoturno",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("pacientes",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("estadoturno",strtoupper($_POST["search"]["value"]));
         } 
         if (isset($_POST['order'])) {
            $this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
         }
         else{
            $this->db->order_by("fecha","DESC");
         }

    
    }



    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);    
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    /*public function Pacientes()
    {
        $this->db->select('pa.pacientesid,p.personasnombrecompuesto');
        $this->db->from('pacientes pa');
        $this->db->join('personas p','pa.personasid=p.personasid');
        $this->db->order_by("personasnombrecompuesto","ASC");
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null; 
    }*/
    public function Pacientes($DNI){
        $this->db->select('personasnrodocumento');
        $this->db->from('pacientes pa');
        $this->db->join('personas p','pa.personasid=p.personasid');
        $this->db->like("personasnrodocumento",strtoupper($DNI));
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null; 
    }

    public function PacientesCarga($DNI){
        $this->db->select("pa.pacientesid,p.personasnombrecompuesto ||' - '|| p.personasnrodocumento as nombredocumento");
        $this->db->from('pacientes pa');
        $this->db->join('personas p','pa.personasid=p.personasid');
        $this->db->like("p.personasnrodocumento",strtoupper($DNI));
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null; 
    }

    public function buscaPacientexidTurno($idTurno){
        $this->db->select('p.personasnrodocumento,pa.pacientesid,p.personasnombrecompuesto');
        $this->db->from('pacientes pa');
        $this->db->join('personas p','pa.personasid=p.personasid');
        $this->db->join('turnos t','pa.pacientesid=t.pacientesid');
        $this->db->where('t.turnosid',$idTurno);
        //$this->db->like("turnosid",strtoupper($idTurno));
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null; 
    }

    public function buscaProfesionalxidTurno($idTurno)
    {
        $this->db->select('prof.profesionalesid,per.personasnombrecompuesto,e.especialidadesid,e.especialidadesnombre');
        $this->db->from('turnos t');
        $this->db->join('profesionales prof','prof.profesionalesid=t.profesionalesid');
        $this->db->join('especialidades e','t.especialidadesid=e.especialidadesid');
        $this->db->join('personas per','per.personasid=prof.personasid');
        $this->db->where('t.turnosid',$idTurno);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null; 
    }
    public function idTurno($idPac)
    {
        $this->db->select('t.turnosid');
        $this->db->from('pacientes pa');
        $this->db->join('turnos t', 'pa.pacientesid=t.pacientesid');
        $this->db->where('pa.pacientesid',$idPac);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    }
    public function Profesionales(){
        $this->db->select("pro.profesionalesid,'Dr. '||p.personasnombrecompuesto||' - ' ||pro.profesionalesmatricula as profesional");
        $this->db->from('profesionales pro');
        $this->db->join('personas p', 'pro.personasid=p.personasid');
        $this->db->order_by("p.personasnombrecompuesto","ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    }

    public function Especialidades($idprof)
    {
        $this->db->select("e.especialidadesid,e.especialidadesnombre,ep.especialidadesxprofesionalesid,ep.estadosid");
        $this->db->from('especialidades e');
        $this->db->join('especialidadesxprofesionales ep', 'e.especialidadesid=ep.especialidadesid');
        $this->db->where('ep.profesionalesid',$idprof);
        $this->db->order_by("e.especialidadesnombre","ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    
    }
    public function TipoTurnos(){
        $this->db->select("t.tipoturnosid, t.tipoturnosnombre");
        $this->db->from('tipoturnos t');
        $this->db->order_by("t.tipoturnosnombre","ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    }

    public function insertTurno($data2){
        $this->db->insert('turnos', $data2);
    }
    public function updateEstado($idPac,$data){
        $this->db->where('pacientesid', $idPac);
        $this->db->update('pacientes', $data);
    }
    public function cancelaTurno($data,$id)
    {
       $this->db->where('turnosid', $id);
       $this->db->update('turnos', $data);
    }

    public function llegadaPaciente($data,$id)
    {
        $this->db->where('turnosid', $id);
        $this->db->update('turnos', $data);
    }

    public function turnoslisto($data,$id)
    {
        $this->db->where('turnosid', $id);
        $this->db->update('turnos', $data);
    }

    public function ObtieneInformacionTurno($turnos){
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('turnosid', $turnos);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;

    }

    public function ActualizaEsatdoTurno()
    {
        $this->db->select("*");
        $this->db->from("fn_actualiza_estado_turnos()");
        $query = $this->db->get();
        if ( $query->num_rows() > 0) {
            return $query->result();
        }
        return null;

    }

}

?>