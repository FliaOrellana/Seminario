<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Especialidades_modelo extends CI_Model{


	var $table = 'especialidades e';
	var $select_column=  array('especialidadesid','especialidadesnombre');
	var $orden_columna = array('especialidadesid','especialidadesnombre');
	

	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    
    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);
          
		 if (isset($_POST['order'])) {
		 	$this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		 }
		 else{
		 	$this->db->order_by("especialidadesid");
		 }
    }


    function get_datatables(){

        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);	
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function registrarEspecialidad($data){
        $this->db->insert('especialidades', $data);
    }

    public function obtieneDatosEspecialidad($id)
    {
        $this->db->select($this->select_column);
        $this->db->from($this->table);
        $this->db->where('especialidadesid',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
        }
        return null; 
    }


    public function modificarEspecialidad($data,$id)
    {
        $this->db->where('especialidadesid', $id);
        $this->db->update('especialidades', $data);
    }
    

    public function buscarEspecialidad($data){
        $data = strtoupper($data);
        $data = $this->reemplazar_acentos($data);
        $this->db->select($this->select_column);
        $this->db->from('especialidades');
        $this->db->where('especialidadesnombre',$data);
        $query=$this->db->get();

        if ($query->num_rows() > 0){
           
            return $query->row_array();
     }
     return null; 


    }


    public function bajaEspecialidad($id)
    {
        $this->db->where('especialidadesid',$id);
        $this->db->delete('especialidades');
    }

    public function reemplazar_acentos($cad){
        $cad = str_replace(
            array('á','é','í','ó','ú'), 
            array('Á','É','Í','Ó','Ú'), $cad);
        return $cad;
    }

}

?>