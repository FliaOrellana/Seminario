<?php
class Login_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function very_sesion(){
		/*$consulta=$this->db->get_where('usuarios',array('usuariosnombre'=>$this->input->post('user',TRUE),
														'usuarioscontrasenia'=>$this->input->post('pass',TRUE)));*/

		$this->db->select('usuariosnombre,usuarioscontrasenia');
		$this->db->from('usuarios');
		$this->db->where('usuariosnombre',$this->input->post('user',TRUE));
		$this->db->where('usuarioscontrasenia',md5($this->input->post('pass',TRUE)));
		$consulta=$this->db->get();


		if ($consulta->num_rows() ==1){
			return true;
		}
		else {
			return false;
		}
	}
	public function Perfiles($nombre){
		$this->db->select('perfilesnombre');
		$this->db->from('usuarios u');
		$this->db->join('perfilesusuarios pu','u.usuariosid=pu.usuariosid');
		$this->db->join('perfiles p','pu.perfilesid=p.perfilesid');
		$this->db->where('usuariosnombre',strtoupper($nombre));
		$consulta=$this->db->get();
		$lista = array();
		foreach ($consulta->result() as $row){
        	$lista[] = $row->perfilesnombre;

		}
		return $lista;
	}

	public function Profesional($nombre){
		$this->db->select('profesionalesid');
		$this->db->from('usuarios u');
		$this->db->join('profesionales p','u.usuariosid=p.usuariosid');
		$this->db->where('usuariosnombre',strtoupper($nombre));
		$consulta=$this->db->get();
		foreach ($consulta->result() as $row){
        	$row->profesionalesid;

		}
		return $row->profesionalesid;
	}

	public function Secretario($nombre){
		$this->db->select('secretariosid');
		$this->db->from('usuarios u');
		$this->db->join('secretarios s','u.usuariosid=s.usuariosid');
		$this->db->where('usuariosnombre',strtoupper($nombre));
		$consulta=$this->db->get();
		foreach ($consulta->result() as $row){
        	$row->secretariosid;

		}
		return $row->secretariosid;
	}

	public function buscaremail($email)
	{
		$this->db->from("fn_buscaremail('".$email."')");
		$query=$this->db->get();
         if ($query->num_rows() > 0){
             return $query->result();
         }
          return null; 
	}
}			
?>