<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Historiaclinica_modelo extends CI_Model{


	var $table = 'historiasclinicas hc';
	var $select_column=  array('historiasclinicasid','personasnombrecompuesto','personasnrodocumento','historiasclinicasfechaalta','personasfechanacimiento','pa.pacientesid');
	var $orden_columna = array('historiasclinicasid','personasnombrecompuesto','personasnrodocumento','historiasclinicasfechaalta');

    var $table2 = 'detallehistoriasclinicas dhc';
    var $select_column2 =  array('dhc.historiasclinicasid','p.personasnombrecompuesto','es.especialidadesnombre','detallehistoriasclinicasfecha','detallehistoriasclinicashora','detallehistoriasclinicasdiagnostico','detallehistoriasclinicasobservaciones');
    var $order_column2 =   array(null,'p.personasnombrecompuesto','es.especialidadesnombre','detallehistoriasclinicasfecha','detallehistoriasclinicashora','detallehistoriasclinicasdiagnostico','detallehistoriasclinicasobservaciones');

	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    
    private function _get_datatables_query2($id){
         
         $this->db->select($this->select_column2);
         $this->db->from($this->table2);
		 $this->db->join('profesionales pr','pr.profesionalesid=dhc.profesionalesid');
         $this->db->join('especialidades es','dhc.especialidadesid=es.especialidadesid');
         $this->db->join('personas p','pr.personasid=p.personasid');
         $this->db->join('historiasclinicas hc','hc.historiasclinicasid=dhc.historiasclinicasid');
         $this->db->where('hc.historiasclinicasid',$id); 
		 if(isset($_POST["search"]["value"])) {
            $this->db->group_start();
			$this->db->or_like("p.personasnombrecompuesto",strtoupper($_POST["search"]["value"]));
			$this->db->or_like("es.especialidadesnombre",strtoupper($_POST["search"]["value"]));
            $this->db->group_end();
		 }

		 if (isset($_POST['order'])) {
		 	$this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		 }
		 else{
		 	$this->db->order_by("dhc.historiasclinicasid","DESC");
		 }    
    }

    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);
         $this->db->join('pacientes pa','pa.pacientesid=hc.pacientesid');
         $this->db->join('personas p','p.personasid=pa.personasid');
         if(isset($_POST["search"]["value"])) {
            $this->db->or_like("personasnombrecompuesto",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("personasnrodocumento",strtoupper($_POST["search"]["value"]));
         } 
         if (isset($_POST['order'])) {
            $this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
         }
         else{
            $this->db->order_by("historiasclinicasid","DESC");
         }    
    }



    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);	
        }
        
        $query = $this->db->get();
        return $query->result();
    }

    function get_datatables2($id)
    {
        $ident = $id;
        $this->_get_datatables_query2($ident);
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);    
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_filtered2($id)
    {
        $this->_get_datatables_query2($id);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function count_all2()
    {
        $this->db->select("*");
        $this->db->from($this->table2);
        return $this->db->count_all_results();
    }

    public function Pacientes($DNI){
        $this->db->select('personasnrodocumento');
        $this->db->from('pacientes pa');
        $this->db->join('personas p','pa.personasid=p.personasid');
        $this->db->like("personasnrodocumento",strtoupper($DNI));
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null; 
    }


    public function PacientesCarga($DNI){
        $this->db->select("pa.pacientesid,p.personasnombrecompuesto ||' - '|| p.personasnrodocumento as nombredocumento,personasfechanacimiento");
        $this->db->from('pacientes pa');
        $this->db->join('personas p','pa.personasid=p.personasid');
        $this->db->like("p.personasnrodocumento",strtoupper($DNI));
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null; 
    }

   

    public function Profesionales(){
        $this->db->select("pro.profesionalesid,'Dr. '||p.personasnombrecompuesto||' - ' ||pro.profesionalesmatricula as profesional");
        $this->db->from('profesionales pro');
        $this->db->join('personas p', 'pro.personasid=p.personasid');
        $this->db->order_by("p.personasnombrecompuesto","ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    }




    public function Especialidades($idprof)
    {
        $this->db->select("e.especialidadesid,e.especialidadesnombre,ep.especialidadesxprofesionalesid,ep.estadosid");
        $this->db->from('especialidades e');
        $this->db->join('especialidadesxprofesionales ep', 'e.especialidadesid=ep.especialidadesid');
        $this->db->where('ep.profesionalesid',$idprof);
        $this->db->order_by("e.especialidadesnombre","ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    
    }

    public function Telefonos($DNI)
    {
        $this->db->select("t.telefonosid, tt.tipotelefonosnombre || ' : ' || t.telefononumero as telefonos" );
        $this->db->from('tipotelefonos tt');
        $this->db->join('telefonos t','t.tipotelefonosid=tt.tipotelefonosid');
        $this->db->join('personas p','p.personasid=t.personasid');
        $this->db->where('p.personasnrodocumento',$DNI);
        $this->db->order_by("tt.tipotelefonosnombre","ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    
    }

    public function ObraSocial($DNI)
    {
        $this->db->select("os.obrasocialid , os.obrasocialnombre" );
        $this->db->from('obrasocial os');
        $this->db->join('obrasocialpacientes osp','os.obrasocialid=osp.obrasocialid');
        $this->db->join('pacientes pa','pa.pacientesid=osp.pacientesid');
        $this->db->join('personas per','per.personasid=pa.personasid');
        $this->db->where('per.personasnrodocumento',$DNI);
        $this->db->order_by("os.obrasocialnombre","ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    
    }

    public function insertHC($data){
        $this->db->insert('historiasclinicas', $data);
    }

    public function insertDHC($data){
        $this->db->insert('detallehistoriasclinicas', $data);
    }

    public function updateHC($data,$id)
    {
       $this->db->where('historiasclinicasid', $id);
       $this->db->update('historiasclinicas', $data);
    }
    
    public function buscarHC($id)
    {
        $this->db->select("hc.historiasclinicasid,hc.historiasclinicasfechaalta,p.pacientesid,per.personasnombrecompuesto,per.personasnrodocumento,s.sexonombre,per.personasfechanacimiento,hc.historiasclinicasprocedenciapaciente,hc.historiasclinicasnumeroospaciente,hc.historiasclinicastitularospaciente,hc.historiasclinicasocupacionpaciente,hc.historiasclinicasderivadopaciente,hc.historiasclinicasmadrepaciente,hc.historiasclinicasocupacionmadrepaciente,hc.historiasclinicaspadrepaciente,hc.historiasclinicasocupacionpadrepaciente");
        $this->db->from('personas per');
        $this->db->join('sexo s', 'per.sexoid=s.sexoid');
        $this->db->join('pacientes p', 'per.personasid=p.personasid');
        $this->db->join('historiasclinicas hc', 'p.pacientesid=hc.pacientesid');
        $this->db->where('p.pacientesid',$id);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    }

    public function buscarPacientexIdHC($idHC)
    {
        $this->db->select('pa.pacientesid');
        $this->db->from('pacientes pa');
        $this->db->join('historiasclinicas hc', 'pa.pacientesid=hc.pacientesid');
        $this->db->where('hc.historiasclinicasid',$idHC);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    }

    public function contarHC($dni)
    {
        $this->db->select("count(hc.historiasclinicasid) as cantidad");
        $this->db->from('personas per');
        $this->db->join('sexo s', 'per.sexoid=s.sexoid');
        $this->db->join('pacientes p', 'per.personasid=p.personasid');
        $this->db->join('historiasclinicas hc', 'p.pacientesid=hc.pacientesid');
        $this->db->where('per.personasnrodocumento',$dni);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        return null;
    }


}

?>