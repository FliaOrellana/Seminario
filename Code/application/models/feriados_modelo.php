<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Feriados_modelo extends CI_Model{


	var $table = 'fn_feriados()';
	var $select_column=  array('id','nombreferiado','fechaferiado');
	var $orden_columna = array('id','nombreferiado');
	

	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    
    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);
          
		 if (isset($_POST['order'])) {
		 	$this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		 }
		 else{
		 	$this->db->order_by("id");
		 }
    }


    function get_datatables(){

        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);	
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

   
    public function registrarFeriados($data)
    {
        $this->db->insert('feriados', $data);
    }
    public function diasFeriados()
    {
        $nombre = array('diasid','dia','mes','anio');
        $this->db->select($nombre);
        $this->db->from('feriados');
        $this->db->where('estadosid', 1);
        $this->db->order_by('feriadosid','ASC');
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;
    }

    public function obtieneDatosFeriados($id)
    {
        $select_columna = array('feriadosid','feriadosdescripcion','feriadosfecha');
        $this->db->select($select_columna);
        $this->db->from('feriados');
        $this->db->where('feriadosid',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
        }
        return null; 
    }

    public function modificarFeriados($data,$id)
    {
        $this->db->where('feriadosid', $id);
        $this->db->update('feriados', $data);
    }

    public function bajaFeriados($id)
    {
        $this->db->where('feriadosid', $id);
        $this->db->delete('feriados');
    }
}

?>