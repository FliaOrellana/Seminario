<?php  
    defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Licencias_modelo extends CI_Model{


   
    var $select_column=  array('licenciasids','fechainicio','fechafin','especialidades');
    var $orden_columna = array(null,'fechainicio','fechafin','especialidades');

    


    public function __construct(){
        parent::__construct();
        $this->load->database();
    }


    public function licencias_query($idProf){    
         $this->db->select($this->select_column);
         $this->db->from('fn_mostrar_licencias('.$idProf.')');
         
         if(isset($_POST["search"]["value"])) {
            
            $this->db->like("especialidades",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("fechainicio",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("fechafin",strtoupper($_POST["search"]["value"]));
         } 
         if (isset($_POST['order'])) {
            $this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
         }
         
         
    }
    

    function get_datatables($idProf)
    {
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);    
        }
        $this->licencias_query($idProf);

        $query = $this->db->get();
        return $query->result();
    }

    function obtieneLicencias($idProf)
    {
        
        $this->db->select($this->select_column);
        $this->db->from('fn_mostrar_licencias('.$idProf.')');

        $query = $this->db->get();
        return $query->result();
    }

    function obtieneUnaFilaLicencia($idProf,$licenciasids){
        $this->db->select($this->select_column);
        $this->db->from('fn_mostrar_licencias('.$idProf.')');
        $this->db->where('licenciasids',$licenciasids);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($idProf)
    {
        
        $this->licencias_query($idProf);
        $query = $this->db->get();
        return $query->num_rows();
    }


 
    public function count_all($table)
    {
    	$this->db->select("*");
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function registrarLicencias($data)
    {
        $this->db->insert('licencias', $data);
    }

    public function modificarLicencias($data,$id)
    {
       $this->db->where('licenciasid', $id);
       $this->db->update('licencias', $data);
    }

    public function validarLicencia($idProf, $fini, $ffin){
        $this->db->select("*");
        $this->db->from('licencias');
        $this->db->where('profesionalesid',$idProf);
        $this->db->where('estadosid',1);
        $this->db->where("(licenciasfechaini between '".$fini."' and '".$ffin."') and (licenciasfechafin between '".$fini."' and '".$ffin."')");
        $query = $this->db->get();

        if ($query->num_rows() == 0){
            return false;
        }else{
            return true;
        }

    }

    public function registrarLicenciasxDia($data)
    {
        $this->db->insert('licenciasxdias', $data);
    }

    public function eliminarLicenciasxDia($id)
    {
       $this->db->where('licenciasid', $id);
       $this->db->delete('licenciasxdias');
    }

    public function diasLicencias($idProf, $idEsp)
    {
        $nombre = array('ld.diasid','ld.dia','ld.mes','ld.anio');
        $this->db->select($nombre);
        $this->db->from('licencias l');
        $this->db->join('licenciasxdias ld','l.licenciasid=ld.licenciasid');
        $this->db->where('l.profesionalesid', $idProf);
        $this->db->where('l.especialidadesid', $idEsp);
        $this->db->where('l.estadosid', 1);
        $this->db->order_by('l.licenciasid','ASC');
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;
    }
}

?>