<?php  
    defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Vacaciones_modelo extends CI_Model{


   
    var $select_column=  array('vacacionesids','fechainicio','fechafin','especialidades');
    var $orden_columna = array(null,'fechainicio','fechafin','especialidades');

    


    public function __construct(){
        parent::__construct();
        $this->load->database();
    }


    public function vacaciones_query($idProf){    
         $this->db->select($this->select_column);
         $this->db->from('fn_mostrar_vacaciones('.$idProf.')');
         
         if(isset($_POST["search"]["value"])) {
            
            $this->db->like("especialidades",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("fechainicio",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("fechafin",strtoupper($_POST["search"]["value"]));
         } 
         if (isset($_POST['order'])) {
            $this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
         }
         
         
    }
    

    function get_datatables($idProf)
    {
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);    
        }
        $this->vacaciones_query($idProf);

        $query = $this->db->get();
        return $query->result();
    }

    

    function count_filtered($idProf)
    {
        
        $this->vacaciones_query($idProf);
        $query = $this->db->get();
        return $query->num_rows();
    }


 
    public function count_all($table)
    {
    	$this->db->select("*");
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function obtieneVacaciones($idProf)
    {
        
        $this->db->select($this->select_column);
        $this->db->from('fn_mostrar_Vacaciones('.$idProf.')');

        $query = $this->db->get();
        return $query->result();
    }

    function obtieneUnaFilaVacaciones($idProf,$vacacionesids){
        $this->db->select($this->select_column);
        $this->db->from('fn_mostrar_vacaciones('.$idProf.')');
        $this->db->where('vacacionesids',$vacacionesids);
        $query = $this->db->get();
        return $query->result();
    }
    public function registrarVacaciones($data)
    {
        $this->db->insert('vacaciones',$data);
    }

    public function registrarVacacionesxDia($data)
    {
        $this->db->insert('vacacionesxdias', $data);
    }

    public function modificarVacaciones($data,$id)
    {
       $this->db->where('vacacionesid', $id);
       $this->db->update('vacaciones', $data);
    }

    public function eliminarVacacionesxDia($id)
    {
       $this->db->where('vacacionesid', $id);
       $this->db->delete('vacacionesxdias');
    }

    public function diasVacaciones($idProf, $idEsp)
    {
        $nombre = array('vd.diasid','vd.dia','vd.mes','vd.anio');
        $this->db->select($nombre);
        $this->db->from('vacaciones v');
        $this->db->join('vacacionesxdias vd','v.vacacionesid=vd.vacacionesid');
        $this->db->where('v.profesionalesid', $idProf);
        $this->db->where('v.especialidadesid', $idEsp);
        $this->db->where('v.estadosid', 1);
        $this->db->order_by('v.vacacionesid','ASC');
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;
    }
}

?>