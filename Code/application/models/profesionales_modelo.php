<?php  
    defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Profesionales_modelo extends CI_Model{


    var $table = 'profesionales pf';
    var $select_column=  array('pf.profesionalesid','profesionalesmatricula','personasnrodocumento','personasnombrecompuesto','especialidadesnombre','estadosnombre','e.especialidadesid');
    var $orden_columna = array(null,'profesionalesmatricula','personasnrodocumento','personasnombrecompuesto','especialidadesnombre','estadosnombre');
    




    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);
         $this->db->join('personas p','pf.personasid=p.personasid');
         $this->db->join('especialidadesxprofesionales ep','pf.profesionalesid=ep.profesionalesid');
         $this->db->join('especialidades e','ep.especialidadesid=e.especialidadesid');
         $this->db->join('estados es','ep.estadosid=es.estadosid');
         if(isset($_POST["search"]["value"])) {
            $this->db->or_like("profesionalesmatricula",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("personasnrodocumento",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("personasnombrecompuesto",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("especialidadesnombre",strtoupper($_POST["search"]["value"]));
             $this->db->or_like("estadosnombre",strtoupper($_POST["search"]["value"]));
         } 
         if (isset($_POST['order'])) {
            $this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
         }
         else{
            $this->db->order_by("p.personasapellido","ASC");
         }

    
    }



    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);    
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join('personas p','pf.personasid=p.personasid');
        $this->db->join('especialidadesxprofesionales ep','pf.profesionalesid=ep.profesionalesid');
        $this->db->join('especialidades e','ep.especialidadesid=e.especialidadesid');
        return $this->db->count_all_results();
    }

    public function registrarProfesional($data2){
        $this->db->insert('profesionales', $data2);
    }

    public function registrarEspecialidadesxProfesionales($data2){
        $this->db->insert('especialidadesxprofesionales', $data2);
    }

    public function buscarProfesional($matricula){
        $nombres=array('profesionalesid',
                       'profesionalesmatricula',
                       'usuariosid',
                       'intervalosid');
        $this->db->select($nombres);
        $this->db->from('profesionales');
        $this->db->where('profesionalesmatricula',$matricula);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
     }
     return null; 


    }

    public function obtieneDatosProfesional($id)
    {
         $this->db->select('p.*,pr.profesionalesid,pr.profesionalesmatricula');
         $this->db->from('profesionales pr');
         $this->db->join('personas p','p.personasid=pr.personasid');
         $this->db->where('pr.profesionalesid',$id);
         $query=$this->db->get();
         if ($query->num_rows() > 0){
             return $query->row_array();
         }
          return null; 
    }

    public function obtieneEspecialidadesProfesional($id)
    {
         $this->db->select('especialidadesid');
         $this->db->from('especialidadesxprofesionales ');
         $this->db->where('profesionalesid',$id);
         $query=$this->db->get();
         if ($query->num_rows() > 0){
             return $query->result();
         }
          return null; 
    }

    public function Especialidades(){
        $nombres=array('especialidadesid','especialidadesnombre');
        $this->db->select($nombres);
        $this->db->from('especialidades');
        $this->db->order_by('especialidadesnombre','DESC');
        $query=$this->db->get();
        return $query->result();

    }

    public function modificarProfesional($data,$id)
    {
       $this->db->where('profesionalesid', $id);
       $this->db->update('profesionales', $data);
    }

    public function borrarEspecialidadesProfesional($id)
    {
        $this->db->where('profesionalesid', $id);
        $this->db->delete('especialidadesxprofesionales');
    }

    public function cancelaProfesional($idesp,$idprof,$data){
        $this->db->where('especialidadesid',$idesp);
        $this->db->where('profesionalesid',$idprof);
        $this->db->update('especialidadesxprofesionales',$data);
    }

    public function HabilitaProfesional($idesp,$idprof,$data){
        $this->db->where('especialidadesid',$idesp);
        $this->db->where('profesionalesid',$idprof);
        $this->db->update('especialidadesxprofesionales',$data);
    }

    public function insertaAgenda($id)
    {
        $this->db->select('*');
        $this->db->from('fn_insertaagendadias('.$id.')');
        $query = $this->db->get();
        return $query->num_rows();
    }
}

?>