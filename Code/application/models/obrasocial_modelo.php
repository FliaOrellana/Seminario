<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Obrasocial_modelo extends CI_Model{


	var $table = 'obrasocial';
	var $select_column=  array('obrasocialid','obrasocialnombre','estadosid');
	var $orden_columna = array('obrasocialid','obrasocialnombre');
	

	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    
    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);
          
		 if (isset($_POST['order'])) {
		 	$this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		 }
		 else{
		 	$this->db->order_by("obrasocialid");
		 }
    }


    function get_datatables(){

        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);	
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function registrarObraSocial($data){
        $this->db->insert('obrasocial', $data);
    }

    public function obtieneDatosObraSocial($id)
    {
        $this->db->select($this->select_column);
        $this->db->from($this->table);
        $this->db->where('obrasocialid',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
        }
        return null; 
    }

    public function modificarObraSocial($data,$id)
    {
        $this->db->where('obrasocialid', $id);
        $this->db->update('obrasocial', $data);
    }

    public function bajaObraSocial($data,$id)
    {
        $this->db->where('obrasocialid',$id);
        $this->db->update('obrasocial',$data);
    }
    
    public function buscarObraSocial($data){

        $this->db->select($this->select_column);
        $this->db->from('obrasocial');
        $this->db->where('obrasocialnombre',$data);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
     }
     return null; 


    }


}

?>