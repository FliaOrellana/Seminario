<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Reportes_modelo extends CI_Model{

	var $table = 'vista_reporteusuarios vu'; 
	var $select_column =  array('vu.usuariosid','vu.usuariosnombre','vu.personasnombrecompuesto','vu.perfilesnombre','e.estadosnombre');

    var $select_column1 = array('e.especialidadesid','e.especialidadesnombre','pa.estadosid');
    
	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function get_pacientes($estadosid){
    	$this->db->select('pa.pacientesid,p.personasapellido,p.personasnombre,p.personasnrodocumento,p.personasdomicilio,t.telefononumero');
         $this->db->from('pacientes pa');
         $this->db->join('personas p','p.personasid=pa.personasid');
         $this->db->join('telefonos t','t.personasid=p.personasid');
         $this->db->where('pa.estadosid',$estadosid);
         $query=$this->db->get();
         if ($query->num_rows() > 0){
             return $query->result();
         }
          return null; 
    }

    public function get_Turnos($idProf,$idEsp)
    {
        $this->db->select('*');
        $tabla = 'fn_reporteturnosdiariosxespecialidad('.$idProf.','.$idEsp.')';
        $this->db->from($tabla);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;

    }

    public function cantidadEspecialidad($idProf)
    {
        $this->db->select('count(*) as cantidad');
        $this->db->from('profesionales p');
        $this->db->join('especialidadesxprofesionales ep','p.profesionalesid = ep.profesionalesid');
        $this->db->where('p.profesionalesid',$idProf);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;

    }

    public function get_cantidadPacientesxOS()
    {
        $this->db->select('os.obrasocialid, os.obrasocialnombre, count(pacientesid) as cantPac');
        $this->db->from('obrasocial os');
        $this->db->join('obrasocialpacientes osp','os.obrasocialid = osp.obrasocialid');
        $this->db->group_by('os.obrasocialid');
        
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;
    }

    public function getCantidadAusentes($id,$fi,$ff)
    {
        $this->db->select('fn_reporteausente as cantidad');
        $this->db->from('fn_reporteAusente('.$id.",'".$fi."','".$ff."')");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;

    }

    public function getPacientesAusentes($id,$fi,$ff)
    {
        $this->db->select('*');
        $this->db->from('fn_reportepacientesausentes('.$id.",'".$fi."','".$ff."')");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;

    }


     public function getCantidadCancelados($id,$fi,$ff)
    {
        
      $this->db->select('fn_reportecancelados as cantidad');
        $this->db->from('fn_reportecancelados('.$id.",'".$fi."','".$ff."')");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;
    }

    public function getPacientesTurnosCancelados($id,$fi,$ff)
    {
        $this->db->select('*');
        $this->db->from('fn_reporteturnoscancelados('.$id.",'".$fi."','".$ff."')");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;

    }

    public function get_dataxmes($idProf, $mes, $year)
    {
        $this->db->select('*');
        $tabla = 'fn_reporteEstidisticoxMes('.$idProf.','.$mes.','.$year.')';
        $this->db->from($tabla);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;
    }

    public function get_dataxdia($idProf, $mes, $year,$cant)
    {
        $this->db->select('*');
        $tabla = 'fn_reportepacientexdias('.$idProf.','.$mes.','.$year.','.$cant.')';
        $this->db->from($tabla);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
         return null;
    }

    public function get_usuarios(){
    	$this->db->select($this->select_column);
    	$this->db->from($this->table);
    	$this->db->join('estados e', 'vu.estadosid=e.estadosid');
    	$query=$this->db->get();
         if ($query->num_rows() > 0){
             return $query->result();
         }
          return null; 
    }

    public function get_historiaclinica($nrodni,$espid){
         
         $this->db->select('*');
         $this->db->from('fn_reporte_HC('."'".$nrodni."',".$espid.")");
         $query=$this->db->get();
         if ($query->num_rows() > 0){
             return $query->result();
         }
          return null; 
    }

    

    public function get_especialidades($nrodni){
        $this->db->distinct();
        $this->db->select($this->select_column1);
        $this->db->from('especialidades e');
        $this->db->join('detallehistoriasclinicas dhc','e.especialidadesid=dhc.especialidadesid');
        $this->db->join('historiasclinicas hc','hc.historiasclinicasid=dhc.historiasclinicasid');
        $this->db->join('pacientes pa','hc.pacientesid=pa.pacientesid');
        $this->db->join('personas per','pa.personasid=per.personasid');
        $this->db->where('per.personasnrodocumento',$nrodni);

        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        return null;
    }
}

?>