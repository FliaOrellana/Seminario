<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Pacientes_modelo extends CI_Model{


	var $table = 'pacientes pa';
	var $select_column=  array('pacientesid','personasapellido','personasnombre','personasnrodocumento','personasdomicilio','estadosnombre');
	var $orden_columna = array(null,'personasapellido','personasnombre','personasnrodocumento','personasdomicilio','sestadosnombre');
	




	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function Sexo(){
        $nombres=array('sexoid','sexonombre');
        $this->db->select($nombres);
        $this->db->from('sexo');
        $this->db->order_by('sexonombre','DESC');
        $query=$this->db->get();
        return $query->result();

    }

    public function TipoTelefono(){
        $nombres  = array('tipotelefonosid', 'tipotelefonosnombre' );
        $this->db->select($nombres);
        $this->db->from('tipotelefonos');
        $this->db->order_by('tipotelefonosnombre','DESC');
        $query=$this->db->get();
        return $query->result();
    }

    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);
         $this->db->join('personas p','pa.personasid=p.personasid');
		 $this->db->join('estados e','pa.estadosid=e.estadosid');
		 if(isset($_POST["search"]["value"])) {
		 	$this->db->like("personasapellido",strtoupper($_POST["search"]["value"]));
			$this->db->or_like("personasnombre",strtoupper($_POST["search"]["value"]));
			$this->db->or_like("personasnrodocumento",strtoupper($_POST["search"]["value"]));
			$this->db->or_like("personasdomicilio",strtoupper($_POST["search"]["value"]));
			$this->db->or_like("estadosnombre",strtoupper($_POST["search"]["value"]));
            //$this->db->like("personastelefono",strtoupper($_POST["search"]["value"]));
		 } 
		 if (isset($_POST['order'])) {
		 	$this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		 }
		 else{
		 	$this->db->order_by("pacientesid","DESC");
		 }

    
    }

    private function _get_datatables_query_telefono($id){
         
         $this->db->select('t.telefonosid,tt.tipotelefonosid,tt.tipotelefonosnombre,t.telefononumero');
         $this->db->from('tipotelefonos tt');
         $this->db->join('telefonos t','t.tipotelefonosid=tt.tipotelefonosid');
         $this->db->where('t.personasid',$id);
         if(isset($_POST["search"]["value"])) {
            $this->db->group_start();
            $this->db->or_like("tipotelefonosnombre",strtoupper($_POST["search"]["value"]));
            $this->db->or_like("telefononumero",strtoupper($_POST["search"]["value"]));
            $this->db->group_end();
         } 
         if (isset($_POST['order'])) {
            $orden_columna = array(null,'tt.tipotelefonosnombre','t.telefononumero');
            $this->db->order_by($orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
         }
         else{
            $this->db->order_by("tipotelefonosnombre","DESC");
         }

    
    }

     private function _get_datatables_query_obra($id){
         
         $this->db->select('o.obrasocialid,o.obrasocialnombre');
         $this->db->from('obrasocial o');
         $this->db->join('obrasocialpacientes op','o.obrasocialid=op.obrasocialid');
         $this->db->join('pacientes p','op.pacientesid=p.pacientesid');
         $this->db->where('p.personasid',$id);
         if(isset($_POST["search"]["value"])) {
            $this->db->group_start();
            $this->db->or_like("o.obrasocialnombre",strtoupper($_POST["search"]["value"]));
            $this->db->group_end();
         } 
         if (isset($_POST['order'])) {
            $orden_columna = array(null,'obrasocialnombre');
            $this->db->order_by($orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
         }
         else{
            $this->db->order_by("o.obrasocialnombre","DESC");
         }

    
    }



    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);	
        }
        
        $query = $this->db->get();
        return $query->result();
    }

    function get_datatables_telefono($id)
    {
        $this->_get_datatables_query_telefono($id);
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);    
        }
        
        $query = $this->db->get();
        return $query->result();
    }

    function get_datatables_obra($id)
    {
        $this->_get_datatables_query_obra($id);
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);    
        }
        
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_filtered_telefono($id)
    {
        $this->_get_datatables_query_telefono($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_filtered_obra($id)
    {
        $this->_get_datatables_query_obra($id);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function count_all_telefono($id)
    {
        $this->db->select("*");
        $this->db->from('tipotelefonos tt');
        $this->db->join('telefonos t','t.tipotelefonosid=tt.tipotelefonosid');
        $this->db->where('t.personasid',$id);
        return $this->db->count_all_results();
    }

    public function count_all_obra($id)
    {
        $this->db->select("*");
        $this->db->from('obrasocial o');
         $this->db->join('obrasocialpacientes op','o.obrasocialid=op.obrasocialid');
         $this->db->join('pacientes p','op.pacientesid=p.pacientesid');
         $this->db->where('p.personasid',$id);
        return $this->db->count_all_results();
    }

    public function registrarPersona($data){
        $this->db->insert('personas', $data);
    }

    public function registrarTelefonoPaciente($data)
    {
        $this->db->insert('telefonos',$data);
    }

    public function registrarObraSocialPaciente($data)
    {
        $this->db->insert('obrasocialpacientes',$data);
    }

    public function modificarPersona($data,$id)
    {
       $this->db->where('personasid', $id);
       $this->db->update('personas', $data);
    }

    public function registrarPaciente($data2){
        $this->db->insert('pacientes', $data2);
    }
    
    public function modificarPaciente($data, $id)
    {
        $this->db->where('pacientesid', $id);
        $this->db->update('pacientes', $data);
    }

    public function obtienePaciente($id)
    {
         
         $this->db->select('p.*,pa.pacientesid,pa.estadosid');
         $this->db->from('pacientes pa');
         $this->db->join('personas p','p.personasid=pa.personasid');
         $this->db->where('pa.pacientesid',$id);
         $query=$this->db->get();
         if ($query->num_rows() > 0){
             return $query->row_array();
         }
          return null; 
        
    }
    public function buscarPaciente($id)
    {
        $this->db->select('pa.pacientesid,pa.estadosid');
         $this->db->from('pacientes pa');
         $this->db->join('personas p','p.personasid=pa.personasid');
         $this->db->where('p.personasid',$id);
         $query=$this->db->get();
         if ($query->num_rows() > 0){
             return $query->row_array();
         }
          return null; 
    }
    public function eliminarTelefonoPaciente($idPa,$idTel)
    {
        $this->db->where('personasid', $idPa);
        $this->db->where('telefonosid', $idTel);
        $this->db->delete('telefonos');
    }


    public function eliminarObraSocialPaciente($idPa,$idOS)
    {
        $this->db->where('pacientesid', $idPa);
        $this->db->where('obrasocialid', $idOS);
        $this->db->delete('obrasocialpacientes');
    }

    public function buscarPersona($dni){
        $nombres=array('personasid',
                       'personasnombre',
                       'personasapellido',
                       'personasnombrecompuesto',
                       'personasnrodocumento',
                       'personasdomicilio',
                       'personasemail');
        $this->db->select($nombres);
        $this->db->from('personas');
        $this->db->where('personasnrodocumento',$dni);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
     }
     return null; 


    }

    public function ObraSociales(){
        $nombres=array('obrasocialid','obrasocialnombre');
        $this->db->select($nombres);
        $this->db->from('obrasocial');
        $this->db->order_by('obrasocialnombre','DESC');
        $query=$this->db->get();
        return $query->result();

    }

    public function modalPaciente($id)
    {
       $this->db->from('fn_modalpaciente('.$id.')');
       $query=$this->db->get();
       if ($query->num_rows() > 0){
            return $query->row_array();
       }
       return null; 
    }

    public function ActualizaEstado(){
        $this->db->select('*');
        $this->db->from('fn_cambia_estado_paciente()');
        $query=$this->db->get();
        if ($query->num_rows() > 0){
             return $query->row_array();
        }
        return null;
    }

    public function getTelefono($idpac,$idTel)
    {
        $select = array('tipotelefonosid','telefononumero');
        $this->db->select($select);
        $this->db->from('telefonos');
        $this->db->where('telefonosid',$idTel);
        $this->db->where('personasid',$idpac);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
             return $query->row_array();
        }
        return null;
    }
    
    
}

?>