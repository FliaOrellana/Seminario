<?php  
	defined('BASEPATH') OR exit('No direct script access allowed/
No se permite el acceso directo a guiones');

class Intervalo_modelo extends CI_Model{


	var $table = 'intervalos i';
	var $select_column=  array('intervalosid','intervalostiempo');
	var $orden_columna = array('intervalosid','intervalostiempo');
	

	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    
    private function _get_datatables_query(){
         
         $this->db->select($this->select_column);
         $this->db->from($this->table);
          
		 if (isset($_POST['order'])) {
		 	$this->db->order_by($this->orden_columna[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		 }
		 else{
		 	$this->db->order_by("intervalosid");
		 }
    }


    function get_datatables(){

        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);	
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function registrarIntervalos($data){
        $this->db->insert('intervalos', $data);
    }

    public function obtieneDatosIntervalo($id)
    {
        $this->db->select($this->select_column);
        $this->db->from($this->table);
        $this->db->where('intervalosid',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
        }
        return null; 
    }

    public function modificarIntervalo($data,$id)
    {
        $this->db->where('intervalosid', $id);
        $this->db->update('intervalos', $data);
    }
    
    public function buscarIntervalo($data){

        $this->db->select($this->select_column);
        $this->db->from('intervalos');
        $this->db->where('intervalostiempo',$data);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->row_array();
     }
     return null; 


    }

    public function bajaIntervalo($id)
    {
        $this->db->where('intervalosid',$id);
        $this->db->delete('intervalos');
    }


}

?>