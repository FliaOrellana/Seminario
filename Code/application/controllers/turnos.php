<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Turnos extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('turnos_modelo');
        $this->load->model('agenda_modelo');
        $this->load->model('licencias_modelo');
        $this->load->model('vacaciones_modelo');
        $this->load->model('feriados_modelo');
        $this->load->model('auditoria_modelo');
        $this->load->library('session'); 
        $this->load->helper('form');
    }
    
    public function index(){
        $actualizaTurnos = $this->turnos_modelo->ActualizaEsatdoTurno();
        $this->load->view('head_view');            
        if($this->session->userdata('logueo') == TRUE || $this->session->userdata('usuario') != '') {
            $this->load->view('turnos_view');            
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }
    public function eliminar_acentos($cadena){
		
		//Reemplazamos la A y a
		$cadena = str_replace(
		array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
		array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
		$cadena
		);
 
		//Reemplazamos la E y e
		$cadena = str_replace(
		array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
		array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
		$cadena );
 
		//Reemplazamos la I y i
		$cadena = str_replace(
		array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
		array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
		$cadena );
 
		//Reemplazamos la O y o
		$cadena = str_replace(
		array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
		array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
		$cadena );
 
		//Reemplazamos la U y u
		$cadena = str_replace(
		array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
		array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
		$cadena );
 
		//Reemplazamos la N, n, C y c
		$cadena = str_replace(
		array('Ñ', 'ñ', 'Ç', 'ç'),
		array('N', 'n', 'C', 'c'),
		$cadena
		);
		
		return $cadena;
	}
    public function enviarMailCancelarTurnos($t)
    {
        
        $com = 'python "C:/Program Files/Python36/cancelarturnoemail.py" '.$this->eliminar_acentos($t->email).' "'.$this->eliminar_acentos($t->profesional).'" "'.$this->eliminar_acentos($t->especialidad).'" "'.$this->eliminar_acentos($t->fecha).'" "'.$this->eliminar_acentos($t->horainicio).'" "'.$this->eliminar_acentos($t->horafin).'" "'.$this->eliminar_acentos($t->pacientes).'"';
        exec($com);
        redirect(base_url().'index.php/turnos');
    }

    public function cancelarTurnos($turnos)
    {
        $ids=split('-',$turnos);
        for($i=0;$i<count($ids);$i++){
            $fecha =  getdate();
             $data = array(
                'estadosturnosid' => 3,
                'turnosbaja' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']

            );

            $transaction = "update turnos set estadosturnosid = ".$data['estadosturnosid']." , turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $dataUpdated = "turnosid = ".$ids[$i].", estadosturnosid = ".$data['estadosturnosid'].", turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $data_auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transaction,
                'datosmodificados' => $dataUpdated
            );

            $this->turnos_modelo->cancelaTurno($data,$ids[$i]);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            unset($data);
        }
        $infoTurnos = $this->turnos_modelo->ObtieneInformacionTurno($turnos);
        foreach ($infoTurnos as $t) {
            $this->enviarMailCancelarTurnos($t);

        }
 
    }

    public function llegadaPaciente($turnos)
    {
        $ids=split('-',$turnos);
        for($i=0;$i<count($ids);$i++){
            $fecha =  getdate();
            $data = array(
                'estadosturnosid' => 4,
                'turnosbaja' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']

            );

            $transaction = "update turnos set estadosturnosid = ".$data['estadosturnosid']." , turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $dataUpdated = "turnosid = ".$ids[$i].", estadosturnosid = ".$data['estadosturnosid'].", turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $data_auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transaction,
                'datosmodificados' => $dataUpdated
            );

            $this->turnos_modelo->llegadaPaciente($data,$ids[$i]);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            unset($data);
        }
        redirect(base_url().'index.php/turnos');
    }

    public function turnoslisto($turnos)
    {
        $ids=split('-',$turnos);
        for($i=0;$i<count($ids);$i++){
            $fecha =  getdate();
             $data = array(
                'estadosturnosid' => 7,
                'turnosbaja' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']

            );

            $transaction = "update turnos set estadosturnosid = ".$data['estadosturnosid']." , turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $dataUpdated = "turnosid = ".$ids[$i].", estadosturnosid = ".$data['estadosturnosid'].", turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $data_auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transaction,
                'datosmodificados' => $dataUpdated
            );

            $this->turnos_modelo->turnoslisto($data,$ids[$i]);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            unset($data);
        }
        redirect(base_url().'index.php/turnos');
    }

    function ajax_list(){

            $list = $this->turnos_modelo->get_datatables();
            $data = array();
            $i = 0;
            foreach ($list as $turnos){

                $row = array();
                $row[] = $turnos->profesional;
                $row[] = $turnos->especialidad;
                $row[] = $turnos->fecha;
                $row[] = $turnos->horainicio;
                $row[] = $turnos->horafin;
                $row[] = $turnos->tipoturno;
                $row[] = $turnos->pacientes;
                $row[] = $turnos->estadoturno;
                if ($turnos->estadoturnoid == 3 || $turnos->estadoturnoid == 8 || $turnos->estadoturnoid == 7){
                    $row[] = '<button type="button" class="btn btn-primary" style="background:#858383; border-color:#858383;" onclick="location.href='."'".base_url()."index.php/turnos/reprogramarTurno/".$turnos->turnosid."'".'" disabled><i class="far fa-clock"></i></button> <button type="button" class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/turnos/cancelarTurnos/".$turnos->turnosid."'".'" disabled><i class="fas fa-times-circle"></i></button>   <button disabled type="button" class="btn btn-primary" style="background:#d6ff00; border-color:#d8ea59;" onclick="location.href='."'".base_url()."index.php/turnos/llegadaPaciente/".$turnos->turnosid."'".'"><i class="fas fa-bell fa "></i></button>  <button disabled type="button" class="btn btn-primary" style="background:#2a8207; border-color:#466d36;" onclick="location.href='."'".base_url()."index.php/turnos/turnoslisto/".$turnos->turnosid."'".'"><i class="fas fa-bell fa "></i></button>';
                }else{

                     $row[] = '<button type="button" data-toggle="tooltip" title="Reprogramar el Turno" class="btn btn-primary" style="background:#858383; border-color:#858383;" onclick="location.href='."'".base_url()."index.php/turnos/reprogramarTurno/".$turnos->turnosid."'".'"><i class="far fa-clock "></i></button>    <button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" ><i data-toggle="tooltip" title="Cancelar Turno" class="fas fa-times-circle" ></i></button>    <button type="button" data-toggle="tooltip" title="Llegada de Paciente. Paciente en Espera" class="btn btn-primary" style="background:#d6ff00; border-color:#d8ea59;" onclick="location.href='."'".base_url()."index.php/turnos/llegadaPaciente/".$turnos->turnosid."'".'"><i class="fas fa-bell fa "></i></button>  <button type="button" data-toggle="tooltip" title="Paciente Atendido. Listo!" class="btn btn-primary" style="background:#2a8207; border-color:#466d36;" onclick="location.href='."'".base_url()."index.php/turnos/turnoslisto/".$turnos->turnosid."'".'"><i class="fas fa-bell fa "></i></button>'.
                     '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																				<div class="modal-dialog modal-lg" role="document">
																					<div class="modal-content">
																						<div class="modal-header" >
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">&times;</span>
																							</button>
																							
																						</div>
																						<div class="modal-body">
																							<label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
																						</div>
																						<div class="row">		
																							<div class="col-sm-3">
																								
																							</div>
																							<div class="col-sm-3">
																								<button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/turnos/cancelarTurnos/".$turnos->turnosid."'".'">Si</button>
																							</div>
																							<div class="col-sm-3">
																							<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
																								
																							</div>
																							
																							<div class="col-sm-3">
																								
																							</div>
																						</div>
																						<br><br>
																										
																					</div>
																				</div>
																			</div>';
                }
               

                $data[] = $row;
                $i = $i + 1;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->turnos_modelo->count_all(),
                            "recordsFiltered" => $this->turnos_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
        }

    public function CerrarSesion(){
        $this->session->sess_destroy();
        redirect(base_url().'index.php/login');
    }
    
    public function registrarTurnos(){
        $list['profesionales'] = $this->turnos_modelo->Profesionales();
        $list['turnos'] = $this->turnos_modelo->TipoTurnos();
        $list['dni'] = '';
        $list['pacienteid'] = '';
        $list['pacientenombre'] = '';
        $list['profesionalid'] = '0';
        $list['especialidadid'] = '0';
        $list['tipoturnosid'] = '0';
        $list['especialidadesxprofesionalesid'] = '0';
        $list['fecha'] = date('D M d Y H:i:s \G\M\TO (T)');
        $list['mensaje'] = '';
        $list['hidden'] = " style='display:none;'";
        $list['hiddenTabla'] = " style='display:none;'";
        $this->load->view('registrarTurno_view',$list);
    }

    public function reprogramarTurno($ids)
    {
        //$list['profesionales'] = $this->turnos_modelo->Profesionales();
        $list['id'] = $ids;
        $ids = explode("-",$ids);
        $idTurno = $ids[0];
        $list['turnos'] = $this->turnos_modelo->TipoTurnos();
        $list['datospaciente'] = $this->turnos_modelo->buscaPacientexidTurno($idTurno);
        $list['datosprofesional'] = $this->turnos_modelo->buscaProfesionalxidTurno($idTurno);
        $list['pacienteid'] = '';
        $list['pacientenombre'] = '';
        $list['profesionalid'] = $list['datosprofesional'][0]->profesionalesid;
        $list['especialidadid'] = $list['datosprofesional'][0]->especialidadesid;
        $list['dias'] = $this->agenda_modelo->diasagenda($list['datosprofesional'][0]->profesionalesid,$list['datosprofesional'][0]->especialidadesid);
        $list['licencias'] = $this->licencias_modelo->diasLicencias($list['datosprofesional'][0]->profesionalesid,$list['datosprofesional'][0]->especialidadesid);
        $list['vacaciones'] = $this->vacaciones_modelo->diasVacaciones($list['datosprofesional'][0]->profesionalesid,$list['datosprofesional'][0]->especialidadesid);
        $list['feriados'] = $this->feriados_modelo->diasFeriados();
        $list['tipoturnosid'] = '0';
        $list['especialidadesxprofesionalesid'] = '0';
        $list['fecha'] = date('D M d Y H:i:s \G\M\TO (T)');
        $list['mensaje'] = '';
        
        $this->load->view('reprogramarTurno_view',$list);
    }

    public function CargaTablaRepTurno($ids){
        $list['id'] = $ids;
        $idPac = $this->input->post('inputId');
        $ids = explode("-",$ids);
        $idTurno = $ids[0];
        $list['turnos'] = $this->turnos_modelo->TipoTurnos();
        $list['datospaciente'] = $this->turnos_modelo->buscaPacientexidTurno($idTurno);
        $list['datosprofesional'] = $this->turnos_modelo->buscaProfesionalxidTurno($idTurno);
        
        $list['dni'] =  $this->input->post('inputDNI');
        $list['pacienteid'] =  $this->input->post('inputId');
        $list['pacientenombre'] =  $this->input->post('inputNombreCompuesto');
        $idProf = $this->input->post('inputProfecionalID');
        $list['profesionalid'] = $idProf;
        $idEsp =  $this->input->post('inputEspecialidadID');
        $list['especialidadid'] = $idEsp;
        $list['tipoturnosid'] =  $this->input->post('inputTipoTurno');
        $tipoturno = $list['tipoturnosid'];
        $fecha =  $this->input->post('fechaN');
        $d = explode("/",$this->input->post('fechaN'));
        $d = $d[2]."-".$d[1]."-".$d[0];
        $iddia = date("w",strtotime($d));
        $list['fecha'] = $fecha;
        $list['intervalo'] = $this->agenda_modelo->obtieneIntervalo($idProf,$idEsp,$iddia);
        $list['profesionales'] = $this->input->post('inputProfesional');
        $list['especialidades'] = $this->input->post('inputEspecialidades');
        $list['turnos'] = $this->turnos_modelo->TipoTurnos();
        $list['mensajeDNI'] ='';
        $list['dias'] = $this->agenda_modelo->diasagenda($list['datosprofesional'][0]->profesionalesid,$list['datosprofesional'][0]->especialidadesid);
        $list['licencias'] = $this->licencias_modelo->diasLicencias($idProf,$idEsp);
        $list['vacaciones'] = $this->vacaciones_modelo->diasVacaciones($idProf,$idEsp);
        $list['feriados'] = $this->feriados_modelo->diasFeriados();
        $bd=0;

        if (($idProf == 0) || ($idEsp == 0) || ($tipoturno == 0)){
            $idProf="null";
            $idEsp="null";
        }
        if ( $this->input->post('inputTipoTurno') == 1){
            $table = "fn_agendaturnonormales(".$idProf.",'".$fecha."',".$idEsp.")"; 
            $list['agenda'] = $this->agenda_modelo->get_datatables($table);
        }else{
            $table = "fn_agendasobreturnos(".$idProf.",'".$fecha."',".$idEsp.")"; 
            $list['agenda'] = $this->agenda_modelo->get_datatables($table);
        }
        

        $b=0;
        $cont=0;
        for($i=1;$i<=79;$i++){
            if(empty($this->input->post("$i"))&&($b==0)){
                $b=0;
            }elseif (empty($this->input->post("$i"))&&($b==1)) {
                $b= 0;
                $cont++;
            }elseif (!empty($this->input->post("$i"))&&($b==0)) {
                $b= 1;
                $cont++;
            }elseif (!empty($this->input->post("$i"))&&($b==1)) {
                $b= 1;
            }     
        }

       if (isset($_POST['Buscar'])) {
          if (empty($this->input->post('inputDNI'))){
                $list['mensajeDNI'] = "El campo DNI es obligatorio";
                $bd = 1;
            }elseif (!$this->esnumerico($this->input->post('inputDNI'))) {
                $list['mensajeDNI']= "El DNI debe ser un valor Numérico";
                $bd =1;
            }
        } 
        
        

        if (isset($_POST['registrar'])){
            if (!$this->esnumerico($this->input->post('inputDNI')) || empty($this->input->post('inputDNI'))) {
               $list['mensaje']="Debe seleccionar un Paciente para registrar el turno!";
            } else {
               if ($cont>2){
                $list['mensaje']="No puede haber espacios de tiempo en los turnos elegidos!";
                }elseif ($cont== 0){
                    $list['mensaje']="Debe seleccionar un horario para registrar el turno!";

                }else{

                    for ($j=1;$j<=79;$j++){
                        if(!empty($this->input->post("$j"))){
                         
                         $data = array(
                            'tipoturnosid' =>$this->input->post('inputTipoTurno'),
                            'profesionalesid' => $list['datosprofesional'][0]->profesionalesid ,
                            'especialidadesid' => $this->input->post('inputEspecialidadID'),
                            'turnosfecha' =>  $list['fecha'],
                            'turnoshoratiempo'  => trim($this->input->post("$j").':00'),
                            'pacientesid'  => $list['pacienteid']
                        );

                        $transaction = "insert into turnos (tipoturnosid, profesionalesid, especialidadesid, turnosfecha, turnoshoratiempo, pacientesid) values ("."'".$data['tipoturnosid']."','".$data['profesionalesid']."','".$data['especialidadesid']."','".$data['turnosfecha']."','".$data['turnoshoratiempo']."','".$data['pacientesid']."')"; 

                        $dataInserted = "tipoturnosid = ".$data['tipoturnosid'].", profesionalesid = ".$data['profesionalesid'].", especialidadesid = ".$data['especialidadesid'].", turnosfecha = ".$data['turnosfecha'].", turnoshoratiempo = ".$data['turnoshoratiempo']." , pacientesid = ".$data['pacientesid'];

                        $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
                        );

                        $this->turnos_modelo->insertTurno($data);
                        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                        unset($data);
                       
                        }
                    }

                    
                    $this->cancelarTurnos($list['id']) ;  
                    
                     
                    redirect(base_url().'index.php/turnos');
                }
            }
            
            
        }else{
            $list['mensaje']="";
        }
        
        $this->load->view('reprogramarTurno_view',$list); 
        

        
}

    public function BuscarPacientes(){
        $DNI = $this->input->get('term');
        $result = $this->turnos_modelo->Pacientes($DNI);
        $data = array();
        foreach ($result as $pa){

            
            $data[] = $pa->personasnrodocumento;
           
        }
        echo json_encode($data);
        
        
    }

    public function CargaPaciente(){
        $DNI = $this->input->post('pacienteDNI');
        $result['respuesta'] = $this->turnos_modelo->PacientesCarga($DNI);
        
        
        echo json_encode($result);
    }

    public function CargaEspecialidades(){
        $idProf =  $this->input->post('profesionalesid');
        $opciones = '';
        if ($idProf == 0){
            $opciones.= "<option value ='0' >Seleccione primero el profesional</option>";
        }else{
            $list = $this->turnos_modelo->Especialidades($idProf);
            
                foreach($list as $esp){
                    if ($esp->estadosid == 1){
                        $opciones.='<option value="'.$esp->especialidadesid.'">'.$esp->especialidadesnombre.'</option>';
                    }
                }
           
        }

        
        echo $opciones;
    }

    public function esnumerico($dni){
        if (is_numeric($dni)){
            return True;
        }
        else{
            return False;
        }
    }

    public function CargaTabla(){
        $list['dni'] =  $this->input->post('inputDNI');
        $list['pacienteid'] =  $this->input->post('inputId');
        $list['pacientenombre'] =  $this->input->post('inputNombreCompuesto');
        $idProf = $this->input->post('profid');
        $list['profesionalid'] = $idProf;
        $idEsp =  $this->input->post('espid');
        $list['especialidadid'] = $idEsp;
        $list['tipoturnosid'] =  $this->input->post('inputTipoTurno');
        $tipoturno = $list['tipoturnosid'];
        $fecha =  $this->input->post('fechaN');
        $d = explode("/",$this->input->post('fechaN'));
        $d = $d[2]."-".$d[1]."-".$d[0];
        $iddia = date("w",strtotime($d));
        $list['fecha'] = $fecha;
        $list['intervalo'] = $this->agenda_modelo->obtieneIntervalo($idProf,$idEsp,$iddia);
        $list['profesionales'] = $this->turnos_modelo->Profesionales();
        $list['especialidades'] = $this->turnos_modelo->Especialidades($idProf);
        $list['turnos'] = $this->turnos_modelo->TipoTurnos();
        $list['mensajeDNI'] ='';
        $list['dias'] = $this->agenda_modelo->diasagenda($idProf,$idEsp);
        $list['diasLicencias'] = $this->licencias_modelo->diasLicencias($idProf,$idEsp);
        $list['hidden'] = '';
        $list['hiddenTabla'] = '';
        $list['licencias'] = $this->input->post('diaslicencias');
        $list['laborales'] = $this->input->post('diasagenda');
        $list['vacaciones'] = $this->input->post('diasvacaciones');
        $list['feriados'] = $this->input->post('diasferiados');
        $bd=0;
        if($idProf == 0){
            $list['especialidadesxprofesionalesid'] = '0';
        }else{
            foreach($list['especialidades'] as $esp){
                if($esp->especialidadesid == $idEsp){
                    $list['especialidadesxprofesionalesid'] = $esp->especialidadesxprofesionalesid;
                }
            }
        }

        if (($idProf == 0) || ($idEsp == 0) || ($tipoturno == 0)){
            $idProf="null";
            $idEsp="null";
        }
        if ( $this->input->post('inputTipoTurno') == 1){
            $table = "fn_agendaturnonormales(".$idProf.",'".$fecha."',".$idEsp.")"; 
            $list['agenda'] = $this->agenda_modelo->get_datatables($table);
        }else{
            $table = "fn_agendasobreturnos(".$idProf.",'".$fecha."',".$idEsp.")"; 
            $list['agenda'] = $this->agenda_modelo->get_datatables($table);
        }
        

        $b=0;
        $cont=0;
        for($i=1;$i<=79;$i++){
            if(empty($this->input->post("$i"))&&($b==0)){
                $b=0;
            }elseif (empty($this->input->post("$i"))&&($b==1)) {
                $b= 0;
                $cont++;
            }elseif (!empty($this->input->post("$i"))&&($b==0)) {
                $b= 1;
                $cont++;
            }elseif (!empty($this->input->post("$i"))&&($b==1)) {
                $b= 1;
            }     
        }

        if (isset($_POST['Buscar'])) {
          if (empty($this->input->post('inputDNI'))){
                $list['mensajeDNI'] = "El campo DNI es obligatorio";
                $bd = 1;
            }elseif (!$this->esnumerico($this->input->post('inputDNI'))) {
                $list['mensajeDNI']= "El DNI debe ser un valor Numérico";
                $bd =1;
            }
        } 
        
        

        if (isset($_POST['registrar'])){
            if (!$this->esnumerico($this->input->post('inputDNI')) || empty($this->input->post('inputDNI'))) {
               $list['mensaje']="Debe seleccionar un Paciente para registrar el turno!";
            } else {
               if ($cont>2){
                $list['mensaje']="No puede haber espacios de tiempo en los turnos elegidos!";
                }elseif ($cont== 0){
                    $list['mensaje']="Debe seleccionar un horario para registrar el turno!";

                }else{

                    for ($j=1;$j<=79;$j++){
                        if(!empty($this->input->post("$j"))){
                        
                         $data = array(
                            'tipoturnosid' => $list['tipoturnosid'],
                            'profesionalesid' =>  $this->input->post('profid'),
                            'especialidadesid' => $this->input->post('espid'),
                            'turnosfecha' =>  $list['fecha'],
                            'turnoshoratiempo'  => trim($this->input->post("$j").':00'),
                            'pacientesid'  => $list['pacienteid']
                        );

                        $transaction = "insert into turnos (tipoturnosid, profesionalesid, especialidadesid, turnosfecha, turnoshoratiempo, pacientesid) values ("."'".$data['tipoturnosid']."','".$data['profesionalesid']."','".$data['especialidadesid']."','".$data['turnosfecha']."','".$data['turnoshoratiempo']."','".$data['pacientesid']."')"; 

                        $dataInserted = "tipoturnosid = ".$data['tipoturnosid'].", profesionalesid = ".$data['profesionalesid'].", especialidadesid = ".$data['especialidadesid'].", turnosfecha = ".$data['turnosfecha'].", turnoshoratiempo = ".$data['turnoshoratiempo']." , pacientesid = ".$data['pacientesid'];

                        $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
                        );
                        $this->turnos_modelo->insertTurno($data);
                        $this->auditoria_modelo->registrarAuditoria($data_auditoria);

                        unset($data);
                       
                        }
                    }
                    $data2 = array(
                                'estadosid' => 1);
                    $this->turnos_modelo->updateEstado($list['pacienteid'],$data2);

                    
                        
                    
                     
                    redirect(base_url().'index.php/turnos');
                }
            }
            
            
        }else{
            $list['mensaje']="";
        }
        
        $this->load->view('registrarTurno_view',$list); 
        

        
}
    public function CargaDias()
    {
        $idProf = $this->input->post('profesionalid');
        $idEsp = $this->input->post('especialidadid');
        $dias[0] = $this->agenda_modelo->diasagenda($idProf,$idEsp);
        $dias[1] = $this->licencias_modelo->diasLicencias($idProf,$idEsp);
        $dias[2] = $this->vacaciones_modelo->diasVacaciones($idProf,$idEsp);
        $dias[3] = $this->feriados_modelo->diasFeriados();
        echo json_encode($dias); 
    }
   
   
    
}

?>