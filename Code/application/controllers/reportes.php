<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('reportes_modelo');
        $this->load->model('turnos_modelo');
        $this->load->model('profesionales_modelo');
        $this->load->model('historiaclinica_modelo');
        $this->load->library('session'); 
    }
    
    public function index(){
        $this->load->view('head_view');            
        if($this->session->userdata('logueo') == TRUE || $this->session->userdata('usuario') != '') {
            
            $this->load->view('reportes_view');            
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }

    public function reporteusuarios(){
        $this->load->view('head_view');            
        $this->load->view('reporteusuarios_view');
    }

    public function reportesHC(){
        $this->load->view('head_view');            
        $this->load->view('reportes_HC_view');
    }

    public function getHC(){
        $DNI = $this->input->post('DNI'); 
        $idEsp =  $this->input->post('EspId');
        $hc = $this->reportes_modelo->get_historiaclinica($DNI,$idEsp);
        $T = $this->historiaclinica_modelo->Telefonos($DNI);
        $O = $this->historiaclinica_modelo->ObraSocial($DNI);
        
        $list[0] = $hc;
        $list[1] = $T;
        $list[2] = $O;

        echo json_encode($list);       
    }

    public function reportesPacientesxOS(){
        $this->load->view('head_view');            
        $this->load->view('reportes_PacientesxOS_view');
    }
    public function reportesPacientesAusentes()
    {
        $list['profesionales'] = $this->turnos_modelo->Profesionales();
        $list['fecha'] = date('D M d Y H:i:s \G\M\TO (T)'); 
        $this->load->view('reportes_pacientes_ausentes_view',$list);
    }

    public function get_CantPacxOS(){
        $datos = $this->reportes_modelo->get_cantidadPacientesxOS();
        echo json_encode($datos);
    }
    public function getPacientesAusentes()
    {
        $idProf = $this->input->post('profesionalid');
        $fechaI = explode("/",$this->input->post('fechaI'));
        $fechaI = $fechaI[2].'-'.$fechaI[1].'-'.$fechaI[0];
        $fechaF = explode("/",$this->input->post('fechaF'));
        $fechaF = $fechaF[2].'-'.$fechaF[1].'-'.$fechaF[0];
        $list[0] = $this->reportes_modelo->getCantidadAusentes($idProf,$fechaI,$fechaF);
        $list[1] = $this->reportes_modelo->getPacientesAusentes($idProf,$fechaI,$fechaF);
        $list[2] = $this->profesionales_modelo->obtieneDatosProfesional($idProf);
        echo json_encode($list);
    }

    public function reportesTurnosCancelados()
    {
        $list['profesionales'] = $this->turnos_modelo->Profesionales();
        $list['fecha'] = date('D M d Y H:i:s \G\M\TO (T)'); 
        $this->load->view('reportes_turnosCancelados_xprofesional_view',$list);
    }


    public function getTurnosCancelados()
    {
        $idProf = $this->input->post('profesionalid');
        $fechaI = explode("/",$this->input->post('fechaI'));
        $fechaI = $fechaI[2].'-'.$fechaI[1].'-'.$fechaI[0];
        $fechaF = explode("/",$this->input->post('fechaF'));
        $fechaF = $fechaF[2].'-'.$fechaF[1].'-'.$fechaF[0];
        $list[0] = $this->reportes_modelo->getCantidadCancelados($idProf,$fechaI,$fechaF);
        $list[1] = $this->reportes_modelo->getPacientesTurnosCancelados($idProf,$fechaI,$fechaF);
        $list[2] = $this->profesionales_modelo->obtieneDatosProfesional($idProf);
        echo json_encode($list);
    }

    public function turnosDiarios()
    {
        $list['profesionales'] = $this->turnos_modelo->Profesionales(); 
        $this->load->view('reportes_turnosDiarios_view',$list);
    }

    public function CargaEspecialidades(){
        $idProf =  $this->input->post('profesionalesid');
        $opciones = '';
        if ($idProf == 0){
            $opciones.= "<option value ='0' >Seleccione primero el profesional</option>";
        }else{
            $list = $this->turnos_modelo->Especialidades($idProf);
                foreach($list as $esp){
                    if ($esp->estadosid == 1){
                        $opciones.='<option value="'.$esp->especialidadesid.'">'.$esp->especialidadesnombre.'</option>';

                    }
                }
        }

        
        echo $opciones;
    }

    public function CargaEspecialidadesHC(){
        $nrodni = $this->input->post('pacienteDNI');
        $opciones = '';
        $list = $this->reportes_modelo->get_especialidades($nrodni);
        print_r($list);

        if ($list == null){
            echo json_encode($list);
        }else{
            
            foreach($list as $esp){
                //if($esp->estadosid == 1){
                    $opciones.='<option value="'.$esp->especialidadesid.'">'.$esp->especialidadesnombre.'</option>';
                //}
            }
            echo $opciones;
        }
    }

    public function getTurnos()
    {
        $idProf = $this->input->post('profesionalid');
        $idEsp = $this->input->post('especialidadid');
        $turnos[0] = $this->reportes_modelo->get_Turnos($idProf,$idEsp);
        $turnos[1] = $this->profesionales_modelo->obtieneDatosProfesional($idProf);
        echo json_encode($turnos);
    }

    public function plantilla(){
        $this->load->view('plantilla_view');
    }
    public function reporteEstadistico()
    {
        $list['meses'] = array(
                '1' => 'Enero',
                '2' => 'Febrero',
                '3' => 'Marzo',
                '4' => 'Abril',
                '5' => 'Mayo',
                '6' => 'Junio',
                '7' => 'Julio',
                '8' => 'Agosto',
                '9' => 'Septiembre',
                '10' => 'Octubre',
                '11' => 'Noviembre',
                '12' => 'Diciembre'
        );
        $list['year'] = date('Y');
        $list['profesionales'] = $this->turnos_modelo->Profesionales();
        $this->load->view('reporteEstadistico_view',$list); 
    }
    public function plantillaEstadistica()
    {
        $this->load->view('test_reposte_view');
    }

    public function getPacientes(){
        $estadoid = $this->input->post('estadoid');
        $paciente = $this->reportes_modelo->get_pacientes($estadoid);
        echo json_encode($paciente);
    }

    public function getDatosEstadisticosMes()
    {
        $idProf = $this->input->post('profesionalid');
        $mes = $this->input->post('mes');
        $year = $this->input->post('year');
        $cant = $this->input->post('cant');
        $data[0] = $this->reportes_modelo->get_dataxmes($idProf,$mes,$year);
        $data[1] = $this->profesionales_modelo->obtieneDatosProfesional($idProf);
        $data[2] = $this->reportes_modelo->cantidadEspecialidad($idProf);
        $data[3] = $this->reportes_modelo->get_dataxdia($idProf,$mes,$year,$cant);
        echo json_encode($data);
    }

    public function getUsuarios(){
        $usuarios = $this->reportes_modelo->get_usuarios();
        echo json_encode($usuarios);
    } 
    
    public function CerrarSesion(){
        $this->session->sess_destroy();
        redirect(base_url().'index.php/login');
    }    
    
}

?>