<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracion extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('tipotelefono_modelo');
        $this->load->model('intervalo_modelo');
        $this->load->model('sexo_modelo');
        $this->load->model('obrasocial_modelo');
        $this->load->model('feriados_modelo');
        $this->load->model('especialidades_modelo');
        $this->load->model('auditoria_modelo');
        $this->load->library('session');
        $this->load->helper('form'); 
    }
    
    public function index(){
        $this->load->view('head_view');            
        if($this->session->userdata('logueo') == TRUE || $this->session->userdata('usuario') != '') {
            $this->load->view('configuracion_view');            
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }
      
    
    function ajax_list(){

            $list = $this->tipotelefono_modelo->get_datatables();
            $data = array();
            foreach ($list as $tipotelefono){

                $row = array();
                $row[] = $tipotelefono->tipotelefonosid;
                $row[] = $tipotelefono->tipotelefonosnombre;
                $row[] = '<button type="button" data-toggle="tooltip" title="Modificar el Tipo de Teléfono" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/configuracion/modificarTipoTelefonos/".$tipotelefono->tipotelefonosid."'".'"><i class="fas fa-pencil-alt"></i></button>';
                $data[] = $row;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->tipotelefono_modelo->count_all(),
                            "recordsFiltered" => $this->tipotelefono_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
    }

    
    function intervalo(){
        $this->load->view('head_view');            
        $this->load->view('intervalo_view');            
    }


    function ajax_list_intervalo(){

        $list = $this->intervalo_modelo->get_datatables();
        $data = array();
        $i = 0;
        foreach ($list as $intervalo){
            $row = array();
            $row[] = $intervalo->intervalosid;
            $row[] = $intervalo->intervalostiempo;
            $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Intervalo de Duración" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/configuracion/modificarIntervalo/".$intervalo->intervalosid."'".'"><i class="fas fa-pencil-alt"></i></button>'.
                    '<button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" ><i data-toggle="tooltip" title="Dar de Baja Intervalo" class="fas fa-times-circle" ></i></button>'.
                    '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                                <div class="modal-dialog modal-lg" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header" >
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                <span aria-hidden="true">&times;</span>
                                                                                            </button>
                                                                                            
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            <label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
                                                                                        </div>
                                                                                        <div class="row">       
                                                                                            <div class="col-sm-3">
                                                                                                
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/configuracion/bajaIntervalo/".$intervalo->intervalosid."'".'">Si</button>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                            <button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
                                                                                                
                                                                                            </div>
                                                                                            
                                                                                            <div class="col-sm-3">
                                                                                                
                                                                                            </div>
                                                                                        </div>
                                                                                        <br><br>
                                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>';
            $data[] = $row;
            $i = $i + 1;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->intervalo_modelo->count_all(),
                            "recordsFiltered" => $this->intervalo_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
    }

    


    public function especialidades(){
        $this->load->view('head_view');            
        $this->load->view('especialidades_view');            
    }

    public function ajax_list_especialidades(){

        $list = $this->especialidades_modelo->get_datatables();
        $data = array();
        $i = 0;
        foreach ($list as $especialidades){
            $row = array();
            $row[] = $especialidades->especialidadesid;
            $row[] = $especialidades->especialidadesnombre;
                //habilitar cuando sea modificar sexo
            $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Especialidad" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/configuracion/modificarEspecialidad/".$especialidades->especialidadesid."'".'"><i class="fas fa-pencil-alt"></i></button>'.
                    '    <button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" ><i data-toggle="tooltip" title="Dar de Baja Especialidad" class="fas fa-times-circle" ></i></button>'.
                    '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                                <div class="modal-dialog modal-lg" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header" >
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                <span aria-hidden="true">&times;</span>
                                                                                            </button>
                                                                                            
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            <label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
                                                                                        </div>
                                                                                        <div class="row">       
                                                                                            <div class="col-sm-3">
                                                                                                
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/configuracion/bajaEspecialidad/".$especialidades->especialidadesid."'".'">Si</button>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                            <button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
                                                                                                
                                                                                            </div>
                                                                                            
                                                                                            <div class="col-sm-3">
                                                                                                
                                                                                            </div>
                                                                                        </div>
                                                                                        <br><br>
                                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>';
                $data[] = $row;
                $i = $i+1;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->especialidades_modelo->count_all(),
                            "recordsFiltered" => $this->especialidades_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
    }

    public function registrarEspecialidad(){
        $list['especialidad'] = $this->input->post('especialidad');
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('especialidad', 'especialidad', 'callback_existenteEspecialidad'); 
            

        if($this->form_validation->run() == FALSE){
            $this->load->view('registrarEspecialidad_view',$list);
        }else{
            $data = array(
                'especialidadesnombre' => $this->input->post('especialidad'),
            );

            $transaction = 'insert into especialidades(especialidadesnombre) values('."'".$data['especialidadesnombre']."')";
            $dataInserted = 'especialidadesnombre='.$data['especialidadesnombre'];
            $data_auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transaction,
                    'datosingresados' => $dataInserted
            );

            $this->especialidades_modelo->registrarEspecialidad($data);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/configuracion/especialidades');
        }
  
    }



    public function modificarEspecialidad($id){
        $list['id'] = $id; 
        $list['datos'] = $this->especialidades_modelo->obtieneDatosEspecialidad($id);
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('especialidad', 'especialidad', 'required|min_length[2]|max_length[20]'); 
            

        if($this->form_validation->run() == FALSE){
            $list['especialidad'] =  $this->input->post('especialidad');
            $this->load->view('modificarEspecialidad_view',$list);
        }else{
            $data = array(
                'especialidadesnombre' => $this->input->post('especialidad'),
            );
            $transaction = 'update especialidades set '.
                            "especialidadesnombre = ".$data['especialidadesnombre'];

            $dataUpdated = 'especialidadesnombre='.$data['especialidadesnombre'];
            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
            );
            
            $this->especialidades_modelo->modificarEspecialidad($data,$id);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);

            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/configuracion/especialidades');
        }
    }


    public function bajaEspecialidad($idEs){
        $datos = $this->especialidades_modelo->obtieneDatosEspecialidad($idEs);
                
        $transaction = 'delete  from especialidades where especialidadesid = '.$idEs;
        $dataDeleted = 'especialidadesid = '. $datos['especialidadesid'].', especialidadesnombre = '.$datos['especialidadesnombre'];
                
        $data_auditoria = array(
                                'nombreusuario' => $this->session->userdata('usuario'),
                                'transaccion' => $transaction,
                                'datosborrados' => $dataDeleted
        );
        
        $this->especialidades_modelo->bajaEspecialidad($idEs);
        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        redirect(base_url().'index.php/configuracion/especialidades');
    }
                

    public function feriados(){
        $this->load->view('head_view');            
        $this->load->view('feriados_view');
    }

    
    function ValidarFeriado(&$list,$fecha,$desc){
        if($desc == null || $desc == ""){
            $list['mensajeerror'] = "Debe ingresar una descripción del feriado.";
            return false;
        }else{
            if (is_null($fecha) || empty($fecha)){
                $list['mensajeerror'] = "Debe ingresar una fecha para el feriado.";
                return false;
            }else{
                return true;
            }
        }
    }


    public function registrarFeriados(){
        $list['mensajeerror'] = '';
        $list['fecha'] = date('D M d Y H:i:s \G\M\TO (T)');
        $fecha = $this->input->post('fecha');
        $descripcion = $this->input->post('feriadoDescripcion');
        if (isset($_POST['Registrar']) && $this->ValidarFeriado($list,$fecha,$descripcion)){
            $fechaF = explode("/",$this->input->post('fecha'));
            $fechaF = $fechaF[0]."-".$fechaF[1]."-".$fechaF[2];
            $data = array(
                        'feriadosdescripcion' => $descripcion,
                        'feriadosfecha' => $fecha,
                        'diasid' => date('w',strtotime($fechaF)),
                        'dia' => date('j',strtotime($fechaF)),
                        'mes' => date('n',strtotime($fechaF)) - 1,
                        'anio' => date('Y',strtotime($fechaF)),
                        'estadosid' => 1
            );
            
            $transaction = 'insert into feriados (feriadosdescripcion,feriadosfecha) values('."'".$data['feriadosdescripcion']."','".$data['feriadosfecha']."')";

            $dataInserted = 'feriadosdescripcion = '.$data['feriadosdescripcion'].',feriadosfecha = '.$data['feriadosfecha'] ;

            $data_auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transaction,
                    'datosingresados' => $dataInserted
            );

            $this->feriados_modelo->registrarFeriados($data);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            redirect(base_url().'index.php/configuracion/feriados');
        }
    
        $this->load->view('registrarFeriados_view',$list);
    }


    public function bajaFeriados($id){
        $datos = $this->feriados_modelo->obtieneDatosFeriados($id);
        $transaction = 'delete  from feriados where feriadosid = '.$id;
        $dataDeleted = 'feriadosid = '. $datos['feriadosid'].', feriadosdescripcion = '.$datos['feriadosdescripcion'].', feriadosfecha = '.$datos['feriadosfecha'];

        $data_auditoria = array(
                                'nombreusuario' => $this->session->userdata('usuario'),
                                'transaccion' => $transaction,
                                'datosborrados' => $dataDeleted
        );

        $this->feriados_modelo->bajaFeriados($id);
        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        redirect(base_url().'index.php/configuracion/feriados');
    }

                

    public function ajax_list_feriados(){

        $list = $this->feriados_modelo->get_datatables();
        $data = array();
        $i = 0;
        foreach ($list as $feriado){

            $row = array();
            $row[] = $feriado->nombreferiado;
            $row[] = $feriado->fechaferiado;
            $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Feriado" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/configuracion/modificarFeriado/".$feriado->id."'".'"><i class="fas fa-pencil-alt"></i></button>    '.
                '<button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" ><i data-toggle="tooltip" title="Dar de Baja al Feriado" class="fas fa-times-circle" ></i></button>'.
                '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header" >
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            
                        </div>
                        <div class="modal-body">
                            <label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
                        </div>
                        <div class="row">		
                            <div class="col-sm-3">
                                
                            </div>
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/configuracion/bajaFeriados/".$feriado->id."'".'">Si</button>
                            </div>
                            <div class="col-sm-3">
                            <button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
                                
                            </div>
                            
                            <div class="col-sm-3">
                                
                            </div>
                        </div>
                        <br><br>
                                        
                    </div>
                </div>
            </div>';
            $data[] = $row;
            $i = $i + 1;

        }        

        $output = array(
                    "draw" => intval($_POST['draw']),
                    "recordsTotal" => $this->feriados_modelo->count_all(),
                    "recordsFiltered" => $this->feriados_modelo->count_filtered(),
                    "data" => $data,
         );

        echo json_encode($output);
    }

    public function modificarFeriado($id){
        $list['id'] = $id;
        $list['mensajeerror'] = '';
        $list['datos'] = $this->feriados_modelo->obtieneDatosFeriados($id);
        $list['fecha'] = explode("-", $list['datos']['feriadosfecha']);
        $list['fecha'] =  $list['fecha'][2] . '/' .  $list['fecha'][1] . '/' .  $list['fecha'][0];

        $fecha = $this->input->post('fecha');
        $descripcion = $this->input->post('feriadoDescripcion');
    
        if (isset($_POST['Registrar']) && $this->ValidarFeriado($list,$fecha,$descripcion)){
            $fechaF = explode("/",$this->input->post('fecha'));
            $fechaF = $fechaF[0]."-".$fechaF[1]."-".$fechaF[2];
            $data = array(
                        'feriadosdescripcion' => $descripcion,
                        'feriadosfecha' => $fecha,
                        'diasid' => date('w',strtotime($fechaF)),
                        'dia' => date('j',strtotime($fechaF)),
                        'mes' => date('n',strtotime($fechaF)) - 1,
                        'anio' => date('Y',strtotime($fechaF)),
                        'estadosid' => 1
            );

            $transaction = 'update feriados set '.
                            "feriadosdescripcion = ".$data['feriadosdescripcion'].", feriadosfecha = ".$data['feriadosfecha'];
            $dataUpdated = 'feriadosid ='.$id.',feriadosdescripcion ='.$data['feriadosdescripcion']."feriadosfecha =".$data['feriadosfecha'];
            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
            );

            $this->feriados_modelo->modificarFeriados($data,$id);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            redirect(base_url().'index.php/configuracion/feriados');
        }
        
        $this->load->view('modificarFeriados_view',$list);
    }

    

    public function sexo(){
        $this->load->view('head_view');            
        $this->load->view('sexo_view');            
    }


    public function ajax_list_sexo(){

        $list = $this->sexo_modelo->get_datatables();
        $data = array();
        $i = 0;
        foreach ($list as $sexo){
            $row = array();
            $row[] = $sexo->sexoid;
            $row[] = $sexo->sexonombre;
                //habilitar cuando sea modificar sexo
            $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Sexo" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/configuracion/modificarSexo/".$sexo->sexoid."'".'"><i class="fas fa-pencil-alt"></i></button>'.
                    '    <button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" ><i data-toggle="tooltip" title="Dar de Baja Sexo" class="fas fa-times-circle" ></i></button>'.
                    '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                                <div class="modal-dialog modal-lg" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header" >
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                <span aria-hidden="true">&times;</span>
                                                                                            </button>
                                                                                            
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            <label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
                                                                                        </div>
                                                                                        <div class="row">       
                                                                                            <div class="col-sm-3">
                                                                                                
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/configuracion/bajaSexo/".$sexo->sexoid."'".'">Si</button>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                            <button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
                                                                                                
                                                                                            </div>
                                                                                            
                                                                                            <div class="col-sm-3">
                                                                                                
                                                                                            </div>
                                                                                        </div>
                                                                                        <br><br>
                                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>';
                $data[] = $row;
                $i = $i+1;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->sexo_modelo->count_all(),
                            "recordsFiltered" => $this->sexo_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
    }


    public function registrarSexo(){
        $list['sexo'] = $this->input->post('sexo');
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('sexo', 'sexo', 'callback_existenteSexo'); 
            

        if($this->form_validation->run() == FALSE){
            $this->load->view('agregarSexo_view',$list);
        }else{
            $data = array(
                        'sexonombre' => $this->input->post('sexo'),
            );

            $transaction = 'insert into sexo(sexonombre) values('."'".$data['sexonombre']."')";
            $dataInserted = 'sexonombre='.$data['sexonombre'];
            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
            );

            $this->sexo_modelo->registrarSexo($data);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/configuracion/sexo');
        }

    }

    
    public function modificarSexo($id){
        $list['id'] = $id; 
        $list['datos'] = $this->sexo_modelo->obtieneDatosSexo($id);
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('sexo', 'sexo', 'required|min_length[2]|max_length[20]'); 
            

        if($this->form_validation->run() == FALSE){
            $list['sexo'] =  $this->input->post('sexo');
            $this->load->view('modificarSexo_view',$list);
        }else{
            $data = array(
                        'sexonombre' => $this->input->post('sexo'),
            );
            $transaction = "update sexo set sexonombre = ".$data['sexonombre'];
            $dataUpdated = 'sexoid ='.$id.',sexonombre ='.$data['sexonombre'];

            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
            );
            $this->sexo_modelo->modificarSexo($data,$id);

            $data['message'] = 'Data Inserted Successfully';
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            redirect(base_url().'index.php/configuracion/sexo');
        }
    }


    public function bajaSexo($idS){
        $datos = $this->sexo_modelo->obtieneDatosSexo($idS);
        $transaction = 'delete  from sexo where sexoid = '.$idS;
        $dataDeleted = 'sexoid = '. $idS.', sexonombre = '.$datos['sexonombre'];

        $data_auditoria = array(
                                'nombreusuario' => $this->session->userdata('usuario'),
                                'transaccion' => $transaction,
                                'datosborrados' => $dataDeleted
        );


        $this->sexo_modelo->bajaSexo($idS);
        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        redirect(base_url().'index.php/configuracion/sexo');
    }



    public function obrasocial(){
        $this->load->view('head_view');            
        $this->load->view('obraSocial_view');            
    }


    public function bajaObraSocial($idOS){

        $data = array(
                'estadosid' => 2
        );
        $transaction = "update obrasocial set estadosid = 2";
        $dataUpdated = 'obrasocialid ='.$idOS.',estadosid = 2';
        $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
        );

        $this->obrasocial_modelo->bajaObraSocial($data,$idOS);
        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        redirect(base_url().'index.php/configuracion/obrasocial');
    }




    public function ajax_list_obra_social(){

        $list = $this->obrasocial_modelo->get_datatables();
        $data = array();
        $i = 0; 
        foreach ($list as $os){
            $row = array();
            $row[] = $os->obrasocialid;
            $row[] = $os->obrasocialnombre;
                
            if($os->estadosid == 1){
                $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Obra Social" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/configuracion/modificarObraSocial/".$os->obrasocialid."'".'"><i class="fas fa-pencil-alt"></i></button>'.
                    '    <button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" ><i data-toggle="tooltip" title="Dar de Baja a Obra Social" class="fas fa-times-circle" ></i></button>'.
                    '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																				<div class="modal-dialog modal-lg" role="document">
																					<div class="modal-content">
																						<div class="modal-header" >
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">&times;</span>
																							</button>
																							
																						</div>
																						<div class="modal-body">
																							<label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
																						</div>
																						<div class="row">		
																							<div class="col-sm-3">
																								
																							</div>
																							<div class="col-sm-3">
																								<button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/configuracion/bajaObraSocial/".$os->obrasocialid."'".'">Si</button>
																							</div>
																							<div class="col-sm-3">
																							<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
																								
																							</div>
																							
																							<div class="col-sm-3">
																								
																							</div>
																						</div>
																						<br><br>
																										
																					</div>
																				</div>
																			</div>';
                }else{
                    $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Obra Social" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/configuracion/modificarObraSocial/".$os->obrasocialid."'".'" disabled ><i class="fas fa-pencil-alt"></i></button>'.
                    '    <button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" disabled><i data-toggle="tooltip" title="Cancelar Turno" class="fas fa-times-circle" ></i></button>';    
                }
                $row[] = $os->estadosid; 
                $data[] = $row;
                $i = $i +1;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->obrasocial_modelo->count_all(),
                            "recordsFiltered" => $this->obrasocial_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
    }

    public function registrarObraSocial(){
        $list['obrasocial'] = $this->input->post('obrasocial');
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('obrasocial', 'obrasocial', 'callback_existenteObraSocial'); 
            

        if($this->form_validation->run() == FALSE){
            $this->load->view('registrarObraSocial_view',$list);
        }else{
            $data = array(
                        'obrasocialnombre' => $this->input->post('obrasocial'),
                        'estadosid' => 1,
            );
            $transaction = 'insert into obrasocial (obrasocialnombre) values('."'".$data['obrasocialnombre']."')";
            $dataInserted = 'obrasocialnombre = '.$data['obrasocialnombre'];
            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
            );

            $this->obrasocial_modelo->registrarObraSocial($data);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/configuracion/obrasocial');
        }
          
    }



    public function modificarObraSocial($id){
        $list['id'] = $id; 
        $list['datos'] = $this->obrasocial_modelo->obtieneDatosObraSocial($id);
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('obrasocial', 'obrasocial', 'min_length[2]|max_length[20]'); 
            

        if($this->form_validation->run() == FALSE){
            $list['obrasocial'] =  $this->input->post('obrasocial');
            $this->load->view('modificarObraSocial_view',$list);
        }else{
            $data = array(
                    'obrasocialnombre' => $this->input->post('obrasocial'),
            );
            $transaction = "update obrasocial set obrasocialnombre = ".$data['obrasocialnombre'];
            $dataUpdated = 'obrasocialid ='.$id.', obrasocialnombre = '.$data['obrasocialnombre'];
            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
            );

            $this->obrasocial_modelo->modificarObraSocial($data,$id);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/configuracion/obrasocial');
        }
    }


    public function modificarTipoTelefonos($id){
        $list['id'] = $id; 
        $list['datos'] = $this->tipotelefono_modelo->obtieneTipoTelefonos($id);
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
        $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('tipoTelefono', 'Tipo Teléfono', 'min_length[2]|max_length[15]'); 
            

        if($this->form_validation->run() == FALSE){
            $list['tipotelefono'] =  $this->input->post('tipoTelefono');
            $this->load->view('modificarTipoTelefono_view',$list);
        }else{
            $data = array(
                    'tipotelefonosnombre' => $this->input->post('tipoTelefono'),
            );
            $transaction = "update tipotelefonos set tipotelefonosnombre = ".$data['tipotelefonosnombre'];
            $dataUpdated = 'tipotelefonosid ='.$id.', tipotelefonosnombre = '.$data['tipotelefonosnombre'];
            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
            );

            $this->tipotelefono_modelo->modificarTipoTelefono($data,$id);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/configuracion');
        }
    }


    public function registrarTipoTelefono(){
            
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
        $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('tipoTelefono', 'Tipo Teléfono', 'required|min_length[2]|max_length[15]'); 
            

            if($this->form_validation->run() == FALSE){
                $list['tipotelefono'] =  $this->input->post('tipoTelefono');
                $this->load->view('agregartipotelefono_view',$list);
            }else{
                $data = array(
                    'tipotelefonosnombre' => $this->input->post('tipoTelefono'),
                    );
                $transaction = 'insert into tipotelefonos (tipotelefonosnombre) values('."'".$data['tipotelefonosnombre']."')";
                $dataInserted = 'tipotelfonosnombre = '.$data['tipotelefonosnombre'];
                $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
                );


                $this->tipotelefono_modelo->registrarTipoTelefono($data);
                $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                $data['message'] = 'Data Inserted Successfully';
                redirect(base_url().'index.php/configuracion');
            }

    }


    public function existente($data){
        $list=$this->intervalo_modelo->buscarIntervalo($data);
            if ($list!=null){
                $this->form_validation->set_message('existente','El Intervalo ya existe');
                return False;
            } 
            else{
                return True;
            }
    }


    public function existenteSexo($data){
        $list=$this->sexo_modelo->buscarSexo($data);
            if ($list!=null){
                $this->form_validation->set_message('existenteSexo','El Sexo ya existe');
                return False;
            } 
            else{
                return True;
            }
    }
    public function existenteObraSocial($data){
        $list=$this->obrasocial_modelo->buscarObraSocial($data);
            if ($list!=null){
                $this->form_validation->set_message('existenteObraSocial','La Obra Social ya existe');
                return False;
            } 
            else{
                return True;
            }
    }

    public function existenteEspecialidad($data){
        $list=$this->especialidades_modelo->buscarEspecialidad($data);
        // print_r($list);
            if ($list!=null){
                $this->form_validation->set_message('existenteEspecialidad','La Especialidad ya existe');
                return False;
            } 
            else{
                return True;
            }
    }


    public function registrarIntervalo(){
            $list['intervalo'] = $this->input->post('intervalo');
            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
            $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
            $this->form_validation->set_rules('intervalo', 'intervalo', 'callback_existente'); 
            

            if($this->form_validation->run() == FALSE){
                $this->load->view('registrarIntervalo_view',$list);
            }else{
                $data = array(
                    'intervalostiempo' => $this->input->post('intervalo'),
                    );
                $transaction = 'insert into intervalos (intervalostiempo) values ('."'".$data['intervalostiempo']."')";
                $dataInserted = 'intervalostiempo = '.$data['intervalostiempo'];
                $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
                );

                $this->intervalo_modelo->registrarIntervalos($data);
                $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                $data['message'] = 'Data Inserted Successfully';
                redirect(base_url().'index.php/configuracion/intervalo');
            }
        
          
    }


    public function modificarIntervalo($id)
    {
            $list['id'] = $id;
            $list['datos'] =  $this->intervalo_modelo->obtieneDatosIntervalo($id);
            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
            $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
            $this->form_validation->set_rules('intervalo', 'intervalo', 'callback_existente'); 
            

            if($this->form_validation->run() == FALSE){
                $list['intervalostiempo'] = $this->input->post('intervalo');
                $this->load->view('modificarIntervalo_view',$list);
            }else{
                $data = array(
                    'intervalostiempo' => $this->input->post('intervalo'),
                    );
                $transaction = "update intervalos set intervalostiempo = ".$data['intervalostiempo'];
                $dataUpdated = 'intervalosid ='.$id.', intervalostiempo = '.$data['intervalostiempo'];
                $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
                );

                $this->intervalo_modelo->modificarIntervalo($data,$id);
                $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                $data['message'] = 'Data Inserted Successfully';
                redirect(base_url().'index.php/configuracion/intervalo');
            }

    }
 


    public function bajaIntervalo($idIn){
        
        $datos = $this->intervalo_modelo->obtieneDatosIntervalo($idIn);
        $transaction = 'delete  from intervalos where intervalosid = '.$idIn;
        $dataDeleted = 'intervalosid = '. $datos['intervalosid'].', intervalostiempo = '.$datos['intervalostiempo'];
        $data_auditoria = array(
                                'nombreusuario' => $this->session->userdata('usuario'),
                                'transaccion' => $transaction,
                                'datosborrados' => $dataDeleted
        );

        $this->intervalo_modelo->bajaIntervalo($idIn);
        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        redirect(base_url().'index.php/configuracion/intervalo');
    }


    public function CerrarSesion(){
        $this->session->sess_destroy();
        redirect(base_url().'index.php/login');
    }    

    
    
}

?>