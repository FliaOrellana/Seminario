<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pacientes extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('pacientes_modelo');
        $this->load->model('auditoria_modelo');
        $this->load->model('turnos_modelo');
        $this->load->library('session'); 
    }
    
    public function index(){
        $this->load->view('head_view');            
        if($this->session->userdata('logueo') == TRUE || $this->session->userdata('usuario') != '') {
            $actualiza = $this->pacientes_modelo->ActualizaEstado();
            $actualizaTurnos = $this->turnos_modelo->ActualizaEsatdoTurno();
            $this->load->view('pacientes_view');            
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }
    
    public function eliminarTelefono($idPac,$idTel)
    {   
        $telefonoData = $this->pacientes_modelo->getTelefono($idPac, $idTel);
        $this->pacientes_modelo->eliminarTelefonoPaciente($idPac,$idTel);
        $dataDeleted = 'tipotelefonosid='.$telefonoData['tipotelefonosid'].';telefononumero:'.$telefonoData['telefononumero'];
        $data = array(
            'nombreusuario' => $this->session->userdata('usuario'),
            'transaccion' => 'delete from telefonos where telefonosid='.$idTel.' and personasid='.$idPac.' ;',
            'datosborrados' => $dataDeleted
        );
        $this->auditoria_modelo->registrarAuditoria($data);
        redirect(base_url().'index.php/pacientes/RegistrarTelefono/'.$idPac);

    }

    public function RegistrarTelefono($id)
    {
        $list['id'] = $id;
        $list['tipotelefonos'] = $this->pacientes_modelo->TipoTelefono();
        $list['tipotelefonoid'] = $this->input->post('pacienteTipoTelefono');
        $list['telefono'] = $this->input->post('pacienteTelefono');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pacienteTelefono', 'Telefono', 'required|min_length[5]|max_length[15]'); 
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
        $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060' name='error'>", "</div>");
        if($this->form_validation->run() == FALSE){
            $list['tipotelefonoid'] = $this->input->post('pacienteTipoTelefono');
            $list['telefono'] = $this->input->post('pacienteTelefono');
            $this->load->view('personaRegistrarTelefono_view',$list);
        }else{
            $data = array(
                    'personasid' => $id,
                    'tipotelefonosid' => $this->input->post('pacienteTipoTelefono'),
                    'telefononumero' => $this->input->post('pacienteTelefono')
                    );
            $transaction = 'insert into telefonos(personasid,tipotelefonosid,telefononumero) values('.
            $id.','.$this->input->post('pacienteTipoTelefono').",'".$this->input->post('pacienteTelefono')."')";
            $dataInserted = 'personasid='.$id.';tipotelefonosid='.$this->input->post('pacienteTipoTelefono').
            ';telefononumero='.$this->input->post('pacienteTelefono');
            $data_auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transaction,
                'datosingresados' => $dataInserted
            );        
            $this->pacientes_modelo->registrarTelefonoPaciente($data);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/pacientes/RegistrarTelefono/'.$id);
        }
    }


    public function escero($value)
    {
            if ($value == 0){
                $this->form_validation->set_message('escero','Para registrar debe seleccionar una Obra Social');
                return False;
            } 
            else{
                return True;
            }
    }

    public function BuscarPaciente()
    {
        $id = $this->input->post('pacienteid');
        $paciente = $this->pacientes_modelo->modalPaciente($id);
        echo json_encode($paciente) ;
    }

    public function eliminarOS($idPa,$idOS)
    {
        $idpaciente = $this->pacientes_modelo->buscarPaciente($idPa);
        $transaction = 'delete from obrasocialpacientes where pacientes='.$idpaciente['pacientesid'].' and obrasocialid='.$idOS;
        $dataDeleted = 'pacientesid='.$idpaciente['pacientesid'].';obrasocialid='.$idOS;
        $data_auditoria = array(
            'nombreusuario' => $this->session->userdata('usuario'),
            'transaccion' => $transaction,
            'datosborrados' => $dataDeleted
        );
        $this->pacientes_modelo->eliminarObraSocialPaciente($idpaciente['pacientesid'],$idOS);
        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        redirect(base_url().'index.php/pacientes/RegistrarObraS/'.$idPa);

    }

    public function RegistrarObraS($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('obrasSociales', 'Obra Social', 'callback_escero'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060' name='error'>", "</div>");
       $list['id'] = $id;
       $list['obraSociales'] = $this->pacientes_modelo->ObraSociales();
       $list['obrasocialid'] = $this->input->post('obrasSociales');
       if($this->form_validation->run() == FALSE){
            $this->load->view('personaRegistrarObraSocial_view',$list);
        }else{
            $idpaciente = $this->pacientes_modelo->buscarPaciente($id);
            $data = array(
                            'obrasocialid' =>  $this->input->post('obrasSociales'),
                            'pacientesid' => $idpaciente['pacientesid'],
                        );
            $transaction = 'insert into obrasocialpacientes (pacientesid,obrasocialid) values('.
            $idpaciente['pacientesid'].','.$this->input->post('obrasSociales').')';
            $dataInserted = 'pacientesid='.$idpaciente['pacientesid'].';obrasocialid='.$this->input->post('obrasSociales');
            $data_auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transaction,
                'datosingresados' => $dataInserted
            );            
            $this->pacientes_modelo->registrarObraSocialPaciente($data);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            redirect(base_url().'index.php/pacientes/RegistrarObraS/'.$id);
        }
       
    }


    public function registrarPaciente(){

            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio');
            $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
            $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
            $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060' name='error'>", "</div>");
            $this->form_validation->set_rules('pacienteApellido', 'Apellido', 'required|min_length[2]|max_length[15]'); 
            $this->form_validation->set_rules('pacienteNombre', 'Nombre', 'required|min_length[2]|max_length[30]');  
            $this->form_validation->set_rules('pacienteDNI', 'DNI', 'required|min_length[8]|max_length[10]|callback_existente|callback_esnumerico');
            $this->form_validation->set_rules('pacienteDomicilio', 'Domicilio', 'required|min_length[5]|max_length[30]'); 
            $this->form_validation->set_rules('pacienteEmail', 'Email', 'valid_email');
            if (isset($_POST['Registrar'])) {
                $list['fecha'] = $this->input->post('fechaN');
            } else {
              $list['fecha'] = date('D M d Y H:i:s \G\M\TO (T)');
            }
            
            
            $list['sexo'] = $this->pacientes_modelo->sexo();
            
            if($this->form_validation->run() == FALSE){
                $list['apellido'] =  $this->input->post('pacienteApellido');
                $list['nombre'] = $this->input->post('pacienteNombre');
                $list['dni'] = $this->input->post('pacienteDNI');
                $list['domicilio'] = $this->input->post('pacienteDomicilio');
                                
                $list['email'] = $this->input->post('pacienteEmail');
                $list['sexoid'] = $this->input->post('pacienteSexo');
                
                $this->load->view('registrarPaciente_view',$list);
            }else{
                $data = array(
                    'personasapellido' => $this->input->post('pacienteApellido'),
                    'personasnombre' => $this->input->post('pacienteNombre'),
                    'personasnrodocumento' => $this->input->post('pacienteDNI'),
                    'sexoid' => $this->input->post('pacienteSexo'),
                    'personasfechanacimiento' => $this->input->post('fechaN'),
                    'personasdomicilio' => $this->input->post('pacienteDomicilio'),
                    'personasemail' => $this->input->post('pacienteEmail'),
                    );
                $transactionPerson = 'insert into personas (personasapellido,personasnombre,personasnrodocumento,sexoid,personasfechanacimiento,personasdomicilio,personasemail) values ('.
                "'".$data['personasapellido']."','".$data['personasnombre']."','".$data['personasnrodocumento'].
                "',".$data['sexoid'].",'".$data['personasfechanacimiento']."','".$data['personasdomicilio'].
                "','".$data['personasemail']."')";
                $dataPersonInserted = 'personasapellido='.$data['personasapellido'].';personasnombre='.$data['personasnombre'].
                ';personasnrodocumento='.$data['personasnrodocumento'].';sexoid='.$data['sexoid'].
                ';personasfechanacimiento='.$data['personasfechanacimiento'].';personasdomicilio='.$data['personasdomicilio'].
                ';personasemail='.$data['personasemail'];
                $dataPerson_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionPerson,
                    'datosingresados' => $dataPersonInserted
                );
                $this->pacientes_modelo->registrarPersona($data);
                $this->auditoria_modelo->registrarAuditoria($dataPerson_Auditoria);
                $paciente = $this->pacientes_modelo->buscarPersona($data['personasnrodocumento']);
                $data2 = array(
                    'personasid' => $paciente['personasid'],
                    'estadosid' => 2
                );
                $transactionPatients = 'insert into pacientes (personasid,estadosid) values('.
                $data2['personasid'].','.$data2['estadosid'].')';
                $dataPatientsInserted = 'personasid='.$data2['personasid'].';estadosid='.$data2['estadosid'];

                $this->pacientes_modelo->registrarPaciente($data2);
                $dataPatients_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionPatients,
                    'datosingresados' => $dataPatientsInserted
                );
                $this->auditoria_modelo->registrarAuditoria($dataPatients_Auditoria);
                $data['message'] = 'Data Inserted Successfully';
                redirect(base_url().'index.php/pacientes/RegistrarTelefono/'.$paciente['personasid']);
            }

        
          
    }

    public function modificarPaciente($id)
    {       
            $list['id'] = $id;
            $list['datos'] = $this->pacientes_modelo->obtienePaciente($id); 
            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio');
            $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
            $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
            $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060' name='error'>", "</div>");
            $this->form_validation->set_rules('pacienteApellido', 'Apellido', 'required|min_length[2]|max_length[15]'); 
            $this->form_validation->set_rules('pacienteNombre', 'Nombre', 'required|min_length[2]|max_length[30]');  
            $this->form_validation->set_rules('pacienteDNI', 'DNI', 'required|min_length[8]|max_length[10]|callback_esnumerico');
            $this->form_validation->set_rules('pacienteDomicilio', 'Domicilio', 'required|min_length[5]|max_length[30]'); 
            $this->form_validation->set_rules('pacienteEmail', 'Email', 'valid_email');
            if (isset($_POST['Registrar'])) {
               $list['fecha'] = $this->input->post('fechaN');
            } else {
               $list['fecha'] =  date_format( date_create($list['datos']['personasfechanacimiento']), 'd/m/Y');
            }
            
            $list['sexo'] = $this->pacientes_modelo->sexo();
            $list['tipotelefonos'] = $this->pacientes_modelo->TipoTelefono();
            if($this->form_validation->run() == FALSE){
                $list['apellido'] =  $this->input->post('pacienteApellido');
                $list['nombre'] = $this->input->post('pacienteNombre');
                $list['dni'] = $this->input->post('pacienteDNI');
                $list['domicilio'] = $this->input->post('pacienteDomicilio');
                $list['email'] = $this->input->post('pacienteEmail');
                $list['sexoid'] = $this->input->post('pacienteSexo');
                $this->load->view('modificarPaciente_view',$list);
            }else{
                $fecha = getdate();
                $data2 = array(
                   
                    'pacientesfechamod' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year'],
                );
                $this->pacientes_modelo->modificarPaciente($data2,$id);
                $data = array(
                    'personasapellido' => $this->input->post('pacienteApellido'),
                    'personasnombre' => $this->input->post('pacienteNombre'),
                    'personasnrodocumento' => $this->input->post('pacienteDNI'),
                    'personasfechanacimiento' => $this->input->post('fechaN'),
                    'sexoid' => $this->input->post('pacienteSexo'),
                    'personasdomicilio' => $this->input->post('pacienteDomicilio'),
                    'personasemail' => $this->input->post('pacienteEmail'),
                    'personasfechamod' =>  $fecha['mday']."/".$fecha['mon']."/".$fecha['year'],
                    );

                // $transactionPerson = 'insert into personas (personasapellido,personasnombre,personasnrodocumento,sexoid,personasfechanacimiento,personasdomicilio,personasemail) values ('.
                // "'".$data['personasapellido']."','".$data['personasnombre']."','".$data['personasnrodocumento'].
                // "',".$data['sexoid'].",'".$data['personasfechanacimiento']."','".$data['personasdomicilio'].
                // "','".$data['personasemail']."')";
                
                $transactionPerson = 'update perosnas set '.
                'personasapellido='.$data['personasapellido'].','.
                'personasnombre='.$data['personasnombre'].','.
                'personasnrodocumento='.$data['personasnrodocumento'].','.
                'personasfechanacimiento='.$data['personasfechanacimiento'].','.
                'sexoid='.$data['sexoid'].','.
                'personasdomicilio='.$data['personasdomicilio'].','.
                'personasemail='.$data['personasemail'];
                 
                $dataPersonUpdate = 'personasapellido = '.$data['personasapellido'].
                ', perosnasnombre ='.$data['personasnombre'].
                ', personasnrodocumento = '.$data['personasnrodocumento'].
                ', personasfechanacimiento='.$data['personasfechanacimiento'].
                ', sexoid='.$data['sexoid'].
                ', personasdomicilio='.$data['personasdomicilio'].
                ', personasemail='.$data['personasemail'];

                $dataPersons_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionPerson,
                    'datosmodificados' => $dataPersonUpdate
                );

                $ident = $list['datos']['personasid'];
                
                $this->pacientes_modelo->modificarPersona($data,$ident);
                $this->auditoria_modelo->registrarAuditoria($dataPersons_Auditoria);
                
                $data['message'] = 'Data Inserted Successfully';
                redirect(base_url().'index.php/pacientes/RegistrarTelefono/'.$ident);
            }
        
    }
    public function existente($dni){
            $list=$this->pacientes_modelo->buscarPersona($dni);
            if ($list!=null){
                $this->form_validation->set_message('existente','El DNI ya existe');
                return False;
            } 
            else{
                return True;
            }
    }

    public function esnumerico($dni){
        if (is_numeric($dni)){
            return True;
        }
        else{
            $this->form_validation->set_message('esnumerico','DNI no valido');
            return False;
        }
    }

    function ajax_list(){

            $list = $this->pacientes_modelo->get_datatables();
            $data = array();
            foreach ($list as $pacientes){

                $row = array();
                $row[] = $pacientes->pacientesid;
                $row[] = $pacientes->personasapellido;
                $row[] = $pacientes->personasnombre;
                $row[] = $pacientes->personasnrodocumento;
                $row[] = $pacientes->personasdomicilio;
                //$row[] = $pacientes->personastelefono;
                $row[] = $pacientes->estadosnombre;
                $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Datos del Paciente" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/pacientes/modificarPaciente/".$pacientes->pacientesid."'".'"><i class="fas fa-pencil-alt"></i></button> <button id="mostrar" type="button"  class="btn btn-primary" style="background:#65e26f; border-color:#78b965;" data-toggle="modal" data-target="#miModal" value="'.$pacientes->pacientesid.'"><i data-toggle="tooltip" title="Mostrar Datos del Paciente" class="fas fa-user" ></i></button>';
                $data[] = $row;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->pacientes_modelo->count_all(),
                            "recordsFiltered" => $this->pacientes_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
        }


    function ajax_telefono(){
            $id = $this->input->post('pacienteid');
            $list = $this->pacientes_modelo->get_datatables_telefono($id);
            $data = array();
            foreach ($list as $telefono){

                $row = array();
                $row[] = $telefono->tipotelefonosnombre;
                $row[] = $telefono->telefononumero;
                $row[] = '<button type="button" class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/pacientes/eliminarTelefono/".$id."/".$telefono->telefonosid."'".'"><i class="fas fa-times-circle"></i></button>';
                $data[] = $row;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->pacientes_modelo->count_all_telefono($id),
                            "recordsFiltered" => $this->pacientes_modelo->count_filtered_telefono($id),
                            "data" => $data,
                    );

            echo json_encode($output);
        }


     function ajax_Obra(){
            $id = $this->input->post('pacienteid');
            
            $list = $this->pacientes_modelo->get_datatables_obra($id);
            $data = array();
            foreach ($list as $obraS){

                $row = array();
                $row[] = $obraS->obrasocialid;
                $row[] = $obraS->obrasocialnombre;
                $row[] = '<button type="button" class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/pacientes/eliminarOS/".$id."/".$obraS->obrasocialid."'".'"><i class="fas fa-times-circle"></i></button>';
                $data[] = $row;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->pacientes_modelo->count_all_obra($id),
                            "recordsFiltered" => $this->pacientes_modelo->count_filtered_obra($id),
                            "data" => $data,
                    );

            echo json_encode($output);
        }
    

    public function CerrarSesion(){
        $this->session->sess_destroy();
        redirect(base_url().'index.php/login');
    }    
    
}


?>