<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Usuarios extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('usuarios_modelo');
        $this->load->model('auditoria_modelo');
        $this->load->library('session'); 
        $this->load->library('email');
        $this->email->initialize();
    }
    
    public function index(){
        $this->load->view('head_view');            
        if($this->session->userdata('logueo') == TRUE || $this->session->userdata('usuario') != '') {
            $this->load->view('usuarios_view');            
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }
    public function CargaPersonas()
    {
        $DNI = $this->input->post('usuarioDNI');
        $perfil = $this->input->post('usuarioperfil');
        $result['respuesta'] = $this->usuarios_modelo->buscarPersona($perfil,$DNI);
        echo json_encode($result);
    }
    public function CargaUsuario()
    {
        $DNI = $this->input->post('usuarioDNI');
        $result['respuesta'] = $this->usuarios_modelo->BuscarUsuarioCompleto($DNI);
        echo json_encode($result);
    }

   
    public function registrarUsuario()
    {
        $list['perfiles'] = $this->usuarios_modelo->Perfiles();
        $b=0;
        $list['mensajeDNI'] ="";
        $list['mensajeUsuario']="";
        $list['mensajePass']="";
        $list['mensajePassrepet']="";
        $list['mensajePassIguales']="";
        $list['DNI'] = '';
        $list['NombreCompuesto'] = '';
        $list['idPerfil'] =  '';
        $list['usarioNombre'] = '';
        
        if (empty($this->input->post('inputDNI'))){
            $list['mensajeDNI'] = "El DNI es Requerido";
            $b=1;
        }else{
            if( !$this->esnumerico($this->input->post('inputDNI'))){
            $list['mensajeDNI'] = "El DNI no es válido";
            $b=1;
            }
        }
        
        if(empty($this->input->post('inputNombreUsuario'))){
            $list['mensajeUsuario'] = "El Usuario es Requerido";
            $b=1;
        }else{
            if ($this->usuarios_modelo->BuscarUsuario($this->input->post('inputNombreUsuario')) != null){
                $list['mensajeUsuario'] = "Ya existe un Usuario con ese Nombre";
                $b=1;
            }
        }
        if(empty($this->input->post('inputPasswordUsuario'))){
            $list['mensajePass'] = "La Contraseña es Requerida";
            $b=1;
        }
        if(empty($this->input->post('inputPasswordUsuario2'))){
            $list['mensajePassrepet'] = "Debe repetir su Contraseña";
            $b=1;
        }
        if (!($this->iguales($this->input->post('inputPasswordUsuario'),$this->input->post('inputPasswordUsuario2')))){
            $list['mensajePassIguales'] = "Las Contraseñas no son iguales";
            $b=1;
        }
        
        if ($b == 1){
            $list['DNI'] = $this->input->post('inputDNI');
            $list['idPerfil'] =  $this->input->post('usuarioPerfil');
            $list['usuarioNombre'] = $this->input->post('inputNombreUsuario');
            $list['NombreCompuesto'] = $this->input->post('inputNombreCompuesto');
            $this->load->view('registrarUsuario_view',$list);
        }else{
            $data = array('usuariosnombre' => $this->input->post('inputNombreUsuario'),
                          'usuarioscontrasenia' =>  $this->input->post('inputPasswordUsuario'));

            $usuario = $this->usuarios_modelo->BuscarUsuario($this->input->post('inputNombreUsuario'));
            if ($usuario == null){
                $perfil = $this->input->post('usuarioPerfil');
                $dni = $this->input->post('inputDNI');
                $result =  $this->usuarios_modelo->buscarPersona($perfil,$dni);
                $id = $result[0]->ident;

                $transactionUser = 'insert into usuarios (usuariosnombre, usuarioscontrasenia) values('.
                "'".$data['usuariosnombre']."','".$data['usuarioscontrasenia']."')";

                $dataUserInserted = 'usuariosnombre = '.$data['usuariosnombre'].
                ', usuarioscontrasenia = '.$data['usuarioscontrasenia'];

                $dataUser_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionUser,
                    'datosingresados' => $dataUserInserted
                );

                $this->usuarios_modelo->RegistrarUsuario($data);
                $this->auditoria_modelo->registrarAuditoria($dataUser_Auditoria);
                $usuario = $this->usuarios_modelo->BuscarUsuario($this->input->post('inputNombreUsuario'));
                $data2 = array('usuariosid' => $usuario[0]->usuariosid,
                              'perfilesid' => $perfil );
                /*--------------------------------------------------------------*/
                $transactionUserProfile = 'insert into perfilesusuarios (usuariosid, perfilesid) values('.
                $data2['usuariosid'].",".$data2['perfilesid'].")";

                $dataUserProfileInserted = 'usuariosid = '.$data2['usuariosid'].', perfilesid = '.$data2['perfilesid'];

                $dataUserProfile_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionUserProfile,
                    'datosingresados' => $dataUserProfileInserted
                );

                $this->usuarios_modelo->RegistrarPerfilesUsuarios($data2);
                $this->auditoria_modelo->registrarAuditoria($dataUserProfile_Auditoria);
                $data3 = array('usuariosid' =>  $usuario[0]->usuariosid );
                if ($perfil == 2){

                    $transactionPS = 'update profesionales set usuariosid ='. 
                    $usuario[0]->usuariosid.' where profesionalesid='.$id;

                    $dataPSUpdated = 'usuariosid ='.$usuario[0]->usuariosid;
                }else{

                    $transactionPS = 'update secretarios set usuariosid ='. 
                    $usuario[0]->usuariosid.' where secretariosid='.$id;

                    $dataPSUpdated = 'usuariosid ='.$usuario[0]->usuariosid;

                }

                $dataPS_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionPS,
                    'datosmodificados' => $dataPSUpdated
                );
                $this->usuarios_modelo->AsignarUsuario($data3,$id,$perfil);
                $this->auditoria_modelo->registrarAuditoria($dataPS_Auditoria);
                redirect(base_url().'index.php/usuarios');
            }
        }
         
    }

    public function CargaPerfil()
    {
        $idUs =  $this->input->post('usuariosid');
        $opciones = '';
        if ($idUs == ''){
            $opciones.= "<option value ='0' >Debe Ingresar primero DNI correcto</option>";
        }else{
            $list = $this->usuarios_modelo->ObtienePerfiles($idUs);
            foreach($list as $u){
                $opciones.='<option value="'.$u->perfilesid.'">'.$u->perfilesnombre.'</option>';
            }
        }

        
        echo $opciones;
    }
     public function enviarMailCambioPass($usu,$email,$pass)
    {

        exec('python "C:/Program Files/Python36/testemail.py" '.$usu." ".$pass." ".$email);

        redirect(base_url().'index.php/usuarios');
    }
    public function CambiarPass()
    {
        $b=0;
        $list['mensajeDNI'] ="";
        $list['DNI'] = '';
        $list['usuarioid'] =  '';
        $list['usuarioNombre'] = '';
        $list['mensajePass']="";
        $list['mensajePassrepet']="";
        $list['mensajePassIguales']="";
        if (empty($this->input->post('inputDNI'))){
            $list['mensajeDNI'] = "El DNI es Requerido";
            $b=1;
        }else{
            if( !$this->esnumerico($this->input->post('inputDNI'))){
            $list['mensajeDNI'] = "El DNI no es válido";
            $b = 1;
            }
        }
        if(empty($this->input->post('inputPasswordUsuario'))){
            $list['mensajePass'] = "La Contraseña es Requerida";
            $b=1;
        }
        if(empty($this->input->post('inputPasswordUsuario2'))){
            $list['mensajePassrepet'] = "Debe repetir su Contraseña";
            $b=1;
        }
        if (!($this->iguales($this->input->post('inputPasswordUsuario'),$this->input->post('inputPasswordUsuario2')))){
            $list['mensajePassIguales'] = "Las Contraseñas no son iguales";
            $b=1;
        }
        if ($b == 1) {
            $list['DNI'] = $this->input->post('inputDNI');
             $list['usuarioid'] =  $this->input->post('inputUsuarioID');
             $list['usuarioNombre'] = $this->input->post('inputUsuarioNombre');
             $list['datosUsu'] =  $this->usuarios_modelo->BuscarUsuarioCompleto($this->input->post('inputDNI'));
             $this->load->view('cambiarpass_view',$list);
        }else{
            $fecha = $fecha = getdate();
            $data = array(
                'usuarioscontrasenia' => $this->input->post('inputPasswordUsuario'),
                'usuariosfechamod' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year'] 
            );
            $this->usuarios_modelo->cambiarPass($this->input->post('inputUsuarioID'),$data);
            $datosUsu =  $this->usuarios_modelo->BuscarUsuarioCompleto($this->input->post('inputDNI'));

            $this->enviarMailCambioPass($datosUsu[0]->usuariosnombre, $datosUsu[0]->personasemail,$this->input->post('inputPasswordUsuario'));

            
        }
    }

   
    public function asignarPerfil()
    {
        $b=0;
        $list['mensajeDNI'] ="";
        $list['DNI'] = '';
        $list['NombreCompuesto'] = '';
        $list['idPerfil'] =  '';
        $list['usarioNombre'] = '';

        if (empty($this->input->post('inputDNI'))){
            $list['mensajeDNI'] = "El DNI es Requerido";
            $b=1;
        }else{
            if( !$this->esnumerico($this->input->post('inputDNI'))){
            $list['mensajeDNI'] = "El DNI no es válido";
            $b = 1;
            }
        }
        if( empty($this->input->post('inputNombreCompuesto')) || empty($this->input->post('inputUsuarioID')) 
            || empty($this->input->post('inputUsuarioNombre')) ){
            $b = 1;
        }
        if ($b == 1) {
             $this->load->view('asignarnuevoperfil_view',$list);
        }else{
            $data2 = array(
                'usuariosid' => $this->input->post('inputUsuarioID'),
                'perfilesid' =>$this->input->post('usuarioPerfil') 
            );

            $transaction = 'inster into perfilesusuarios (usuariosid, perfilesid) values('.
            $data2['usuariosid'].','.$data2['perfilesid'].')';

            $dataAsignacion = 'usuariosid = '.$data['usuariosid'].', perfilesid = '.$data['perfilesid'];

            $dataAsignacion_Auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transaction,
                'datosingresados' => $dataAsignacion
            );

            $this->usuarios_modelo->RegistrarPerfilesUsuarios($data2);
            $this->auditoria_modelo->registrarAuditoria($dataAsignacion_Auditoria);
            redirect(base_url().'index.php/usuarios');
        }
       
    }

    public function esnumerico($dni){
        if (is_numeric($dni)){
            return True;
        }
        else{
            return False;
        }
    }

    public function iguales($data1,$data2)
    {
        if (strcmp($data1, $data2) == 0){
            return True;
        }else{
            return False;
        }
    }




    function ajax_list(){

            $list = $this->usuarios_modelo->get_datatables();
            $data = array();
            $i = 0;
            foreach ($list as $usuarios){

                $row = array();
                $row[] = $usuarios->usuariosid;
                $row[] = $usuarios->usuariosnombre;
                $row[] = $usuarios->personasnombrecompuesto;
                $row[] = $usuarios->perfilesnombre;
                //$row[] = $usuarios->estadosid;
                if ($usuarios->estadosid == 1){
                    $row[] = '<button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal" data-target="#miModal'.$i.'" ><i data-toggle="tooltip" title="Eliminar Usuario" class="fas fa-times-circle" ></i></button>'.
                    '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																				<div class="modal-dialog modal-lg" role="document">
																					<div class="modal-content">
																						<div class="modal-header" >
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">&times;</span>
																							</button>
																							
																						</div>
																						<div class="modal-body">
																							<label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
																						</div>
																						<div class="row">		
																							<div class="col-sm-3">
																								
																							</div>
																							<div class="col-sm-3">
																								<button type="button"  class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/usuarios/cancelarUsuario/".$usuarios->usuariosid."'".'">Si</button>
																							</div>
																							<div class="col-sm-3">
																							<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
																								
																							</div>
																							
																							<div class="col-sm-3">
																								
																							</div>
																						</div>
																						<br><br>
																										
																					</div>
																				</div>
																			</div>';
                }
                else{
                    $row[] = '';
                }
                $i = $i + 1;
                $data[] = $row;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->usuarios_modelo->count_all(),
                            "recordsFiltered" => $this->usuarios_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
        }

    public function cancelarUsuario($idUs){
        $fecha =  getdate();
        $data = array(
            'estadosid'=>2,
            'usuariosfechabaja'=> $fecha['mday']."/".$fecha['mon']."/".$fecha['year']    
        );
        $transaction = 'update usuarios set estadosid='
        .$data['estadosid'].', usuariosfechabaja='."'"
        .$data['usuariosfechabaja']."'";

        $dataUsersUpdated = 'estadosid='.$data['estadosid'].', usuariosfechabaja='.$data['usuariosfechabaja'];

        $dataUser_Auditoria = array(
            'nombreusuario' => $this->session->userdata('usuario'),
            'transaccion' => $transaction,
            'datosmodificados' => $dataUsersUpdated
        );

        $this->usuarios_modelo->cancelaUsuario($idUs,$data);
        $this->auditoria_modelo->registrarAuditoria($dataUser_Auditoria);
        redirect(base_url().'index.php/usuarios');
    }

    public function CerrarSesion(){
        $this->session->sess_destroy();
        redirect(base_url().'index.php/login');
    }    
    
}

?>