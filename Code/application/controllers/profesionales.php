<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Profesionales extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('profesionales_modelo');
        $this->load->model('auditoria_modelo');
        $this->load->model('pacientes_modelo');
        $this->load->library('session'); 
    }
    
    public function index(){
        $this->load->view('head_view');            
        if($this->session->userdata('logueo') == TRUE || $this->session->userdata('usuario') != '') {
            $this->load->view('profesionales_view');            
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }

    public function registrarProfesional(){
            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio');
            $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
            $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
            $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
            $this->form_validation->set_rules('profesionalMatricula', 'Matricula', 'required|min_length[3]|max_length[15]'); 
            $this->form_validation->set_rules('pacienteApellido', 'Apellido', 'required|min_length[2]|max_length[15]'); 
            $this->form_validation->set_rules('pacienteNombre', 'Nombre', 'required|min_length[2]|max_length[30]');  
            $this->form_validation->set_rules('pacienteDNI', 'DNI', 'required|min_length[8]|max_length[10]|callback_existente|callback_esnumerico');
            $this->form_validation->set_rules('pacienteDomicilio', 'Domicilio', 'required|min_length[5]|max_length[30]'); 
            
            $this->form_validation->set_rules('pacienteEmail', 'Email', 'valid_email');

            $list['sexo'] = $this->pacientes_modelo->sexo();
            $list['especialidades'] = $this->profesionales_modelo->Especialidades();
            if (isset($_POST['Registrar'])) {
                $list['fecha'] = $this->input->post('fechaN');
            } else {
              $list['fecha'] = date('D M d Y H:i:s \G\M\TO (T)');
            }

            if($this->form_validation->run() == FALSE){
                
                $list['matricula'] = $this->input->post('profesionalMatricula');
                $list['apellido'] =  $this->input->post('pacienteApellido');
                $list['nombre'] = $this->input->post('pacienteNombre');
                $list['dni'] = $this->input->post('pacienteDNI');
                $list['domicilio'] = $this->input->post('pacienteDomicilio');
                $list['email'] = $this->input->post('pacienteEmail');
                $list['sexoid'] = $this->input->post('pacienteSexo');
                $this->load->view('registrarProfesional_view',$list);
            }else{
                $data = array(
                    'personasapellido' => $this->input->post('pacienteApellido'),
                    'personasnombre' => $this->input->post('pacienteNombre'),
                    'personasnrodocumento' => $this->input->post('pacienteDNI'),
                    'sexoid' => $this->input->post('pacienteSexo'),
                    'personasdomicilio' => $this->input->post('pacienteDomicilio'),
                    'personasfechanacimiento' => $this->input->post('fechaN'),
                    'personasemail' => $this->input->post('pacienteEmail'),
                    );

                $transactionPerson = 'insert into personas (personasapellido,personasnombre,personasnrodocumento,sexoid,personasfechanacimiento,personasdomicilio,personasemail) values ('.
                "'".$data['personasapellido']."','".$data['personasnombre']."','".$data['personasnrodocumento'].
                "',".$data['sexoid'].",'".$data['personasfechanacimiento']."','".$data['personasdomicilio'].
                "','".$data['personasemail']."')";

                $dataPersonInserted = 'personasapellido='.$data['personasapellido'].';personasnombre='.$data['personasnombre'].
                ';personasnrodocumento='.$data['personasnrodocumento'].';sexoid='.$data['sexoid'].
                ';personasfechanacimiento='.$data['personasfechanacimiento'].';personasdomicilio='.$data['personasdomicilio'].
                ';personasemail='.$data['personasemail'];
                
                $dataPerson_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionPerson,
                    'datosingresados' => $dataPersonInserted
                );

                $this->pacientes_modelo->registrarPersona($data);
                $this->auditoria_modelo->registrarAuditoria($dataPerson_Auditoria);
                $paciente = $this->pacientes_modelo->buscarPersona($data['personasnrodocumento']);
                $data2 = array(
                    'personasid' => $paciente['personasid'],
                    'intervalosid' => 1,
                    'profesionalesmatricula' => $this->input->post('profesionalMatricula'),
                );

                $transactionProfessional = 'insert into profesionales (personasid,intervalosid,profesionalesmatricula) values('.
                $data2['personasid'].','.$data2['intervalosid'].",'".$data2['profesionalesmatricula']."')";
                $dataProfessionalInserted = 'personasid = '.$data2['personasid'].
                ', intervalosid = '.$data2['intervalosid'].
                ', profesionalesmatricula = '.$data2['profesionalesmatricula'];

                $dataProfessional_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionProfessional,
                    'datosingresados' => $dataProfessionalInserted
                );

                $this->profesionales_modelo->registrarProfesional($data2);
                $this->auditoria_modelo->registrarAuditoria($dataProfessional_Auditoria);
                $matricula = strtoupper ( $this->input->post('profesionalMatricula') );
                $idprof = $this->profesionales_modelo->buscarProfesional($matricula);
                $especialidades = $this->input->post('profesionalEspecialidades');
                for ($i=0; $i < count($especialidades); $i++) { 
                    $data3 = array(
                        'profesionalesid' => $idprof['profesionalesid'],
                        'especialidadesid' => $especialidades[$i]
                    );

                    $transactionEspxProf = 'insert into especialidadesxprofesionales (profesionalesid,especialidades) values('.
                    $data3['profesionalesid'].','.$data3['especialidadesid'].')';

                    $dataEspxProfInserted = 'profesionalesid = '.$data3['profesionalesid'].', especialidadesid = '.$data3['especialidadesid'];

                    $this->profesionales_modelo->registrarEspecialidadesxProfesionales($data3);
                    
                    $dataEspxProf_Auditoria = array(
                        'nombreusuario' => $this->session->userdata('usuario'),
                        'transaccion' => $transactionEspxProf,
                        'datosingresados' => $dataEspxProfInserted
                    );
                    $this->auditoria_modelo->registrarAuditoria($dataEspxProf_Auditoria);
                    
                    unset($data3);
                }

                $this->profesionales_modelo->insertaAgenda($idprof['profesionalesid']);
                $data['message'] = 'Data Inserted Successfully';
                redirect(base_url().'index.php/profesionales');
            }
    }

    public function modificarProfesional($id){
            $list['id'] = $id;
            $list['datos'] = $this->profesionales_modelo->obtieneDatosProfesional($id);
            $list['datos2'] = $this->profesionales_modelo->obtieneEspecialidadesProfesional($id);
            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio');
            $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
            $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
            $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
            $this->form_validation->set_rules('profesionalMatricula', 'Matricula', 'required|min_length[3]|max_length[15]'); 
            $this->form_validation->set_rules('pacienteApellido', 'Apellido', 'required|min_length[2]|max_length[15]'); 
            $this->form_validation->set_rules('pacienteNombre', 'Nombre', 'required|min_length[2]|max_length[30]');  
            $this->form_validation->set_rules('pacienteDNI', 'DNI', 'required|min_length[8]|max_length[10]|callback_esnumerico');
            $this->form_validation->set_rules('pacienteDomicilio', 'Domicilio', 'required|min_length[5]|max_length[30]'); 
            $this->form_validation->set_rules('pacienteEmail', 'Email', 'valid_email');

            $list['sexo'] = $this->pacientes_modelo->sexo();
            $list['especialidades'] = $this->profesionales_modelo->Especialidades();

             if (isset($_POST['Registrar'])) {
                           $list['fecha'] = $this->input->post('fechaN');
                        } else {
                           $list['fecha'] =  date_format( date_create($list['datos']['personasfechanacimiento']), 'd/m/Y');
                        }
            if($this->form_validation->run() == FALSE){
                
                $list['matricula'] = $this->input->post('profesionalMatricula');
                $list['apellido'] =  $this->input->post('pacienteApellido');
                $list['nombre'] = $this->input->post('pacienteNombre');
                $list['dni'] = $this->input->post('pacienteDNI');
                $list['domicilio'] = $this->input->post('pacienteDomicilio');
                $list['email'] = $this->input->post('pacienteEmail');
                $list['sexoid'] = $this->input->post('pacienteSexo');
                $list['profesionalEspecialidades'] = $this->input->post('profesionalEspecialidades');
                $this->load->view('modificarProfesional_view',$list);
            }else{
                $data = array(
                    'personasapellido' => $this->input->post('pacienteApellido'),
                    'personasnombre' => $this->input->post('pacienteNombre'),
                    'personasnrodocumento' => $this->input->post('pacienteDNI'),
                    'sexoid' => $this->input->post('pacienteSexo'),
                    'personasdomicilio' => $this->input->post('pacienteDomicilio'),
                    'personasfechanacimiento' => $this->input->post('fechaN'),
                    'personasemail' => $this->input->post('pacienteEmail'),
                    );

                $transactionPerson = 'update  personas set '.
                "perosnasapellido = ".$data['personasapellido'].', '.
                "personasnombre = ".$data['personasnombre'].', '.
                "personasnrodocumento = ".$data['personasnrodocumento'].', '.
                "sexoid = ".$data['sexoid'].', '.
                "personasfechanacimiento = ".$data['personasfechanacimiento'].', '.
                "personasdomicilio = ".$data['personasdomicilio'].', '.
                "personasemail = ".$data['personasemail'];
                
                $dataPersonUpdated = 'personasapellido='.$data['personasapellido'].';personasnombre='.$data['personasnombre'].
                ';personasnrodocumento='.$data['personasnrodocumento'].';sexoid='.$data['sexoid'].
                ';personasfechanacimiento='.$data['personasfechanacimiento'].';personasdomicilio='.$data['personasdomicilio'].
                ';personasemail='.$data['personasemail'];
                
                $dataPerson_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionPerson,
                    'datosmodificados' => $dataPersonUpdated
                );

                $iden = $list['datos']['personasid'];
                
                $this->pacientes_modelo->modificarPersona($data,$iden);
                $this->auditoria_modelo->registrarAuditoria($dataPerson_Auditoria);
                $data2 = array(
                    'personasid' => $iden,
                    'intervalosid' => 1,
                    'profesionalesmatricula' => $this->input->post('profesionalMatricula'),
                );

                $transactionProfessional = 'update profesionales  set '.
                'personasid = '.$data2['personasid'].','.
                'intervalosid = '.$data2['intervalosid'].",".
                'profesionalesmatricula = '."'".$data2['profesionalesmatricula']."')";
                $dataProfessionalUpdated = 'personasid = '.$data2['personasid'].
                ', intervalosid = '.$data2['intervalosid'].
                ', profesionalesmatricula = '.$data2['profesionalesmatricula'];

                $dataProfessional_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionProfessional,
                    'datosmodificados' => $dataProfessionalUpdated
                );

                $this->profesionales_modelo->modificarProfesional($data2,$id);
                $this->auditoria_modelo->registrarAuditoria($dataProfessional_Auditoria);
                
                $transactionEspxProf = 'delete from especialidadesxprofesionales where profesionalesid = '.$id;
                $dataEspxProfDeleted = 'se borro todas las especialidades de ese profesional';
                $dataEspxProf_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionEspxProf,
                    'datosmodificados' => $dataEspxProfDeleted
                );
                $this->profesionales_modelo->borrarEspecialidadesProfesional($id);
                $this->auditoria_modelo->registrarAuditoria( $dataEspxProf_Auditoria);
                $especialidades = $this->input->post('profesionalEspecialidades');
                for ($i=0; $i < count($especialidades); $i++) { 
                    $data3 = array(
                        'profesionalesid' =>$id,
                        'especialidadesid' => $especialidades[$i]
                    );
                    $transactionEspxProf = 'insert into especialidadesxprofesionales (profesionalesid,especialidades) values('.
                    $data3['profesionalesid'].','.$data3['especialidadesid'].')';

                    $dataEspxProfInserted = 'profesionalesid = '.$data3['profesionalesid'].', especialidadesid = '.$data3['especialidadesid'];

                    
                    $dataEspxProf2_Auditoria = array(
                        'nombreusuario' => $this->session->userdata('usuario'),
                        'transaccion' => $transactionEspxProf,
                        'datosingresados' => $dataEspxProfInserted
                    );
                    $this->auditoria_modelo->registrarAuditoria($dataEspxProf2_Auditoria);

                    $this->profesionales_modelo->registrarEspecialidadesxProfesionales($data3);
                    unset($data3);
                }
                $data['message'] = 'Data Inserted Successfully';
                redirect(base_url().'index.php/profesionales');
            }
    }
    public function cancelarProfesional($idEsp,$idprof){
        $fecha =  getdate();
        $data = array(
            'estadosid'=>2,
            'profesionalesbaja'=> $fecha['mday']."/".$fecha['mon']."/".$fecha['year'],
            'profxespefechamod' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']
        );

        $transactionEspxProf = 'update especialidadesxprofesionales set estadosid = 2 , profesionalesbaja ='.
        $fecha['mday']."/".$fecha['mon']."/".$fecha['year'].', '.
        'profxespefechamod = '.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'].' '.
        'where profesioanalesid = '.$idprof.' and '.
        'especialidadesid = '.$idEsp;
        
        $dataEspxProfUpdated = 'estadosid =2, '.
        'profesionalesbaja ='.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'].', '.
        'profxespefechamod = '.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

        $dataEspxProf_Auditoria = array(
            'nombreusuario' => $this->session->userdata('usuario'),
            'transaccion' => $transactionEspxProf,
            'datosmodificados' => $dataEspxProfUpdated
        );
        
        $this->profesionales_modelo->cancelaProfesional($idEsp,$idprof,$data);
        $this->auditoria_modelo->registrarAuditoria($dataEspxProf_Auditoria);
        redirect(base_url().'index.php/profesionales');
    }
    public function habilitarProfesional($idEsp,$idprof)
    {
        $fecha =  getdate();
        $data = array(
            'estadosid'=>1,
            'profxespefechamod' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']
        );

        $transactionEspxProf = 'update especialidadesxprofesionales set estadosid = 1 , '.
        'profxespefechamod = '.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'].' '.
        'where profesioanalesid = '.$idprof.' and '.
        'especialidadesid = '.$idEsp;
        
        $dataEspxProfUpdated = 'estadosid =1, '.
        'profxespefechamod = '.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

        $dataEspxProf_Auditoria = array(
            'nombreusuario' => $this->session->userdata('usuario'),
            'transaccion' => $transactionEspxProf,
            'datosmodificados' => $dataEspxProfUpdated
        );
        
        $this->profesionales_modelo->HabilitaProfesional($idEsp,$idprof,$data);
        $this->auditoria_modelo->registrarAuditoria($dataEspxProf_Auditoria);
        redirect(base_url().'index.php/profesionales');
    }


    public function existente($dni){
            $list=$this->pacientes_modelo->buscarPersona($dni);
            if ($list!=null){
                $this->form_validation->set_message('existente','El DNI ya existe');
                return False;
            } 
            else{
                return True;
            }
    }

    public function esnumerico($dni){
        if (is_numeric($dni)){
            return True;
        }
        else{
            $this->form_validation->set_message('esnumerico','DNI no valido');
            return False;
        }
    }

    function ajax_list(){

            $list = $this->profesionales_modelo->get_datatables();
            $data = array();
            $i = 0;
            foreach ($list as $profesionales){

                $row = array();
                $row[] = $profesionales->profesionalesid;
                $row[] = $profesionales->profesionalesmatricula;
                $row[] = $profesionales->personasnrodocumento;
                $row[] = $profesionales->personasnombrecompuesto;
                $row[] = $profesionales->especialidadesnombre;
                $row[] = $profesionales->estadosnombre;
                if ($profesionales->estadosnombre =='NO ACTIVO'){
                    $row[] = '      <button  type="button" class="btn btn-primary" style="background:#337ab7; border-color:#2e6da4;" onclick="location.href='."'".
                    base_url()."index.php/profesionales/habilitarProfesional/".$profesionales->especialidadesid."/".
                    $profesionales->profesionalesid."'".'"><i class="fas fa-check"></i></button> ';
                }else{
                    $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Datos de Profesional" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".
                    base_url()."index.php/profesionales/modificarProfesional/".$profesionales->profesionalesid."'".
                    '"><i class="fas fa-pencil-alt"></i></button>'.
                    '<button id="mostrar'.$i.'" type="button" class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal" data-target="#miModal'.$i.'" ><i class="fas fa-times-circle" data-toggle="tooltip" title="Eliminar Profesional" ></i></button>'.
                    '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																				<div class="modal-dialog modal-lg" role="document">
																					<div class="modal-content">
																						<div class="modal-header" >
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">&times;</span>
																							</button>
																							
																						</div>
																						<div class="modal-body">
																							<label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
																						</div>
																						<div class="row">		
																							<div class="col-sm-3">
																								
																							</div>
																							<div class="col-sm-3">
																								<button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/profesionales/cancelarProfesional/".$profesionales->especialidadesid."/".$profesionales->profesionalesid."'".'">Si</button>
																							</div>
																							<div class="col-sm-3">
																							<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
																								
																							</div>
																							
																							<div class="col-sm-3">
																								
																							</div>
																						</div>
																						<br><br>
																										
																					</div>
																				</div>
																			</div>';
                }
                

                
                $data[] = $row;
                $i = $i + 1;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->profesionales_modelo->count_all(),
                            "recordsFiltered" => $this->profesionales_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
        }

    public function CerrarSesion(){
        $this->session->sess_destroy();
        redirect(base_url().'index.php/login');
    }    
    
}

?>