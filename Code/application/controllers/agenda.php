<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Agenda extends CI_Controller
{

	public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->model('agenda_modelo');
        $this->load->model('profesionales_modelo');
        $this->load->model('licencias_modelo');
        $this->load->model('turnos_modelo');
        $this->load->model('vacaciones_modelo');
        $this->load->model('feriados_modelo');
        $this->load->model('pacientes_modelo');
        $this->load->model('auditoria_modelo');
        $this->load->library('session'); 
        $this->load->helper('form');
        $this->load->helper('date');        
    }
    
    public function index(){
        $actualiza = $this->pacientes_modelo->ActualizaEstado();
        $actualizaTurnos = $this->turnos_modelo->ActualizaEsatdoTurno();
        $this->load->view('head_view');
        $profesionalid=$this->session->userdata('profesionalid');
        if($this->session->userdata('logueo') == TRUE || $this->session->userdata('usuario') != '') {
            $especialidad['especialidades']=$this->agenda_modelo->Especialidades($profesionalid);
            $especialidad['fecha']=date('D M d Y H:i:s \G\M\TO (T)');

            $especialidad['dias'] = $this->agenda_modelo->diasagendaprof($profesionalid);
            $especialidad['licencias'] = $this->licencias_modelo->diasLicencias($profesionalid,$especialidad['especialidades'][0]->especialidadesid);
            $especialidad['vacaciones'] = $this->vacaciones_modelo->diasVacaciones($profesionalid,$especialidad['especialidades'][0]->especialidadesid);
            $especialidad['feriados'] = $this->feriados_modelo->diasFeriados();
            $this->load->view('agenda_view',$especialidad);            
        }
        else{
            redirect(base_url().'index.php/login');
            
        }
        
    }

    


    public function formaConsulta(){

       
        $idEsp = $this->input->post('especialidadesid');
        $fecha =$this->input->post('especialidadesid');
        $table = "fn_agendaturnonormales(".$idProf.",'".$fecha."',".$idEsp.")";
        
        return $table;
    } 

    public function turnoNormales(){
        $idProf = $this->session->userdata('profesionalid');
        $iddia = date("w",strtotime($this->input->post('fechaN')));
        if (isset($_POST['Buscar'])){
            if(empty($this->input->post('especialidadesid'))){
                $fecha = $this->input->post('fechaN');
                $table = "fn_agendaturnonormales(null,'".$fecha."',null)";
                $table2 = "fn_agendasobreturnos(null,'".$fecha."',null)";
                $list['agenda'] = $this->agenda_modelo->get_datatables($table);
                $list['agenda'] = array();
                $list['agenda2'] = $this->agenda_modelo->get_datatables($table2);
                $list['agenda2'] = array();
                
            }else{
                $idEsp = $this->input->post('especialidadesid');
                $fecha = $this->input->post('fechaN');
                $d = explode("/",$this->input->post('fechaN'));
                $d = $d[2]."-".$d[1]."-".$d[0];
                $iddia = date("w",strtotime($d));
                $list['intervalo'] = $this->agenda_modelo->obtieneIntervalo($idProf,$idEsp,$iddia); 
                $table = "fn_agendaturnonormales(".$idProf.",'".$fecha."',".$idEsp.")";
                $table2 = "fn_agendasobreturnos(".$idProf.",'".$fecha."',".$idEsp.")";
                $list['agenda'] = $this->agenda_modelo->get_datatables($table);
                $list['agenda2'] = $this->agenda_modelo->get_datatables($table2);
            }
            
            
            
           
        }else{
            $fecha = $this->input->post('fechaN');
            $table = "fn_agendaturnonormales(null,'".$fecha."',null)";
            $table2 = "fn_agendasobreturnos(null,'".$fecha."',null)";
            $list['agenda'] = $this->agenda_modelo->get_datatables($table);
            $list['agenda'] = array();
            $list['agenda2'] = $this->agenda_modelo->get_datatables($table2);
            $list['agenda2'] = array();
        }
        $list['fecha']=$fecha;
        $list['test'] = strtotime($this->input->post('fechaN'));
        $list['especialidades']=$this->agenda_modelo->Especialidades($idProf);
        $list['dias'] = $this->agenda_modelo->diasagenda($idProf,$this->input->post('especialidadesid'));
        $list['licencias'] = $this->licencias_modelo->diasLicencias($idProf,$list['especialidades'][0]->especialidadesid);
        $list['vacaciones'] = $this->vacaciones_modelo->diasVacaciones($idProf,$list['especialidades'][0]->especialidadesid);
        $list['feriados'] = $this->feriados_modelo->diasFeriados();
        $this->load->view('head_view');
        $this->load->view('agenda_view',$list);
         
    }

    public function DiasyHorasLab()
    {
        $id= $this->session->userdata('profesionalid');
        $list['ocultar'] ='hidden=true';
        $list['datosprof'] = $this->profesionales_modelo->obtieneDatosProfesional($id);
        $idEsp = $this->input->post('inputEspecialidades');
        $list['especialidades'] = $this->agenda_modelo->Especialidades($id);
        $list['dias'] =  $this->agenda_modelo->dias();
        
        $list['MensajeTurnoMañana'] ='';
        $list['MensajeTurnoTarde'] ='';
        $list['mensajeinsert'] = '';
        $TMini = $this->input->post('inputMañanaInicio');
        $TMfin = $this->input->post('inputMañanaFin');
        $TTini = $this->input->post('inputTardeInicio');
        $TTfin = $this->input->post('inputTardeFin');
        $band = 0;  
        if (isset($_POST['Buscar'])|| isset($_POST['Registrar'])) {

             $list['ocultar']='';               
                    if ($this->agenda_modelo->VerificaDias($id,$idEsp,$this->input->post('inputDias'))) {
                        $list['mensajeinsert'] = "No puede registrar dias laborales repetidos para la misma especialidad";
                        $band =1 ;
                    }
                $fecha = getdate();
                if ($this->input->post('inputFranjaHoraria') == 1) {
                    $b = 0;

                    if ($TMini >= $TMfin){
                         $list['MensajeTurnoMañana'] ='La hora inicio no puede ser mayor o igual a la hora fin del turno mañana';
                         $b = 1;
                    }
                    if(($b == 0)&&($band == 0)){                    
                        $data = array('profesionalesid'=> $id,
                              'especialidadesid'=>  $this->input->post('inputEspecialidades'),
                              'diasid' => $this->input->post('inputDias'),
                              'agendastmini' =>  $TMini,
                              'agendastmfin' =>  $TMfin,
                              'agendasfecharegistro' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year']
                        );
                        $this->agenda_modelo->insertarAgenda($data);
                    }
                }
                if ($this->input->post('inputFranjaHoraria') == 2 ) {
                    $b = 0;

                    if ($TTini >= $TTfin){
                        $list['MensajeTurnoTarde'] ='La hora inicio no puede ser mayor o igual a la hora fin del turno tarde';
                        $b = 1;
                    }
                    if(($b == 0)&&($band == 0)){ 
                        $data = array('profesionalesid'=> $id,
                              'especialidadesid'=>  $this->input->post('inputEspecialidades'),
                              'diasid' => $this->input->post('inputDias'),
                              'agendasttini' =>  $TTini,
                              'agendasttfin' =>  $TTfin,
                              'agendasfecharegistro' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year']
                                );
                        $this->agenda_modelo->insertarAgenda($data);
                    }
                }
                if ($this->input->post('inputFranjaHoraria') == 0){
                    $b = 0;

                    if ($TMini >= $TMfin){
                         $list['MensajeTurnoMañana'] ='La hora inicio no puede ser mayor o igual a la hora fin del turno mañana';
                         $b = 1;
                    }
                    if ($TTini >= $TTfin){
                        $list['MensajeTurnoTarde'] ='La hora inicio no puede ser mayor o igual a la hora fin del turno tarde';
                        $b = 1;
                    } 
                    if(($b == 0)&&($band == 0)){
                        $data = array('profesionalesid'=> $id,
                                  'especialidadesid'=>  $this->input->post('inputEspecialidades'),
                                  'diasid' => $this->input->post('inputDias'),
                                  'agendastmini' =>  $TMini,
                                  'agendastmfin' =>  $TMfin,
                                  'agendasttini' =>  $TTini,
                                  'agendasttfin' =>  $TTfin,
                                  'agendasfecharegistro' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year']
                        );
              
                        $transaction = "insert into agendas (profesionalesid, especialidadesid, diasid, agendastmini, agendastmfin, agendasttini, agendasttfin, agendasfecharegistro) values ("."'".$data['profesionalesid']."','".$data['especialidadesid']."','".$data['diasid']."','".$data['agendastmini']."','".$data['agendastmfin']."','".$data['agendasttini']."','".$data['agendasttfin']."','".$data['agendasfecharegistro']; 

                        $dataInserted = "profesionalesid = ".$data['profesionalesid'].", especialidadesid = ".$data['especialidadesid'].", diasid = ".$data['diasid'].", agendastmini = ".$data['agendastmini'].", agendastmfin = ".$data['agendastmfin'].", agendasttini = ".$data['agendasttini'].", agendasttfin = ".$data['agendasttfin'].", agendasfecharegistro = ".$data['agendasfecharegistro']; 

                        $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
                        );

                        $this->agenda_modelo->insertarAgenda($data);  
                        $this->auditoria_modelo->registrarAuditoria($data_auditoria);        
                    }   
                }
        } 
        
        
        
        $list['agenda'] = $this->agenda_modelo->get_datatables2($id,$idEsp);
        $this->load->view('registrarDiasHorasLaborales_view',$list);
    }



public function registrarLicencias(){
    $id= $this->session->userdata('profesionalid');
    $list['mensajeerror'] = "";
    $list['datosprof'] = $this->profesionales_modelo->obtieneDatosProfesional($id);
    $list['fechaI'] = date('D M d Y H:i:s \G\M\TO (T)');//nuevo
    $list['fechaF'] = date('D M d Y H:i:s \G\M\TO (T)');//nuevo
    
    $especialidades = $this->profesionales_modelo->obtieneEspecialidadesProfesional($id);
    
    if (isset($_POST['Registrar']) && $this->validarfecha($list, $id, $this->input->post('fechaI'), $this->input->post('fechaF'))) {
    
        foreach ($especialidades as $e) {
           $data = array(
            'licenciasfechaini' => $this->input->post('fechaI'),
            'licenciasfechafin' => $this->input->post('fechaF'),
            'profesionalesid' => $id, 
            'especialidadesid' => $e->especialidadesid,
            'estadosid' => 1
            );

           $transaction = "insert into licencias (licenciasfechaini, licenciasfechafin, profesionalesid, especialidadesid, estadosid) values ("."'".$data['licenciasfechaini']."','".$data['licenciasfechafin']."','".$data['profesionalesid']."','".$data['especialidadesid']."','".$data['estadosid'];

           $dataInserted = "licenciasfechaini = ".$data['licenciasfechaini'].", licenciasfechafin = ".$data['licenciasfechafin'].", profesionalesid = ".$data['profesionalesid'].", especialidadesid = ".$data['especialidadesid'].", estadosid = ".$data['estadosid'];

           $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
            );

            $this->licencias_modelo->registrarLicencias($data);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        }
        $licencias = $this->licencias_modelo->obtieneLicencias($id);
        $fechaIni = explode("/",$this->input->post('fechaI'));
        $fechaIni = $fechaIni[0]."-".$fechaIni[1]."-".$fechaIni[2];
       
        $fechaFin = explode("/",$this->input->post('fechaF'));
        $fechaFin = $fechaFin[0]."-".$fechaFin[1]."-".$fechaFin[2];
        
        $listaDiasNombre = [];
        $listaMeses = [];
        $listaAnio = [];
        $listaDias = [];
        $fechacompara = $fechaIni;
     
        while(strtotime($fechaFin) >= strtotime($fechacompara) ){
                $diasid = date('w',strtotime($fechacompara));
                $mes = date('n', strtotime($fechacompara));
                $anio = date('Y', strtotime($fechacompara));
                $dia = date('j', strtotime($fechacompara));
                $listaDiasNombre[] = $diasid;
                $listaMeses[] = $mes - 1;
                $listaAnio[] = $anio;
                $listaDias[] = $dia;
                $fechacompara = date("d-m-Y", strtotime($fechacompara . " + 1 day"));
        }
        $tam = sizeof($listaDiasNombre);
        foreach ($licencias as $l) {
            for ($i=0 ; $i < $tam ; $i++ ) { 
                $ids=explode('-',$l->licenciasids);
                for($j = 0;$j< count($ids);$j++){
                    $data = array(
                        'licenciasid' => $ids[$j],
                        'diasid' => $listaDiasNombre[$i],
                        'mes' => $listaMeses[$i],
                        'anio' => $listaAnio[$i],
                        'dia' => $listaDias[$i] 
                    );


                    $transaction = "insert into licenciasxdias (licenciasid, diasid, mes, anio, dia) values ("."'".$data['licenciasid']."','".$data['diasid']."','".$data['mes']."','".$data['anio']."','".$data['dia']; 

                    $dataInserted = "licenciasid = ".$data['licenciasid'].", diasid = ".$data['diasid'].", mes = ".$data['mes'].", anio = ".$data['anio'].", dia = ".$data['dia'];

                    $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
                    );

                    $this->licencias_modelo->registrarLicenciasxDia($data);
                    $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                }
               
            }
        }
    

    
    }
    $list['dias'] =  $this->agenda_modelo->diasagendaprof($id);
    $list['licencias'] = $this->licencias_modelo->diasLicencias($id,$especialidades[0]->especialidadesid);
    $list['vacaciones'] = $this->vacaciones_modelo->diasVacaciones($id,$especialidades[0]->especialidadesid);
    $this->load->view('registrarLicencias_view',$list);
}



public function modificarLicencias($licenciasids){
    $id= $this->session->userdata('profesionalid');
    $list['mensajeerror'] = "";
    $list['datosprof'] = $this->profesionales_modelo->obtieneDatosProfesional($id);
    $list['fechaI'] = date('D M d Y H:i:s \G\M\TO (T)');//nuevo
    $list['fechaF'] = date('D M d Y H:i:s \G\M\TO (T)');//nuevo
    $list['ids'] = $licenciasids;
    $ids = explode('-' , $licenciasids);
    $especialidades = $this->profesionales_modelo->obtieneEspecialidadesProfesional($id);
    $infolicencias = $this->licencias_modelo->obtieneUnaFilaLicencia($id,$licenciasids);

    foreach ($infolicencias as $li ) {
        $fechainicio = explode("-",$li->fechainicio);
        $fechainicio = $fechainicio[2]."/".$fechainicio[1]."/".$fechainicio[0];

        $fechafin = explode("-",$li->fechafin);
        $fechafin = $fechafin[2]."/".$fechafin[1]."/".$fechafin[0];
        
        $list['fechainicio'] = $fechainicio;
        $list['fechafin'] = $fechafin;

    }

    if (isset($_POST['Registrar']) && $this->validarfecha($list, $id, $this->input->post('fechaI'), $this->input->post('fechaF'))) {
        
        for ($i=0;$i<count($ids);$i++) {
            $fecha =  getdate();
            $data = array(
                'licenciasfechaini' => $this->input->post('fechaI'),
                'licenciasfechafin' => $this->input->post('fechaF'),
                'licenciasfechamod' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']

            );

            $transaction = "update licencias set licenciasfechaini = ".$data['licenciasfechaini'].", licenciasfechafin = ".$data['licenciasfechafin'].", licenciasfechamod = ".$data['licenciasfechamod'];

            $dataUpdated = " licenciasid = ".$licenciasids." , licenciasfechaini = ".$data['licenciasfechaini'].", licenciasfechafin = ".$data['licenciasfechafin'].", licenciasfechamod = ".$data['licenciasfechamod'];

            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
            );

            $this->licencias_modelo->modificarLicencias($data,$ids[$i]);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        }
        
        $licencias = $this->licencias_modelo->obtieneUnaFilaLicencia($id,$licenciasids);
        $fechaIni = explode("/",$this->input->post('fechaI'));
        $fechaIni = $fechaIni[0]."-".$fechaIni[1]."-".$fechaIni[2];
       
        $fechaFin = explode("/",$this->input->post('fechaF'));
        $fechaFin = $fechaFin[0]."-".$fechaFin[1]."-".$fechaFin[2];
        
        $listaDiasNombre = [];
        $listaMeses = [];
        $listaAnio = [];
        $listaDias = [];
        $fechacompara = $fechaIni;
     
        while(strtotime($fechaFin) >= strtotime($fechacompara) ){
                $diasid = date('w',strtotime($fechacompara));
                $mes = date('n', strtotime($fechacompara));
                $anio = date('Y', strtotime($fechacompara));
                $dia = date('j', strtotime($fechacompara));
                $listaDiasNombre[] = $diasid;
                $listaMeses[] = $mes - 1;
                $listaAnio[] = $anio;
                $listaDias[] = $dia;
                $fechacompara = date("d-m-Y", strtotime($fechacompara . " + 1 day"));
        }
        $tam = sizeof($listaDiasNombre);
        foreach ($licencias as $l) {
            for ($j=0; $j < count($ids) ; $j++) {
                $this->licencias_modelo->eliminarLicenciasxDia($ids[$j]);

                for ($i=0 ; $i < $tam ; $i++ ) { 
                    $data = array(
                    'licenciasid' => $ids[$j],
                    'diasid' => $listaDiasNombre[$i],
                    'mes' => $listaMeses[$i],
                    'anio' => $listaAnio[$i],
                    'dia' => $listaDias[$i] 
                );

                $transaction = "update licenciasxdias set licenciasid = ".$data['licenciasid'].", diasid = ".$data['diasid'].", mes = ".$data['mes'].", anio = ".$data['anio'].", dia = ".$data['dia']; 

                $dataUpdated = " licenciasxdiasid = ".$ids[$j]." , licenciasid = ".$data['licenciasid'].", diasid = ".$data['diasid'].", mes = ".$data['mes'].", anio = ".$data['anio'].", dia = ".$data['dia']; 

                $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
                );

                $this->licencias_modelo->registrarLicenciasxDia($data);
                $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                }
                
            }
        }
        redirect(base_url().'index.php/agenda/registrarLicencias');
    }
    $list['dias'] =  $this->agenda_modelo->diasagendaprof($id);
    //$list['licencias'] = $this->licencias_modelo->diasLicencias($id,$especialidades[0]->especialidadesid);
    $this->load->view('modificarLicencias_view',$list);   
}




            
public function validarfecha(&$list,$idProf, $fi, $ff){
        $fi = explode("/",$fi);
        $fi = $fi[2]."-".$fi[1]."-".$fi[0];
        //$fechaIni = date("Y-m-d", strtotime($fechaIni));
        $ff = explode("/",$ff);
        $ff = $ff[2]."-".$ff[1]."-".$ff[0];
        $existelicencia = $this->licencias_modelo->validarLicencia($idProf, $fi, $ff);

        if(strtotime($ff) < strtotime($fi) ){
            $list["mensajeerror"] = "La fecha de fin de licencia no debe ser menor que la fecha de inicio de licencia";
            return false;
        }else{
            if ($existelicencia){
                $list["mensajeerror"] = "Ya existe una licencia registrada con esa fecha";
                return false;
            }else{
                return true;
            }
            
        }
}

public function registrarVacaciones(Type $var = null)
{
    $list['fechaI'] = date('D M d Y H:i:s \G\M\TO (T)');//nuevo
    $list['fechaF'] = date('D M d Y H:i:s \G\M\TO (T)');//nuevo
  
    $id= $this->session->userdata('profesionalid');
    $list['mensajeerror'] = "";
    $list['datosprof'] = $this->profesionales_modelo->obtieneDatosProfesional($id);
    $especialidades = $this->profesionales_modelo->obtieneEspecialidadesProfesional($id);
    
    if(isset($_POST['Registrar'])){
        foreach ($especialidades as $e) {
            $data = array(
             'vacacionesfechaini' => $this->input->post('fechaI'),
             'vacacionesfechafin' => $this->input->post('fechaF'),
             'profesionalesid' => $id, 
             'especialidadesid' => $e->especialidadesid,
             'estadosid' => 1
            );

            $transaction = "insert into vacaciones (vacacionesfechaini, vacacionesfechafin, profesionalesid, especialidadesid, estadosid) values ("."'".$data['vacacionesfechaini']."','".$data['vacacionesfechafin']."','".$data['profesionalesid']."','".$data['especialidadesid']."','".$data['estadosid'];

            $dataInserted = "vacacionesfechaini = ".$data['vacacionesfechaini'].", vacacionesfechafin = ".$data['vacacionesfechafin'].", profesionalesid = ".$data['profesionalesid'].", especialidadesid = ".$data['especialidadesid'].", estadosid = ".$data['estadosid']; 
            
            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
            );           

            $this->vacaciones_modelo->registrarVacaciones($data);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        }
        $vacaciones = $this->vacaciones_modelo->obtieneVacaciones($id);
        $fechaIni = explode("/",$this->input->post('fechaI'));
        $fechaIni = $fechaIni[0]."-".$fechaIni[1]."-".$fechaIni[2];
       
        $fechaFin = explode("/",$this->input->post('fechaF'));
        $fechaFin = $fechaFin[0]."-".$fechaFin[1]."-".$fechaFin[2];
        
        $listaDiasNombre = [];
        $listaMeses = [];
        $listaAnio = [];
        $listaDias = [];
        $fechacompara = $fechaIni;
     
        while(strtotime($fechaFin) >= strtotime($fechacompara) ){
                $diasid = date('w',strtotime($fechacompara));
                $mes = date('n', strtotime($fechacompara));
                $anio = date('Y', strtotime($fechacompara));
                $dia = date('j', strtotime($fechacompara));
                $listaDiasNombre[] = $diasid;
                $listaMeses[] = $mes - 1;
                $listaAnio[] = $anio;
                $listaDias[] = $dia;
                $fechacompara = date("d-m-Y", strtotime($fechacompara . " + 1 day"));
        }
        $tam = sizeof($listaDiasNombre);
        foreach ($vacaciones as $v) {
            for ($i=0 ; $i < $tam ; $i++ ) { 
                $ids=explode('-',$v->vacacionesids);
                for($j = 0;$j< count($ids);$j++){
                    $data = array(
                        'vacacionesid' => $ids[$j],
                        'diasid' => $listaDiasNombre[$i],
                        'mes' => $listaMeses[$i],
                        'anio' => $listaAnio[$i],
                        'dia' => $listaDias[$i] 
                    );

                    $transaction = "insert into vacacionesxdias (vacacionesid, diasid, mes, anio, dia) values ("."'".$data['vacacionesid']."','".$data['diasid']."','".$data['mes']."','".$data['anio']."','".$data['dia']; 

                    $dataInserted = "vacacionesid = ".$data['vacacionesid'].", diasid = ".$data['diasid'].", mes = ".$data['mes'].", anio = ".$data['anio'].", dia = ".$data['dia'];

                    $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
                    );

                    $this->vacaciones_modelo->registrarVacacionesxDia($data);
                    $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                }
               
            }
        }

        
    }
    $list['dias'] =  $this->agenda_modelo->diasagendaprof($id);
    $list['licencias'] = $this->licencias_modelo->diasLicencias($id,$especialidades[0]->especialidadesid);
    $list['vacaciones'] = $this->vacaciones_modelo->diasVacaciones($id,$especialidades[0]->especialidadesid);
    $this->load->view('registrarVacaciones_view',$list);

}





public function modificarVacaciones($vacacionesids){
    $id= $this->session->userdata('profesionalid');
    $list['mensajeerror'] = "";
    $list['datosprof'] = $this->profesionales_modelo->obtieneDatosProfesional($id);
    $list['fechaI'] = date('D M d Y H:i:s \G\M\TO (T)');//nuevo
    $list['fechaF'] = date('D M d Y H:i:s \G\M\TO (T)');//nuevo
    $list['ids'] = $vacacionesids;
    $ids = explode('-' , $vacacionesids);
    $especialidades = $this->profesionales_modelo->obtieneEspecialidadesProfesional($id);
    $infovacaciones = $this->vacaciones_modelo->obtieneUnaFilaVacaciones($id,$vacacionesids);

    foreach ($infovacaciones as $va ) {
        $fechainicio = explode("-",$va->fechainicio);
        $fechainicio = $fechainicio[2]."/".$fechainicio[1]."/".$fechainicio[0];

        $fechafin = explode("-",$va->fechafin);
        $fechafin = $fechafin[2]."/".$fechafin[1]."/".$fechafin[0];
        
        $list['fechainicio'] = $fechainicio;
        $list['fechafin'] = $fechafin;

    }

    if (isset($_POST['Registrar']) && $this->validarfecha($list, $id, $this->input->post('fechaI'), $this->input->post('fechaF'))) {
        
        for ($i=0;$i<count($ids);$i++) {
            $fecha =  getdate();
            $data = array(
                'vacacionesfechaini' => $this->input->post('fechaI'),
                'vacacionesfechafin' => $this->input->post('fechaF'),
                'vacacionesfechamod' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']

            );

            $transaction = "update vacaciones set vacacionesfechaini = ".$data['vacacionesfechaini'].", vacacionesfechafin = ".$data['vacacionesfechafin'].", vacacionesfechamod = ".$data['vacacionesfechamod'];

            $dataUpdated = " vacacionesid = ".$vacacionesids." , vacacionesfechaini = ".$data['vacacionesfechaini'].", vacacionesfechafin = ".$data['vacacionesfechafin'].", vacacionesfechamod = ".$data['vacacionesfechamod'];

            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
            );

            $this->vacaciones_modelo->modificarVacaciones($data,$ids[$i]);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        }
        
        $vacaciones = $this->vacaciones_modelo->obtieneUnaFilaVacaciones($id,$vacacionesids);
        $fechaIni = explode("/",$this->input->post('fechaI'));
        $fechaIni = $fechaIni[0]."-".$fechaIni[1]."-".$fechaIni[2];
       
        $fechaFin = explode("/",$this->input->post('fechaF'));
        $fechaFin = $fechaFin[0]."-".$fechaFin[1]."-".$fechaFin[2];
        
        $listaDiasNombre = [];
        $listaMeses = [];
        $listaAnio = [];
        $listaDias = [];
        $fechacompara = $fechaIni;
     
        while(strtotime($fechaFin) >= strtotime($fechacompara) ){
                $diasid = date('w',strtotime($fechacompara));
                $mes = date('n', strtotime($fechacompara));
                $anio = date('Y', strtotime($fechacompara));
                $dia = date('j', strtotime($fechacompara));
                $listaDiasNombre[] = $diasid;
                $listaMeses[] = $mes - 1;
                $listaAnio[] = $anio;
                $listaDias[] = $dia;
                $fechacompara = date("d-m-Y", strtotime($fechacompara . " + 1 day"));
        }
        $tam = sizeof($listaDiasNombre);
        foreach ($vacaciones as $l) {
            for ($j=0; $j < count($ids) ; $j++) {
                $this->vacaciones_modelo->eliminarVacacionesxDia($ids[$j]);

                for ($i=0 ; $i < $tam ; $i++ ) { 
                    $data = array(
                    'vacacionesid' => $ids[$j],
                    'diasid' => $listaDiasNombre[$i],
                    'mes' => $listaMeses[$i],
                    'anio' => $listaAnio[$i],
                    'dia' => $listaDias[$i] 
                );


                $transaction = "update vacacionesxdias set vacacionesid = ".$data['vacacionesid'].", diasid = ".$data['diasid'].", mes = ".$data['mes'].", anio = ".$data['anio'].", dia = ".$data['dia']; 

                $dataUpdated = " vacacionesxdiaid = ".$ids[$j]." , vacacionesid = ".$data['vacacionesid'].", diasid = ".$data['diasid'].", mes = ".$data['mes'].", anio = ".$data['anio'].", dia = ".$data['dia']; 

                $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
                );
                $this->vacaciones_modelo->registrarVacacionesxDia($data);
                $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                }
                
            }
        }
        redirect(base_url().'index.php/agenda/registrarVacaciones');
    }
    $list['dias'] =  $this->agenda_modelo->diasagendaprof($id);
    $this->load->view('modificarVacaciones_view',$list);   
}



public function modificarHorasLab($idAg)
{
        $id= $this->session->userdata('profesionalid');
        $idEsp = $this->agenda_modelo->Especialidades($this->session->userdata('profesionalid'));
        $list['ocultar'] ='';
        $list['MensajeTurnoMañana'] ='';
        $list['MensajeTurnoTarde'] ='';
        $list['mensajeinsert'] = '';
        $TMini = $this->input->post('inputMañanaInicio');
        $TMfin = $this->input->post('inputMañanaFin');
        $TTini = $this->input->post('inputTardeInicio');
        $TTfin = $this->input->post('inputTardeFin');

        if (isset($_POST['Registrar'])) {
                $list['ocultar']='';               
                    
                $fecha = getdate();
                if ($this->input->post('inputFranjaHoraria') == 1) {
                    $b = 0;

                    if ($TMini >= $TMfin){
                         $list['MensajeTurnoMañana'] ='La hora inicio no puede ser mayor o igual a la hora fin del turno mañana';
                         $b = 1;
                    }
                    if($b == 0){                    
                        $data = array(
                              'agendastmini' =>  $TMini,
                              'agendastmfin' =>  $TMfin,
                              'agendasttini' =>  NULL,
                              'agendasttfin' =>  NULL,
                              'agendasfechamod' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year']
                        );

                        $transaction = "update agendas set agendastmini = ".$data['agendastmini'].", agendastmfin = ".$data['agendastmfin'].", agendasttini = ".$data['agendasttini'].", agendasttfin = ".$data['agendasttfin'].", agendasfechamod = ".$data['agendasfechamod']; 

                        $dataUpdated = " agendasid = ".$idAg." , agendastmini = ".$data['agendastmini'].", agendastmfin = ".$data['agendastmfin'].", agendasttini = ".$data['agendasttini'].", agendasttfin = ".$data['agendasttfin'].", agendasfechamod = ".$data['agendasfechamod']; 

                        $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
                        );

                        $this->agenda_modelo->updateHorasLab($data,$idAg);
                        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                        $data['message'] = 'Data Inserted Successfully';
                        redirect(base_url().'index.php/agenda/DiasyHorasLab');
                    }
                }
                if ($this->input->post('inputFranjaHoraria') == 2 ) {
                    $b = 0;

                    if ($TTini >= $TTfin){
                        $list['MensajeTurnoTarde'] ='La hora inicio no puede ser mayor o igual a la hora fin del turno tarde';
                        $b = 1;
                    }
                    if($b == 0){ 
                        $data = array(
                              'agendastmini' =>  NULL,
                              'agendastmfin' =>  NULL,
                              'agendasttini' =>  $TTini,
                              'agendasttfin' =>  $TTfin,
                              'agendasfechamod' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year']
                                );
                        $this->agenda_modelo->updateHorasLab($data,$idAg);
                        $data['message'] = 'Data Inserted Successfully';
                        redirect(base_url().'index.php/agenda/DiasyHorasLab');
                    }
                }
                if ($this->input->post('inputFranjaHoraria') == 0){
                    $b = 0;

                    if ($TMini >= $TMfin){
                         $list['MensajeTurnoMañana'] ='La hora inicio no puede ser mayor o igual a la hora fin del turno mañana';
                         $b = 1;
                    }
                    if ($TTini >= $TTfin){
                        $list['MensajeTurnoTarde'] ='La hora inicio no puede ser mayor o igual a la hora fin del turno tarde';
                        $b = 1;
                    } 
                    if($b == 0){
                        $data = array(
                                  'agendastmini' =>  $TMini,
                                  'agendastmfin' =>  $TMfin,
                                  'agendasttini' =>  $TTini,
                                  'agendasttfin' =>  $TTfin,
                                  'agendasfechamod' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year']
                        );
                        
                        $transaction = "update agendas set agendastmini = ".$data['agendastmini'].", agendastmfin = ".$data['agendastmfin'].", agendasttini = ".$data['agendasttini'].", agendasttfin = ".$data['agendasttfin'].", agendasfechamod = ".$data['agendasfechamod']; 

                        $dataUpdated = " agendasid = ".$idAg." , agendastmini = ".$data['agendastmini'].", agendastmfin = ".$data['agendastmfin'].", agendasttini = ".$data['agendasttini'].", agendasttfin = ".$data['agendasttfin'].", agendasfechamod = ".$data['agendasfechamod']; 

                        $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
                        );

                        $this->agenda_modelo->updateHorasLab($data,$idAg);
                        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                        $data['message'] = 'Data Inserted Successfully';
                        redirect(base_url().'index.php/agenda/DiasyHorasLab');          
                    }   
                }
        } 
        
        
        
        $list['id'] = $idAg;
        $this->load->view('modificarHorasLaborales_view',$list);
}
                
   function buscarAgenda(){
            $id = $this->session->userdata('profesionalid');
            $idEsp = $this->input->post('inputEspecialidades'); 
            $list['agenda'] = $this->agenda_modelo->get_datatables2($id,$idEsp);
            $list['datosprof'] = $this->profesionales_modelo->obtieneDatosProfesional($id);
            $list['especialidades'] = $this->agenda_modelo->Especialidades($id);
            $list['dias'] =  $this->agenda_modelo->dias();
            $this->load->view('registrarDiasHorasLaborales_view',$list);
            
    }
     
     public function cancelarDiaLab($idAg)
     {
        $fecha =  getdate();
        $data = array(
            'estadosid'=>2,
            'agendasfechabaja'=> $fecha['mday']."/".$fecha['mon']."/".$fecha['year']
        );

        $transaction = 'update agendas set estadosid = 2 , agendasfechabaja ='.
        $fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

        $dataUpdated = "agendasid = ".$idAg.', estadosid =2, '.' agendasfechabaja ='.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

        $data_auditoria = array(
            'nombreusuario' => $this->session->userdata('usuario'),
            'transaccion' => $transaction,
            'datosmodificados' => $dataUpdated
        );

        $this->agenda_modelo->cancelaDia($idAg,$data);
        $this->auditoria_modelo->registrarAuditoria($data_auditoria);
        redirect(base_url().'index.php/agenda/DiasyHorasLab');
     }



    
public function CerrarSesion(){
    $this->session->sess_destroy();
    redirect(base_url().'index.php/login');
} 

 public function agendalisto($turnos)
{
        $ids=split('-',$turnos);
        for($i=0;$i<count($ids);$i++){
            $fecha =  getdate();
             $data = array(
                'estadosturnosid' => 8,
                'turnosbaja' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']

            );

            $transaction = "update turnos set estadosturnosid = ".$data['estadosturnosid']." , turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $dataUpdated = "turnosid = ".$ids[$i].", estadosturnosid = ".$data['estadosturnosid'].", turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $data_auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transaction,
                'datosmodificados' => $dataUpdated
            );


            $this->agenda_modelo->agendalisto($data,$ids[$i]);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            unset($data);
        }
        redirect(base_url().'index.php/agenda/Turnos');
}

function ajax_list_turnos(){
           
            $fech = date('d/m/Y');
            $table = 'mostrarturnosxprofesional('.$this->session->userdata('profesionalid').",'".$fech."')";
            $list = $this->agenda_modelo->get_datatables_turnos($table);
            $data = array();
            foreach ($list as $turnos){

                $row = array();
                $row[] = $turnos->profesional;
                $row[] = $turnos->especialidad;
                $row[] = $turnos->fecha;
                $row[] = $turnos->horainicio;
                $row[] = $turnos->horafin;
                $row[] = $turnos->tipoturno;
                $row[] = $turnos->pacientes;
                $row[] = $turnos->estadoturno;
                if ($turnos->estadoturnoid == 3){
                    $row[] = ' <button disabled type="button" class="btn btn-primary" style="background:#48a2b7; border-color:#327282;" onclick="location.href='."'".base_url()."index.php/agenda/pacienteAtendiendo/".$turnos->turnosid."'".'"><i class="fas fa-bell fa "></i></button>  <button disabled type="button" class="btn btn-primary" style="background:#5ff523; border-color:#466d36;" onclick="location.href='."'".base_url()."index.php/agenda/pacienteAtendido/".$turnos->turnosid."'".'"><i class="fas fa-bell fa "></i></button> <button  type="button" data-toggle="tooltip" title="Cancelación Confirmada" class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/agenda/agendalisto/".$turnos->turnosid."'".'"><i class="fas fa-check "></i></button>';
                }else{
                    if ($turnos->estadoturnoid == 8){
                        $row[] = '<button  disabled type="button" class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/agenda/agendalisto/".$turnos->turnosid."'".'"><i class="fas fa-check "></i></button>';
                    } else{
                     $row[] = '<button type="button" data-toggle="tooltip" title="Atendiendo Paciente" class="btn btn-primary" style="background:#48a2b7; border-color:#327282;" onclick="location.href='."'".base_url()."index.php/agenda/pacienteAtendiendo/".$turnos->turnosid."'".'"><i class="fas fa-bell fa "></i></button>  <button type="button" data-toggle="tooltip" title="Paciente Atendido" class="btn btn-primary" style="background:#5ff523; border-color:#466d36;" onclick="location.href='."'".base_url()."index.php/agenda/pacienteAtendido/".$turnos->turnosid."'".'"><i class="fas fa-bell fa "></i></button>';
                    }
                }
                $data[] = $row;
            }

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->agenda_modelo->count_all_turnos($table),
                            "recordsFiltered" => $this->agenda_modelo->count_filtered_turnos($table),
                            "data" => $data,
                    );

            echo json_encode($output);
        }

    public function Turnos()
    {
        $this->load->view('agendaTurnos_view');
    }    

    public function pacienteAtendiendo($turnos)
    {
        $ids=split('-',$turnos);
        for($i=0;$i<count($ids);$i++){
            $fecha =  getdate();
             $data = array(
                'estadosturnosid' => 5,
                'turnosbaja' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']

            );

            $transaction = "update turnos set estadosturnosid = ".$data['estadosturnosid']." , turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $dataUpdated = "turnosid = ".$ids[$i].", estadosturnosid = ".$data['estadosturnosid'].", turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $data_auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transaction,
                'datosmodificados' => $dataUpdated
            );


            $this->agenda_modelo->pacienteAtendiendo($data,$ids[$i]);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            unset($data);
        }
        redirect(base_url().'index.php/agenda/Turnos');
    }   
    


    public function pacienteAtendido($turnos)
    {
        $ids=split('-',$turnos);
        for($i=0;$i<count($ids);$i++){
            $fecha =  getdate();
             $data = array(
                'estadosturnosid' => 6,
                'turnosbaja' =>$fecha['mday']."/".$fecha['mon']."/".$fecha['year']

            );

            $transaction = "update turnos set estadosturnosid = ".$data['estadosturnosid']." , turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $dataUpdated = "turnosid = ".$ids[$i].", estadosturnosid = ".$data['estadosturnosid'].", turnosbaja = ".$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

            $data_auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transaction,
                'datosmodificados' => $dataUpdated
            );

            $this->agenda_modelo->pacienteAtendido($data,$ids[$i]);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            unset($data);
        }
        redirect(base_url().'index.php/agenda/Turnos');
    }   


    function Licencias(){
        $id= $this->session->userdata('profesionalid');
        $list = $this->licencias_modelo->get_datatables($id);
        $data = array();
        foreach ($list as $licencias){
    
            $row = array();
            $row[] = $licencias->licenciasids;
            $row[] = $licencias->fechainicio;
            $row[] = $licencias->fechafin;
            $row[] = $licencias->especialidades;
            $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Días de Licencia" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/agenda/modificarLicencias/".$licencias->licenciasids."'".'" ><i class="fas fa-pencil-alt"></i></button>';
            $data[] = $row;
    
        }        
        $output = array(
                        "draw" => 0,
                        "recordsTotal" => $this->licencias_modelo->count_all('licencias'),
                        "recordsFiltered" => $this->licencias_modelo->count_filtered($id),
                        "data" => $data,
                );
    
        echo json_encode($output);
    }

    function Vacaciones(){
        $id= $this->session->userdata('profesionalid');
        $list = $this->vacaciones_modelo->get_datatables($id);
        $data = array();
        foreach ($list as $vacaciones){
    
            $row = array();
            $row[] = $vacaciones->vacacionesids;
            $row[] = $vacaciones->fechainicio;
            $row[] = $vacaciones->fechafin;
            $row[] = $vacaciones->especialidades;
            $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Días de Vacaciones" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/agenda/modificarVacaciones/".$vacaciones->vacacionesids."'".'" ><i class="fas fa-pencil-alt"></i></button>';
            $data[] = $row;
    
        }        
        $output = array(
                        "draw" => 0,
                        "recordsTotal" => $this->vacaciones_modelo->count_all('vacaciones'),
                        "recordsFiltered" => $this->vacaciones_modelo->count_filtered($id),
                        "data" => $data,
                );
    
        echo json_encode($output);
    }
}




?>