<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Secretario extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('pacientes_modelo');
        $this->load->model('auditoria_modelo');
        $this->load->model('secretario_modelo');
        $this->load->library('session'); 
    }
    
    public function index(){
        $this->load->view('head_view');            
        if($this->session->userdata('logueo') == TRUE || $this->session->userdata('usuario') != '') {
            $list['sexo'] = $this->pacientes_modelo->sexo();
            $list['tipotelefonos'] = $this->pacientes_modelo->TipoTelefono();
            $list['apellido'] =  '';
            $list['nombre'] = '';
            $list['dni'] = '';
            $list['domicilio'] = '';
            $list['telefono'] = '';
            $list['tipotelefonoid'] = '';
            $list['email'] = '';
            $list['sexoid'] = '';
            $this->load->view('secretario_view',$list);            
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }

    public function secretarioList()
    {
        $this->load->view('head_view'); 
        $this->load->view('secretarioList_view');
    }

    public function registrarSecretario(){

            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio');
            $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
            $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
            $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
            $this->form_validation->set_rules('pacienteApellido', 'Apellido', 'required|min_length[2]|max_length[15]'); 
            $this->form_validation->set_rules('pacienteNombre', 'Nombre', 'required|min_length[2]|max_length[30]');  
            $this->form_validation->set_rules('pacienteDNI', 'DNI', 'required|min_length[8]|max_length[10]|callback_existente|callback_esnumerico');
            $this->form_validation->set_rules('pacienteDomicilio', 'Domicilio', 'required|min_length[5]|max_length[30]'); 
            $this->form_validation->set_rules('pacienteEmail', 'Email', 'valid_email');

            $list['sexo'] = $this->pacientes_modelo->sexo();
            if($this->form_validation->run() == FALSE){
                $list['apellido'] =  $this->input->post('pacienteApellido');
                $list['nombre'] = $this->input->post('pacienteNombre');
                $list['dni'] = $this->input->post('pacienteDNI');
                $list['domicilio'] = $this->input->post('pacienteDomicilio');
                $list['email'] = $this->input->post('pacienteEmail');
                $list['sexoid'] = $this->input->post('pacienteSexo');
                $this->load->view('secretario_view',$list);
            }else{
                $data = array(
                    'personasapellido' => $this->input->post('pacienteApellido'),
                    'personasnombre' => $this->input->post('pacienteNombre'),
                    'personasnrodocumento' => $this->input->post('pacienteDNI'),
                    'sexoid' => $this->input->post('pacienteSexo'),
                    'personasdomicilio' => $this->input->post('pacienteDomicilio'),                    
                    'personasemail' => $this->input->post('pacienteEmail'),
                    );
                
                $transactionPerson = 'insert into personas (personasapellido, personasnombre, personasnrodocumento, sexoid, personasdomicilio, personasemail) values('.
                "'".$data['personasapellido']."',".
                "'".$data['personasnombre']."',".
                "'".$data['personasnrodocumento']."',".
                $data['sexoid'].",".
                "'".$data['personasdomicilio']."',".
                "'".$data['personasemail']."')";

                $dataPersonInserted = "personasapellido = ".$data['personasapellido'].", ".
                "personasnombre = ".$data['personasnombre'].", ".
                "personasnrodocumento = ".$data['personasnrodocumento'].", ".
                "sexoid = ".$data['sexoid'].", ".
                "personasdomicilio = ".$data['personasdomicilio'].", ".
                "personasemail = ".$data['personasemail'];

                
                $this->pacientes_modelo->registrarPersona($data);

                $dataPerson_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionPerson,
                    'datosingresados' => $dataPersonInserted
                );

                $paciente = $this->pacientes_modelo->buscarPersona($data['personasnrodocumento']);
                $this->auditoria_modelo->registrarAuditoria($dataPerson_Auditoria);

                $data2 = array(
                    'personasid' => $paciente['personasid'],
                    'estadosid' => 1
                );

                $transactionSecretary = 'insert into secretario (personasid) values ('.
                $data2['personasid'].')';

                $dataSecretaryInserted = 'personasid = '.$data2['personasid'];


                $this->secretario_modelo->registrarSecretario($data2);

                $dataSecretary_Auditoria = array(
                    'nombreusuario' => $this->session->userdata('usuario'),
                    'transaccion' => $transactionSecretary,
                    'datosingresados' => $dataSecretaryInserted
                );
                $this->auditoria_modelo->registrarAuditoria($dataSecretary_Auditoria);
                $data['message'] = 'Data Inserted Successfully';
                redirect(base_url().'index.php/secretario/RegistrarTelefono/'.$paciente['personasid']);
            }

        
          
    }

    public function modificarSecretario($id)
    {
        $list['id'] = $id;
        $list['datos'] = $this->secretario_modelo->buscarSecretario($id);
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
        $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('personasApellido', 'Apellido', 'required|min_length[2]|max_length[15]'); 
        $this->form_validation->set_rules('personasNombre', 'Nombre', 'required|min_length[2]|max_length[30]');  
        $this->form_validation->set_rules('personasDNI', 'DNI', 'required|min_length[8]|max_length[10]|callback_esnumerico');
        $this->form_validation->set_rules('personasDomicilio', 'Domicilio', 'required|min_length[5]|max_length[30]'); 
        $this->form_validation->set_rules('personasEmail', 'Email', 'valid_email');

        $list['sexo'] = $this->pacientes_modelo->sexo();
        if($this->form_validation->run() == FALSE){
            
            $list['apellido'] =  $this->input->post('personasApellido');
            $list['nombre'] = $this->input->post('personasNombre');
            $list['dni'] = $this->input->post('personasDNI');
            $list['domicilio'] = $this->input->post('personasDomicilio');
            $list['email'] = $this->input->post('personasEmail');
            $list['sexoid'] = $this->input->post('personasSexo');
            $this->load->view('modificarSecretario_view',$list);
        }else{
            $data = array(
                'personasapellido' => $this->input->post('personasApellido'),
                'personasnombre' => $this->input->post('personasNombre'),
                'personasnrodocumento' => $this->input->post('personasDNI'),
                'sexoid' => $this->input->post('personasSexo'),
                'personasdomicilio' => $this->input->post('personasDomicilio'),                    
                'personasemail' => $this->input->post('personasEmail'),
                );
            
            $transactionPerson = 'update personas set personas '.
            "personasapellido = ".$data['personasapellido'].",".
            "personasnombre = ".$data['personasnombre'].",".
            "personasnrodocumento = ".$data['personasnrodocumento'].",".
            "sexoid = ".$data['sexoid'].",".
            "personasdomicilio = ".$data['personasdomicilio'].",".
            "personasemail = ".$data['personasemail'];

            $dataPersonUpdated = "personasapellido = ".$data['personasapellido'].", ".
            "personasnombre = ".$data['personasnombre'].", ".
            "personasnrodocumento = ".$data['personasnrodocumento'].", ".
            "sexoid = ".$data['sexoid'].", ".
            "personasdomicilio = ".$data['personasdomicilio'].", ".
            "personasemail = ".$data['personasemail'];

            
            $this->secretario_modelo->modificarPersona($list['datos']['id'],$data);

            $dataPerson_Auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transactionPerson,
                'datosmodificados' => $dataPersonInserted
            );

            $paciente = $this->pacientes_modelo->buscarPersona($data['personasnrodocumento']);
            $this->auditoria_modelo->registrarAuditoria($dataPerson_Auditoria);

            // $data2 = array(
            //     'personasid' => $paciente['personasid'],
            // );

            // $transactionSecretary = 'insert into secretario (personasid) values ('.
            // $data2['personasid'].')';

            // $dataSecretaryInserted = 'personasid = '.$data2['personasid'];


            //$this->secretario_modelo->registrarSecretario($data2);

            // $dataSecretary_Auditoria = array(
            //     'nombreusuario' => $this->session->userdata('usuario'),
            //     'transaccion' => $transactionSecretary,
            //     'datosingresados' => $dataSecretaryInserted
            // );
            //$this->auditoria_modelo->registrarAuditoria($dataSecretary_Auditoria);
            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/secretario/RegistrarTelefono/'.$paciente['personasid']);
        }
    }


    public function RegistrarTelefono($id)
    {
        $list['id'] = $id;
        $list['tipotelefonos'] = $this->pacientes_modelo->TipoTelefono();
        $list['tipotelefonoid'] = $this->input->post('secretarioTipoTelefono');
        $list['telefono'] = $this->input->post('secretarioTelefono');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('secretarioTelefono', 'Telefono', 'required|min_length[5]|max_length[15]'); 
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
        $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060' name='error'>", "</div>");
        if($this->form_validation->run() == FALSE){
            $list['tipotelefonoid'] = $this->input->post('secretarioTipoTelefono');
            $list['telefono'] = $this->input->post('secretarioTelefono');
            $this->load->view('secretarioRegistrarTelefono_view',$list);
        }else{
            $data = array(
                    'personasid' => $id,
                    'tipotelefonosid' => $this->input->post('secretarioTipoTelefono'),
                    'telefononumero' => $this->input->post('secretarioTelefono')
                    );

            $transactionPhone = 'insert into telefonos (personasid, tipotelefonosid, telefononumeto) values('.
            $data['personasid'].','.
            $data['tipotelefonosid'].",'".
            $data['telefononumero']."')";
            
            $dataPhoneInserted = 'personasid = '.$data['personasid'].', '.
            'tipotelefonosid = '.$data['tipotelefonosid'].', '.
            'telefononumero = '.$data['telefononumero'];

            $dataPhone_Auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transactionPhone,
                'datosingresados' => $dataPhoneInserted
            );

            $this->pacientes_modelo->registrarTelefonoPaciente($data);
            $this->auditoria_modelo->registrarAuditoria($dataPhone_Auditoria);
            
            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/secretario/RegistrarTelefono/'.$id);
        }
    }

    public function modificarSecretario($id)
    {
        $list['id'] = $id;
        $list['datos'] = $this->secretario_modelo->buscarSecretario($id);
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s caracteres de longitud');
        $this->form_validation->set_message('max_length', 'El campo %s debe tener a lo sumo %s caracteres de longitud'); 
        $this->form_validation->set_error_delimiters("<div class='error' style='color:#f76060'>", "</div>");
        $this->form_validation->set_rules('personasApellido', 'Apellido', 'required|min_length[2]|max_length[15]'); 
        $this->form_validation->set_rules('personasNombre', 'Nombre', 'required|min_length[2]|max_length[30]');  
        $this->form_validation->set_rules('personasDNI', 'DNI', 'required|min_length[8]|max_length[10]|callback_esnumerico');
        $this->form_validation->set_rules('personasDomicilio', 'Domicilio', 'required|min_length[5]|max_length[30]'); 
        $this->form_validation->set_rules('personasEmail', 'Email', 'valid_email');

        $list['sexo'] = $this->pacientes_modelo->sexo();
        if($this->form_validation->run() == FALSE){
            
            $list['apellido'] =  $this->input->post('personasApellido');
            $list['nombre'] = $this->input->post('personasNombre');
            $list['dni'] = $this->input->post('personasDNI');
            $list['domicilio'] = $this->input->post('personasDomicilio');
            $list['email'] = $this->input->post('personasEmail');
            $list['sexoid'] = $this->input->post('personasSexo');
            $this->load->view('modificarSecretario_view',$list);
        }else{
            $data = array(
                'personasapellido' => $this->input->post('personasApellido'),
                'personasnombre' => $this->input->post('personasNombre'),
                'personasnrodocumento' => $this->input->post('personasDNI'),
                'sexoid' => $this->input->post('personasSexo'),
                'personasdomicilio' => $this->input->post('personasDomicilio'),                    
                'personasemail' => $this->input->post('personasEmail'),
                );
            
            $transactionPerson = 'update personas set personas '.
            "personasapellido = ".$data['personasapellido'].",".
            "personasnombre = ".$data['personasnombre'].",".
            "personasnrodocumento = ".$data['personasnrodocumento'].",".
            "sexoid = ".$data['sexoid'].",".
            "personasdomicilio = ".$data['personasdomicilio'].",".
            "personasemail = ".$data['personasemail'];

            $dataPersonUpdated = "personasapellido = ".$data['personasapellido'].", ".
            "personasnombre = ".$data['personasnombre'].", ".
            "personasnrodocumento = ".$data['personasnrodocumento'].", ".
            "sexoid = ".$data['sexoid'].", ".
            "personasdomicilio = ".$data['personasdomicilio'].", ".
            "personasemail = ".$data['personasemail'];

            
            $this->secretario_modelo->modificarPersona($list['datos']['id'],$data);

            $dataPerson_Auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transactionPerson,
                'datosmodificados' => $dataPersonInserted
            );

            $paciente = $this->pacientes_modelo->buscarPersona($data['personasnrodocumento']);
            $this->auditoria_modelo->registrarAuditoria($dataPerson_Auditoria);

            // $data2 = array(
            //     'personasid' => $paciente['personasid'],
            // );

            // $transactionSecretary = 'insert into secretario (personasid) values ('.
            // $data2['personasid'].')';

            // $dataSecretaryInserted = 'personasid = '.$data2['personasid'];


            //$this->secretario_modelo->registrarSecretario($data2);

            // $dataSecretary_Auditoria = array(
            //     'nombreusuario' => $this->session->userdata('usuario'),
            //     'transaccion' => $transactionSecretary,
            //     'datosingresados' => $dataSecretaryInserted
            // );
            //$this->auditoria_modelo->registrarAuditoria($dataSecretary_Auditoria);
            $data['message'] = 'Data Inserted Successfully';
            redirect(base_url().'index.php/secretario/RegistrarTelefono/'.$paciente['personasid']);
        }
    }

    public function bajaSecretario($id)
    {
        $data = array(
            'estadosid' => 2,
            'secretariosfechabaja' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year']
        );

        $transactionSecretary = 'update secretario set estadosid = 2, secretariosfechabaja ='.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];
        $dataSecretaryUpdated = 'estadosid=2, secretariosfechabaja ='.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

        $this->secretario_modelo->bajaSecretario($id,$data);
        $dataSecretary_Auditoria = array(
                'nombreusuario' => $this->session->userdata('usuario'),
                'transaccion' => $transactionSecretary,
                'datosmodificados' => $dataSecretaryUpdated
        );
        $this->auditoria_modelo->registrarAuditoria($dataSecretary_Auditoria);

    }

    public function ajax_list()
    {
        $secretarios = $this->secretario_modelo->get_datatables();

        $data = array();
        $i = 0; 
        foreach ($secretarios as $sec){
            $row = array();
            $row[] = $sec->id;
            $row[] = $sec->nombrecompleto;
            $row[] = $sec->dni;
            $row[] = $sec->domicilio;
            $row[] = $sec->email;
            $row[] = $sec->telefono;
                
            if($sec->estadosid == 1){
                $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Secretario" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/secretario/modificarSecretario/".$sec->id."'".'"><i class="fas fa-pencil-alt"></i></button>'.
                    '    <button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" ><i data-toggle="tooltip" title="Dar de Baja Secretario" class="fas fa-times-circle" ></i></button>'.
                    '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																				<div class="modal-dialog modal-lg" role="document">
																					<div class="modal-content">
																						<div class="modal-header" >
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">&times;</span>
																							</button>
																							
																						</div>
																						<div class="modal-body">
																							<label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
																						</div>
																						<div class="row">		
																							<div class="col-sm-3">
																								
																							</div>
																							<div class="col-sm-3">
																								<button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/secretario/bajaSecretario/".$sec->id."'".'">Si</button>
																							</div>
																							<div class="col-sm-3">
																							<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
																								
																							</div>
																							
																							<div class="col-sm-3">
																								
																							</div>
																						</div>
																						<br><br>
																										
																					</div>
																				</div>
																			</div>';
                }else{
                    $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Secretario" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/secretario/modificarSecretario/".$sec->id."'".'" disabled ><i class="fas fa-pencil-alt"></i></button>'.
                    '    <button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" disabled><i data-toggle="tooltip" title="Dar de baja Secretario" class="fas fa-times-circle" ></i></button>';    
                }
                //$row[] = ''; 
                $data[] = $row;
                $i = $i +1;
                

            }        

            $output = array(
                            "draw" => 0,
                            "recordsTotal" => $this->secretario_modelo->count_all(),
                            "recordsFiltered" => $this->secretario_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
    }

    public function existente($dni){
            $list=$this->pacientes_modelo->buscarPersona($dni);
            if ($list!=null){
                $this->form_validation->set_message('existente','El DNI ya existe');
                return False;
            } 
            else{
                return True;
            }
    }

    public function ajax_list()
    {
        $secretarios = $this->secretario_modelo->get_datatables();

        $data = array();
        $i = 0; 
        foreach ($secretarios as $sec){
            $row = array();
            $row[] = $sec->id;
            $row[] = $sec->nombrecompleto;
            $row[] = $sec->dni;
            $row[] = $sec->domicilio;
            $row[] = $sec->email;
            $row[] = $sec->telefono;
                
            if($sec->estadosid == 1){
                $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Secretario" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/secretario/modificarSecretario/".$sec->id."'".'"><i class="fas fa-pencil-alt"></i></button>'.
                    '    <button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" ><i data-toggle="tooltip" title="Dar de Baja Secretario" class="fas fa-times-circle" ></i></button>'.
                    '<div class="modal fade" id="miModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																				<div class="modal-dialog modal-lg" role="document">
																					<div class="modal-content">
																						<div class="modal-header" >
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">&times;</span>
																							</button>
																							
																						</div>
																						<div class="modal-body">
																							<label style="color:black;">¿Esta seguro de que desea cancelar la operación?</label>
																						</div>
																						<div class="row">		
																							<div class="col-sm-3">
																								
																							</div>
																							<div class="col-sm-3">
																								<button type="button" class="btn btn-primary submitBtn"  style="background-color:#337ab7;border-color:#2e6da4;padding:15px 50px" value="Si" onclick="location.href='."'".base_url()."index.php/secretario/bajaSecretario/".$sec->id."'".'">Si</button>
																							</div>
																							<div class="col-sm-3">
																							<button type="button" class="btn btn-primary submitBtn" style="background-color:#f76060;border-color:#f76060;padding:15px 50px" data-dismiss="modal" aria-label="Close">No</button>
																								
																							</div>
																							
																							<div class="col-sm-3">
																								
																							</div>
																						</div>
																						<br><br>
																										
																					</div>
																				</div>
																			</div>';
                }else{
                    $row[] = '<button type="button" data-toggle="tooltip" title="Modificar Secretario" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/secretario/modificarSecretario/".$sec->id."'".'" disabled ><i class="fas fa-pencil-alt"></i></button>'.
                    '    <button id="mostrar'.$i.'" type="button"  class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" data-toggle="modal"   data-target="#miModal'.$i.'" disabled><i data-toggle="tooltip" title="Dar de baja Secretario" class="fas fa-times-circle" ></i></button>';    
                }
                //$row[] = '';
                $row[] = $sec->estadosid; 
                $data[] = $row;
                $i = $i +1;
                

            }        

            $output = array(
                            "draw" => 0,
                            "recordsTotal" => $this->secretario_modelo->count_all(),
                            "recordsFiltered" => $this->secretario_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
    }

    public function eliminarTelefono($idP,$idTel)
    {   
        $telefonoData = $this->pacientes_modelo->getTelefono($idP, $idTel);
        $this->pacientes_modelo->eliminarTelefonoPaciente($idP,$idTel);
        $dataDeleted = 'tipotelefonosid='.$telefonoData['tipotelefonosid'].';telefononumero:'.$telefonoData['telefononumero'];
        $data = array(
            'nombreusuario' => $this->session->userdata('usuario'),
            'transaccion' => 'delete from telefonos where telefonosid='.$idTel.' and personasid='.$idPac.' ;',
            'datosborrados' => $dataDeleted
        );
        $this->auditoria_modelo->registrarAuditoria($data);
        redirect(base_url().'index.php/secretario/RegistrarTelefono/'.$idP);

    }

    function ajax_telefono(){
        $id = $this->input->post('personasid');
        $list = $this->secretario_modelo->get_datatables_telefono($id);
        $data = array();
        foreach ($list as $telefono){

            $row = array();
            $row[] = $telefono->tipotelefonosnombre;
            $row[] = $telefono->telefononumero;
            $row[] = '<button type="button" class="btn btn-primary" style="background:#f76060; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/secretario/eliminarTelefono/".$id."/".$telefono->telefonosid."'".'"><i class="fas fa-times-circle"></i></button>';
            $data[] = $row;

        }        

        $output = array(
                        "draw" => intval($_POST['draw']),
                        "recordsTotal" => $this->pacientes_modelo->count_all_telefono($id),
                        "recordsFiltered" => $this->pacientes_modelo->count_filtered_telefono($id),
                        "data" => $data,
                );

        echo json_encode($output);
    }

    public function bajaSecretario($id)
    {
        $fecha =  getdate();
        $data = array(
            'estadosid' => 2,
            'secretariosfechabaja' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year']
        );
        $transactionSecretary = 'update secretarios set estadosid = 2, secretariosfechabaja='.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];
        $dataSecretaryUpdated = 'estadosid = 2, secretariosfechabaja='.$fecha['mday']."/".$fecha['mon']."/".$fecha['year'];

        $this->secretario_modelo->bajaSecretario($id,$data);

        $dataSecretary_Auditoria = array(
            'nombreusuario' => $this->session->userdata('usuario'),
            'transaccion' => $transactionSecretary,
            'datosmodificados' => $dataSecretaryUpdated
        );
        $this->auditoria_modelo->registrarAuditoria($dataSecretary_Auditoria);
        redirect(base_url().'index.php/secretario/secretarioList');
    }

    public function esnumerico($dni){
        if (is_numeric($dni)){
            return True;
        }
        else{
            $this->form_validation->set_message('esnumerico','DNI no valido');
            return False;
        }
    }


    public function CerrarSesion(){
        $this->session->sess_destroy();
        redirect(base_url().'index.php/login');
    }    
    
}

?>