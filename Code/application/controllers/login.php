<?php
	class Login extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			 
			 $this->load->library('form_validation');
			 $this->load->model('login_model');
			 $this->load->model('usuarios_modelo');
			 $this->load->library('session'); 
			 $this->load->helper('url');
			 
			}

		public function index()
		{
			$this->load->view('head_view');            
			$this->load->view('login_view');
		}

		public function very_sesion()
		{
			$this->load->view('head_view');
			if ($this-> input->post('submit'))
			{
				$variable = $this->login_model->very_sesion();
				if($variable == true)
				{
					$variable = array('usuario'   =>   $this->input->post('user'),
									  'logueo'    =>   TRUE,
									  'perfil'	  => $this->login_model->Perfiles($this->input->post('user')),
									  'profesionalid' => $this->login_model->Profesional($this->input->post('user')),
									  'secretarioid' =>$this->login_model->Secretario($this->input->post('user')));
					$this->session->set_userdata($variable);
					$listPer = $this->session->userdata('perfil');
					if (count($listPer)>1) {
						$perfil = 'ADMINISTRADOR';
					}else{
						$perfil = strtoupper($listPer[0]);
					}
					if ($perfil == 'ADMINISTRADOR' || $perfil == 'SECRETARIO') {
					 	redirect(base_url().'index.php/pacientes');
					 } else {
					 	redirect(base_url().'index.php/agenda');
					 }
					  
					
					
					
					
				}
				else
				{
					$data = array('mensaje' => 'El usuario/contraseña son incorrectos');
					
					$this->load->view('login_view',$data);
				}
			}
			else
			{

				redirect(base_url().'index.php/login');
			}
		}

		public function recuperarPass()
		{
			$list['mensajerecu']= '';
			if (isset($_POST['Buscar'])){
				$list['correo'] = $this->input->post('pass');
				$correo = $this->login_model->buscaremail($list['correo']);
				if (empty($correo[0]->correo)){
					$list['mensajerecu']= 'Correo Incorrecto o no encontrado. Comuniquese con el ADMINISTRADOR';
				}else{
					$pass = substr( md5(microtime()), 1, 8);
					$data = array(
							'usuarioscontrasenia' => $pass );
					$this->usuarios_modelo->cambiarpass($correo[0]->id,$data);
					exec('python "C:/Program Files/Python36/Recuperacion.py" '.$correo[0]->usuario." ".$pass." ".$correo[0]->correo);
					redirect(base_url().'index.php/login/recuperarPassMen');
				}
			}
			
			
			$this->load->view('recuperacion_view',$list);
		}

		public function recuperarPassMen()
		{
			if (isset($_POST['Continuar'])) {
				redirect(base_url().'index.php/login');
			} else {
				$this->load->view('envioClave_view');
			}
			
			
		}
	}

?>