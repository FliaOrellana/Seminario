<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Historiaclinica extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('historiaclinica_modelo');
        $this->load->model('auditoria_modelo');
        $this->load->library('session');
        $this->load->helper('form'); 
    }
    
    public function index(){
        $this->load->view('head_view');            
        if($this->session->userdata('logueo') == TRUE || $this->session->userdata('usuario') != '') {
            $this->load->view('historiaclinica_view');            
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }
      
    
    function ajax_list(){

            $list = $this->historiaclinica_modelo->get_datatables();
            $data = array();
            foreach ($list as $historiasclinicas){

                $row = array();
                $row[] = $historiasclinicas->historiasclinicasid;
                $row[] = $historiasclinicas->personasnombrecompuesto;
                $row[] = $historiasclinicas->personasnrodocumento;
                $row[] = $historiasclinicas->historiasclinicasfechaalta;
                $row[] = $historiasclinicas->personasfechanacimiento;
                $listPer = $this->session->userdata('perfil');
                if (count($listPer)>1) {
                    $perfil = 'ADMINISTRADOR';
                }else{
                    $perfil = strtoupper($listPer[0]);
                }
                if ($perfil == 'ADMINISTRADOR' || $perfil == 'SECRETARIO') {
                    $row[] = '<button type="button" data-toggle="tooltip" title="Registrar Nuevo Diagnóstico" class="btn btn-primary" onclick="location.href='."'".base_url()."index.php/historiaclinica/detalleHC/".$historiasclinicas->pacientesid."'".'"><i class="fa fa-plus"></i></button> <button type="button" data-toggle="tooltip" title="Modificar Datos de Historia Clínica" class="btn btn-primary" style="background:#e07e2a; border-color:#e07e2a;" onclick="location.href='."'".base_url()."index.php/historiaclinica/modificarHC/".$historiasclinicas->historiasclinicasid."'".'"><i class="fas fa-pencil-alt"></i></button>';
                }else{
                    $row[] = '<button type="button" data-toggle="tooltip" title="Registrar Nuevo Diagnóstico" class="btn btn-primary" onclick="location.href='."'".base_url()."index.php/historiaclinica/detalleHC/".$historiasclinicas->pacientesid."'".'"><i class="fa fa-plus"></i></button> ';
                }

                

                $data[] = $row;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->historiaclinica_modelo->count_all(),
                            "recordsFiltered" => $this->historiaclinica_modelo->count_filtered(),
                            "data" => $data,
                    );

            echo json_encode($output);
    }

    function ajax_list_detalle(){
            $id = $this->input->post('HCid');
            $list = $this->historiaclinica_modelo->get_datatables2($id);
            $data = array();
            foreach ($list as $detalleHC){

                $row = array();
                $row[] = $detalleHC->historiasclinicasid;
                $row[] = 'Dr. '.$detalleHC->personasnombrecompuesto;
                $row[] = $detalleHC->especialidadesnombre;
                $row[] = $detalleHC->detallehistoriasclinicasfecha;
                $row[] = $detalleHC->detallehistoriasclinicashora;
                $row[] = $detalleHC->detallehistoriasclinicasdiagnostico;
                $row[] = $detalleHC->detallehistoriasclinicasobservaciones;

                $data[] = $row;

            }        

            $output = array(
                            "draw" => intval($_POST['draw']),
                            "recordsTotal" => $this->historiaclinica_modelo->count_all2(),
                            "recordsFiltered" => $this->historiaclinica_modelo->count_filtered2($id),
                            "data" => $data,
                    );

            echo json_encode($output);
    }

    public function CerrarSesion(){
        $this->session->sess_destroy();
        redirect(base_url().'index.php/login');
    }    

    public function registrarHC(){
        $list['dni'] = '';
        $list['pacienteid'] = '';
        $list['pacientenombre'] = '';
        $list['fecha'] = date('D M d Y H:i:s \G\M\TO (T)');
        $list['fechanac'] = '';
        $list['pacienteedad'] = '';
        $list['pacientetelefonos'] = '';
        $list['pacienteprocedencia'] = '';
        $list['pacienteobrasocial'] = '';
        $list['pacientenroos'] = '';
        $list['pacientetitularos'] = '';
        $list['pacienteocupacion'] = '';
        $list['pacientederivado'] = '';
        $list['pacientemadre'] = '';
        $list['pacienteocupacionm'] = '';
        $list['pacientepadre'] = '';
        $list['pacienteocupacionp'] = '';
        $list['band'] = 0;
        $list['mensaje'] = '';
        $this->load->view('registrarHC_view',$list);
        
    }


    public function BuscarPacientes(){
        $DNI = $this->input->get('term');
        $result = $this->historiaclinica_modelo->Pacientes($DNI);
        $data = array();
        foreach ($result as $pa){
            $data[] = $pa->personasnrodocumento;
        }
        echo json_encode($data);    
    }


    public function CargaPaciente(){
        $DNI = $this->input->post('pacienteDNI');
        $result['respuesta'] = $this->historiaclinica_modelo->PacientesCarga($DNI);
        
        echo json_encode($result);
    }


    public function CargaEspecialidades(){
        $idProf =  $this->input->post('profesionalesid');
        $opciones = '';
        if ($idProf == 0){
            $opciones.= "<option value ='0' >Seleccione primero el profesional</option>";
        }else{
            $list = $this->historiaclinica_modelo->Especialidades($idProf);
            foreach($list as $esp){
                if($esp->estadosid == 1){
                $opciones.='<option value="'.$esp->especialidadesid.'">'.$esp->especialidadesnombre.'</option>';
                }
            }
        }

        echo $opciones;
    }

    public function CargaTelefonos(){
        $DNI =  $this->input->post('personasnrodocumento');
        $opciones = '';
        if ($DNI == 0){
        }else{
            $list = $this->historiaclinica_modelo->Telefonos($DNI);
            if ($list != null){
                foreach($list as $tel){
                    $opciones.= $tel->telefonos." - ";
                }
            }
        }

        echo ($opciones) ;
    }

    public function CargaObrasSociales(){
        $DNI =  $this->input->post('personasnrodocumento');
        $opciones = '';
        if ($DNI != 0){
            $list = $this->historiaclinica_modelo->ObraSocial($DNI);
            if ($list != null){
                foreach($list as $os){
                    $opciones.=$os->obrasocialnombre." - ";
                }
            }
        }
        echo ($opciones);
    }

    public function detalleHC($id){
        $list['id'] = $id;
        $list['hc'] = $this->historiaclinica_modelo->buscarHC($id);
        //aqui habra que calcular la edad con php?
        $list['pacienteedad'] = '';
        $DNI = $list['hc'][0]->personasnrodocumento;
        $op = '';
        $T = $this->historiaclinica_modelo->Telefonos($DNI);
        if (!empty($T)) {
           foreach($T as $tel){
                    $op.= $tel->telefonos." - ";
                }       
        } 
        $list['tel'] = $op;
        
        $op2 = '';
        $O = $this->historiaclinica_modelo->ObraSocial($DNI);
        if (!empty($O)) {
           foreach($O as $os){
                    $op2.= $os->obrasocialnombre." - ";
                }
        }
        
        
        $list['os'] = $op2;
        $list['profesionales'] = $this->historiaclinica_modelo->Profesionales();
        $list['profesionalid'] = '0';
        $list['especialidadid'] = '0';
        $list['especialidadesxprofesionalesid'] = '0';
        $list['diagnostico'] = '';
        $list['fecha'] = date('D M d Y H:i:s \G\M\TO (T)');
        $this->load->view('detalleHC_view',$list); 
    }

    public function esnumerico($dni){
        if (is_numeric($dni)){
            return True;
        }
        else{
            return False;
        }
    }

    public function CargaHC(){
        $DNI =  $this->input->post('pacienteDNI');
        $list['dni'] =  $this->input->post('inputDNI');
        $list['pacienteid'] =  $this->input->post('inputId');
        $list['pacientenombre'] =  $this->input->post('inputNombreCompuesto');
        $fecha =  $this->input->post('fechaN');
        $list['fecha'] = $fecha;
        $list['fechanac'] = $this->input->post('inputFechaNac');
        $list['pacienteedad'] = $this->input->post('inputEdad');
        
        $list['pacientetelefonos'] = $this->historiaclinica_modelo->Telefonos($DNI);
        $list['pacienteprocedencia'] = $this->input->post('inputProcedencia');;
        $list['pacienteobrasocial'] = $this->historiaclinica_modelo->ObraSocial($DNI);
        $list['pacientenroos'] = $this->input->post('inputNroOS');
        $list['pacientetitularos'] = $this->input->post('inputTitularOS');
        $list['pacienteocupacion'] = $this->input->post('inputOcupacion');
        $list['pacientederivado'] = $this->input->post('inputDerivado');
        $list['pacientemadre'] = $this->input->post('inputMadre');
        $list['pacienteocupacionm'] = $this->input->post('inputOcuapcionM');
        $list['pacientepadre'] = $this->input->post('inputPadre');
        $list['pacienteocupacionp'] = $this->input->post('inputOcupacionP');
        $list['mensajeDNI'] = "";
        $list['mensajeFecha'] = "";
        $list['mensajeProcedencia'] = "";
        $list['mensajeNroOs'] = "";
        $list['mensajeTitularOs'] = "";
        $list['mensajeOcupacion'] = "";
        $list['mensajeMadre'] = "";
        $list['mensajeOcupacionM'] = "";
        $list['mensajePadre'] = "";
        $list['mensajeOcupacionP'] = "";
        $cant = $this->historiaclinica_modelo->contarHC($this->input->post('inputDNI'));
        $b=0;

        if (empty($this->input->post('inputDNI'))){
            $list['mensajeDNI'] = "El DNI es Requerido";
            $b = 1;
        }else{
            if( !$this->esnumerico($this->input->post('inputDNI'))){
                $list['mensajeDNI'] = "El DNI no es válido";
                $b = 1;
            }else{
                $DNI = $this->input->post('inputDNI');
                if (strlen($DNI)< 8 || strlen($DNI)>10 ){
                    $list['mensajeDNI'] = "La longitud del DNI no es válido";
                    $b = 1 ;
                }  
            }
        }
        if (empty($this->input->post('fechaN'))) {
           $list['mensajeFecha'] = "La Fecha es requerida";
           $b = 1;
        }
        if ($cant[0]->cantidad > 0){
            $list['mensajeDNI'] = "Ya existe una Historia Clínica con ese DNI";
            $b = 1;
        }
        if (empty($this->input->post('inputProcedencia'))) {
           $list['mensajeProcedencia'] = "La Procedencia es requerida";
           $b = 1;
        }

        if($this->historiaclinica_modelo->ObraSocial($DNI) != '' ){
            if (empty($this->input->post('inputNroOS'))){
                $list['mensajeNroOs'] = "El Número es requerido cuando tiene Obra Social";
                $b = 1;
            }
            
        }
        if($this->historiaclinica_modelo->ObraSocial($DNI) != '' ){
            if (empty($this->input->post('inputTitularOS'))){
                $list['mensajeTitularOs'] = "El Titular es requerido cuando tiene Obra Social";
                $b = 1;
            }
        } 
        if (empty($this->input->post('inputOcupacion'))) {
           $list['mensajeOcupacion'] = "La Ocupación es requerida";
           $b = 1;
        }
        if ($this->input->post('inputEdad') < 18){
            if(empty($this->input->post('inputMadre'))){
                $list['mensajeMadre'] = "Nombre de la Madre es requerido por ser menor de edad";
                $b = 1;
            }
            if(empty($this->input->post('inputOcupacionM'))) {
                $list['mensajeOcupacionM'] = "Ocupación de la Madre es requerido";
                $b = 1;
            }
            if(empty($this->input->post('inputPadre'))){
                $list['mensajePadre'] = "Nombre del Padre es requerido por ser menor de edad";
                $b = 1;
            }
            if(empty($this->input->post('inputOcupacionP'))){
                $list['mensajeOcupacionP'] = "Ocupación del Padre es requerido";
                $b = 1;
            }

        }
        if ($b == 1) {

            $this->load->view('registrarHC_view',$list);
        }else{
                $data = array('pacientesid' => $this->input->post('inputId') ,
                              'historiasclinicasfechaalta'=>$this->input->post('fechaN'),
                              'historiasclinicasprocedenciapaciente'=>$this->input->post('inputProcedencia'),
                              'historiasclinicasnumeroospaciente'=>$this->input->post('inputNroOS'),
                              'historiasclinicastitularospaciente'=>$this->input->post('inputTitularOS'),
                              'historiasclinicasocupacionpaciente'=>$this->input->post('inputOcupacion'),
                              'historiasclinicasderivadopaciente'=>$this->input->post('inputDerivado'),
                              'historiasclinicasmadrepaciente'=>$this->input->post('inputMadre'),
                              'historiasclinicasocupacionmadrepaciente'=>$this->input->post('inputOcupacionM'),
                              'historiasclinicaspadrepaciente'=>$this->input->post('inputPadre'),
                              'historiasclinicasocupacionpadrepaciente'=>$this->input->post('inputOcupacionP'),


                               );

                $transaction = "insert into historiasclinicas (pacientesid,historiasclinicasfechaalta,historiasclinicasprocedenciapaciente,historiasclinicasnumeroospaciente,historiasclinicastitularospaciente,historiasclinicasocupacionpaciente,historiasclinicasderivadopaciente,historiasclinicasmadrepaciente,historiasclinicasocupacionmadrepaciente,historiasclinicaspadrepaciente,historiasclinicasocupacionpadrepaciente) values ("."'".$data['pacientesid']."','".$data['historiasclinicasfechaalta']."','".$data['historiasclinicasprocedenciapaciente']."','".$data['historiasclinicasnumeroospaciente']."','".$data['historiasclinicastitularospaciente']."','".$data['historiasclinicasocupacionpaciente']."','".$data['historiasclinicasderivadopaciente']."','".$data['historiasclinicasmadrepaciente']."','".$data['historiasclinicasocupacionmadrepaciente']."','".$data['historiasclinicaspadrepaciente']."','".$data['historiasclinicasocupacionpadrepaciente']."')";

                $dataInserted = "pacientesid = ".$data['pacientesid'].", historiasclinicasfechaalta = ".$data['historiasclinicasfechaalta'].", historiasclinicasprocedenciapaciente = ".$data['historiasclinicasprocedenciapaciente'].", historiasclinicasnumeroospaciente = ".$data['historiasclinicasnumeroospaciente'].", historiasclinicastitularospaciente = ".$data['historiasclinicastitularospaciente'].", historiasclinicasocupacionpaciente = ".$data['historiasclinicasocupacionpaciente'].", historiasclinicasderivadopaciente = ".$data['historiasclinicasderivadopaciente'].", historiasclinicasmadrepaciente = ".$data['historiasclinicasmadrepaciente'].", historiasclinicasocupacionmadrepaciente = ".$data['historiasclinicasocupacionmadrepaciente'].", historiasclinicaspadrepaciente = ".$data['historiasclinicaspadrepaciente'].", historiasclinicasocupacionpadrepaciente = ".$data['historiasclinicasocupacionpadrepaciente'];

                $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
                );


                $this->historiaclinica_modelo->insertHC($data);
                $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                redirect(base_url().'index.php/historiaclinica/detalleHC/'.$this->input->post('inputId'));
        }       
        
        
    }



    public function modificarHC($id){
        $list['id'] = $id;
        $idPa = $this->historiaclinica_modelo->buscarPacientexIdHC($id);

        $list['datos'] = $this->historiaclinica_modelo->buscarHC($idPa[0]->pacientesid);
        $list['pacienteedad'] = $this->input->post('inputEdad');
       
        $DNI = $list['datos'][0]->personasnrodocumento;
        $op = '';
        $T = $this->historiaclinica_modelo->Telefonos($DNI);
        if (!empty($T)) {
               foreach($T as $tel){
                        $op.= $tel->telefonos." - ";
                    }       
        } 
        $list['tel'] = $op;
        $op2 = '';
        $O = $this->historiaclinica_modelo->ObraSocial($DNI);
        if (!empty($O)) {
           foreach($O as $os){
                    $op2.= $os->obrasocialnombre." - ";
                }
        }
        
        
        $list['os'] = $op2;

        
        $list['mensajeProcedencia'] = "";
        $list['mensajeNroOs'] = "";
        $list['mensajeTitularOs'] = "";
        $list['mensajeOcupacion'] = "";
        $list['mensajeMadre'] = "";
        $list['mensajeOcupacionM'] = "";
        $list['mensajePadre'] = "";
        $list['mensajeOcupacionP'] = "";
        $b=0;

        
        
        
        if (empty($this->input->post('inputProcedencia'))) {
           $list['mensajeProcedencia'] = "La Procedencia es requerida";
           $b = 1;
        }
        if($this->historiaclinica_modelo->ObraSocial($DNI) != '' ){
            if (empty($this->input->post('inputNroOS'))){
                $list['mensajeNroOs'] = "El Número es requerido";
                $b = 1;
            }
            
        }
        if($this->historiaclinica_modelo->ObraSocial($DNI) != '' ){
            if (empty($this->input->post('inputTitularOS'))){
                $list['mensajeTitularOs'] = "El Titular es requerido ";
                $b = 1;
            }
        } 
 
        if (empty($this->input->post('inputOcupacion'))) {
           $list['mensajeOcupacion'] = "La Ocupación es requerida";
           $b = 1;
        }
        if ($this->input->post('inputEdad') < 18){
            if(empty($this->input->post('inputMadre'))){
                $list['mensajeMadre'] = "Nombre de la Madre es requerido por ser menor de edad";
                $b = 1;
            }
            if(empty($this->input->post('inputOcupacionM'))){
                $list['mensajeOcupacionM'] = "Ocupación de la Madre es requerido";
                $b = 1;
            }
            if(empty($this->input->post('inputPadre'))){
                $list['mensajePadre'] = "Nombre del Padre es requerido por ser menor de edad";
                $b = 1;
            }
            if(empty($this->input->post('inputOcupacionP'))){
                $list['mensajeOcupacionP'] = "Ocupación del Padre es requerido";
                $b = 1;
            }

        }
        if ($b == 1) {
            $DNI =  $list['datos'][0]->personasnrodocumento;
            $list['pacientedni'] =  $this->input->post('inputDNI');
            $list['pacienteid'] =  $this->input->post('inputID');
            $list['pacientenombre'] =  $this->input->post('inputNombreCompuesto');
            //$fecha =  $this->input->post('hcFechaAlta');
            $list['fecha'] = $this->input->post('hcFechaAlta');
            $list['fechanac'] = $this->input->post('inputFechaNac');
            $list['pacienteedad'] = $this->input->post('inputEdad');
            /*if ($list['pacienteedad'] < 18){
                $list['band'] = 1; 
            }else{
                $list['band'] = 0; 
            }*/

            //$DNI = $list['datos'][0]->personasnrodocumento;
            $op = '';
            $T = $this->historiaclinica_modelo->Telefonos($DNI);
            if (!empty($T)) {
               foreach($T as $tel){
                        $op.= $tel->telefonos." - ";
                    }       
            } 
            $list['tel'] = $op;
            $op2 = '';
            $O = $this->historiaclinica_modelo->ObraSocial($DNI);
            if (!empty($O)) {
               foreach($O as $os){
                        $op2.= $os->obrasocialnombre." - ";
                    }
            }
            $list['os'] = $op2;
            //$list['pacientetelefonos'] = $this->historiaclinica_modelo->Telefonos($DNI);
            $list['pacienteprocedencia'] = $this->input->post('inputProcedencia');;
            //$list['pacienteobrasocial'] = $this->historiaclinica_modelo->ObraSocial($DNI);
            $list['pacientenroos'] = $this->input->post('inputNroOS');
            $list['pacientetitularos'] = $this->input->post('inputTitularOS');
            $list['pacienteocupacion'] = $this->input->post('inputOcupacion');
            $list['pacientederivado'] = $this->input->post('inputDerivado');
            $list['pacientemadre'] = $this->input->post('inputMadre');
            $list['pacienteocupacionm'] = $this->input->post('inputOcuapcionM');
            $list['pacientepadre'] = $this->input->post('inputPadre');
            $list['pacienteocupacionp'] = $this->input->post('inputOcupacionP');
            $this->load->view('modificarHC_view',$list);
        }else{
                $fecha = getdate();
                $data2 = array(
                   
                    'historiasclinicasfechamod' => $fecha['mday']."/".$fecha['mon']."/".$fecha['year'],
                );
                $this->historiaclinica_modelo->updateHC($data2,$id);

                $data = array(
                              
                              'historiasclinicasprocedenciapaciente'=>$this->input->post('inputProcedencia'),
                              'historiasclinicasnumeroospaciente'=>$this->input->post('inputNroOS'),
                              'historiasclinicastitularospaciente'=>$this->input->post('inputTitularOS'),
                              'historiasclinicasocupacionpaciente'=>$this->input->post('inputOcupacion'),
                              'historiasclinicasderivadopaciente'=>$this->input->post('inputDerivado'),
                              'historiasclinicasmadrepaciente'=>$this->input->post('inputMadre'),
                              'historiasclinicasocupacionmadrepaciente'=>$this->input->post('inputOcupacionM'),
                              'historiasclinicaspadrepaciente'=>$this->input->post('inputPadre'),
                              'historiasclinicasocupacionpadrepaciente'=>$this->input->post('inputOcupacionP'),


                               );
                $transaction = "update historiasclinicas set historiasclinicasfechaalta = ".$data['historiasclinicasfechaalta'].", historiasclinicasprocedenciapaciente = ".$data['historiasclinicasprocedenciapaciente'].", historiasclinicasnumeroospaciente = ".$data['historiasclinicasnumeroospaciente'].", historiasclinicastitularospaciente = ".$data['historiasclinicastitularospaciente'].", historiasclinicasocupacionpaciente = ".$data['historiasclinicasocupacionpaciente'].", historiasclinicasderivadopaciente = ".$data['historiasclinicasderivadopaciente'].", historiasclinicasmadrepaciente = ".$data['historiasclinicasmadrepaciente'].", historiasclinicasocupacionmadrepaciente = ".$data['historiasclinicasocupacionmadrepaciente'].", historiasclinicaspadrepaciente = ".$data['historiasclinicaspadrepaciente'].", historiasclinicasocupacionpadrepaciente = ".$data['historiasclinicasocupacionpadrepaciente'];

                $dataUpdated = " pacientesid = ".$id." , historiasclinicasfechaalta = ".$data['historiasclinicasfechaalta'].", historiasclinicasprocedenciapaciente = ".$data['historiasclinicasprocedenciapaciente'].", historiasclinicasnumeroospaciente = ".$data['historiasclinicasnumeroospaciente'].", historiasclinicastitularospaciente = ".$data['historiasclinicastitularospaciente'].", historiasclinicasocupacionpaciente = ".$data['historiasclinicasocupacionpaciente'].", historiasclinicasderivadopaciente = ".$data['historiasclinicasderivadopaciente'].", historiasclinicasmadrepaciente = ".$data['historiasclinicasmadrepaciente'].", historiasclinicasocupacionmadrepaciente = ".$data['historiasclinicasocupacionmadrepaciente'].", historiasclinicaspadrepaciente = ".$data['historiasclinicaspadrepaciente'].", historiasclinicasocupacionpadrepaciente = ".$data['historiasclinicasocupacionpadrepaciente'];

                $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosmodificados' => $dataUpdated
                );
                
                $this->historiaclinica_modelo->updateHC($data,$id);
                $this->auditoria_modelo->registrarAuditoria($data_auditoria);
                redirect(base_url().'index.php/historiaclinica');
        }  
        
    }



    public function CargaTablaDHC(){
        
        $idProf = $this->input->post('inputProfesional');
        $list['id'] = $this->input->post('idpaciente');
        $list['hc'] = $this->historiaclinica_modelo->buscarHC($this->input->post('idpaciente'));
        $list['profesionalid'] = $idProf;
        $idEsp =  $this->input->post('inputEspecialidades');
        $list['especialidadid'] = $idEsp;
        $fecha =  $this->input->post('fechaN');
        $list['fecha'] = $fecha;
        $list['profesionales'] = $this->historiaclinica_modelo->Profesionales();
        $list['especialidades'] = $this->historiaclinica_modelo->Especialidades($idProf);
        $list['MensajeProfesional'] = "";
        $list['MensajeEspecialidad'] ="";
        $list['MensajeFecha'] = "";
        $list['MensajeHora'] = "";
        $list['MensajeDiagnostico'] = "";
        $list['pacienteedad'] = $this->input->post('inputEdad');
        $DNI = $this->input->post('pacienteDNI');
        $op='';
        $T = $this->historiaclinica_modelo->Telefonos($DNI);
        foreach($T as $tel){
                    $op.= $tel->telefonos." - ";
                }
        $list['tel'] = $op;
        $op2='';
        $O = $this->historiaclinica_modelo->ObraSocial($DNI);
        foreach($O as $os){
                    $op2.= $os->obrasocialnombre." - ";
                }
        $list['os'] = $op2;
        $b = 0;
        if($idProf == 0){
            $list['especialidadesxprofesionalesid'] = '0';
        }else{
            foreach($list['especialidades'] as $esp){
                if($esp->especialidadesid == $idEsp){
                    $list['especialidadesxprofesionalesid'] = $esp->especialidadesxprofesionalesid;
                }
            }
        }

        if ( $this->input->post('inputProfesional') == 0){
            $list['MensajeProfesional'] = "Debe seleccionar un Profesional";
            $b = 1;
        }
        if ( $this->input->post('inputProfesional') == 0){
            $list['MensajeEspecialidad'] = "Debe seleccionar una Especialidad";
            $b = 1;
        }

        if( empty($this->input->post('fechaN'))){
            $list['MensajeFecha'] = "La fecha es requerida";
            $b = 1;
        }

        if( empty($this->input->post('DHChora'))){
            $list['MensajeHora'] = "La Hora es requerida";
            $b = 1;
        }
        if(empty($this->input->post('DHCdiagnostico'))  ){
            $list['MensajeDiagnostico'] = "El Diagnostico es requerido";
            $b = 1;
        }
        if($b == 1){
            $this->load->view('detalleHC_view',$list);
        }else{
             $data = array(
            'historiasclinicasid' =>  $this->input->post('idHC'),
            'profesionalesid' => $this->input->post('inputProfesional'),
            'especialidadesid' => $this->input->post('inputEspecialidades'),
            'detallehistoriasclinicasfecha' =>  $list['fecha'],
            'detallehistoriasclinicashora'  => $this->input->post('DHChora'),
            'detallehistoriasclinicasdiagnostico'  => $this->input->post('DHCdiagnostico'),
            'detallehistoriasclinicasobservaciones' => $this->input->post('DHCobservaciones'),

            );

            $transaction = "insert into detallehistoriasclinicas (historiasclinicasid, profesionalesid, especialidadesid, detallehistoriasclinicasfecha, detallehistoriasclinicashora, detallehistoriasclinicasdiagnostico, detallehistoriasclinicasobservaciones) values ("."'".$data['historiasclinicasid']."','".$data['profesionalesid']."','".$data['especialidadesid']."','".$data['detallehistoriasclinicasfecha']."','".$data['detallehistoriasclinicashora']."','".$data['detallehistoriasclinicasdiagnostico']."','".$data['detallehistoriasclinicasobservaciones']."')";

            $dataInserted = "historiasclinicasid = ".$data['historiasclinicasid'].", profesionalesid = ".$data['profesionalesid'].", especialidadesid = ".$data['especialidadesid'].", detallehistoriasclinicasfecha = ".$data['detallehistoriasclinicasfecha'].", detallehistoriasclinicashora = ".$data['detallehistoriasclinicashora'].", detallehistoriasclinicasdiagnostico = ".$data['detallehistoriasclinicasdiagnostico'].", detallehistoriasclinicasobservaciones = ".$data['detallehistoriasclinicasobservaciones'];  

            $data_auditoria = array(
                                    'nombreusuario' => $this->session->userdata('usuario'),
                                    'transaccion' => $transaction,
                                    'datosingresados' => $dataInserted
            );

            $this->historiaclinica_modelo->insertDHC($data);
            $this->auditoria_modelo->registrarAuditoria($data_auditoria);
            redirect(base_url().'index.php/historiaclinica/detalleHC/'.$this->input->post('idpaciente'));
        }
       
                   
    }


   
}

?>