CREATE OR REPLACE FUNCTION  fnc_especialidadesxprofesionales_cambio_estadosid() RETURNS TRIGGER AS $$



BEGIN
	NEW.estadosid:=1;
	RETURN NEW;
END; 
$$
language 'plpgsql' 


CREATE TRIGGER tr_especialidadesxprofesionales_cambio_estadosid BEFORE INSERT  
    ON especialidadesxprofesionales FOR EACH ROW 
    EXECUTE PROCEDURE fnc_especialidadesxprofesionales_cambio_estadosid();