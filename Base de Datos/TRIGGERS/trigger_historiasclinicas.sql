﻿CREATE OR REPLACE FUNCTION  fnc_fecharegistro_historiasclinicas() RETURNS TRIGGER AS $$
BEGIN
	NEW.HistoriasClinicasFechaAlta:=now();
	RETURN NEW;
END; 
$$
language 'plpgsql' 


CREATE TRIGGER tr_fecharegistro_historiasclinicas BEFORE INSERT  
    ON historiasclinicas FOR EACH ROW 
    EXECUTE PROCEDURE fnc_fecharegistro_historiasclinicas();