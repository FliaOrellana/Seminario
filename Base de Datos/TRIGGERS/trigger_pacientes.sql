﻿CREATE OR REPLACE FUNCTION  fnc_fecharegistro_pacientes() RETURNS TRIGGER AS $$



BEGIN
	NEW.pacientesfecharegistro:=now();
	RETURN NEW;
END; 
$$
language 'plpgsql' 


CREATE TRIGGER tr_fecharegistro_pacientes BEFORE INSERT  
    ON pacientes FOR EACH ROW 
    EXECUTE PROCEDURE fnc_fecharegistro_pacientes();


select * from pacientes
select * from pacientesestado
insert into pacientesestado (pacientesestadonombre) values('Activo'),('No Activo');
select * from personas
insert into Pacientes (personasid,pacientesestadoid) values
(5,1),
(6,1),
(7,1),
(8,1),
(9,2),
(10,1),
(11,1),
(12,2),
(13,2),
(14,1),
(15,1);