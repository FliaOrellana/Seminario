CREATE OR REPLACE FUNCTION public.fn_fecharegistro_feriados()
  RETURNS trigger AS
$BODY$

BEGIN
	if TG_OP ='INSERT' then
		NEW.Feriadosfecharegistro:=now();
	elsif TG_OP= 'UPDATE' then
		if NEW.estadosid = 1 then
			NEW.Feriadosfechamod:=now();
		else
			New.Feriadosfechabaja:=now();
		end if;
	end if;
	return NEW;
			
			
	
END; 
$BODY$
  LANGUAGE 'plpgsql'

 CREATE TRIGGER tr_fecharegistro_feriados BEFORE INSERT  
    ON feriados FOR EACH ROW 
    EXECUTE PROCEDURE fn_fecharegistro_feriados();