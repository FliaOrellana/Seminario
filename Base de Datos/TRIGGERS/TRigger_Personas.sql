﻿CREATE OR REPLACE FUNCTION  fn_NombreCompuesto() RETURNS TRIGGER AS 
$$

BEGIN
	NEW.personasapellido:=upper(NEW.personasapellido);
	NEW.personasnombre:=upper(NEW.personasnombre);
	NEW.PersonasNombreCompuesto:=upper(NEW.PersonasNombre||' '||NEW.PersonasApellido);
	NEW.personasnrodocumento:=upper(NEW.personasnrodocumento);
	NEW.personasdomicilio:=upper(NEW.personasdomicilio);
	NEW.personastelefono:=upper(NEW.personastelefono);
	NEW.personasemail:=upper(NEW.personasemail);
	NEW.PersonasFEchaRegistro:=now();
	RETURN NEW;
END; 
$$
language 'plpgsql' 


CREATE TRIGGER tr_NombreCompuesto BEFORE INSERT  
    ON Personas FOR EACH ROW 
    EXECUTE PROCEDURE fn_NombreCompuesto();



    insert into personas (personasNombre,PersonasApellido,PersonasNroDocumento,PersonasDomicilio,PersonasEmail,PersonasTelefono,Sexoid) 
values ('Janet Veronica','Campos','35282057','Ledesma','camposjanetveronica@gmail.com','3875864595',1),
('César Andrés','Orellana','36127956','ohiggins 464','orellanacesar.1992@gmail.com','3875851100',2),
('Pablo Facundo','Orellana','36127955','ohiggins 464','facundo_3330@hotmail.com','3875456876',2),
('Mirta Adriana','Palomo','32473123','Dean funes 123','PalomoMirtaAdriana@hotmail.com','0116831381',1),
('Eliana','Fagiolo','33326479','Autodromo','fagioloeliana@gmail.com','3874576876',1),
('Marisol Anabela','Campos','39201388','Autodromo 2','CamposMArisolanabela@gmail.com','388364578',1),
('Hugo Rene','Campos','11494816','Autodromo 3','camposhugo@gmail.com','3886525770',2),
('Victoria','Rioja','21318770','Autodromo 4','RiojaVictoria@gmail.com','3886467180',1),
('Martin','Diaz','38765532','Pueyrredon 456','mdiaz@gmail.com','3876435987',2),
('Cristian Marcelo','Diaz','38723232','Dean funes 245','DiazCristian@gmail.com','3875334100',2),
('Patricia','Aballay','38766786','Los toldos 667','paballay@gmail.com','3874345564',1);


insert into personas (personasNombre,PersonasApellido,PersonasNroDocumento,PersonasDomicilio,PersonasEmail,PersonasTelefono,Sexoid) 
values ('Mariela','palomo','123456789','Zuviria 1289','pmariela@gmail.com','3875249670',1),
('Nombre 1',' Apellido 1','987654321','Calle 1','profesinal1@gmail.com','3762763737',2);

select * from personas


create or replace function fn_datosModificados_personas()
returns TRIGGER
as 
$$
begin
	NEW.personasapellido:=upper(NEW.personasapellido);
	NEW.personasnombre:=upper(NEW.personasnombre);
	NEW.PersonasNombreCompuesto:=upper(NEW.PersonasNombre||' '||NEW.PersonasApellido);
	NEW.personasnrodocumento:=upper(NEW.personasnrodocumento);
	NEW.personasdomicilio:=upper(NEW.personasdomicilio);
	NEW.personastelefono:=upper(NEW.personastelefono);
	NEW.personasemail:=upper(NEW.personasemail);
	NEW.personasfechamod := now();
	return NEW;
end;
$$
language 'plpgsql'

CREATE TRIGGER tr_datosModificados_personas
  BEFORE UPDATE 
  ON personas
  FOR EACH ROW
  EXECUTE PROCEDURE fn_datosModificados_personas();