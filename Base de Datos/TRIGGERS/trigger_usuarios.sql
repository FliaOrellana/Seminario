﻿CREATE OR REPLACE FUNCTION  fnc_encriptar_pass() RETURNS TRIGGER AS $$

BEGIN
	NEW.usuariosnombre:=upper(NEW.usuariosnombre);
	NEW.usuarioscontrasenia:=md5(NEW.usuarioscontrasenia);
	NEW.usuariosfechaingreso:=now();
	RETURN NEW;
END; 
$$
language 'plpgsql' 


CREATE TRIGGER tr_encriptar_pass BEFORE INSERT OR UPDATE 
    ON USUARIOS FOR EACH ROW 
    EXECUTE PROCEDURE fnc_encriptar_pass();

/*
*/

CREATE OR REPLACE FUNCTION  fnc_usuarioiguales() RETURNS TRIGGER AS $$
DECLARE
cant INTEGER ;
BEGIN
	SELECT count(*) INTO cant FROM usuarios WHERE usuariosnombre=new.usuariosnombre;
	IF cant>=1 THEN
		RAISE EXCEPTION 'USUARIO YA EXISTE';
	ELSE
		RETURN NEW;
	END IF;
END; 	
$$
language 'plpgsql'

CREATE TRIGGER tr_usuarioiguales BEFORE INSERT OR UPDATE  
    ON USUARIOS FOR EACH ROW 
    EXECUTE PROCEDURE fnc_usuarioiguales(); 

--function y trigger para asignar a un usuario recien creado estado=1

CREATE OR REPLACE FUNCTION  fnc_usuariosestado() RETURNS TRIGGER AS $$
	BEGIN
		NEW.estadosid:=1;
		RETURN NEW;
	END; 
	$$
	language 'plpgsql' 


CREATE TRIGGER tr_usuarioestado BEFORE INSERT 
    ON USUARIOS FOR EACH ROW 
    EXECUTE PROCEDURE fnc_usuariosestado();

