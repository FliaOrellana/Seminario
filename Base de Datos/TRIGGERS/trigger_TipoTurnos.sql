﻿CREATE OR REPLACE FUNCTION  fn_TipoTurnos() RETURNS TRIGGER AS $$

BEGIN
	NEW.TipoTurnosnombre=upper(NEW.TipoTurnosnombre);
	RETURN NEW;
END; 
$$
language 'plpgsql' ;


CREATE TRIGGER tr_TipoTurnos BEFORE INSERT  
    ON TipoTurnos FOR EACH ROW 
    EXECUTE PROCEDURE fn_TipoTurnos();