﻿CREATE OR REPLACE FUNCTION  fn_fecharegistro_Secretarios() RETURNS TRIGGER AS $$



BEGIN
	NEW.SecretariosFechaRegistro:=now();
	RETURN NEW;
END; 
$$
language 'plpgsql' ;


CREATE TRIGGER tr_fecharegistro_Secretarios BEFORE INSERT  
    ON secretarios FOR EACH ROW 
    EXECUTE PROCEDURE fn_fecharegistro_Secretarios();