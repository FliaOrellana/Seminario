﻿CREATE OR REPLACE FUNCTION  fn_fecharegistro_Turnos() RETURNS TRIGGER AS $$

BEGIN
	
	NEW.TurnosFechaRegistro:=now();
	NEW.TurnosHora:=trim(NEW.TurnosHora);
	RETURN NEW;
END; 
$$
language 'plpgsql' ;


CREATE TRIGGER tr_fecharegistro_Turnos BEFORE INSERT  
    ON turnos FOR EACH ROW 
    EXECUTE PROCEDURE fn_fecharegistro_Turnos();

