CREATE OR REPLACE FUNCTION public.fn_fecharegistro_vacaciones()
  RETURNS trigger AS
$BODY$

BEGIN
	if TG_OP ='INSERT' then
		NEW.Vacacionesfecharegistro:=now();
	elsif TG_OP= 'UPDATE' then
		if NEW.estadosid = 1 then
			NEW.Vacacionesfechamod:=now();
		else
			New.Vacacionesfechabaja:=now();
		end if;
	end if;
	return NEW;
			
			
	
END; 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 CREATE TRIGGER tr_fecharegistro_vacaciones BEFORE INSERT  
    ON vacaciones FOR EACH ROW 
    EXECUTE PROCEDURE fn_fecharegistro_vacaciones();