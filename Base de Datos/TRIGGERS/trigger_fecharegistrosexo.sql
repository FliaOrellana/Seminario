﻿CREATE OR REPLACE FUNCTION  fnc_fecharegistro_sexo() RETURNS TRIGGER AS $$



BEGIN
	NEW.sexofecharegistro:=now();
	RETURN NEW;
END; 
$$
language 'plpgsql' 


CREATE TRIGGER tr_fecharegistro_sexo BEFORE INSERT  
    ON sexo FOR EACH ROW 
    EXECUTE PROCEDURE fnc_fecharegistro_sexo();


drop trigger tr_fecharegistro_sexo on pacientes