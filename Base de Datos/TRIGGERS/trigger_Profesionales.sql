﻿CREATE OR REPLACE FUNCTION  fn_fecharegistro_Profesionales() RETURNS TRIGGER AS $$

BEGIN
	NEW.profesionalesMatricula=upper(NEW.profesionalesMatricula);
	NEW.ProfesionalesFechaRegistro:=now();
	RETURN NEW;
END; 
$$
language 'plpgsql' ;


CREATE TRIGGER tr_fecharegistro_Profesionales BEFORE INSERT  
    ON profesionales FOR EACH ROW 
    EXECUTE PROCEDURE fn_fecharegistro_Profesionales();



Create or replace function fn_turnoshora()returns TRIGGER as 
$$

Declare 
inicio time='08:00:00';
sumador time=inicio;
prueba time;
Inter record;
intervalo time;
con integer;
Begin
raise notice 'profesionalesid: %',new.profesionalesid;
for Inter in (select intervalostiempo from profesionales inner join intervalos using(intervalosid) where profesionalesid=NEW.profesionalesid) loop
	intervalo=Inter.intervalostiempo;
	end loop;
raise notice 'intervalo: %',intervalo;
while(sumador<'21:00:00')loop
	begin
		/*con=0;
		select sumador+intervalo::unknown into prueba;
		select count(turnoshora)into con from turnoshora where turnoshoratiempo=prueba;
		if(con=0) then
			sumador=sumador+intervalo;
			insert into turnoshora values(sumador);
		else
			sumador=sumador+intervalo;
		end if;*/
		con=0;
		CASE 
			WHEN intervalo='00:10:00' then
				begin
						select count(turnoshora)into con from turnoshora where turnoshoratiempo=sumador+'00:10:00';
						sumador=sumador+'00:10:00';
						if(con=0) then
							insert into turnoshora values(sumador);
						end if;
				end;
			WHEN intervalo='00:15:00' then
				begin
						select count(turnoshora)into con from turnoshora where turnoshoratiempo=sumador+'00:15:00';
						sumador=sumador+'00:15:00';
						if(con=0) then
							insert into turnoshora values(sumador);
						end if;
				end;
			WHEN intervalo='00:20:00' then
				begin
						select count(turnoshora)into con from turnoshora where turnoshoratiempo=sumador+'00:20:00';
						sumador=sumador+'00:20:00';
						if(con=0) then
							insert into turnoshora values(sumador);
						end if;
				end;
			ELSE  if intervalo='00:30:00' then
				begin
						select count(turnoshora)into con from turnoshora where turnoshoratiempo=sumador+'00:30:00';
						sumador=sumador+'00:30:00';
						if(con=0) then
							insert into turnoshora values(sumador);
						end if;
				end;
			     end if;
		end CASE;
	end ;
end loop;

return New;
end;
$$

language 'plpgsql';
-- YA LO RESOLVI MAMILA

CREATE TRIGGER tr_intervalos_Profesionales after INSERT or UPDATE  
    ON  Profesionales FOR EACH ROW 
    EXECUTE PROCEDURE fn_turnoshora();


update profesionales set intervalosid=1;

create or replace function fn_datosModificados_profesionales()
returns TRIGGER
as 
$$
begin
	NEW.profesionalesmatricula := upper(NEW.profesionalesmatricula);
	NEW.profesionalesfechamod := now();
	return NEW;
end;
$$
language 'plpgsql'

CREATE TRIGGER tr_datosModificados_profesionales
  BEFORE UPDATE 
  ON profesionales
  FOR EACH ROW
  EXECUTE PROCEDURE fn_datosModificados_profesionales();