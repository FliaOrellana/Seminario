﻿CREATE OR REPLACE FUNCTION  fn_fecharegistro_Especilidades() RETURNS TRIGGER AS $$



BEGIN
	NEW.EspecialidadesNombre:=upper(New.EspecialidadesNombre);
	NEW.EspecialidadesFechaRegistro:=now();
	RETURN NEW;
END; 
$$
language 'plpgsql' ;


CREATE TRIGGER tr_fecharegistro_Especilidades BEFORE INSERT  
    ON Especialidades FOR EACH ROW 
    EXECUTE PROCEDURE fn_fecharegistro_Especilidades();