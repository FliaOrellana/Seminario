CREATE OR REPLACE FUNCTION  fnc_fechamod_sexo() RETURNS TRIGGER AS $$

BEGIN
	NEW.sexofechamod:=now();
	RETURN NEW;
END; 
$$
language 'plpgsql' 


CREATE TRIGGER tr_fechamod_sexo BEFORE UPDATE  
    ON sexo FOR EACH ROW 
    EXECUTE PROCEDURE fnc_fechamod_sexo();