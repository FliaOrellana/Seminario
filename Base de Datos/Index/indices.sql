﻿create unique index Inx_Perfiles on Perfiles(PerfilesNombre);
create unique index Inx_UsuariosNombre on Usuarios(UsuariosNombre);
create unique index Inx_PersonasNroDocumetno on Personas(PersonasNroDocumento);
create unique index Inx_SexoNombre on Sexo(SexoNombre);
create unique index Inx_ObraSocialNombre on ObraSocial(ObraSocialNombre);
create unique index Inx_PacientesEsatadoNombre on PacientesEstado(PacientesEstadoNombre);
create unique index Inx_Pacientes on Pacientes(PacientesId,PersonasId);
create unique index Inx_Secretarios on Secretarios(SecretariosId,PersonasId);
create unique index Inx_Profesionales on Profesionales(ProfesionalesId,PersonasId);
create unique index Inx_PrefesionalesMatricula on Profesionales(ProfesionalesMatricula);
create unique index Inx_EspecialidadesNombre on Especialidades(EspecialidadesNombre);
create unique index Inx_Profesionles_Especialidades on EspecialidadesXProfesionales(ProfesionalesId,EspecialidadesId);
create unique index Inx_TipoTurnosNombre on TipoTurnos(TipoTurnosNombre);
create unique index Inx_TurnosFechaHora on Turnos(especialidadesxprofesionalesid,turnosfecha,turnoshoratiempo);
create unique index Inx_IntervalosTiempo on Intervalos(IntervalosTiempo);
create unique index Inx_turnoshoras on Turnoshora(turnoshoratiempo);
create unique index Inx_agendaProfEspDias on Agendas(especialidadesxprofesionalesid,diasid);


