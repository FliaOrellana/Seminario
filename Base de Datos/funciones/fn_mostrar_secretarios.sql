CREATE OR REPLACE FUNCTION public.fn_mostrar_secretarios(
    OUT id integer,
    OUT nombrecompleto character varying,
    OUT dni character varying,
    OUT domicilio character varying,
    OUT email character varying,
    OUT telefono character varying,
    OUT estadosid integer)
  RETURNS SETOF record AS
$BODY$
 declare
 lr_registro1 record;
 lr_registro2 record;
 tel character varying;
 cantidad integer;

 
 begin
	for lr_registro1 in (
		select personasid,secretariosid,personasnombrecompuesto,personasnrodocumento,personasdomicilio,personasemail,s.estadosid
		from personas inner join secretarios s using(personasid)
		)loop
		tel = '';
		select count(*) into cantidad from telefonos where personasid = lr_registro1.personasid;
		if (cantidad <> 0) then
			for lr_registro2 in (
				select telefononumero 
				from telefonos
				where personasid = lr_registro1.personasid 
			)loop
			 tel = tel || lr_registro2.telefononumero ||' / ';
			end loop;
			telefono = substring(tel from 1 for char_length(tel) - 2);
		else 
			telefono = '';
		end if;
		id = lr_registro1.secretariosid;
		nombrecompleto = lr_registro1.personasnombrecompuesto;
		dni = lr_registro1.personasnrodocumento;
		domicilio = lr_registro1.personasdomicilio;
		email = lr_registro1.personasemail;
		estadosid = lr_registro1.estadosid;
		return next;
	end loop;
	return;     
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_mostrar_secretarios()
  OWNER TO postgres;