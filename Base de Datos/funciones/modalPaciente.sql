CREATE OR REPLACE FUNCTION public.fn_modalpaciente(IN id integer)
  RETURNS TABLE(nombre character varying, dni character varying, fechanacimiento text, edad integer, sexo character varying, domicilio character varying, email character varying, telefonos text, obras text) AS
$BODY$
begin 
	return query (select p.personasnombrecompuesto,
			     p.personasnrodocumento,
			     to_char(p.personasfechanacimiento,'DD/MM/YYYY'),
			     fn_Edad(p.personasfechanacimiento) as edad,
			     s.sexonombre,
			     p.personasdomicilio,
			     p.personasemail,
			     fn_Telefonos(id),
			     fn_ObraSociales(id)
		      from personas p 
		      inner join pacientes pa using(personasid)
		      inner join sexo s using(sexoid)
		      where pa.pacientesid=id);
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_modalpaciente(integer)
  OWNER TO postgres;