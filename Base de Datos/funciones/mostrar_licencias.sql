CREATE OR REPLACE FUNCTION public.fn_mostrar_licencias(
    IN profesionalid integer,
    OUT licenciasids text,
    OUT fechainicio text,
    OUT fechafin text,
    OUT especialidades text)
  RETURNS SETOF record AS
$BODY$
 declare
 lr_registro1 record;
 lr_registro2 record;

 id text ;
 esp text;
 
 begin
	for lr_registro1 in (select l.licenciasfechaini,l.licenciasfechafin
			     from licencias l inner join especialidades e using(especialidadesid)
			     where estadosid = 1 and l.profesionalesid = profesionalid group by 1,2) loop
		id := '';
		esp := ''; 
		for lr_registro2 in (select l.licenciasid,l.licenciasfechaini,l.licenciasfechafin,e.especialidadesnombre 
				from licencias l inner join especialidades e using(especialidadesid)
				where estadosid = 1 and  l.licenciasfechaini = lr_registro1.licenciasfechaini and  l.licenciasfechafin = lr_registro1.licenciasfechafin and l.profesionalesid = profesionalid) loop
			id := id || lr_registro2.licenciasid ||'-';	 
			esp := esp || lr_registro2.especialidadesnombre || '-';
			end loop;
		licenciasids := substring(id from 1 for char_length(id)-1);
		fechainicio := lr_registro1.licenciasfechaini;
		fechafin := lr_registro1.licenciasfechafin;
		especialidades := substring(esp from 1 for char_length(esp)-1);
		return next;
		end loop;
	return;     
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_mostrar_licencias(integer)
  OWNER TO postgres;