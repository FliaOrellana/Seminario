﻿CREATE OR REPLACE FUNCTION public.fn_reportecancelados(
    profid integer,
    fechai date,
    fechaf date)
  RETURNS integer AS
$BODY$
declare

reg record;
cant_cancelados integer;

begin
	
		select count(*) into cant_cancelados
			    from mostrarturnos() 
			    where profesionalid = profid 
			    and ( to_date(fecha,'dd/mm/yyyy') between fechaI and fechaF ) 
			    and estadoturnoid = 3;
			  
			return cant_cancelados;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.fn_reportecancelados(integer, date, date)
  OWNER TO postgres;


select * from fn_reportecancelados(44,'2020-03-29','2020-04-10')