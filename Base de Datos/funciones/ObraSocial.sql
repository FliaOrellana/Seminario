create or replace function fn_ObraSociales(id int)
returns text
as
$$
declare 
os text :='';
cont int :=0;
reg1 record;
begin
	for reg1 in (select o.obrasocialnombre as obra
			from personas p 
			inner join pacientes pa using (personasid)
			inner join obrasocialpacientes op using(pacientesid)
			inner join obrasocial o using(obrasocialid)
			where pa.pacientesid = id) loop
		os:=os||reg1.obra||' / ';
		cont:=con+1;
	end loop;
	if(cont>=1)then
		os := substring(os from 1 for char_length(os)-2);
	end if;
	return os;
end;
$$
language 'plpgsql';