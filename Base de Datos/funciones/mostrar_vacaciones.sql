CREATE OR REPLACE FUNCTION public.fn_mostrar_vacaciones(
    IN profesionalid integer,
    OUT vacacionesids text,
    OUT fechainicio text,
    OUT fechafin text,
    OUT especialidades text)
  RETURNS SETOF record AS
$BODY$
 declare
 lr_registro1 record;
 lr_registro2 record;

 id text ;
 esp text;
 
 begin
	for lr_registro1 in (select v.vacacionesfechaini,v.vacacionesfechafin
			     from vacaciones v inner join especialidades e using(especialidadesid)
			     where estadosid = 1 and v.profesionalesid = profesionalid group by 1,2) loop
		id := '';
		esp := ''; 
		for lr_registro2 in (select v.vacacionesid,v.vacacionesfechaini,v.vacacionesfechafin,e.especialidadesnombre
			     from vacaciones v inner join especialidades e using(especialidadesid)
			     where estadosid = 1 and  v.vacacionesfechaini = lr_registro1.vacacionesfechaini and  v.vacacionesfechafin = lr_registro1.vacacionesfechafin and v.profesionalesid = profesionalid) loop
			id := id || lr_registro2.vacacionesid ||'-';	 
			esp := esp || lr_registro2.especialidadesnombre || '-';
			end loop;
		vacacionesids := substring(id from 1 for char_length(id)-1);
		fechainicio := lr_registro1.vacacionesfechaini;
		fechafin := lr_registro1.vacacionesfechafin;
		especialidades := substring(esp from 1 for char_length(esp)-1);
		return next;
		end loop;
	return;     
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_mostrar_vacaciones(integer)
  OWNER TO postgres;