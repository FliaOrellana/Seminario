﻿drop function fn_reporte_HC(integer,integer);

CREATE OR REPLACE FUNCTION public.fn_reporte_HC(
    IN pacienteDNI character varying,
    IN especialidad integer,
    OUT hc_id integer,
    OUT hc_fechaalta text, 
    OUT p_paciente integer,
    OUT per_nombre text,
    OUT per_documento text,
    OUT s_nombre text,
    OUT per_fechanacimiento text,
    OUT hc_procedencia text,
    OUT hc_numeroos text,
    OUT hc_titularos text, 
    OUT hc_ocupacion text,
    OUT hc_derivado text,
    OUT hc_madrepaciente text, 
    OUT hc_ocupacionmadre text, 
    OUT hc_padre text, 
    OUT hc_ocupacionpadre text,
    OUT per_nombreprofesional text,
    OUT dhc_fecha text,
    OUT dhc_hora text,
    OUT dhc_diagnostico text,
    OUT dhc_observaciones text
  )
  RETURNS SETOF record AS
$BODY$
 declare
 lr_registro1 record;
 lr_registro2 record;

 id text ;
 esp text;
 
 begin
    for lr_registro1 in (select hc.historiasclinicasid,
        to_char(hc.historiasclinicasfechaalta,'dd/mm/yyyy') as fechaalta, 
        p.pacientesid, per.personasnombrecompuesto,
        per.personasnrodocumento, s.sexonombre,
        to_char(per.personasfechanacimiento,'dd/mm/yyyy') as fechanac, 
        hc.historiasclinicasprocedenciapaciente,
        hc.historiasclinicasnumeroospaciente,
        hc.historiasclinicastitularospaciente, 
        hc.historiasclinicasocupacionpaciente,
        hc.historiasclinicasderivadopaciente,
        hc.historiasclinicasmadrepaciente, 
        hc.historiasclinicasocupacionmadrepaciente, 
        hc.historiasclinicaspadrepaciente, 
        hc.historiasclinicasocupacionpadrepaciente
  
        from personas per
        inner join sexo s using(sexoid)
        inner join pacientes p using(personasid)
        inner join historiasclinicas hc using(pacientesid)

        where per.personasnrodocumento = pacienteDNI) loop

          for lr_registro2 in (select dhc.historiasclinicasid, per.personasnombrecompuesto,
                to_char(dhc.detallehistoriasclinicasfecha,'dd/mm/yyyy') as detalle_fecha,
                to_char(dhc.detallehistoriasclinicashora,'HH24:MI') as detalle_hora,
                dhc.detallehistoriasclinicasdiagnostico,
                dhc.detallehistoriasclinicasobservaciones

                from detallehistoriasclinicas dhc
                inner join profesionales prof using(profesionalesid)
                inner join especialidadesxprofesionales exp using(especialidadesid)
                inner join especialidades e using(especialidadesid)
                inner join personas per using(personasid)
                inner join historiasclinicas hc using(historiasclinicasid)

                where hc.historiasclinicasid = lr_registro1.historiasclinicasid and e.especialidadesid = especialidad
            )loop
              hc_id := lr_registro1.historiasclinicasid ;
              hc_fechaalta := lr_registro1.fechaalta; 
              p_paciente := lr_registro1.pacientesid;
              per_nombre := lr_registro1.personasnombrecompuesto;
              per_documento := lr_registro1.personasnrodocumento;
              s_nombre := lr_registro1.sexonombre;
              per_fechanacimiento := lr_registro1.fechanac;
              hc_procedencia := lr_registro1.historiasclinicasprocedenciapaciente;
              hc_numeroos := lr_registro1.historiasclinicasnumeroospaciente;
              hc_titularos := lr_registro1.historiasclinicastitularospaciente; 
              hc_ocupacion := lr_registro1.historiasclinicasocupacionpaciente;
              hc_derivado := lr_registro1.historiasclinicasderivadopaciente;
              hc_madrepaciente := lr_registro1.historiasclinicasmadrepaciente; 
              hc_ocupacionmadre := lr_registro1.historiasclinicasocupacionmadrepaciente; 
              hc_padre := lr_registro1.historiasclinicaspadrepaciente; 
              hc_ocupacionpadre := lr_registro1.historiasclinicasocupacionpadrepaciente;
              per_nombreprofesional := lr_registro2.personasnombrecompuesto;
              dhc_fecha := lr_registro2.detalle_fecha;
              dhc_hora := lr_registro2.detalle_hora;
              dhc_diagnostico := lr_registro2.detallehistoriasclinicasdiagnostico;
              dhc_observaciones := lr_registro2.detallehistoriasclinicasobservaciones;

              return next;          
            end loop;
    end loop;
    return;
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_reporte_HC(character varying,integer)
  OWNER TO postgres;

  select * from fn_reporte_HC('35282057',3)