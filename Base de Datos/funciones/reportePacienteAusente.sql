CREATE OR REPLACE FUNCTION public.fn_reporteAusente(
    IN profid integer,
    IN fechaI date,
    IN fechaF date)
  RETURNS integer AS
$BODY$
declare

reg record;
cant_ausentes integer;

begin
	
		select count(*) into cant_ausentes
			    from mostrarturnos() 
			    where profesionalid = profid 
			    and ( to_date(fecha,'dd/mm/yyyy') between fechaI and fechaF ) 
			    and estadoturnoid = 9;
			  
			return cant_ausentes;

end;

$BODY$
language 'plpgsql'


 select fn_reporteAusente as cantidad from fn_reporteAusente(44,'2020-04-01','2020-05-01')