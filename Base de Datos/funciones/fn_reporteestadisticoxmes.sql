CREATE OR REPLACE FUNCTION public.fn_reporteestidisticoxmes(
    IN profid integer,
    IN mes integer,
    IN anio integer,
    OUT especialidad character varying,
    OUT atendidos integer,
    OUT cancelados integer,
    OUT ausente integer)
  RETURNS SETOF record AS
$BODY$
declare
rg_Esp  record;
cant_atendido integer;
cant_cancelado integer;
cant_ausentes integer;

begin
	for rg_Esp in (select especialidadesnombre, especialidadesid 
			from especialidades inner join especialidadesxprofesionales  using(especialidadesid) 
			where profesionalesid = profid )loop
	    especialidad := rg_Esp.especialidadesnombre;

	    select count(*) into cant_atendido from mostrarturnos() t
	    where date_part('month',to_date(t.fecha,'DD-MM-YYYY')) = mes and 
	          date_part('year',to_date(t.fecha,'DD-MM-YYYY')) = anio and
	          (t.estadoturnoid = 2 or t.estadoturnoid = 6 or t.estadoturnoid = 7) and 
	          t.especialidad = rg_Esp.especialidadesnombre and
	          t.profesionalid = profid;
	    select count(*) into cant_cancelado from mostrarturnos() t
	    where date_part('month',to_date(t.fecha,'DD-MM-YYYY')) = mes and 
	          date_part('year',to_date(t.fecha,'DD-MM-YYYY')) = anio and
	          (t.estadoturnoid = 3 or t.estadoturnoid = 8) and 
	          t.especialidad = rg_Esp.especialidadesnombre and
	          t.profesionalid = profid;
	    select count(*) into cant_ausentes from mostrarturnos() t 
	    where date_part('month',to_date(t.fecha,'DD-MM-YYYY')) = mes and 
	          date_part('year',to_date(t.fecha,'DD-MM-YYYY')) = anio and
	          t.estadoturnoid = 9 and 
	          t.especialidad = rg_Esp.especialidadesnombre and
	          t.profesionalid = profid;
	    atendidos := cant_atendido;
	    cancelados := cant_cancelado;
	    ausente := cant_ausentes;
	    return next;
	end loop;
	return;
	end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_reporteestidisticoxmes(integer, integer, integer)
  OWNER TO postgres;

