CREATE OR REPLACE FUNCTION public.fn_reporteturnosdiariosxespecialidad(
    IN profesionalid integer,
    IN especialidadid integer,
    OUT turnohora text,
    OUT pacientes text,
    OUT especialidades text,
    OUT tipoturno text)
  RETURNS SETOF record AS
$BODY$
declare
rg_Esp  record;
rg_TurnosNormales record;
rg_SobreTurnos record;
pac text;
TT text;
esp character varying;

begin
	for rg_Esp in (select especialidadesnombre from especialidades where especialidadesid = especialidadid)loop
	    esp = rg_Esp.especialidadesnombre;
	end loop;
	for rg_TurnosNormales in (select * from fn_agendaturnonormales(profesionalid,to_char(now(),'DD/MM/YYYY'),especialidadid))loop
	    pac := '';
	    TT := '';
	    for rg_SobreTurnos in (select * from fn_agendasobreturnos(profesionalid,to_char(now(),'DD/MM/YYYY'),especialidadid) where tb_turnohora = rg_TurnosNormales.tb_turnohora)loop
		IF ((rg_TurnosNormales.tb_paciente <> '') and (rg_SobreTurnos.tb_paciente <> '') ) THEN
			pac := pac || rg_TurnosNormales.tb_paciente || ' / ' || rg_SobreTurnos.tb_paciente;
			TT := TT || rg_TurnosNormales.tb_tipoturnos || ' / ' || rg_SobreTurnos.tb_tipoturnos;
		ELSIF (rg_TurnosNormales.tb_paciente <> '') THEN
			pac := pac || rg_TurnosNormales.tb_paciente;
			TT := TT || rg_TurnosNormales.tb_tipoturnos;
		ElSIF (rg_SobreTurnos.tb_paciente <> '') THEN
			pac := pac || rg_SobreTurnos.tb_paciente;
			TT := TT || rg_SobreTurnos.tb_tipoturnos;
		END IF;
			
	    end loop;
	    turnohora :=  rg_TurnosNormales.tb_turnohora;
	    pacientes := pac;
	    especialidades := esp;
	    tipoturno := TT;
	    return next;	
	end loop;
	return;
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_reporteturnosdiariosxespecialidad(integer, integer)
  OWNER TO postgres;