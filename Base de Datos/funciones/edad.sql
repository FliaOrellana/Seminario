create or replace function fn_Edad(fecha date )
returns int
as 
$$
declare
 edad int :=0;
begin
	edad:=date_part('year',now()) - date_part('year',fecha);
	
	if (date_part('month',now())<  date_part('month',fecha))then
		edad:=edad -1;
	elsif (date_part('month',now())=  date_part('month',fecha))then
		if(date_part('day',now())<  date_part('day',fecha))then
			edad:=edad-1;
		end if;
	end if;
	return edad;
end;
$$
language 'plpgsql'