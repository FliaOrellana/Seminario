create or replace function fn_actualiza_estado_turnos()
returns integer
as
$body$

declare
cont integer := 0;
reg1 record;
reg2 record;
begin
	for reg1 in (select * from mostrarturnos()
		    where to_date(fecha,'dd/mm/yyyy') < to_date(to_char(now()::timestamp with time zone, 'dd/MM/yyyy'::text),'dd/MM/yyyy')
		    and estadoturnoid = 1)loop

		    for reg2 in (select UNNEST(string_to_array(reg1.turnosid,'-'))::integer as id) loop
			
			update turnos set estadosturnosid = 9 where turnosid = reg2.id;
			cont = cont + 1;

		    end loop;
		
	end loop;
	return cont;
	
end;

$body$
language 'plpgsql';