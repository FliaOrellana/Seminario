CREATE OR REPLACE FUNCTION public.fn_feriados()
  RETURNS TABLE(id integer, nombreferiado character varying, fechaferiado text) AS
$BODY$
begin 
	return query (select feriadosid, feriadosdescripcion ,  to_char(feriadosfecha,'DD/MM/YYYY')
		      from feriados order by 1 );
end;
$BODY$
  LANGUAGE 'plpgsql'; 

