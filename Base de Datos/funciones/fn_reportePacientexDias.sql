﻿create or replace function fn_reportePacientexDias(
	IN profid integer,
	IN mes integer,
	IN anio integer,
	IN cantdias integer,
	OUT dia integer,
	OUT cantidad integer)
RETURNS setof record as
$body$

declare

rg1 record;
rg2 record;
c integer;


begin
    for i in 1 .. cantdias loop
	select count(*) into c from mostrarturnos() t 
	where t.profesionalid = profid and 
	date_part('month',to_date(t.fecha,'DD-MM-YYYY')) = mes and 
	date_part('year',to_date(t.fecha,'DD-MM-YYYY')) = anio and
	date_part('day',to_date(t.fecha,'DD-MM-YYYY')) = i;

	dia := i;
	cantidad := c;
	return next;     
    end loop;
    return;
end;
$body$
language 'plpgsql'



 