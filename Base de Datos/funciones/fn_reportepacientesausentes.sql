﻿CREATE OR REPLACE FUNCTION public.fn_reportepacientesausentes(
    IN profid integer,
    IN fechai date,
    IN fechaf date,
    OUT t_profesional character varying,
    OUT t_especialidad character varying,
    OUT t_fecha text,
    OUT t_hinicio text,
    OUT t_hfin text,
    OUT t_paciente character varying,
    OUT t_estadoturno character varying)
  RETURNS SETOF record AS
$BODY$
declare

reg record;
cant_ausentes integer;

begin
	
		for reg in (select *  from mostrarturnos() 
			    where profesionalid = profid 
			    and ( to_date(fecha,'dd/mm/yyyy') between fechaI and fechaF ) 
			    and estadoturnoid = 9) loop
			  
			t_profesional := reg.profesional;
			t_especialidad := reg.especialidad;
			t_fecha := reg.fecha;
			t_hinicio := reg.horainicio;
			t_hfin := reg.horafin;
			t_paciente := reg.pacientes;
			t_estadoturno := reg.estadoturno;
			return next;
		end loop;
		return;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.fn_reportepacientesausentes(integer, date, date)
  OWNER TO postgres;
