CREATE OR REPLACE FUNCTION public.mostrarturnos(
    OUT turnosid text,
    OUT profesionalid integer,
    OUT profesional character varying,
    OUT especialidad character varying,
    OUT fecha text,
    OUT horainicio text,
    OUT horafin text,
    OUT tipoturno character varying,
    OUT pacientes character varying,
    OUT email character varying,
    OUT estadoturnoid integer,
    OUT estadoturno character varying)
  RETURNS SETOF record AS
$BODY$
 declare
 lr_registro1 record;
 lr_registro2 record;
 b int;
 id text ;
 inicio text;
 sumador time;
 begin
	 for lr_registro1 in (SELECT p.personasnombrecompuesto AS profesional,
				    profesionales.profesionalesid as idProf,
				    especialidades.especialidadesnombre AS especialidad,
				    to_char(turnos.turnosfecha::timestamp with time zone, 'dd/MM/yyyy'::text) AS turnosfecha,
				    tipoturnos.tipoturnosnombre,
				    t.personasnombrecompuesto AS pacientes,
				    t.personasemail AS email,
				    turnos.estadosturnosid as estadosturnosid,
				    et.estadosturnosnombre as estado
			      FROM profesionales
			     JOIN personas p USING (personasid)
			     JOIN turnos USING (profesionalesid)
			    
			     JOIN especialidades USING (especialidadesid)
			    
			     JOIN tipoturnos USING (tipoturnosid)
			     JOIN pacientes pa USING (pacientesid)
			     JOIN estadosturnos et using(estadosturnosid)
			     JOIN ( SELECT personas.personasid,
				    personas.personasnombrecompuesto,
				    personas.personasemail
				   FROM personas) t ON pa.personasid = t.personasid
		             
			     group by 1,2,3,4,5,6,7,8,9
	 
				)LOOP
		b:=0;
		id:='';
		for lr_registro2 in (SELECT tu.turnosid ::text as turnosid,to_char(tu.turnoshoratiempo::interval, 'HH24:MI'::text) AS turnoshora
					   FROM profesionales
					     JOIN personas p USING (personasid)
					     JOIN turnos tu USING (profesionalesid)
					     
					     JOIN especialidades e USING (especialidadesid)
					     
					     JOIN tipoturnos tt USING (tipoturnosid)
					     JOIN pacientes pa USING (pacientesid)
					     JOIN ( SELECT personas.personasid,
						    personas.personasnombrecompuesto
						   FROM personas) t ON pa.personasid = t.personasid
					     where p.personasnombrecompuesto=lr_registro1.profesional and 
						   e.especialidadesnombre = lr_registro1.especialidad and 
						   to_char(tu.turnosfecha::timestamp with time zone, 'dd/MM/yyyy'::text)=lr_registro1.turnosfecha and
						    
						   tt.tipoturnosnombre=lr_registro1.tipoturnosnombre and
						   t.personasnombrecompuesto = lr_registro1.pacientes) LOOP

			if (b=0) then
				inicio := lr_registro2.turnoshora;
				b:=1;
				sumador = to_timestamp(inicio, 'HH24:MI');
			end if;
			id:= id ||lr_registro2.turnosid||'-';
			sumador = sumador + '00:10:00';
		end LOOP;
		 Turnosid := substring(id from 1 for char_length(id)-1);
		 Profesionalid := lr_registro1.idProf;
		 Profesional:=lr_registro1.profesional;
		 Especialidad :=lr_registro1.especialidad;
		 Fecha :=lr_registro1.turnosfecha;
		 HoraInicio :=inicio;
		 HoraFin := to_char(sumador, 'HH24:MI');
		 TipoTurno :=lr_registro1.tipoturnosnombre;
		 Pacientes:=lr_registro1.pacientes;
		 email:= lr_registro1.email;
		 estadoturnoid:=lr_registro1.estadosturnosid;
		 estadoturno:=lr_registro1.estado;
		 return next;				   
	end LOOP;
	return;
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.mostrarturnos()
  OWNER TO postgres;



  /* Agenda Turnos*/

  CREATE OR REPLACE FUNCTION public.mostrarturnosxProfesional(
    IN IDprof integer,
    OUT turnosid text,
    OUT profesional character varying,
    OUT especialidad character varying,
    OUT fecha text,
    OUT horainicio text,
    OUT horafin text,
    OUT tipoturno character varying,
    OUT pacientes character varying,
    OUT estadoturnoid integer,
    OUT estadoturno character varying)
  RETURNS SETOF record AS
$BODY$
 declare
 lr_registro1 record;
 lr_registro2 record;
 b int;
 id text ;
 inicio text;
 sumador time;
 begin
	 for lr_registro1 in (SELECT p.personasnombrecompuesto AS profesional,
				    especialidades.especialidadesnombre AS especialidad,
				    to_char(turnos.turnosfecha::timestamp with time zone, 'dd/MM/yyyy'::text) AS turnosfecha,
				    tipoturnos.tipoturnosnombre,
				    t.personasnombrecompuesto AS pacientes,
				    turnos.estadosturnosid as estadosturnosid,
				    et.estadosturnosnombre as estado
			      FROM profesionales pf
			     JOIN personas p USING (personasid)
			     JOIN turnos USING (profesionalesid)
			    
			     JOIN especialidades USING (especialidadesid)
			    
			     JOIN tipoturnos USING (tipoturnosid)
			     JOIN pacientes pa USING (pacientesid)
			     JOIN estadosturnos et using(estadosturnosid)
			     JOIN ( SELECT personas.personasid,
				    personas.personasnombrecompuesto
				   FROM personas) t ON pa.personasid = t.personasid
			     where pf.profesionalesid = IDprof
		             
			     group by 1,2,3,4,5,6,7
	 
				)LOOP
		b:=0;
		id:='';
		for lr_registro2 in (SELECT tu.turnosid ::text as turnosid,to_char(tu.turnoshoratiempo::interval, 'HH24:MI'::text) AS turnoshora
					   FROM profesionales
					     JOIN personas p USING (personasid)
					     JOIN turnos tu USING (profesionalesid)
					     
					     JOIN especialidades e USING (especialidadesid)
					     
					     JOIN tipoturnos tt USING (tipoturnosid)
					     JOIN pacientes pa USING (pacientesid)
					     JOIN ( SELECT personas.personasid,
						    personas.personasnombrecompuesto
						   FROM personas) t ON pa.personasid = t.personasid
					     where p.personasnombrecompuesto=lr_registro1.profesional and 
						   e.especialidadesnombre = lr_registro1.especialidad and 
						   to_char(tu.turnosfecha::timestamp with time zone, 'dd/MM/yyyy'::text)=lr_registro1.turnosfecha and
						    
						   tt.tipoturnosnombre=lr_registro1.tipoturnosnombre and
						   t.personasnombrecompuesto = lr_registro1.pacientes) LOOP

			if (b=0) then
				inicio := lr_registro2.turnoshora;
				b:=1;
				sumador = to_timestamp(inicio, 'HH24:MI');
			end if;
			id:= id ||lr_registro2.turnosid||'-';
			sumador = sumador + '00:10:00';
		end LOOP;
		 Turnosid := substring(id from 1 for char_length(id)-1);
		 Profesional:=lr_registro1.profesional;
		 Especialidad :=lr_registro1.especialidad;
		 Fecha :=lr_registro1.turnosfecha;
		 HoraInicio :=inicio;
		 HoraFin := to_char(sumador, 'HH24:MI');
		 TipoTurno :=lr_registro1.tipoturnosnombre;
		 Pacientes:=lr_registro1.pacientes;
		 estadoturnoid:=lr_registro1.estadosturnosid;
		 estadoturno:=lr_registro1.estado;
		 return next;				   
	end LOOP;
	return;
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.mostrarturnosxProfesional( integer)
  OWNER TO postgres;
