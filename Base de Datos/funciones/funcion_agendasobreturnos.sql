﻿CREATE OR REPLACE FUNCTION public.fn_agendasobreturnos(
    IN idprof integer,
    IN fecha character varying,
    IN idesp integer)
  RETURNS TABLE(tb_turnohora text, tb_paciente character varying, tb_tipoturnos character varying) AS
$BODY$

begin
	if  idesp=null or idprof=null then
		raise notice 'nada' ;
	elsif idesp is not null then
		begin
		return query select to_char(th.turnoshoratiempo,'HH24:MI'), t.personasnombrecompuesto, t.tipoturnosnombre from turnoshora th  left join
				(select * from profesionales 
				inner join turnos using (profesionalesid )
				inner join especialidades using (especialidadesid) 
				 
				inner join tipoturnos using (tipoturnosid)
				inner join pacientes using (pacientesid)
				inner join estadosturnos using(estadosturnosid)  
				join personas on pacientes.personasid=personas.personasid
				where tipoturnosnombre='SOBRETURNO' 
				AND especialidadesid = IdEsp 
				AND profesionalesid = IdProf 
				AND to_char(turnosfecha,'dd/mm/yyyy') = fecha) t on th.turnoshoratiempo=t.turnoshoratiempo
				AND (estadosturnosid <> 3 or estadosturnosid <>8)
				order by 1;
		end;
	end if;
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_agendasobreturnos(integer, character varying, integer)
  OWNER TO postgres;