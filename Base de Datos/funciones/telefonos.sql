create or replace function fn_Telefonos(id int)
returns text
as
$$
declare 
tel text :='';
cont int :=0;
reg1 record;
begin
	for reg1 in (select tt.tipotelefonosnombre||':'||t.telefononumero as tele
			from personas p 
			inner join pacientes pa using (personasid)
			inner join telefonos t using(personasid)
			inner join tipotelefonos tt using(tipotelefonosid)
			where pa.pacientesid = id) loop
		tel:=tel||reg1.tele||' / ';
		cont := cont +1;
	end loop;
	if(cont >=1) then
		tel := substring(tel from 1 for char_length(tel)-2);
	end if;
	return tel;
end;
$$
language 'plpgsql';