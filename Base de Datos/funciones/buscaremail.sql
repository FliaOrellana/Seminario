create or replace function fn_buscaremail( 
	IN email varchar,
	OUT correo varchar,
	OUT usuario varchar)
RETURNS SETOF record 	
as
$$
declare
reg  record;

begin

	if ((email <> null) or trim(email) <>'') then
		for reg in(select personasemail, usuariosnombre from personas
			     inner join profesionales using(personasid)
			     inner join usuarios using (usuariosid)
			     where  personasemail=email and personasemail<>''
		union 

		select personasemail ,usuariosnombre from personas
				     inner join secretarios using(personasid)
				     inner join usuarios using (usuariosid)
				     where personasemail=email and personasemail<>'')loop
				     correo:= reg.personasemail;
				     usuario:= reg.usuariosnombre;
				     return next;
		end loop;

	else
		correo:=null;
		usuario:=null;
		return next;
	end if;
	
end;
$$
language 'plpgsql';