﻿create table Auditoria(
	AuditoriasId Serial primary key,
	NombreUsuario character varying,
	FechaTransaccion date,
	HoraTransaccion time,
	Transaccion text,
	DatosIngresados text,
	DatosBorrados text,
	DatosModificados text
)

drop table auditoria
delete from auditoria

 CREATE OR REPLACE FUNCTION fn_auditoria()
  RETURNS trigger AS
$BODY$

BEGIN
	NEW.FechaTransaccion:=now();
	NEW.HoraTransaccion := now();
	return NEW;			
	
END; 
$BODY$
  LANGUAGE 'plpgsql'


 CREATE TRIGGER tr_auditoria BEFORE INSERT  
    ON auditoria FOR EACH ROW 
    EXECUTE PROCEDURE fn_auditoria();