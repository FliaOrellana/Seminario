create or replace function fn_insertaAgendaDias()
returns integer
as
$$
declare 
	cont integer :=0;
	reg_1 record;
	reg_2 record;
	reg_3 record;

begin
	for reg_1 in (select profesionalesid from profesionales) loop
		if(reg_1.profesionalesid <> 13) then
			for reg_2 in (select especialidadesid from especialidadesxprofesionales where estadosid=1 and profesionalesid=reg_1.profesionalesid) loop
				for reg_3 in (select diasid from dias) loop
					if(reg_3.diasid <> 0) then
						insert into agendas (profesionalesid,especialidadesid,diasid,agendastmini,agendastmfin,agendasttini,agendasttfin)
						values(reg_1.profesionalesid,reg_2.especialidadesid,reg_3.diasid,'08:00:00','13:00:00','13:00:00','21:00:00');
						cont := cont +1;
					end if;
				end loop;
			end loop;
		end if;
	end loop;
	return cont;
end;

$$
language 'plpgsql';