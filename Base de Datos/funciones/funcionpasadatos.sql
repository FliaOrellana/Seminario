
 create or replace function fn_pasaDatosTelefono()
 returns integer
 as 
 $$

 declare 

 cont int :=0;
 lista record;
 begin 
	for lista in (Select * from personas) loop
	    insert into Telefonos (personasid,tipotelefonosid,telefononumero)values (lista.personasid,lista.tipotelefonosid,lista.personastelefono);
	    cont := cont +1;
	end loop;
	return cont;	

 end;
 $$
 language 'plpgsql'

 select * from fn_pasaDatosTelefono()

