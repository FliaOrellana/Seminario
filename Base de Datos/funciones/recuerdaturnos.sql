create or replace function fn_recuerdaturnos()
returns table( email varchar,
	       nombre varchar,
	       dias double precision,
	       profesional text,
	       fecha date )
as
$$
begin
	return query (select * from (select per.personasemail, per.personasnombrecompuesto , date_part('days',turnosfecha-now()) as dias, 'Dr. ' || prf.personasnombrecompuesto as profe, t.turnosfecha date
					from personas per inner join pacientes using (personasid) inner join turnos t using (pacientesid)
					inner join profesionales p using (profesionalesid)
					join (SELECT personas.personasid,personas.personasnombrecompuesto FROM personas) prf on p.personasid =prf.personasid 
					where personasemail<>'' and personasemail<>'' and t.estadosturnosid=1
					group by 1,2,turnosfecha,prf.personasnombrecompuesto) rt
			where rt.dias =0);
end;
$$
language 'plpgsql';