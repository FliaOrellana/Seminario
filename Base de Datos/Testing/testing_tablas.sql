﻿--drop table modulos
create table modulos(
modulosid integer primary key,
modulonombre varchar,
modulodescripcion varchar
)

create table casospruebas(
casospruebasid serial primary key ,
casospruebasnombre varchar,
casospruebadescripcion varchar,
modulosid integer,
casospruebascantidadcorridas integer,
casospruebasexitos integer,
foreign key (modulosid) references modulos(modulosid)
);

create table casospruebasejecutados(
casospruebasejecutadosid serial primary key,
casosopruebasejecutadosnombre varchar,
casospruebasid integer,
casospreubasejecutadosdescripcion varchar,
casospruebasejecutadosfechaejecucon date,
casospruebasejecutadoshoraejecucion timestamp,
casospruebasejecutadosvaloringresado varchar,
casospruebasejecutadosvaloresperado varchar,
casospruebasejecutadosvalorobtenido varchar,
casospruebasejecutadosresutado varchar
);