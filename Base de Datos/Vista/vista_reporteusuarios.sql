﻿CREATE OR REPLACE VIEW public.vista_reporteusuarios AS 
 SELECT secretarios.usuariosid,
    usuarios.usuariosnombre,
    personas.personasnombrecompuesto,
    perfiles.perfilesnombre,
    usuarios.estadosid
   FROM secretarios
     JOIN personas USING (personasid)
     JOIN usuarios USING (usuariosid)
     JOIN perfilesusuarios USING (usuariosid)
     JOIN perfiles USING (perfilesid)
  
UNION
 SELECT profesionales.usuariosid,
    usuarios.usuariosnombre,
    personas.personasnombrecompuesto,
    perfiles.perfilesnombre,
    usuarios.estadosid
   FROM profesionales
     JOIN personas USING (personasid)
     JOIN usuarios USING (usuariosid)
     JOIN perfilesusuarios USING (usuariosid)
     JOIN perfiles USING (perfilesid);
  

ALTER TABLE public.vista_reporteusuarios
  OWNER TO postgres;
select * from vista_reporteusuarios