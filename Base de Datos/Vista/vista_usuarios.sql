﻿Create View Vista_ListaUsuarios as
select usuariosid,usuariosnombre,personasnombrecompuesto,perfilesnombre from secretarios 
inner join personas using(personasid) 
inner join usuarios using(usuariosid)
inner join perfilesusuarios using(usuariosid)
inner join perfiles using(perfilesid)
union
select usuariosid,usuariosnombre,personasnombrecompuesto,perfilesnombre from profesionales 
inner join personas using (personasid)
inner join usuarios using(usuariosid)
inner join perfilesusuarios using(usuariosid)
inner join perfiles using(perfilesid);