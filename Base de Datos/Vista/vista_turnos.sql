﻿create view View_Turnos as
select p.personasnombrecompuesto as profesional,especialidadesnombre as especialidad,to_char(turnosfecha,'dd/MM/yyyy')as Turnosfecha,turnoshora,tipoturnosnombre,t.personasnombrecompuesto as pacientes  from
profesionales  inner join personas p  using(personasid)
inner join EspecialidadesXProfesionales using (profesionalesid)
inner join Especialidades using (especialidadesid)
inner join Turnos using (especialidadesxprofesionalesid)
inner join Tipoturnos using(Tipoturnosid)
inner join pacientes pa using(pacientesid)
join (select personasid,personasnombrecompuesto from personas) t on pa.personasid=t.personasid;


select * from view_turnos