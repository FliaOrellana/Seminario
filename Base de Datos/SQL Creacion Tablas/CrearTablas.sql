﻿create table Perfiles(
	PerfilesId serial primary key,
	PerfilesNombre  varchar(30) not null,
	PerfilesFechaIngreso  date not null,
	PerfilFechaMod date,
	PerfilFechaBaja date
);



insert into Perfiles (PerfilesNombre,PerfilesFechaIngreso) values('Administrador',now());
insert into Perfiles (PerfilesNombre,PerfilesFechaIngreso) values('Profesional',now());
insert into Perfiles (PerfilesNombre,PerfilesFechaIngreso) values('Secretario',now());

create table PerfilesUsuarios(
	PerfilesId integer,
	UsuariosId integer,
	primary key(PerfilesId,UsuariosId),
	foreign key (PerfilesId) references Perfiles(PerfilesId),
	foreign key (UsuariosId) references Usuarios(UsuariosId)
	);


select * from Perfiles
select * from Usuarios where usuariosnombre='MPALOMO' and usuarioscontrasenia=md5('2222')
insert into PerfilesUsuarios values (2,5);

select * from PerfilesUsuarios
delete from PerfilesUsuarios where usuariosid=4 or usuariosid=5

create table Usuarios(
	UsuariosId serial primary key,
	UsuariosNombre  varchar(30) not null,
	UsuariosContrasenia  text not null,
	UsuariosFechaIngreso  date not null,
	UsuariosFechaMod  date,
	UsuariosFechaBaja  date,
	ProfesionalesId integer,
	SecretariosId integer
	);

alter table usuarios drop constraint usuarios_secretariosid_fkey;
alter table usuarios drop column secretariosid
alter table usuarios drop constraint usuarios_profesionalesid_fkey;
alter table usuarios drop column profesionalesid
alter table usuarios add constraint usuarios_profesionalesid_fkey foreign key (profesionalesid) references Profesionales (profesionalesid)
alter table usuarios add constraint usuarios_secretariosid_fkey foreign key (secretariosid) references Secretarios (secretariosid)
insert into Usuarios (UsuariosNombre,UsuariosContrasenia,UsuariosFechaIngreso) values ('ADMIN',md5('1234'),now());
insert into Usuarios (UsuariosNombre,UsuariosContrasenia) values ('mpalomo','2222');




delete from Usuarios where usuariosid=4 or usuariosid=5  
select * from usuarios where usuariosnombre='MPALOMO' and usuarioscontrasenia= md5('2222')

select * from usuarios where usuarioscontrasenia=md5('2222')
select * from perfilesusuarios

insert into perfilesusuarios values(1,6)

select md5('2222')
select * from usuarios

create table ObraSocial(
	ObraSocialId serial primary key,
	ObraSocialNombre  varchar(30) not null
	);



create table PacientesEstado(
	PacientesEstadoId serial primary key,
	PacientesEstadoNombre  varchar(15)not null
	);


create table Pacientes(
	PacientesId Serial primary key,
	PersonasId integer,
	PacientesEstadoId integer,
	PacientesFechaRegistro date not null ,
	PacientesFechaMod date,
	foreign key (PersonasId) references Personas(PersonasId),
	foreign key (PacientesEstadoId) references PacientesEstado(PacientesEstadoId)
	 );



CREATE TABLE ObraSocialPacientes(
	PacientesId  integer not null,
	ObraSocialId  integer not null,
	primary key(PacientesId,ObraSocialId),
	foreign key (PacientesId) references Pacientes(PacientesId),
	foreign key (ObrasocialId) references ObraSocial(ObrasocialId)
	);



CREATE TABLE Sexo
(
  Sexoid serial  primary key,
  Sexonombre  varchar(15) not null,
  Sexofecharegistro  date not null,
  Sexofechamod date,
  Sexousariomod integer
  
);

insert into Sexo (Sexonombre,sexofecharegistro) values ('Femenino',now()),('Masculino',now());

CREATE TABLE Personas(
	PersonasId serial primary key,
	PersonasNombre varchar(30) not null,
	PersonasApellido varchar(30) not null,
	PersonasNombreCompuesto varchar(100),
	PersonasNroDocumento varchar(10)not null,
	PersonasDomicilio varchar(30),
	PersonasEmail varchar(30)not null,
	PersonasTelefono varchar(30)not null,
	PersonasFechaRegistro  date not null, 
	PersonasFechaMod date,
	PersonaFechaBaja date,
	Sexoid integer not null,
	foreign key (Sexoid) references Sexo(Sexoid)
	);

	select * from sexo
insert into Personas (PersonasNombre,PersonasApellido,PersonasNroDocumento,PersonasDomicilio,PersonasEmail,PersonasTelefono,SexoId)
Values  ('Andrea','Gonzales','22222222','lapachos 23','gonzales@gmail.com','421421',1),
	('Marcelo','Lopez','33333333','flores 55','lopez@gmail.com','421422',2),
	('Natalia','Díaz','35222111','naranjos 66','diaz@gmail.com','421423',1),
	('Walter','Ramirez','27444666','acacias 40','ramirez@gmail.com','421424',2),
	('Cristina','Perez','29555777','floresta 67','perez@gmail.com','421425',1),
	('Rene','Torrez','33222111','san martin 45','torrez@gmail.com','421426',2),
	('Gabriela','Burgos','34999888','pueyrredon 79','burgos@gmail.com','421427',1),
	('Martin','Rojas','28666555','bicentenario 34','rojas@gmail.com','421428',2),
	('Fernanda','Flores','35666999','lavalle 67','flores@gmail.com','421429',1),
	('Ramiro','Gutierrez','29666555','vicente lopez 43','gutierrez@gmail.com','421430',2),
	('Adriana','Sanchez','26555333','alsina 46','sanchez@gmail.com','421431',1),
	('Gustavo','Martinez','30999111','necochea 78','martinez@gmail.com','421432',2),
	('Noelia','Sandoval','34666777','santiago 56','sandoval@gmail.com','42143',1),
	('Fabian','Mendoza','32444555','guemes 25','mendoza@gmail.com','421433',2),
	('Claudia','Leon','28555444','belgrano 35','leon@gmail.com','421434',1),
	('Jorge','Romero','27666999','urquiza 56','romero@gmail.com','421435',2),
	('Veronica','Velazco','21333222','avarado 45','velazco@gmail.com','421436',1),
	('Andres','Ruiz','29444222','leguizamon 67','ruiz@gmail.com','421437',2),
	('Araceli','Marzochetti','26333222','rivadavia 78','marzochetti@gmail.com','421438',1),
	('Nicolas','Guzman','21111333','alvear 78','guzman@gmail.com','421439',2),
	('Maria','Unco','25333666','olivos 69','unco@gmail.com','421440',1),
	('Cesar','Choque','26555000','bolivar 90','choque@gmail.com','421441',2),
	('Vanesa','Caminos','20553888','bicentenario 30','caminos@gmail.com','421442',1),
	('Angel','Castillo','20333111','la rioja 98','castillo@gmail.com','421443',2),
	('Victoria','Fernandez','32433233','san luis 99','fernandez@gmail.com','421444',1),
	('Hugo','Areco','30211455','tucuman 23','areco@gmail.com','421445',2),
	('Antonio','Segovia','32566877','necochea 21','segovia@gmail.com','421446',2) returning *;
	

	
Create Table Secretarios(
	SecretariosId serial not null primary key,
	PersonasId integer not null,
	SecretariosFechaRegistro date not null,
	SecretariosFechaMod date,
	SecretariosFechaBaja date,
	UsuariosId integer,
	foreign key (PersonasId) references Personas(PersonasId),
	foreign key (UsuariosId) references Usuarios(usuariosid)
	);

	alter table Secretarios drop constraint secretarios_usuariosid_fkey;
	alter table Secretarios drop column usuariosid
	select * from Secretarios
	alter table secretarios add column usuariosid integer
alter table secretarios add constraint secretarios_usuariosid_fkey foreign key (usuariosid) references usuarios (usuariosid)

	select * from personas
	select * from usuarios
	select * from Secretarios
	--Para el usuario secretario su id es 6
	--Para el usuario profesionale su id es 7
	update profesionales set usuariosid=7
	select * from Profesionales
	insert into Secretarios (personasid,usuariosid) values(16,6)
	insert into Profesionales (personasid,usuariosid) values(17,7)

select * from secretarios
	delete from Secretarios 
	
Create Table Profesionales(
	ProfesionalesId serial not null primary key,
	PersonasId integer not null,
	IntervalosId integer,
	ProfesionalesMatricula varchar,
	ProfesionalesFechaRegistro date not null,
	ProfesionalesFechaMod date,
	ProfesionalesBaja date,
	UsuariosId integer,
	foreign key (PersonasId) references Personas(PersonasId),
	foreign key (UsuariosId) references Usuarios(UsuariosId),
	foreign key (IntervalosId) references Intervalos(IntervalosId)
);


alter table usuarios add column estadosid integer;
alter table usuarios add constraint usuarios_estadosid_fkey foreign key (estadosid) references estados (estadosid)

alter table profesionales add column IntervalosId integer;
alter table Profesionales add constraint profesionales_intervalosid_fkey foreign key (intervalosid) references intervalos (intervalosid)
select * from profesionales

CREATE table Especialidades(
	Especialidadesid serial primary key,
	EspecialidadesNombre varchar(30),
	EspecialidadesFechaRegistro date,
	EspecialidadesFechaMod date,
	EspecialidadesBaja date
);

insert into Especialidades (EspecialidadesNombre) values ('Ortodoncia'),('Odontología Niños'),('Odontología Adultos'),
							 ('Odontología General'),('Pediatría'),('Fisioterapia'),('Kiniesioterapia'),
							 ('Psicología'),('Fonoaudiología');

select * from Especialidades;

Create table EspecialidadesxProfesionales(
	EspecialidadesxProfesionalesId serial primary key,
	ProfesionalesId integer,
	EspecialidadesId integer,
	foreign key (ProfesionalesId) references Profesionales(Profesionalesid),
	foreign key (EspecialidadesId) references Especialidades(EspecialidadesId)
);

create table TipoTurnos(
TipoTurnosId serial primary key,
TipoTurnosnombre varchar(30));

create table Turnos(
	TurnosId serial primary key,
	TipoTurnosId integer not null,
	PersonasId integer not null
	EspecialidadesxProfesionalesId integer not null,
	TurnosFechaRegistro timestamp not null,
	TurnosFecha date not null,
	TurnosHora varchar(10) not null,
	TurnosFechaMod date,
	TurnosBaja date,
	foreign key (TipoTurnosId) references TipoTurnos(TipoTurnosId),
	foreign key (EspecialidadesxProfesionalesId) references EspecialidadesxProfesionales(EspecialidadesxProfesionalesId));

select now() 
alter table Turnos alter column turnoshora type time using turnoshora::time;
alter table Turnos add column Pacientesid integer not null;
alter table Turnos drop constraint Turnos_personasid_fkey;
alter table Turnos drop column personasid;
alter table Turnos add constraint Turnos_pacientesid_fkey foreign key (Pacientesid) references Pacientes (Pacientesid)
select * from turnos

alter table Turnos add constraint Turnos_Turnoshoratiempo_fkey foreign key (turnoshoratiempo) references turnoshora(turnoshoratiempo)

create table Intervalos(
	IntervalosId serial primary key,
	IntervalosTiempo time not null
);
insert into Intervalos (intervalostiempo)values('00:10'),('00:15'),('00:20'),('00:30') returning*

select to_date(  turnoshora,'HH24:MI') from turnos

drop table turnoshora
create table TurnosHora(
	
	TurnosHoraTiempo time primary key);

insert into turnoshora values('08:00:00'),('21:00:00')
/*
	alter table profesionales add column usuariosid integer
alter table profesionales add constraint profesionales_usuariosid_fkey foreign key (usuariosid) references usuarios (usuariosid)


alter table Profesionales drop constraint profesionales_usuariosid_fkey;
	alter table profesionales drop column usuariosid

alter table Profesionales add ProfesionalesMatricula  varchar(30);

drop table Personas cascade

drop table pacientes cascade

delete from sexo

delete from personas;

delete from pacientes;

delete from pacientesestado cascade

drop table sexo cascade

drop table pacientesestado cascade*/

/*drop table sexo;

drop table PacientesEstado;

insert into TipoUsuarios (TipoUsuariosNombre) values ('Administrador')

insert into Usuarios (UsuariosNombre,Usuarioscontrasenia) values ('Lola','Mento');
*/

alter table turnos rename column turnoshora to turnoshoratiempo;

create table HistoriasClinicas(
	HistoriasClinicasId serial primary key,
	PacientesId integer,
	HistoriasClinicasFechaAlta date not null,
	foreign key (PacientesId) references Pacientes(PacientesId)
);

create table DetalleHistoriasClinicas(
	DetalleHistoriasClinicasId serial primary key,
	HistoriasClinicasId integer,
	EspecialidadesxProfesionalesId integer,
	DetalleHistoriasClinicasFecha date,
	DetalleHistoriasClinicasHora time,
	DetalleHistoriasClinicasDiagnostico varchar,
	DetalleHistoriasClinicasObservaciones varchar,
	foreign key (HistoriasClinicasId) references HistoriasClinicas(HistoriasClinicasId),
	foreign key (EspecialidadesxProfesionalesId) references EspecialidadesxProfesionales(EspecialidadesxProfesionalesId)
	
);


create table Telefonos(
 TelefonosId serial primary key,
 PersonasId integer,
 TipoTelefonosId integer,
 telefonoNumero varchar(30),
 foreign key (PersonasId) references Personas(PersonasId),
 foreign key (TipoTelefonosId) references TipoTelefonos(TipoTelefonosId)
 );


 alter table historiasclinicas add column HistoriasClinicasProcedenciaPaciente varchar(60);
 alter table historiasclinicas add column HistoriasClinicasNumeroOSPaciente varchar(15);
 alter table historiasclinicas add column HistoriasClinicasTitularOSPaciente varchar(50);
 alter table historiasclinicas add column HistoriasClinicasOcupacionPaciente varchar(15);
 alter table historiasclinicas add column HistoriasClinicasDerivadoPaciente varchar(40);
 alter table historiasclinicas add column HistoriasClinicasMadrePaciente varchar(60);
 alter table historiasclinicas add column HistoriasClinicasOcupacionmadrePaciente varchar(30);
 alter table historiasclinicas add column HistoriasClinicasPadrePaciente varchar(60);
 alter table historiasclinicas add column HistoriasClinicasOcupacionpadrePaciente varchar(30);

 create table estadosturnos(
	estadosturnosid serial primary key,
	estadosturnosnombre varchar(30) not null
	);


insert into estadosturnos (estadosturnosnombre)values('PENDIENTE'),('ATENDIDO'),('CANCELADO') 

alter table turnos add column estadosturnosid integer
alter table turnos add constraint turnos_estadosturnosid_fkey foreign key (estadosturnosid) references estadosturnos (estadosturnosid)



create table Agendas(
	agendasid Serial primary key,
	especialidadesxprofesionalesid integer not null,
	diasid integer not null,
	agendastmini time,
	agendastmfin time,
	agendasttini time,
	agendasttfin time,
	agendasfecharegistro date,
	agendassfechamod date,
	foreign key (especialidadesxprofesionalesid) references especialidadesxprofesionales(especialidadesxprofesionalesid),
	foreign key (diasid) references dias(diasid)
	 );

CREATE TABLE public.dias
(
  diasid integer NOT NULL DEFAULT nextval('dias_diasid_seq'::regclass),
  diasnombre character varying,
  CONSTRAINT dias_pkey PRIMARY KEY (diasid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.dias
  OWNER TO postgres;

create table LicenciasxDias(
	LicenciasxDiasID Serial primary key,
	LicenciasId integer,
	DiasId integer,
	dia integer,
	mes integer,
	anio integer,
	foreign key (LicenciasId) references Licencias(LicenciasId),
	foreign key (DiasId) references Dias(DiasId)
);

alter table obrasocial add column estadosid integer

alter table obrasocial add constraint OS_estadosid_fkey foreign key (estadosid) references estados (estadosid)

update obrasocial set estadosid=1


create table vacaciones(
	vacacionesid Serial primary key,
	vacacionesfechaini date,
	vacacionesfechafin date,
	vacacionesfecharegistro date,
	vacionesfechamod date,
	profesionalesid integer,
	especialidadesid integer,
	estadosid integer,
	foreign key (profesionalesid) references Profesionales(profesionalesid),
	foreign key (especialidadesid) references Especialidades(especialidadesid),
	foreign key (estadosid) references Estados(estadosid)
)

create table vacacionesxdias(
	vacaionesxdiaid Serial primary key,
	vacacionesid integer,
	diasid integer,
	dia integer,
	mes integer,
	anio integer,
	foreign key (vacacionesid) references Vacaciones(vacacionesid),
	foreign key (diasid) references Dias(diasid)
)

  create table Feriados(
	feriadosid Serial primary key,
	feriadosdescripcion varchar(60),
	feriadosfecha date,
	feriadosfecharegistro date,
	feriadosfechamod date,
	feriadosfechabaja date,
	estadosid integer,
	diasid integer,
	dia integer,
	mes integer,
	anio integer,
	foreign key (diasid) references Dias(diasid),
	foreign key (estadosid) references Estados (estadosid)
	)