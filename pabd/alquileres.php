<html>
	<head>
		<link rel="stylesheet" type="text/css" href="datatables.min.css"/>
		<style>
			span.error {
				font-size: small;
				color: red;
				font-weight: normal;

			}
		</style>
		<script type="text/javascript" src="jQuery-2.1.4/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="jQuery-2.1.4/jquery.validate.min.js"></script>

		<script >
		$(document).ready(function comprueba() {
		    $("#ok").hide();

		    $("#formulario").validate({
		        rules: {	
		        	staff_id: { required: true, minlength: 1},
		            film_id	: { required: true, minlength: 1},
		            customer_id: { required:true, minlength:1}
		           
		        },
		        messages: {
		        	staff_id:" Debe introducir el empleado",
		            film_id : "Debe introducir el codigo .",
		            customer_id: "Introduce el numero de socio",
		          
		        },
		    submitHandler: function (form){
            var dataString ="film_id="+$('#film_id').val();
            $.ajax({
                type: "POST",
                url:"alqui_process.php",
                data: dataString,
                success: function(data){
                    $("#loco").html(data);
                   
                }
            });
        }

		    });
		});
		</script>

		<script >
          function validar (){
          	comprueba();
		</script>
		
			<script >
          function validar_2 (){
            if(confirm('Estas seguro de enviar este formulario?'))

                document.formulario.submit()
          }
          </script>

	</head>
	<body>
	<?php
	$dbconn=pg_connect("host=localhost port=5432 dbname=dvdrental user=postgres password=orellana") or die('No se ha podido conectar'.pg_last_error());
	$estado=pg_connection_status($dbconn);
	$empleado="select * from staff";
	$resp=pg_query($empleado);
	$cod="";
	while ( $fila = pg_fetch_assoc($resp)) {
	
		$cod=$cod."<option value='".$fila['staff_id']."'>".$fila['first_name']." ".$fila['last_name']."</option>\n";
				
			}
	?>
	<h1>Alquileres</h1>
	<div class="col-xs-6">
	<div id="ok" ></div>
	
	<form method="post" class="form-horizontal" role="form" name="formulario" id="formulario" action="alqui2_process.php" >
	
	<p><label class="control-label" for="staff_id">Personal:</label>	
	<select class="form-control" name="staff_id" id="staff_id">
	<?php echo $cod; ?>
	</select><span></span></p>

	<p><label class="control-label" for="film_id">Codigo de la Pelicula:</label>	
	 
	<input class="form-control" type="text" name="film_id" id="film_id" />

	<span></span></p>


	<div id ="loco">
	<input type="submit" value="Guardar"  onclick="validar()" style=" margin-left:565px;" />
	</div>
	</form>

	</div>

	</body>
</html>