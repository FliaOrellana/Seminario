<html>

<title>Customers</title>
<link rel="stylesheet" type="text/css" href="datatables.min.css"/>
<script type="text/javascript" src="datatables.min.js"></script>
<script type='text/javascript' language='javascript'>
$(document).ready(function() {
// es dataTable, sino no te andan las funciones x ej  si queres después hacerle un fnGetData te daria error.
	var table = $('#example').dataTable({
		'processing': true,
		'paging': true,
		'pagingType': 'full_numbers', //full
		'serverSide': true,         
		'ajax': 'dtcustomers_process.php'			
	}); 
});

</script>
<body>
	<h1>Customers</h1>
	<table id="example" class="table table-striped table-bordered" cellspacing="0" width="98%">
	 <thead>
		<tr>
			<th>customerid</th>
			<th>firstname</th>
			<th>lastname</th>
			<th>address1</th>
			<th>city</th>
			<th>country</th>
			<th>email</th>
			<th>age</th>
		</tr>
	 </thead>
	</table>    
</body>
</html>
