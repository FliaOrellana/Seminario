<?php

include('ssp.class.pg.php');
ini_set("memory_limit","128M");

/* DB tables to use with necesary joins */
$table = "customers";

// Table's primary key
$primaryKey = 'customerid';

// Array of database columns which should be read and sent back to DataTables. The `db` parameter represents the column name in the database
//, while the `dt` parameter represents the DataTables column identifier.
$columns = array(
	array('db' => 'customerid','dt' => 0),
	array('db' => 'firstname','dt' => 1),
	array('db' => 'lastname','dt' => 2),
	array('db' => 'address1','dt' => 3),
	array('db' => 'city','dt' => 4),
	array('db' => 'country','dt' => 5),
	array('db' => 'email','dt' => 6),
	array('db' => 'age','dt' => 7)
);

//Condiciones adicionales para el WHERE
$filtroAdd=" country <> 'US' ";

	//Parámetros de conexión
$pg_details = array(
 'user' => 'postgres',
 'pass' => 'orellana',
 'db'   => 'dellstore2',
 'host' => '127.0.0.1',
 'port' => '5432'
);

echo json_encode(
 SSP::simple( $_GET, $pg_details, $table, $primaryKey, $columns,$filtroAdd )
);

?>