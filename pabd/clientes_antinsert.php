<html>
	<head>
		<link rel="stylesheet" type="text/css" href="datatables.min.css"/>
		<style>
			span.error {
				font-size: small;
				color: red;
				font-weight: normal;

			}
		</style>
		<script type="text/javascript" src="jQuery-2.1.4/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="jQuery-2.1.4/jquery.validate.min.js"></script>
		<script >
		$(document).ready(function comprueba() {
		    $("#ok").hide();

		    $("#formulario").validate({
		        rules: {
		            first_name: { required: true, minlength: 2},
		            last_name: { required: true, minlength: 2},
		            email: { required:true, email: true},
		            phone: { required:true, minlength: 2, maxlength: 15},
		            district: { required: true, minlength: 2},
		            postal_code: { required: true, minlength: 2},
		            address: { required: true, minlength: 2}
		        },
		        messages: {
		            first_name: "Debe introducir su nombre.",
		            last_name: "Debe introducir su apellido.",
		            email : "Debe introducir un email valido.",
		            phone : "El numero de telefono introducido no es correcto.",
		            district : "Debe introducir una Provincia.",
		            postal_code : "Debe introducir un Codigo Postal.",
		            address : "Debe introducir una Direccion.",
		        },

		    });
		});
		</script>

		<script >
          function validar (){
          	comprueba();
            if(confirm('Estas seguro de enviar este formulario?'))

                document.formulario.submit()
          }
          </script>	

         <script >
  			$(document).ready(function(){
    			$("#country_id").change(function(){
   				$.ajax({
      				url:"procesa.php",
      				type: "POST",
      				data:"country_id="+$("#country_id").val(),
      				success: function(opciones){
       					$("#city").html(opciones);
     					}
   					})
				});
			});
		</script>	
	</head>
	<body>
	<?php
	$dbconn=pg_connect("host=localhost port=5432 dbname=dvdrental user=postgres password=orellana") or die('No se ha podido conectar'.pg_last_error());
	$estado=pg_connection_status($dbconn);
	$country="select country_id,country from country";
	$query="select address_id,address from store inner join address using(address_id)";
	$resultado=pg_query($query) or die('La consulta fallo: '.pg_last_error());
	$resp=pg_query($country) or die('La consulta fallo: '.pg_last_error());
	$cosa="";
	while ( $array = pg_fetch_assoc($resultado)) {
	
		$cosa=$cosa."<option value='".$array['address_id']."'>".$array['address']."</option>\n";
			
			}
	$cod="";
	while ( $fila = pg_fetch_assoc($resp)) {
	
		$cod=$cod."<option value='".$fila['country_id']."'>".$fila['country']."</option>\n";
				
			}
	

	?>
	<h1>Nuevo Cliente</h1>
	<div class="col-xs-6">
	<div id="ok" ></div>
	<form method="post" class="form-horizontal" role="form" name="formulario" id="formulario" action="clientes_insertar.php" >
	<p><label class="control-label" for="firstname">Nombre:</label>	
	<input class="form-control" type="text" name="first_name" id="first_name" />
	<span></span></p>

	<p><label class="control-label" for="lastname">Apellido:</label>	
	<input class="form-control" type="text" name="last_name" id="last_name" />
	<span></span></p>

	<p><label class="control-label" for="email">E-mail:</label>	
	<input class="form-control" type="text" name="email" id="email" />
	<span></span></p>


	<p><label class="control-label" for="store_id">Store:</label>	
	<select class="form-control" name="store_id" id="store_id">
	<?php echo $cosa; ?>
	</select><span></span></p>

	<div>
	<p><label class="control-label" for="country">Pais:</label>	
	<select class="form-control" name="country_id" id="country_id">
	<?php echo $cod; ?>
	</select><span></span></p></div>


	<p><label class="control-label" for="district">Provincia:</label>	
	<input class="form-control" type="text" name="district" id="district"/>
	<span></span></p>


	
	<div>
	<p><label class="control-label" for="city">Ciudad:</label>	
	<select class="form-control" name="city" id="city">
	<option value='0'>Ingrese primero un pais</option>
	</select><span></span></p></div>

	<p><label class="control-label" for="postal_code">Codigo Potal:</label>	
	<input class="form-control" type="text" name="postal_code" id="postal_code"/>
	<span></span></p>

	<p><label class="control-label" for="address">Direccion:</label>	
	<input class="form-control" type="text" name="address" id="address"/>
	<span></span></p>

	<p><label class="control-label" for="phone">Telefono:</label>	
	<input class="form-control" type="text" name="phone" id="phone"/>
	<span></span></p>

	<p><label class="control-label" for="active">Activo:</label>	
	<select class="form-control" name="active" id="active" >
	<option value='True'>Si</option>
	<option value='False'>No</option>
	</select><span></span></p>

	

		<input type="submit" value="Guardar"  onclick="validar()" style=" margin-left:565px;" />
	</form>	
	</div>
	</body>
</html>