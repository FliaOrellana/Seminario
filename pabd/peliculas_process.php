<?php

include('ssp.class.pg.php');
ini_set("memory_limit","128M");

/* DB tables to use with necesary joins */
$table = "film inner join language using(language_id)  ";

// Table's primary key
$primaryKey = 'film_id';

// Array of database columns which should be read and sent back to DataTables. The `db` parameter represents the column name in the database
//, while the `dt` parameter represents the DataTables column identifier.
$columns = array(
	array('db' => 'film_id','dt' => 0),
	array('db' => 'title','dt' => 1),
	array('db' => 'description','dt' => 2),
	array('db' => 'release_year','dt' => 3),
	array('db' => 'name','dt' => 4),
	array('db' => 'rental_duration','dt' =>5 ),
	array('db' => 'rental_rate','dt' => 6),
	array('db' => 'length','dt' => 7),
	array('db' => 'replacement_cost','dt' => 8),
	array('db' => 'rating','dt' => 9)
);

//Condiciones adicionales para el WHERE
//$filtroAdd=" country <> 'US' ";

	//Parámetros de conexión
$pg_details = array(
 'user' => 'postgres',
 'pass' => 'orellana',
 'db'   => 'dvdrental',
 'host' => '127.0.0.1',
 'port' => '5432'
);

echo json_encode(
 SSP::simple( $_GET, $pg_details, $table, $primaryKey, $columns,$filtroAdd )
);

?>