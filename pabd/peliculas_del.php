<html>
	<head>
		<link rel="stylesheet" type="text/css" href="datatables.min.css"/>
		<style>
			span.error {
				font-size: small;
				color: red;
				font-weight: normal;

			}
		</style>
		<script type="text/javascript" src="jQuery-2.1.4/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="jQuery-2.1.4/jquery.validate.min.js"></script>
		<script>
		$(document).ready(function(){

			$("#formulario").validate({
	        rules:{
				customer_id: {required: true, number: true},
				first_name: {required: true},
				last_name: {required: true},
				email: {required: true, email: true},
				create_date: {required: true, number: true}
				store_id{required: true, number:true},
				address{required:true},
				active{required:true,number:true}
	        },

			errorElement: "span",
			errorPlacement: function(error, element){
				error.appendTo(element.next());
			}

	    	})
		})
		</script>
		<script >
          function validar (){
            if(confirm('Estas seguro de enviar este formulario?'))

                document.formulario.submit()
          }
          </script>	


	</head>
	<body>
	<?php
	$dbconn=pg_connect("host=localhost port=5432 dbname=dvdrental user=postgres password=orellana") or die('No se ha podido conectar'.pg_last_error());
	$estado=pg_connection_status($dbconn);
	$id=$_REQUEST['id'];
	$query="select * from film where film_id=".$_REQUEST['id'];
	$resultado=pg_query($query) or die('La consulta fallo: '.pg_last_error());
	$fila=pg_fetch_assoc($resultado) ;
	$query2="select * from  language where language_id=".$fila['language_id'];
	$result=pg_query($query2) or die('La consulta fallo: '.pg_last_error());
	$fila2=pg_fetch_assoc($result);
	
	
	$resul=pg_query($query2)
	
	
	
	
	

	?>
	<h1>Pelicula a Eliminar</h1>
	<div class="col-xs-6">
	<form class="form-horizontal" role="form" name="formulario" id="formulario" action=<?php $var="peliculas_eliminar.php?id=".$id; echo $var; ?> method="post">
	<p><label class="control-label" for="title">Titulo:</label>	
	<input class="form-control" type="text" name="title" id="title" value=<?php echo "'".$fila['title']."'" ?>>
	<span></span></p>

	<p><label class="control-label" for="description">Descripcion:</label>	
	<input class="form-control" type="text" name="description" id="description" value=<?php echo "'".$fila['description']."'" ?>>
	<span></span></p>

	<p><label class="control-label" for="release_year">Fecha de Lanzamiento:</label>	
	<input class="form-control" type="text" name="release_year" id="release_year" value=<?php echo "'".$fila['release_year']."'" ?> >
	<span></span></p>


	<p><label class="control-label" for="language">Lenguaje:</label>	
	<input class="form-control" name="language" id="language" value=<?php echo "'".$fila2['name']."'" ?>>
	<span></span></p>

	
	<p><label class="control-label" for="rental_duration">Duracion del ALquiler:</label>	
	<input class="form-control" type="text" name="rental_duration" id="country_id" value=<?php echo "'".$fila['rental_duration']."'" ?>>
	<span></span></p>


	<p><label class="control-label" for="rental_rate">Precio:</label>	
	<input class="form-control" type="text" name="rental_rate" id="rental_rate" value=<?php echo "'".$fila['rental_rate']."'" ?>>
	<span></span></p>


	<p><label class="control-label" for="length">Duracion de la Pelicula:</label>	
	<input class="form-control" text="text" name="length" id="length" value=<?php echo "'".$fila['length']."'" ?>>
	<span></span></p>

	<p><label class="control-label" for="replacement_cost">Costo:</label>	
	<input class="form-control" type="text"  name="replacement_cost" id="replacement_cost" value=<?php echo "'".$fila['replacement_cost']."'" ?>>
	<span></span></p>

	<p><label class="control-label" for="rating">Clasificacion:</label>	
	<input class="form-control"  name="rating" id="rating" value=<?php echo "'".$fila['rating']."'" ?>>
	<span></span></p>

		<input type=button onclick="validar()" value="Eliminar">
	</form>	
	</div>

	</body>
</html>