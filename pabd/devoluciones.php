<html>
	<head>
		<link rel="stylesheet" type="text/css" href="datatables.min.css"/>
		<style>
			span.error {
				font-size: small;
				color: red;
				font-weight: normal;

			}
		</style>
		<script type="text/javascript" src="jQuery-2.1.4/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="jQuery-2.1.4/jquery.validate.min.js"></script>
		<script >
		$(document).ready(function comprueba() {
		    $("#ok").hide();

		    $("#formulario").validate({
		        rules: {	
		        	inventory_id: { required: true, minlength: 1},
		            customer_id: { required:true, minlength:1}
		           
		        },
		        messages: {
		        	inventory_id:" Debe introducir el empleado",
		            customer_id: "Introduce el numero de socio",
		          
		        },
		    submitHandler: function (form){
            var dataString ="inventory_id="+$('#inventory_id').val() +'&customer_id='+$('#customer_id').val();
            $.ajax({
                type: "POST",
                url:"devoluciones_process.php",
                data: dataString,
                success: function(data){
                    $("#ajax").html(data);
                   
                }
            });
        }

		    });
		});
		</script>


		<script >
          function validar (){
          	comprueba();

          </script>	

	
	</head>
	<body>

	<h1>Devoluciones</h1>
	<div class="col-xs-6">
	<div id="ok" ></div>
	<form method="post" class="form-horizontal" role="form" name="formulario" id="formulario"  >
	<p><label class="control-label" for="inventory_id">Codigo de la copia:</label>	
	<input class="form-control" type="text" name="inventory_id" id="inventory_id" />
	<span></span></p>

	<p><label class="control-label" for="customer_id">Codigo del Cliente:</label>	
	<input class="form-control" type="text" name="customer_id" id="customer_id" />
	<span></span></p>

	<input type="submit" value="Guardar"  onclick="validar()" style=" margin-left:565px;" />
	</form>	
	</div>
	<div id="ajax"></div>
	</body>
</html>