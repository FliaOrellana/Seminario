<html>

<title>Peliculas</title>
<link rel="stylesheet" type="text/css" href="datatables.min.css"/>
<style>

	div.contenedor{
		padding: 20px;
		font-size: small
	}
</style>
<script type="text/javascript" src="datatables.min.js"></script>
<script type='text/javascript' language='javascript'>
$(document).ready(function() {
// es dataTable, sino no te andan las funciones x ej  si queres después hacerle un fnGetData te daria error.
	var table = $('#example').dataTable({
		'processing': true,
		'paging': true,
		'pagingType': 'full_numbers', //full
		'serverSide': true,         
		'ajax': 'peliculas_process.php',
		columnDefs: [
						{ "width": "50px", "targets": [0, -2] },
						{ "width": "100px", "targets": 10 },
						{ orderable: false, "targets": -1 },
						{
						    render: function ( data, type, row ) {
						        return "<a class='btn btn-info btn-xs' href='peliculas_mod.php?id="+row[0]+"' role='button'>Modif</a>&nbsp;&nbsp;"+
						               "<a class='btn btn-danger btn-xs' href='peliculas_del.php?id="+row[0]+"' role='button'>Borrar</a>";    
						    },"targets": 10
						}						
					]			
	}); 
});

</script>
<body>
	<h1>Pelicuclas <a class='btn btn btn-success' href='peliculas_antinsert.php' role='button'>Nueva Pelicula</a></h1>
	<div class="contenedor">
	<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="font-size:small">
	 <thead>
		<tr>
			<th>Codigo de Pelicula</th>
			<th>Titulo</th>
			<th>Descripcion</th>
			<th>Fecha de lanzamiento</th>
			<th>Lenguaje</th>
			<th>Duracion de Alquiler</th>
			<th>Precio</th>
			<th>Duracion de la Pelicula</th>
			<th>Costo</th>
			<th>Clasificacion</th>
			<th>Accion</th>
		</tr>
	 </thead>
	</table>    
	</div>
</body>
</html>
