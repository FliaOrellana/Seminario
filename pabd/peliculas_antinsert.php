<html>
	<head>
		<link rel="stylesheet" type="text/css" href="datatables.min.css"/>
		<style>
			span.error {
				font-size: small;
				color: red;
				font-weight: normal;

			}
		</style>
		<script type="text/javascript" src="jQuery-2.1.4/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="jQuery-2.1.4/jquery.validate.min.js"></script>
		<script >
		$(document).ready(function comprueba() {
		    $("#ok").hide();

		    $("#formulario").validate({
		        rules: {
		            title: { required: true, minlength: 2},
		            description: { required: true, minlength: 2},
		            release_year: { required:true},
		            rental_duration: { required: true, minlength: 2},
		            rental_rate: { required: true, minlength: 2},
		            length: { required: true, minlength: 2},
		            replacement_cost: { required: true, minlength: 2}
		            
		        },
		        messages: {
		            title: "Debe introducir el Titulo.",
		            description: "Debe introducir la descripcion.",
		            release_year : "Debe introducir el año de estreno.",
		            rental_duration: "Debe introducir el tiempo de Alquiler.",
		            rental_rate : "Debe introducir el Precio del Alquiler.",
		            length : "Debe introducir el tiempo de duracion de la pelicula en minutos.",
		            replacement_cost : "Debe introducir el Costo por daño.",
		          }  

		    });
		});
		</script>

		<script >
          function validar (){
          	comprueba();
            if(confirm('Estas seguro de enviar este formulario?'))

                document.formulario.submit()
          }
          </script>	


	</head>
	<body>
	<?php
	$dbconn=pg_connect("host=localhost port=5432 dbname=dvdrental user=postgres password=orellana") or die('No se ha podido conectar'.pg_last_error());
	$estado=pg_connection_status($dbconn);
	$query="select * from  language";
	$query2="select * from film";
	$resultado=pg_query($query) or die('La consulta fallo: '.pg_last_error());
	$resul=pg_query($query2) or die('La consulta fallo: '.pg_last_error());
	$cosa="";
	$cod="";
	while ( $array = pg_fetch_assoc($resultado)) {
	
		$cosa=$cosa."<option value='".$array['language_id']."'>".$array['name']."</option>\n";
			
			}
	
	while ( $array2 = pg_fetch_assoc($resul)) {
	
		$cod=$cod."<option value='".$array2['rating']."'>".$array2['rating']."</option>\n";
			
			}
	
	

	?>
	<h1>Nueva Pelicula</h1>
	<div class="col-xs-6">
	<div id="ok" ></div>
	<form class="form-horizontal" role="form" name="formulario" id="formulario" action="peliculas_insertar.php" method="post">
	<p><label class="control-label" for="title">Titulo:</label>	
	<input class="form-control" type="text" name="title" id="title">
	<span></span></p>

	<p><label class="control-label" for="description">Descripcion:</label>	
	<input class="form-control" type="text" name="description" id="description">
	<span></span></p>

	<p><label class="control-label" for="release_year">Fecha de Lanzamiento:</label>	
	<input class="form-control" type="text" name="release_year" id="release_year">
	<span></span></p>


	<p><label class="control-label" for="language">Lenguaje:</label>	
	<select class="form-control" name="language" id="language">
	<?php echo $cosa; ?>
	</select><span></span></p>

	
	<p><label class="control-label" for="rental_duration">Duracion del ALquiler:</label>	
	<input class="form-control" type="text" name="rental_duration" id="country_id">
	<span></span></p>


	<p><label class="control-label" for="rental_rate">Precio:</label>	
	<input class="form-control" type="text" name="rental_rate" id="rental_rate">
	<span></span></p>


	<p><label class="control-label" for="length">Duracion de la Pelicula:</label>	
	<input class="form-control" text="text" name="length" id="length">
	<span></span></p>

	<p><label class="control-label" for="replacement_cost">Costo:</label>	
	<input class="form-control" type="text"  name="replacement_cost" id="replacement_cost">
	<span></span></p>

	<p><label class="control-label" for="rating">Clasificacion:</label>	
	<select class="form-control"  name="rating" id="rating">
	<?php echo $cod; ?>
	</select><span></span></p>

		<input type="submit" value="Guardar"  onclick="validar()" style=" margin-left:565px;" />
	</form>	
	</div>

	</body>
</html>