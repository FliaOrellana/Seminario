<html>
	<head>
		<link rel="stylesheet" type="text/css" href="datatables.min.css"/>
		<style>
			span.error {
				font-size: small;
				color: red;
				font-weight: normal;

			}
		</style>
		<script type="text/javascript" src="jQuery-2.1.4/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="jQuery-2.1.4/jquery.validate.min.js"></script>
		<script>
		$(document).ready(function(){

			$("#formulario").validate({
	        rules:{
				customer_id: {required: true, number: true},
				first_name: {required: true},
				last_name: {required: true},
				email: {required: true, email: true},
				create_date: {required: true, number: true}
				store_id{required: true, number:true},
				address{required:true},
				active{required:true,number:true}
	        },

			errorElement: "span",
			errorPlacement: function(error, element){
				error.appendTo(element.next());
			}

	    	})
		})
		</script>		
		<script >
          function validar (){
            if(confirm('¿Estas seguro de enviar este formulario?'))

                document.formulario.submit()
          }
          </script>	


	</head>
	<body>
	<?php
	$dbconn=pg_connect("host=localhost port=5432 dbname=dvdrental user=postgres password=orellana") or die('No se ha podido conectar'.pg_last_error());
	$estado=pg_connection_status($dbconn);
	$id=$_REQUEST['id'];
	$cliente="select * from customer where customer_id=".$_REQUEST['id'];
	$resp2=pg_query($cliente);
	$fila2=pg_fetch_assoc($resp2);
	$Direccion="select * from address where address_id=".$fila2['address_id'];
	$resp3=pg_query($Direccion);
	$fila3=pg_fetch_assoc($resp3);
	$store="select address from address where address_id=".$fila2['store_id'];
	$resp=pg_query($store);
	$fila=pg_fetch_assoc($resp);
	$city="select * from city where city_id=".$fila3['city_id'];
	$resp4=pg_query($city);
	$fila4=pg_fetch_assoc($resp4);
	$country2="select country_id,country from country where country_id=".$fila4['country_id'];
	$resp5=pg_query($country2);
	$fila5=pg_fetch_assoc($resp5);

	?>
	<h1>Cliente a Eliminar</h1>
	<div class="col-xs-6">
	<form class="form-horizontal" role="form" name="formulario" id="formulario" action=<?php $var="clientes_eliminar.php?id=".$id;echo $var;  ?> method="post">
	<p><label class="control-label" for="firstname">Nombre:</label>	
	<input class="form-control" type="text" name="first_name" id="first_name" value=<?php echo "'".$fila2['first_name']."'"?> disabled>
	<span></span></p>

	<p><label class="control-label" for="lastname">Apellido:</label>	
	<input class="form-control" type="text" name="last_name" id="last_name" value=<?php echo "'".$fila2['last_name']."'"?>>
	<span></span></p>

	<p><label class="control-label" for="email">E-mail:</label>	
	<input class="form-control" type="text" name="email" id="email" value=<?php echo "'".$fila2['email']."'"?>>
	<span></span></p>


	<p><label class="control-label" for="store_id">Store:</label>	
	<input class="form-control" name="store_id" id="store_id" value=<?php echo "'".$fila['address']."'"?>>
	
	<span></span></p>

	<div>
	<p><label class="control-label" for="country">Pais:</label>	
	<input class="form-control" name="country_id" id="country_id" value=<?php echo "'".$fila5['country']."'"?>>
	<span></span></p></div>


	<p><label class="control-label" for="district">Provincia:</label>	
	<input class="form-control" type="text" name="district" id="district" value=<?php echo "'".$fila3['district']."'"?>>
	<span></span></p>


	
	<div>
	<p><label class="control-label" for="city">Ciudad:</label>	
	<input class="form-control" name="city" id="city"  value=<?php echo "'".$fila4['city']."'"?>>
	<span></span></p></div>

	<p><label class="control-label" for="postal_code">Codigo Potal:</label>	
	<input class="form-control" type="text" name="postal_code" id="postal_code\" value=<?php echo "'".$fila3['postal_code']."'"?>>
	<span></span></p>

	<p><label class="control-label" for="address">Direccion:</label>	
	<input class="form-control" type="text" name="address" id="address" value=<?php echo "'".$fila3['address']."'"?>>
	<span></span></p>

	<p><label class="control-label" for="phone">Telefono:</label>	
	<input class="form-control" type="text" name="phone" id="phone" value=<?php echo "'".$fila3['phone']."'"?>>
	<span></span></p>

	<p><label class="control-label" for="active">Activo:</label>	
	<input class="form-control" name="active" id="active" value=<?php echo "'".$fila2['activebool']."'"?>>
	<span></span></p>

	

		<input type=button onclick="validar()" value="Eliminar">
	</form>	
	</div>

	<?php
	
	
	?>
	</body>
</html>