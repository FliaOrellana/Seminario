<?php

include('ssp.class.pg.php');
ini_set("memory_limit","128M");

/* DB tables to use with necesary joins */
$table = "customer inner join address using(address_id)  ";

// Table's primary key
$primaryKey = 'customer_id';

// Array of database columns which should be read and sent back to DataTables. The `db` parameter represents the column name in the database
//, while the `dt` parameter represents the DataTables column identifier.
$columns = array(
	array('db' => 'customer_id','dt' => 0),
	array('db' => 'first_name','dt' => 1),
	array('db' => 'last_name','dt' => 2),
	array('db' => 'email','dt' => 3),
	array('db' => 'create_date','dt' => 4),
	array('db' => 'store_id','dt' =>5 ),
	array('db' => 'address','dt' => 6),
	array('db' => 'activebool','dt' => 7)
);

//Condiciones adicionales para el WHERE
//$filtroAdd=" country <> 'US' ";

	//Parámetros de conexión
$pg_details = array(
 'user' => 'postgres',
 'pass' => 'orellana',
 'db'   => 'dvdrental',
 'host' => '127.0.0.1',
 'port' => '5432'
);

echo json_encode(
 SSP::simple( $_GET, $pg_details, $table, $primaryKey, $columns,$filtroAdd )
);

?>