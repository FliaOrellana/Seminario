<html>
	<head>
		<link rel="stylesheet" type="text/css" href="datatables.min.css"/>
		<style>
			span.error {
				font-size: small;
				color: red;
				font-weight: normal;

			}
		</style>
		<script type="text/javascript" src="jQuery-2.1.4/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="jQuery-2.1.4/jquery.validate.min.js"></script>
		<script >
		$(document).ready(function comprueba() {
		    $("#ok").hide();

		    $("#formulario").validate({
		        rules: {
		            first_name: { required: true, minlength: 2},
		            last_name: { required: true, minlength: 2},
		            email: { required:true, email: true},
		            phone: { required:true, minlength: 2, maxlength: 15},
		            district: { required: true, minlength: 2},
		            postal_code: { required: true, minlength: 2},
		            address: { required: true, minlength: 2}
		        },
		        messages: {
		            first_name: "Debe introducir su nombre.",
		            last_name: "Debe introducir su apellido.",
		            email : "Debe introducir un email valido.",
		            phone : "El numero de telefono introducido no es correcto.",
		            district : "Debe introducir una Provincia.",
		            postal_code : "Debe introducir un Codigo Postal.",
		            address : "Debe introducir una Direccion.",
		        },

		    });
		});
		</script>

		<script >
          function validar (){
          	comprueba();
            if(confirm('Estas seguro de enviar este formulario?'))

                document.formulario.submit()
          }
          </script>

         <script >
  			$(document).ready(function(){
    			$("#country_id").change(function(){
   				$.ajax({
      				url:"procesa.php",
      				type: "POST",
      				data:"country_id="+$("#country_id").val(),
      				success: function(opciones){
       					$("#city").html(opciones);
     					}
   					})
				});
			});
		</script>	
	</head>
	<body>
	<?php
	$dbconn=pg_connect("host=localhost port=5432 dbname=dvdrental user=postgres password=orellana") or die('No se ha podido conectar'.pg_last_error());
	$estado=pg_connection_status($dbconn);
	$id=$_REQUEST['id'];
	$cliente="select * from customer where customer_id=".$_REQUEST['id'];
	$country="select country_id,country from country";
	$query="select address_id,address from store inner join address using(address_id)";
	$resultado=pg_query($query) or die('La consulta fallo: '.pg_last_error());
	$resp=pg_query($country) or die('La consulta fallo: '.pg_last_error());
	$resp2=pg_query($cliente);
	$fila2=pg_fetch_assoc($resp2);
	$Direccion="select * from address where address_id=".$fila2['address_id'];
	$resp3=pg_query($Direccion);
	$fila3=pg_fetch_assoc($resp3);
	$city="select * from city where city_id=".$fila3['city_id'];
	$resp4=pg_query($city);
	$fila4=pg_fetch_assoc($resp4);
	$country2="select country_id,country from country where country_id=".$fila4['country_id'];
	$resp5=pg_query($country2);
	$fila5=pg_fetch_assoc($resp5);
	$cosa="";
	while ( $array = pg_fetch_assoc($resultado)) {
	
		$cosa=$cosa."<option value='".$array['address_id']."'>".$array['address']."</option>\n";
			
			}
	$cod="";
	while ( $fila = pg_fetch_assoc($resp)) {
	
		$cod=$cod."<option value='".$fila['country_id']."'>".$fila['country']."</option>\n";
				
			}
	

	?>
	<h1>Cliente a Modificar</h1>
	<div class="col-xs-6">
	<div id="ok" ></div>
	<form class="form-horizontal" role="form" name="formulario" id="formulario" action=<?php $var="clientes_modifica.php?id=".$id;echo $var;  ?> method="post">
	<p><label class="control-label" for="firstname">Nombre:</label>	
	<input class="form-control" type="text" name="first_name" id="first_name" value=<?php echo "'".$fila2['first_name']."'"?>>
	<span></span></p>

	<p><label class="control-label" for="lastname">Apellido:</label>	
	<input class="form-control" type="text" name="last_name" id="last_name" value=<?php echo "'".$fila2['last_name']."'"?>>
	<span></span></p>

	<p><label class="control-label" for="email">E-mail:</label>	
	<input class="form-control" type="text" name="email" id="email" value=<?php echo "'".$fila2['email']."'"?>>
	<span></span></p>


	<p><label class="control-label" for="store_id">Store:</label>	
	<select class="form-control" name="store_id" id="store_id" value=<?php echo "'".$fila3['address']."'"?>>
	<?php echo $cosa; ?>
	</select><span></span></p>

	<div>
	<p><label class="control-label" for="country">Pais:</label>	
	<select class="form-control" name="country_id" id="country_id" >
	"<option value='0'><?php echo $fila5['country']?>
	<?php echo $cod; ?>
	</select><span></span></p></div>


	<p><label class="control-label" for="district">Provincia:</label>	
	<input class="form-control" type="text" name="district" id="district" value=<?php echo "'".$fila3['district']."'"?>>
	<span></span></p>


	
	<div>
	<p><label class="control-label" for="city">Ciudad:</label>	
	<select class="form-control" name="city" id="city" >
	<option value='0'><?php echo $fila4['city']?></option>
	</select><span></span></p></div>

	<p><label class="control-label" for="postal_code">Codigo Potal:</label>	
	<input class="form-control" type="text" name="postal_code" id="postal_code\" value=<?php echo "'".$fila3['postal_code']."'"?>>
	<span></span></p>

	<p><label class="control-label" for="address">Direccion:</label>	
	<input class="form-control" type="text" name="address" id="address" value=<?php echo "'".$fila3['address']."'"?>>
	<span></span></p>

	<p><label class="control-label" for="phone">Telefono:</label>	
	<input class="form-control" type="text" name="phone" id="phone" value=<?php echo "'".$fila3['phone']."'"?>>
	<span></span></p>

	<p><label class="control-label" for="activebool">Activo:</label>	
	<select class="form-control" name="activebool" id="activebool" value=<?php if ($fila2['activebool']== True) {echo "'Si'";} else{echo "'No'";}?>>
	<option value='True'>Si</option>
	<option value='False'>No</option>
	</select><span></span></p>

	
	<input type="submit" value="Modificar"  onclick="validar()" style=" margin-left:565px;" />
	</form>	
	</div>

	
	
	</body>
</html>