# -*- coding: utf-8 -*-
from Sqlconexion import Conexion
from selenium import selenium
import unittest, time, re

class LoginIncorrecto():
    def __init__(self):
        self.casonombre=u"Logue con contraseña incorrecta"
        self.conection=Conexion('testing','postgres','orellana','192.168.1.33')
        consulta="Select usuariosnombre from public.usuarios limit 1"
        self.conection.cur.execute(consulta)
        rows=self.conection.cur.fetchone()
        self.usuario=rows[0]
        self.RespEsperada= u"El usuario/contraseña son incorrectos"
        self.selenium = selenium("localhost", 4444, "*chrome", "http://localhost/")
        
    def CorrePrueba(self):
        self.selenium.start()
        sel=self.selenium
        sel.open("/Seminario/Code/index.php/login")
        sel.type("id=form-username", "ADMIN")
        sel.type("id=form-password", "erhjk")
        sel.click("name=submit")
        sel.wait_for_page_to_load("30000")
        self.RespObtenida=sel.get_text("name=form_iniciar")
        if (self.RespEsperada==self.RespObtenida):
            self.Grabar(True)
        
        else:
            self.Grabar(False)

    def Cerrar(self):
        self.selenium.stop()        

    def Grabar(self,a):
        consulta="select casospruebasid from testing.casospruebas where casospruebasnombre='"+self.casonombre+"';"
        self.conection.cur.execute(consulta)
        rows=self.conection.cur.fetchone()
        if rows==None:
            if (a==True):
                insert="insert into testing.casospruebas (historiasusuariosid,casospruebasnombre,casospruebasfecharegistro,casospruebascantidadcorridas,casospruebasok) values (1,'"+self.casonombre+"',now(),1,1);"
                self.conection.cur.execute(insert)
                self.conection.conn.commit()
                ant_ins="insert into testing.corridascasospruebas (casospruebasid,corridascasospruebasfechacorrida,corridascasospruebasresultadoobtenido,corridascasospruebasresultadoesperado,corridascasospruebasestadoresultado) "
                select="select casospruebasid,now(),'"+self.RespObtenida+"','"+self.RespEsperada+"','OK' from testing.casospruebas where casospruebasnombre='"+self.casonombre+"';"
                insert=ant_ins+select
                self.conection.cur.execute(insert)
                self.conection.conn.commit()
            else:
                insert="insert into testing.casospruebas (historiasusuariosid,casospruebasnombre,casospruebasfecharegistro,casospruebascantidadcorridas,casospruebaserror) values (1,'"+self.casonombre+"',now(),1,1);"
                self.conection.cur.execute(insert)
                self.conection.conn.commit()
                ant_ins="insert into testing.corridascasospruebas (casospruebasid,corridascasospruebasfechacorrida,corridascasospruebasresultadoobtenido,corridascasospruebasresultadoesperado,corridascasospruebasestadoresultado) "
                select="select casospruebasid,now(),'"+self.RespObtenida+"','"+self.RespEsperada+"','ERROR' from testing.casospruebas where casospruebasnombre='"+self.casonombre+"';"
                insert=ant_ins+select
                self.conection.cur.execute(insert)
                self.conection.conn.commit()
        else:
            if(a==True):
                ant_upd="update testing.casospruebas set casospruebascantidadcorridas=casospruebascantidadcorridas+1,casospruebasok=casospruebasok+1"
                where="where casospruebasid="+str(rows[0])
                update=ant_upd+where
                self.conection.cur.execute(update)
                self.conection.conn.commit()
                ant_ins="insert into testing.corridascasospruebas (casospruebasid,corridascasospruebasfechacorrida,corridascasospruebasresultadoobtenido,corridascasospruebasresultadoesperado,corridascasospruebasestadoresultado) "
                select="select casospruebasid,now(),'"+self.RespObtenida+"','"+self.RespEsperada+"','OK' from testing.casospruebas where casospruebasnombre='"+self.casonombre+"';"
                insert=ant_ins+select
                self.conection.cur.execute(insert)
                self.conection.conn.commit()
            else:
                ant_upd="update testing.casospruebas set casospruebascantidadcorridas=casospruebascantidadcorridas+1,casospruebaserror=casospruebaserror+1"
                where="where casospruebasid="+str(rows[0])
                update=ant_upd+where
                self.conection.cur.execute(update)
                self.conection.conn.commit()
                ant_ins="insert into testing.corridascasospruebas (casospruebasid,corridascasospruebasfechacorrida,corridascasospruebasresultadoobtenido,corridascasospruebasresultadoesperado,corridascasospruebasestadoresultado) "
                select="select casospruebasid,now(),'"+self.RespObtenida+"','"+self.RespEsperada+"','ERROR' from testing.casospruebas where casospruebasnombre='"+self.casonombre+"';"
                insert=ant_ins+select
                self.conection.cur.execute(insert)
                self.conection.conn.commit()
                
        self.conection.conn.close()    
    
    


def main():
        logueo=LoginIncorrecto()
        logueo.CorrePrueba()
        logueo.Cerrar()
        

        
main()
