<!-- <div id="capa"></div> -->

	<!-- <h1>Cargo Persona</h1>
	<form action="<?php echo base_url(); ?>cpersona/guardar" method="POST">
		<table>
			<tr>
				<td><label>DNI</label></td>
				<td><input type="text" name="txtDNI" maxlength="8"></td>
			</tr>
			<tr>
				<td><label>Nombre</label></td>
				<td><input type="text" name="txtNombre"></td>
			</tr>
			<tr>
				<td><label>Ap Paterno</label></td>
				<td><input type="text" name="txtApPaterno"></td>
			</tr>
			<tr>
				<td><label>Ap Materno</label></td>
				<td><input type="text" name="txtApMaterno"></td>
			</tr>
			<tr>
				<td><label>Email</label></td>
				<td><input type="email" name="txtEmail"></td>
			</tr>
			<tr>
				<td><label>Fec.Nac.</label></td>
				<td><input type="date" name="datFecNac"></td>
			</tr>
			<tr>
				<td><label>Ciudad</label></td>
				<td><div class="form-group">
                  <select id="cboCiudad" class="form-control">
                    <option value="">::Elija</option>

                  </select>
                </div></td>
			</tr>
			<tr>
				<td colspan="2"><label>Usuario</label></td>
			</tr>
			<tr>
				<td><label>Usuario</label></td>
				<td><input type="text" name="txtUsuario"></td>
			</tr>
			<tr>
				<td><label>Clave</label></td>
				<td><input type="password" name="txtClave"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Guardar"></td>
			</tr>
		</table>
	</form>
	<a href="<?php echo base_url();?>clogin">Loguearse</a> -->

	<br><br><br>
	<div class="box box-primary">
		<div class="box-body">
		<div class="col-sm-10">
	    	<!-- <div class="box box-primary"> -->
	    	<table id="tblPersonas" class="table table-bordered table-striped">
			    <thead>
				    <tr>
				      <th style="width: 5%;background-color: #006699; color: white;">#</th>
				      <th style="width: 10%;background-color: #006699; color: white;">Nombre</th>
				      <th style="width: 10%;background-color: #006699; color: white;">Paterno</th>
				      <th style="width: 10%;background-color: #006699; color: white;">Materno</th>
				      <th style="width: 10%;background-color: #006699; color: white;">DNI</th>
				      <th style="width: 10%;background-color: #006699; color: white;">Ciudad</th>
				      <th style="width: 10%;background-color: #006699; color: white;">Estado</th>
				      <th style="width: 10%;background-color: #006699; color: white;">Acción</th>
				    </tr>
			    </thead>
			    <tbody></tbody>
			  </table>

			<!-- </div> -->
		</div>
		<div class="col-sm-2"><span class='label label-warning' id="spSuma"></span></div>
		</div>
	</div>
	<button type="button" id="btnGetPersonas" class="btn btn-flat"><i class="fa fa-search"></i> &nbsp;Buscar</button>


<!-- Modal Cierre -->
<div class="modal fade" id="modalEditPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Persona</h4>
      </div>

      <div class="modal-body">
	      <form class="form-horizontal">
	      	<!-- parametros ocultos -->
	      	<input type="hidden" id="mhdnIdPersona">
	      	
			<div class="box-body">
		        <div class="form-group">
		            <label class="col-sm-3 control-label">Nombre</label>
		            <div class="col-sm-9"> 
		              <input type="text" name="mtxtNombre" class="form-control" id="mtxtNombre" placeholder="">
		            </div>
		        </div>

		        <div class="form-group">
		            <label class="col-sm-3 control-label">Ap.Paterno</label>
		            <div class="col-sm-9"> 
		              <input type="text" name="mtxtApPaterno" class="form-control" id="mtxtApPaterno" value="" >
		            </div>
		        </div>

		        <div class="form-group">
		            <label class="col-sm-3 control-label">Ap.Materno</label>
		            <div class="col-sm-9"> 
		              <input type="text" name="mtxtApMaterno" class="form-control" id="mtxtApMaterno">
		            </div>
		        </div>

		        <div class="form-group">
		            <label class="col-sm-3 control-label">Email</label>
		            <div class="col-sm-9"> 
		              <input type="text" name="mtxtEmail" class="form-control" id="mtxtEmail">
		            </div>
		        </div>

		        <div class="form-group">
		            <label class="col-sm-3 control-label">Otro</label>
		            <div class="col-sm-9">
		            	<select class="form-control" id="mcboOtro" name="mcboOtro">
		            		<option value="">:: Elija</option>
		            		<option value="3">1</option>
		            		<option value="5">2</option>
		            		<option value="7">3</option>
		            	</select>
		            </div>
		        </div>
			</div>
		  </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="mbtnCerrarModal" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-info" id="mbtnUpdPerona">Actualizar</button>
      </div>
    </div>
  </div>
</div>
    
<script type="text/javascript">
	var baseurl = "<?php echo base_url(); ?>";
</script>