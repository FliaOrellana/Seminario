<div class="col-md-6">
  <!-- USERS LIST -->
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Ciudades</h3>

      <div class="box-tools pull-right">
        <span class="label label-danger">8 New Members</span>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
        </button>
      </div>
    </div>




    <!-- /.box-header -->
    <div class="box-body no-padding">
      <ul class="users-list clearfix" id="listCiudades">
        
      </ul>
      <!-- /.users-list -->
    </div>




    <!-- /.box-body -->
    <div class="box-footer text-center">
      <a href="javascript:void(0)" class="uppercase">View All Users</a>
    </div>
    <!-- /.box-footer -->
    <button type="button" class="btn btn-box-tool" id="btnGrabar"><i class="fa fa-times"></i> &nbsp;Grabar</button>
  </div>
  <!--/.box -->

  
</div>

<div class="col-md-6">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Lista de ciudades</h3>

          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="txtBuscarCiudad" id="txtBuscarCiudad" class="form-control pull-right" placeholder="Buscar">

              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover" id="tblListCiudades">
            <thead>
              <tr>
                <th>#</th>
                <th>Ciudad</th>
                <th>Habitantes</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody></tbody>
            
            <!-- <tr>
              <td>175</td>
              <td>Mike Doe</td>
              <td>11-7-2014</td>
              <td><span class="label label-danger">Denied</span></td>
              <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
            </tr> -->
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
<script type="text/javascript">
  var baseurl = "<?php echo base_url();?>";
</script>